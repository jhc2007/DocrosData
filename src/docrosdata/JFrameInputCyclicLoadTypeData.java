
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JFrameInputCyclicLoadTypeData extends JFrame {
    
    JFrameDocrosData jFrameDocrosData;
    Point location;
    JLabel jLabelMain[];
    JTextField jTextFieldMain[];
    JComboBox jComboBoxMain[];
    JLabel jLabelProp[];
    JLabel jLabelMessageMain[];
    JLabel jLabelMessageProp[];
    JLabel jLabelStepProp;
    JTextField jTextFieldProp[];
    JButton jButtonMain[];
    JButton jButtonMainPrev, jButtonMainNext, jButtonMainClose, jButtonUsedName;
    JButton jButtonAddSteps;
    JButton jButtonProp[];
    JButton jButtonHelp;
    JPanel jPanelProp;
    int step;
    int cycles[];
    double amplitude[];
    String jLabelMainText[] = {"Cyclic Load Name", "Pattern Name", "Layer Number", "Strain Increment", "Steps"};
    String jButtonMainText[] = {"Delete", "Reset", "OK"};
    String jLabelPropText[] = {"Cycles", "Amplitude"};
    String jButtonPropText[] = {"Previous", "Next", "Cancel"};
    String jLabelMessageMainText[] = {
        "This name is already used", //0
        "Insert number of steps", //1
        "Select layer pattern", //2
        "Add cycles and amplitude", //3 
        "Insert cyclic load name", //4
        "Insert strain increment", //5
        "Select layer number" //6
    };
    String jLabelMessagePropText[] = {"Insert number of cycles", "Insert amplitude"};
    
    public JFrameInputCyclicLoadTypeData(JFrameDocrosData jf, Point loc) {
        
        super("<CYCLIC_LOAD_TYPE_DATA>");
        jFrameDocrosData = jf;
        location = loc;
        init();
    }
    
    private void init() {
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        //this.setAlwaysOnTop(true);             
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, jLabelWidth, xPosition = 10, yPosition = 10;
        
        jLabelWidth = jFrameDocrosData.charWidth * JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelMainText);

        JLabel jLabelTitle = new JLabel("Cyclic Load Data");
        jLabelTitle.setSize(jFrameDocrosData.charWidth * (jLabelTitle.getText().length() + 6), jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(xPosition + jLabelWidth, yPosition);
        this.add(jLabelTitle);
        
        if(getClass().getResourceAsStream("/help/helpCyclicLoadTypeData.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("CyclicLoadTypeData"));//, "Cyclic Load Type Data"));
            this.add(jButtonHelp);
        }
        
        yPosition += jFrameDocrosData.charHeight;
        
        jLabelMain = new JLabel[jLabelMainText.length];
        jTextFieldMain = new JTextField[jLabelMainText.length];
        for(i=0; i<jLabelMainText.length; i++) {
           jLabelMain[i] = new JLabel(jLabelMainText[i]);
           jLabelMain[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
           jLabelMain[i].setLocation(xPosition, yPosition);  
           this.add(jLabelMain[i]);
           jTextFieldMain[i] = new JTextField("");
           if(i == 0) {
               jTextFieldMain[i].setSize(jLabelTitle.getWidth() - 64, jFrameDocrosData.charHeight);
               jTextFieldMain[i].setName("NameCyclicLoad");
           }
           else {
               if(i == 4) {
                   jTextFieldMain[i].setSize(8 * jFrameDocrosData.charWidth, jFrameDocrosData.charHeight);
                   jTextFieldMain[i].setName("Int");
               }
               else    
                   jTextFieldMain[i].setSize(jLabelTitle.getSize());
           }
           jTextFieldMain[i].setLocation(xPosition + jLabelWidth, yPosition);
           jTextFieldMain[i].addKeyListener(new JTextFieldKeyListener());
           this.add(jTextFieldMain[i]);
           yPosition += jFrameDocrosData.charHeight;
        }
        
        jButtonUsedName = new JButton("Used");
        jButtonUsedName.setSize(63, 26);
        jButtonUsedName.setLocation(xPosition + jLabelWidth + jLabelTitle.getWidth() - 64, jTextFieldMain[0].getY());
        jButtonUsedName.addActionListener(new JButtonInputCyclicLoadTypeDataActionListener(this));
        this.add(jButtonUsedName);
        
        jComboBoxMain = new JComboBox[3];
        for(i=0; i<3; i++) {
            jComboBoxMain[i] = new JComboBox();
            if(i == 2)
                jComboBoxMain[i].setSize(8 * jFrameDocrosData.charWidth, jFrameDocrosData.charHeight);
            else
                jComboBoxMain[i].setSize(jLabelTitle.getWidth() - 1, jFrameDocrosData.charHeight);
            jComboBoxMain[i].setLocation(jTextFieldMain[i].getLocation());
            jComboBoxMain[i].addItem("Select");
            this.add(jComboBoxMain[i]);
        }
        jComboBoxMain[1].setName("LayerPatternsName");
      
        if(jFrameDocrosData.layerPatternsList != null && jFrameDocrosData.layerPatternsList.first != null) {
            LayerPatterns node = jFrameDocrosData.layerPatternsList.first;
            while(node != null){
                jComboBoxMain[1].addItem(node.name);
                node = node.next;
            }
            jComboBoxMain[1].setSelectedIndex(0);
        }
        //*****************************
        /*
        else {
            for(i=1; i<4; i++)
                jComboBoxMain[1].addItem("LPAT_" + i);
        }
        */
        //*****************************
        jTextFieldMain[1].setVisible(false);          
        jTextFieldMain[2].setVisible(false);
        
        jButtonAddSteps = new JButton("Add");
        jButtonAddSteps.setSize(jLabelTitle.getWidth() - jTextFieldMain[4].getWidth() - 1, jFrameDocrosData.charHeight - 1);
        jButtonAddSteps.setLocation(jTextFieldMain[4].getX() + jTextFieldMain[4].getWidth(), jTextFieldMain[4].getY());
        jButtonAddSteps.setName("AddSteps");
        jButtonAddSteps.addActionListener(new JButtonInputCyclicLoadTypeDataActionListener(this));
        this.add(jButtonAddSteps);
        
        jLabelMessageMain = new JLabel[jLabelMessageMainText.length];
        for(i=0; i<jLabelMessageMainText.length; i++) {
            jLabelMessageMain[i] = new JLabel(jLabelMessageMainText[i]);
            jLabelMessageMain[i].setSize(jLabelTitle.getSize());
            jLabelMessageMain[i].setLocation(xPosition + jLabelWidth, yPosition);
            jLabelMessageMain[i].setForeground(Color.red);
            jLabelMessageMain[i].setVisible(false);
            this.add(jLabelMessageMain[i]);
        }
        
        //yPosition += jFrameDocrosData.charHeight;
        
        jPanelProp = new JPanel();
        jPanelProp.setLayout(null);
        jPanelProp.setSize(jLabelWidth + jLabelTitle.getWidth() + 20, 4 * jFrameDocrosData.charHeight + 10);
        jPanelProp.setLocation(0, yPosition + 10);
        jPanelProp.setBackground(Color.LIGHT_GRAY);
        this.add(jPanelProp);
        
        jLabelProp = new JLabel[jLabelPropText.length];
        jTextFieldProp = new JTextField[jLabelPropText.length];
        for(i=0; i<2; i++) {
            jLabelProp[i] = new JLabel(jLabelPropText[i]);
            jLabelProp[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
            jLabelProp[i].setLocation(xPosition, xPosition + i * jFrameDocrosData.charHeight);
            jPanelProp.add(jLabelProp[i]);
            jTextFieldProp[i] = new JTextField("");
            jTextFieldProp[i].setSize(jLabelTitle.getSize());
            jTextFieldProp[i].setLocation(xPosition + jLabelWidth, xPosition + i * jFrameDocrosData.charHeight);
            if(i == 0)
                jTextFieldProp[i].setName("Int");
            jTextFieldProp[i].addKeyListener(new JTextFieldKeyListener());
            jPanelProp.add(jTextFieldProp[i]);
        }
        jTextFieldProp[0].setSize(jTextFieldMain[4].getSize());
        
        jLabelMessageProp = new JLabel[jLabelMessagePropText.length];
        for(i=0; i<jLabelMessagePropText.length; i++) {
            jLabelMessageProp[i] = new JLabel(jLabelMessagePropText[i]);
            jLabelMessageProp[i].setSize(jLabelTitle.getSize());
            jLabelMessageProp[i].setLocation(xPosition + jLabelWidth, jTextFieldProp[1].getY() + jFrameDocrosData.charHeight);
            jLabelMessageProp[i].setForeground(Color.red);
            jLabelMessageProp[i].setVisible(false);
            jPanelProp.add(jLabelMessageProp[i]);
        }
        
        jLabelStepProp = new JLabel("");
        jLabelStepProp.setSize(jTextFieldProp[1].getWidth() - jTextFieldProp[0].getWidth(), jFrameDocrosData.charHeight);
        jLabelStepProp.setLocation(jTextFieldProp[0].getX() + 130, jTextFieldProp[0].getY());
        jPanelProp.add(jLabelStepProp);
        
        jButtonProp = new JButton[jButtonPropText.length];
        for(i=0; i<jButtonPropText.length; i++) {
            jButtonProp[i] = new JButton(jButtonPropText[i]);
            jButtonProp[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
            jButtonProp[i].setLocation(xPosition + i * jButtonProp[i].getWidth(), jTextFieldProp[1].getY() + 2 * jFrameDocrosData.charHeight);
            jButtonProp[i].addActionListener(new JButtonInputCyclicLoadTypeDataActionListener(this));
            jPanelProp.add(jButtonProp[i]);
        }
        jButtonProp[0].setName("PrevProp");
        jButtonProp[1].setName("NextProp");

        jTextFieldProp[0].addFocusListener(new JTextFieldIntNumberFocusListener(jLabelMessageProp, 0, null, jButtonProp[1]));
        jTextFieldProp[1].addFocusListener(new JTextFieldDoubleFocusListener(jLabelMessageProp, 1, null, jButtonProp[1]));
        
        this.add(jPanelProp);
        jPanelProp.setVisible(false);
        
        yPosition += jFrameDocrosData.charHeight;
        //yPosition += 4 * jFrameDocrosData.charHeight + 14;
        
        jButtonMain = new JButton[jButtonMainText.length];
        for(i=0; i<jButtonMainText.length; i++) {
            jButtonMain[i] = new JButton(jButtonMainText[i]);
            jButtonMain[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, jFrameDocrosData.charHeight);
            jButtonMain[i].setLocation(xPosition + i * jButtonMain[i].getWidth(), yPosition);
            jButtonMain[i].addActionListener(new JButtonInputCyclicLoadTypeDataActionListener(this));
            this.add(jButtonMain[i]);
        }
        jButtonMain[0].setVisible(false);
        jButtonMain[1].setVisible(false);
              
        if(jFrameDocrosData.cyclicLoadTypeDataList != null && jFrameDocrosData.cyclicLoadTypeDataList.first != null) {
            CyclicLoadTypeData node = jFrameDocrosData.cyclicLoadTypeDataList.first;
            while(node != null) {
                jComboBoxMain[0].addItem(node.name);
                node = node.next;
            }
            jComboBoxMain[0].setSelectedIndex(0);
            jComboBoxMain[1].setEnabled(false);
            jComboBoxMain[2].setEnabled(false);
            for(i=0; i<jTextFieldMain.length; i++)
                jTextFieldMain[i].setEnabled(false);
            jTextFieldMain[0].setVisible(false);
            jButtonUsedName.setVisible(false);
            jButtonAddSteps.setEnabled(false);
            jButtonMain[2].setText("New");
        }
        else {
            jComboBoxMain[0].setVisible(false);
            jButtonUsedName.setEnabled(false);
        }
       

        yPosition += jFrameDocrosData.charHeight + 10;
        
        jButtonMainPrev = new JButton("Previous");
        jButtonMainPrev.setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
        jButtonMainPrev.setLocation(xPosition, yPosition);
        jButtonMainPrev.setName("PrevMain");
        jButtonMainPrev.addActionListener(new JButtonInputCyclicLoadTypeDataActionListener(this));
        this.add(jButtonMainPrev);
        
        jButtonMainClose = new JButton("Close");
        jButtonMainClose.setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
        jButtonMainClose.setLocation(xPosition + jButtonMainPrev.getWidth(), yPosition);
        //jButtonMainNext.setName("NextMain");
        jButtonMainClose.addActionListener(new JButtonInputCyclicLoadTypeDataActionListener(this));
        this.add(jButtonMainClose);
        
        jButtonMainNext = new JButton("Next");
        jButtonMainNext.setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
        jButtonMainNext.setLocation(xPosition + 2 * jButtonMainPrev.getWidth(), yPosition);
        jButtonMainNext.setName("NextMain");
        jButtonMainNext.addActionListener(new JButtonInputCyclicLoadTypeDataActionListener(this));
        this.add(jButtonMainNext);
        
        if((jFrameDocrosData.monotonicLoadTypeDataList == null || jFrameDocrosData.monotonicLoadTypeDataList.first == null) && (jFrameDocrosData.cyclicLoadTypeDataList == null || jFrameDocrosData.cyclicLoadTypeDataList.first == null))
            jButtonMainNext.setEnabled(false);
        
        yPosition += 18 + jFrameDocrosData.charHeight;
        
        jTextFieldMain[0].addFocusListener(new JTextFieldNameFocusListener(jLabelMessageMain, 4, jLabelMessageMain[0], jButtonMain[2]));
        jTextFieldMain[3].addFocusListener(new JTextFieldDoubleFocusListener(jLabelMessageMain, 5, null, jButtonMain[2]));
        jTextFieldMain[4].addFocusListener(new JTextFieldIntNumberFocusListener(jLabelMessageMain, 1, jButtonMain[2], jButtonAddSteps));
        
        jComboBoxMain[0].addActionListener(new JComboBoxInputCyclicLoadTypeData(this));
        //jComboBoxMain[1].addFocusListener(new JComboBoxFocusListener(jLabelMessageMain, jButtonMain[1]));
        jComboBoxMain[2].addFocusListener(new JComboBoxFocusListener(jLabelMessageMain, 6, jButtonMain[2]));
        jComboBoxMain[1].addActionListener(new JComboBoxInputCyclicLoadTypeData(this));
        
        if(jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList("_NLMM103").count > 0 ||
           jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList("_NLMM104").count > 0 ||
           jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList("_NLMM202").count > 0) {
            for(i=0; i<jLabelMain.length; i++)
                jLabelMain[i].setForeground(Color.LIGHT_GRAY);
            for(i=0; i<jTextFieldMain.length; i++)
                jTextFieldMain[i].setEnabled(false);
            for(i=0; i<jComboBoxMain.length; i++)
                jComboBoxMain[i].setEnabled(false);
            jButtonAddSteps.setEnabled(false);
            jButtonMain[2].setVisible(false);
            JLabel jLabelMonotonicLoad[] = new JLabel[2];
            jLabelMonotonicLoad[0] = new JLabel("Cyclic loading requires a cyclic model for the intervening");
            jLabelMonotonicLoad[0].setSize(jLabelWidth + jLabelTitle.getWidth(), 30);
            jLabelMonotonicLoad[0].setLocation(xPosition, jLabelMain[4].getY() + jFrameDocrosData.charHeight + 5);
            jLabelMonotonicLoad[0].setForeground(Color.red);
            this.add(jLabelMonotonicLoad[0]);
            jLabelMonotonicLoad[1] = new JLabel("materials. Verify the selected material models.");
            jLabelMonotonicLoad[1].setSize(jLabelWidth + jLabelTitle.getWidth(), 30);
            jLabelMonotonicLoad[1].setLocation(xPosition, jLabelMain[4].getY() + 2 * jFrameDocrosData.charHeight);
            jLabelMonotonicLoad[1].setForeground(Color.red);
            this.add(jLabelMonotonicLoad[1]);
        }
        
        this.setSize(jLabelWidth + jLabelTitle.getWidth() + 26, yPosition + 12);
        this.setVisible(true);
    }
    
    public void hideJPanelProp() {
        
        int i;
        int yPosition = jTextFieldMain[4].getY() + 2 * jFrameDocrosData.charHeight;
        jPanelProp.setVisible(false);
        for(i=0; i<jButtonMainText.length; i++)
            jButtonMain[i].setLocation(jButtonMain[i].getX(), yPosition);
        yPosition += jFrameDocrosData.charHeight + 10;
        jButtonMainPrev.setLocation(jButtonMainPrev.getX(), yPosition);
        jButtonMainNext.setLocation(jButtonMainNext.getX(), yPosition);
        jButtonMainClose.setLocation(jButtonMainClose.getX(), yPosition);
        yPosition += 18 + jFrameDocrosData.charHeight;
        this.setSize(this.getWidth(), yPosition + 12);
        for(i=0; i<jTextFieldMain.length; i++)
            jTextFieldMain[i].setEnabled(true);
        for(i=0; i<jComboBoxMain.length; i++)
            jComboBoxMain[i].setEnabled(true);
        jButtonAddSteps.setEnabled(true);
        jButtonMain[2].setEnabled(true);
    }
    
    public void showJPanelProp() {
        
        int i;
        int yPosition = jTextFieldMain[4].getY() + 6 * jFrameDocrosData.charHeight + 4;
        step = 1;
        jPanelProp.setVisible(true);
        for(i=0; i<jLabelMessageProp.length; i++)
            jLabelMessageProp[i].setVisible(false);
        for(i=0; i<jLabelMessageMain.length; i++)
            jLabelMessageMain[i].setVisible(false);
        for(i=0; i<2; i++) {
            jTextFieldProp[i].setText("");
            jTextFieldProp[i].setBackground(Color.white);
            jButtonProp[i].setText(jButtonPropText[i]);
            jButtonProp[i].setVisible(true);
            jButtonProp[i].setEnabled(true);
            //jButtonMain[i].setEnabled(false);
        }
        for(i=0; i<jButtonMainText.length; i++)
            jButtonMain[i].setLocation(jButtonMain[i].getX(), yPosition);
        yPosition += jFrameDocrosData.charHeight + 10;
        jButtonMainPrev.setLocation(jButtonMainPrev.getX(), yPosition);
        jButtonMainNext.setLocation(jButtonMainNext.getX(), yPosition);
        jButtonMainClose.setLocation(jButtonMainClose.getX(), yPosition);
        yPosition += 18 + jFrameDocrosData.charHeight;
        this.setSize(this.getWidth(), yPosition + 12);
        for(i=0; i<jTextFieldMain.length; i++) {
            jTextFieldMain[i].setBackground(Color.white);
            jTextFieldMain[i].setEnabled(false);
        }
        for(i=0; i<jComboBoxMain.length; i++)
            jComboBoxMain[i].setEnabled(false);
        jButtonAddSteps.setEnabled(false);
        jButtonMain[2].setEnabled(false);
        
    }
}
