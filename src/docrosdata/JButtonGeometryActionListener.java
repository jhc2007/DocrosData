
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class JButtonGeometryActionListener implements ActionListener {

    JFrameDocrosData jFrameDocrosData;
    JPanelLayerPatterns jPanelLayerPatterns;
    LayerProperties layerProperties;
    LaminateGeoProp laminateGeoProp;
    JFrame jFrameGeometry;
    JTextField jTextField[];
    JButton jButton[];
    JTextField jTextFieldLayerPropertiesName;
    JLabel jLabel1, jLabel2, jLabel3, jLabelLayerPropertiesName;
    Boolean viewChanged;
    Boolean newLayerPropertiesName;
    String properties[] = {"name", "thickness", "width"};

    public JButtonGeometryActionListener(JFrameDocrosData jfdd, JPanelLayerPatterns jplp, JFrame jf, JTextField[] jtf, JButton[] jb) {

        jFrameDocrosData = jfdd;
        jPanelLayerPatterns = jplp;
        layerProperties = jplp.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties];
        laminateGeoProp = jplp.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].geoProp;
        jFrameGeometry = jf;
        jTextField = jtf;
        jButton = jb;
        
        viewChanged = false;
        newLayerPropertiesName = false;
        jTextFieldLayerPropertiesName = new JTextField("");
        jTextFieldLayerPropertiesName.setVisible(false);
        jLabelLayerPropertiesName = new JLabel("Name");
        jLabel1 = new JLabel("Insert a New Layer Properties Name");
        jLabel2 = new JLabel("This name is already used");
        jLabel3 = new JLabel("Insert a New Geometry Properties Name");
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        switch (ae.getActionCommand()) {
                case "View":
                    if(!hasError()) {
                        viewJPanelLayerPatterns();
                    }
                    break;
                case "OK":
                    if(!textFieldValuesChanged()) {
               
                        jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                        jFrameGeometry.dispose();//setVisible(false);
                    }
                    else {
                        if(!hasError() && newLayerPropertiesNameInserted() && newLaminateGeoPropNameInserted()) {

                            //jFrameDocrosData.changedLists[laminateGeoPropListIndex] = true;
                            changeLists();
                            jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                            jFrameGeometry.dispose();//setVisible(false);
                        }
                    }
                    break;
                case "Close":
                    jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                    jFrameGeometry.dispose();//setVisible(false);
                    break;
            }
    }

    Boolean hasError() {
        
        int i;
        String text;
        Boolean error = false;
        for(i=1; i<3; i++) {
            try {
                Double.valueOf(jTextField[i].getText());
            }
            catch(Exception e) {
                error = true;
                text = jTextField[i].getText();
                jTextField[i].setText(text + " NOT A NUMBER");
                jTextField[i].setBackground(Color.yellow);
             }
        }
        if(error)
            for(i=0; i<2; i++)
                jButton[i].setEnabled(false);
        return error;
    }

    Boolean textFieldValuesChanged() {
        int i;
        if(!jTextField[0].getText().equals(laminateGeoProp.getPropertyValue(properties[0])))
            return true;
        for(i=1; i<properties.length; i++)
            if(!Double.valueOf(jTextField[i].getText()).equals(Double.valueOf(laminateGeoProp.getPropertyValue(properties[i]))))
                return true;
        return false;
    }

    void changeLists() {

        int i;
        //Boolean changed = false;
        //Caso A
        if(laminateGeoProp.countLayerPatterns == 1) {

            if(!jTextField[0].getText().equals(laminateGeoProp.getPropertyValue(properties[0])))
                    laminateGeoProp.setPropertyValue(properties[0], jTextField[0].getText());
            for(i=1; i<properties.length; i++)
                if(!Double.valueOf(jTextField[i].getText()).equals(Double.valueOf(laminateGeoProp.getPropertyValue(properties[i]))))
                    laminateGeoProp.setPropertyValue(properties[i], jTextField[i].getText());
        }
        else {
            if(layerProperties.countLayerPatterns > 1) {
                //Caso B
                LayerProperties copy = new LayerProperties(jTextFieldLayerPropertiesName.getText());//, layerProperties);
                jFrameDocrosData.layerPropertiesList.addLayerProperties(copy);
                copy.countLayerPatterns++;
                layerProperties.countLayerPatterns--;
                copy.materialModel = layerProperties.materialModel;
                jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties] = copy;
                //jFrameDocrosData.changedLists[jFrameDocrosData.layerPatternsListIndex] = true;
            } //Caso B + C
            LaminateGeoProp node = jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(jTextField);
            if(node == null) {
                node = new LaminateGeoProp(jTextField);
                jFrameDocrosData.laminateGeoPropList.addLaminateGeoProp(node);
            }
            jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].geoProp = node;
            node.countLayerPatterns++;
            laminateGeoProp.countLayerPatterns--;
            
            //jFrameDocrosData.changedLists[jFrameDocrosData.layerPropertiesListIndex] = true;

        }
        //jFrameDocrosData.changedLists[jFrameDocrosData.laminateGeoPropListIndex] = true;
        jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameDocrosData.dataFile != null)
            jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }

    Boolean newLaminateGeoPropNameInserted() {

        if(laminateGeoProp.countLayerPatterns <= 1 || jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(jTextField) != null)
            return true;

        int i;

        if(jLabel3.isVisible()) {

            if(jTextField[0].getText().equals("") || jTextField[0].getText().equals("Insert a name"))
                return false;

            if(jFrameDocrosData.laminateGeoPropList.existsLaminateGeoPropName(jTextField[0].getText())) {
                jLabel2.setLocation(jTextField[jTextField.length - 1].getX(), jTextField[jTextField.length - 1].getY() + jFrameDocrosData.charHeight);
                jLabel2.setVisible(true);
                jTextField[0].setBackground(Color.yellow);
                return false;
            }
            else
                return true;

        }
        else {

            jTextFieldLayerPropertiesName.setVisible(false);
            jLabelLayerPropertiesName.setVisible(false);
            jLabel1.setVisible(false);
            jLabel2.setVisible(false);

            jLabel3.setSize(jFrameDocrosData.charWidth * jLabel3.getText().length(), jFrameDocrosData.charHeight);
            jLabel3.setLocation(jButton[0].getX(), jTextField[jTextField.length - 1].getY() + 2 * jFrameDocrosData.charHeight);
            jLabel3.setVisible(true);
            jFrameGeometry.add(jLabel3);

            jTextField[0].addFocusListener(new JTextFieldNameFocusListener(null, 0, jLabel2, jButton[1]));
            jTextField[0].setText("");//setBackground(Color.yellow);

            for(i=0; i<jButton.length; i++)
                jButton[i].setLocation(jButton[i].getX(), jLabel3.getY() + jFrameDocrosData.charHeight);

            jFrameGeometry.setSize(jFrameGeometry.getWidth(), jButton[0].getY() + 2 * jFrameDocrosData.charHeight + 15);

        }

        return false;
    }

    Boolean newLayerPropertiesNameInserted() {

        if(newLayerPropertiesName || layerProperties.countLayerPatterns <= 1)
            return true;

        int i;

        if(jTextFieldLayerPropertiesName.isVisible()) {

            if(jTextFieldLayerPropertiesName.getText().equals("") || jTextFieldLayerPropertiesName.getText().equals("Insert a name"))
                return false;

            if(jFrameDocrosData.layerPropertiesList.existsLayerPropertiesName(jTextFieldLayerPropertiesName.getText())) {
                jTextFieldLayerPropertiesName.setBackground(Color.yellow);
                jLabel2.setVisible(true);
            }
            else {
                newLayerPropertiesName = true;
                return true;
            }
        }
        else {

            jLabel3.setVisible(false);

            //jLabelLine2 = new JLabel("Insert a new layer properties name");
            jLabel1.setSize(jFrameDocrosData.charWidth * jLabel1.getText().length(), jFrameDocrosData.charHeight);
            jLabel1.setLocation(jButton[0].getLocation());
            jFrameGeometry.add(jLabel1);


            jLabelLayerPropertiesName.setSize(jFrameDocrosData.charWidth * jLabelLayerPropertiesName.getText().length(), jFrameDocrosData.charHeight);
            jLabelLayerPropertiesName.setLocation(jLabel1.getX(), jLabel1.getY() + 2 * jFrameDocrosData.charHeight);
            jFrameGeometry.add(jLabelLayerPropertiesName);

            jTextFieldLayerPropertiesName.setSize(jTextField[0].getWidth(), jFrameDocrosData.charHeight);
            jTextFieldLayerPropertiesName.setLocation(jTextField[0].getX(), jLabelLayerPropertiesName.getY());
            jTextFieldLayerPropertiesName.setVisible(true);
            jTextFieldLayerPropertiesName.addFocusListener(new JTextFieldNameFocusListener(null, 0, jLabel2, jButton[1]));
            jFrameGeometry.add(jTextFieldLayerPropertiesName);

            jLabel2.setSize(jFrameDocrosData.charWidth * jLabel2.getText().length(), jFrameDocrosData.charHeight);
            jLabel2.setLocation(jTextFieldLayerPropertiesName.getX(), jTextFieldLayerPropertiesName.getY() + jFrameDocrosData.charHeight);
            jLabel2.setVisible(false);
            jFrameGeometry.add(jLabel2);

            for(i=0; i<jButton.length; i++)
                jButton[i].setLocation(jButton[i].getX(), jLabelLayerPropertiesName.getY() + 2 * jFrameDocrosData.charHeight);

            jFrameGeometry.setSize(jFrameGeometry.getWidth(), jFrameGeometry.getHeight() + 4 * jFrameDocrosData.charHeight);
        }

        return false;
    }

    void viewJPanelLayerPatterns() {
    
        if(!textFieldValuesChanged() && viewChanged) {
            jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
            viewChanged = false;
        }
        if(textFieldValuesChanged()) {
            //Case A
            if(laminateGeoProp.countLayerPatterns == 1) {
                int i;
                String laminateGeoPropValues[] = {
                    laminateGeoProp.name,
                    Double.toString(laminateGeoProp.thickness),
                    Double.toString(laminateGeoProp.width)
                };
                laminateGeoProp.setPropertiesValues(jTextField);
                //for(i=0; i<properties.length; i++)
                //    if(!jTextField[i].getText().equals(laminateGeoProp.getPropertyValue(properties[i])))
                //        laminateGeoProp.setPropertyValue(properties[i], jTextField[i].getText());
                jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                laminateGeoProp.setPropertiesValues(laminateGeoPropValues);
            }
            else {
                
                LayerProperties copy = null;
                LaminateGeoProp node;
                Boolean newLayerProperties;//, newLaminateGeoProp;
                newLayerProperties = false;
                //newLaminateGeoProp = false;
    
                if(layerProperties.countLayerPatterns > 1) {
                    //Case B
                    copy = new LayerProperties(layerProperties.name);
                    //jFrameDocrosData.layerPropertiesList.addLayerProperties(copy);
                    copy.materialModel = layerProperties.materialModel;
                    jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties] = copy;
                    //copy.countLayerPatterns++;
                    //layerProperties.countLayerPatterns--;
                    newLayerProperties = true;
                } //Caso B + C
                node = jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(jTextField); //criar funcao
                if(node == null) {
                    node = new LaminateGeoProp(jTextField);
                    //jFrameDocrosData.laminateGeoPropList.addLaminateGeoProp(node);
                    //newLaminateGeoProp = true;
                }
                jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].geoProp = node;
                //node.countLayerPatterns++;
                //laminateGeoProp.countLayerPatterns--;
                jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                if(newLayerProperties) {
                    //jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].countLayerPatterns--;
                    //copy.countLayerPatterns--;
                    jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties] = layerProperties;
                    //layerProperties.countLayerPatterns++;
                    //jFrameDocrosData.layerPropertiesList.delLayerProperties();
                }
                //node.countLayerPatterns--;
                jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].geoProp = laminateGeoProp;
                //laminateGeoProp.countLayerPatterns++;
                //if(newLaminateGeoProp)
                //    jFrameDocrosData.laminateGeoPropList.delLaminateGeoProp();
            }
            viewChanged = true;
        }

    }
}

