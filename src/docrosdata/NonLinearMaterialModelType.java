
package docrosdata;

public class NonLinearMaterialModelType {
    
    String name;
    int numberParameters;
    NonLinearMaterialModelList list;
    NonLinearMaterialModelType next;
    
    public NonLinearMaterialModelType(String n) {
        name = n;
        numberParameters = 0;
        list = null;
        next = null;
    }
}
