
package docrosdata;

public class MonotonicLoadTypeData {
    
    String name;
    int layerNumber;
    String layerPatternsName;
    double compressionStrainIncrement;
    //String stopConditionName;
    //double strain;
    MonotonicLoadTypeData next;

    public MonotonicLoadTypeData(String nam, int layer, String lpn, double csi) {
        
        name = nam;
        layerNumber = layer;
        layerPatternsName = lpn;
        compressionStrainIncrement = csi;
        next = null;
        
    }
    /*
    public MonotonicLoadTypeData(String nam, int layer, String lpn, double csi, String scn, double str) {
        
        name = nam;
        layerNumber = layer;
        layerPatternsName = lpn;
        compressionStrainIncrement = csi;
        stopConditionName = scn;
        strain = str;
        next = null;
        
    }
    */
}
