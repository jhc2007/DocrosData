
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

public class JFrameInputPhaseData extends JFrame{
    
    JFrameDocrosData jFrameDocrosData;
    Point location;
    JLabel jLabelPhaseNumber;
    JLabel jLabel[];
    JLabel jLabelMessage[];
    JComboBox jComboBox[];
    JButton jButton[];
    JButton jButtonHelp;
    JRadioButton jRadioButton[];
                                                  
    String jLabelText[] = {"Phase Number", "Load Type", "Load Name", "Stop Condition Name"};
    String jRadioButtonText[] = {"Monotonic", "Cyclic"};
    String jButtonText[] = {"Delete", "Reset", "OK", "Previous", "Close", "Finish"};
    String jLabelMessageText[] = {"Select load type", "Select load name", "Select stop condition name"};
    
    public JFrameInputPhaseData(JFrameDocrosData jf, Point loc) {

        super("<PHASE_DATA>");
        jFrameDocrosData = jf;
        location = loc;
        init();
    }
    
    private void init() {
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        //this.setAlwaysOnTop(true);             
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, j, jLabelLeftWidth, jLabelTitleWidth, xPosition = 10, yPosition = 10;
        
        jLabelLeftWidth = jFrameDocrosData.charWidth * (JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText));
        jLabelTitleWidth = jFrameDocrosData.charWidth * (6 + jRadioButtonText[0].length() + jRadioButtonText[1].length());
        
        JLabel jLabelTitle = new JLabel("Phase Data");
        jLabelTitle.setSize(jLabelTitleWidth, jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(xPosition + jLabelLeftWidth, yPosition);
        this.add(jLabelTitle);
        
        if(getClass().getResourceAsStream("/help/helpPhaseData.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("PhaseData"));//, "Phase Data"));
            this.add(jButtonHelp);
        }
        yPosition += jFrameDocrosData.charHeight + 12;      
        //yPosition += jFrameDocrosData.charHeight;
        
        jLabel = new JLabel[jLabelText.length];
        jRadioButton = new JRadioButton[jRadioButtonText.length];
        for(i=0; i<jLabelText.length; i++) {
            jLabel[i] = new JLabel(jLabelText[i]);
            jLabel[i].setSize(jLabelLeftWidth, jFrameDocrosData.charHeight);
            jLabel[i].setLocation(xPosition, yPosition);
            this.add(jLabel[i]);
            if(i == 0)
                yPosition += 12;
            if(i == 1) {      
                for(j=0; j<jRadioButtonText.length; j++) {
                    jRadioButton[j] = new JRadioButton(jRadioButtonText[j]);
                    jRadioButton[j].setName(jRadioButtonText[j]);
                    jRadioButton[j].setSize(jFrameDocrosData.charWidth * (3 + jRadioButtonText[j].length()), jFrameDocrosData.charHeight);
                    jRadioButton[j].setLocation(xPosition + jLabelLeftWidth + j * jRadioButton[0].getWidth(), yPosition);
                    jRadioButton[j].addActionListener(new JRadioButtonIntupPhaseDataActionListener(this));
                    this.add(jRadioButton[j]);
                }
                //jRadioButton[0].setSelected(true);
                yPosition += 12;
            }
            yPosition += jFrameDocrosData.charHeight;
        }
        jLabel[3].setVisible(false);
        
        jComboBox = new JComboBox[3];
        for(i=0; i<3; i++) {
            jComboBox[i] = new JComboBox();
            jComboBox[i].addItem("Select");
            if(i == 0) {
                jComboBox[i].setSize(10 + 7 * jFrameDocrosData.charWidth, jFrameDocrosData.charHeight); 
                if(jFrameDocrosData.phaseDataList != null && jFrameDocrosData.phaseDataList.first != null) {
                    for(j=1; j<=jFrameDocrosData.phaseDataList.count; j++)
                        jComboBox[i].addItem(j);
                }
                else {
                    jComboBox[i].setEnabled(false);
                }
            }
            else {
                if(i == 2 && jFrameDocrosData.stopConditionLayerStrainList != null && jFrameDocrosData.stopConditionLayerStrainList.first != null) {
                    StopConditionLayerStrain node = jFrameDocrosData.stopConditionLayerStrainList.first;
                    while(node != null) {
                        jComboBox[i].addItem(node.name);
                        node = node.next;
                    }
                }
                jComboBox[i].setSize(jLabelTitleWidth, jFrameDocrosData.charHeight);     
            }
            if(i < 2)
                jComboBox[i].setLocation(xPosition + jLabelLeftWidth, jLabel[i * 2].getY());                
            else {
                jComboBox[i].setLocation(xPosition + jLabelLeftWidth, jLabel[3].getY());                
                jComboBox[i].setVisible(false);
            }
            jComboBox[i].setSelectedIndex(0);
            this.add(jComboBox[i]);
        }
        
        jLabelPhaseNumber = new JLabel("1", SwingConstants.CENTER);
        jLabelPhaseNumber.setSize(40, jComboBox[0].getHeight());
        jLabelPhaseNumber.setLocation(jComboBox[0].getLocation());
        jLabelPhaseNumber.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        if(jComboBox[0].getItemCount() == 1) 
            jComboBox[0].setVisible(false);
        else
            jLabelPhaseNumber.setVisible(false);

        
        this.add(jLabelPhaseNumber);

        
        
        jLabelMessage = new JLabel[jLabelMessageText.length];
        for(i=0; i<jLabelMessageText.length; i++) {
            jLabelMessage[i] = new JLabel(jLabelMessageText[i], SwingConstants.RIGHT);
            jLabelMessage[i].setSize(jLabelTitle.getSize());
            jLabelMessage[i].setLocation(xPosition + jLabelLeftWidth, yPosition + 10);
            jLabelMessage[i].setForeground(Color.red);
            jLabelMessage[i].setVisible(false);
            this.add(jLabelMessage[i]);
        }
            
        yPosition += jFrameDocrosData.charHeight + 10;
        
        jButton = new JButton[jButtonText.length];
        
        for(i=0; i<3; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelLeftWidth + jLabelTitleWidth) / 3, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * jButton[i].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputPhaseDataActionListener(this));
            this.add(jButton[i]);
        }
        if(jComboBox[0].getItemCount() == 1) {
            jButton[0].setVisible(false);
            jButton[1].setVisible(false);
        }
        
        yPosition += jFrameDocrosData.charHeight + 12;
               
        for(i=3; i<jButtonText.length; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelLeftWidth + jLabelTitleWidth) / 3, 18);
            jButton[i].setLocation(xPosition + (i - 3) * jButton[3].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputPhaseDataActionListener(this));
            this.add(jButton[i]);
        }
        
        jComboBox[0].addActionListener(new JComboBoxInputPhaseData(this));
        jComboBox[1].addFocusListener(new JComboBoxFocusListener(jLabelMessage, 1, jButton[2]));
        jComboBox[2].addFocusListener(new JComboBoxFocusListener(jLabelMessage, 2, jButton[2]));
                
        if(jFrameDocrosData.phaseDataList == null || jFrameDocrosData.phaseDataList.first == null) 
            jButton[5].setEnabled(false);
        else
            jComboBox[0].setSelectedIndex(0);
        
        yPosition += 2 * jFrameDocrosData.charHeight;
        
        this.setSize(jLabelLeftWidth + jLabelTitleWidth + 26, yPosition);
        this.setVisible(true);
    }
    
}
