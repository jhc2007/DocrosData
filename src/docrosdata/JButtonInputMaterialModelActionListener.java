
package docrosdata;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

class JButtonInputMaterialModelActionListener implements ActionListener {
    
    JFrameInputMaterialModel jFrameInputMaterialModel;
    
    public JButtonInputMaterialModelActionListener(JFrameInputMaterialModel jf) {
        
        jFrameInputMaterialModel = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        switch (ae.getActionCommand()) {
            case "New":
                setParametersVisible(true);
                jFrameInputMaterialModel.jButton[0].setVisible(false);
                jFrameInputMaterialModel.jButton[1].setText("Back");
                jFrameInputMaterialModel.jButton[1].setVisible(true);
                jFrameInputMaterialModel.jButton[2].setText("OK");
                break;            
            case "OK":
                if(jFrameInputMaterialModel.jTextFieldName.isVisible()) {
                    if(existsMaterialModelName()) {
                        jFrameInputMaterialModel.jTextFieldName.setFocusable(false);
                        jFrameInputMaterialModel.jTextFieldName.setBackground(Color.yellow);
                        jFrameInputMaterialModel.jLabelMessage.setVisible(true);
                        jFrameInputMaterialModel.jButton[2].setEnabled(false);
                        jFrameInputMaterialModel.jTextFieldName.setFocusable(true);                                                                      
                    }
                    else {
                        if(!hasError(true)) {
                            addMaterialModel();
                            setParametersVisible(false);
                        }
                    }
                }
                else {
                    if(!hasError(false)) {
                        updateParameters();
                        setParametersVisible(false);
                    }
                }
                break;
            case "Delete":
                deleteMaterialModel();
                jFrameInputMaterialModel.jButton[0].setVisible(false);
                jFrameInputMaterialModel.jButton[1].setText("New");
                jFrameInputMaterialModel.jButton[1].setVisible(false);
                break;
            case "Reset":
                resetParameters();
                break;
            case "Back":
                setParametersVisible(false);
                break;
            case "Next":
                jFrameInputMaterialModel.dispose();
                JFrameInputLaminateGeoProp jFrameInputLaminateGeoProp = new JFrameInputLaminateGeoProp(jFrameInputMaterialModel.jFrameDocrosData, jFrameInputMaterialModel.getLocation());
                break;
            case "Previous":
                jFrameInputMaterialModel.dispose();
                JFrameInputMainParameters jFrameInputMainParameters = new JFrameInputMainParameters(jFrameInputMaterialModel.jFrameDocrosData, jFrameInputMaterialModel.getLocation());
                break;
            case "Close":
                jFrameInputMaterialModel.dispose();
                break;
        }
    }
    
    void deleteMaterialModel() {
        
        NonLinearMaterialModel node = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7), String.valueOf(jFrameInputMaterialModel.jComboBoxName.getSelectedItem()));
        jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.delNonLinearMaterialModel(node);
        setParametersVisible(false);
        if(!jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.existsMaterialModel()) {
            jFrameInputMaterialModel.jButtonNext.setEnabled(false);
            jFrameInputMaterialModel.jFrameDocrosData.jMenuItemEdit[2].setEnabled(false);
            jFrameInputMaterialModel.jFrameDocrosData.jMenuItemEdit[3].setEnabled(false);
        }
        //jFrameInputMaterialModel.jFrameDocrosData.changedLists[jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeListIndex] = true;
        jFrameInputMaterialModel.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputMaterialModel.jFrameDocrosData.dataFile != null)
            jFrameInputMaterialModel.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void updateParameters() {
        
        int i;
        NonLinearMaterialModel node = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7), String.valueOf(jFrameInputMaterialModel.jComboBoxName.getSelectedItem()));
        for(i=0; i<node.parameters.length; i++)
            node.parameters[i] = Double.valueOf(jFrameInputMaterialModel.jTextFieldParameter[i].getText());
        jFrameInputMaterialModel.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputMaterialModel.jFrameDocrosData.dataFile != null)
            jFrameInputMaterialModel.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void resetParameters() {
        
        int i;
        NonLinearMaterialModel node = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7), String.valueOf(jFrameInputMaterialModel.jComboBoxName.getSelectedItem()));
        for(i=0; i<node.parameters.length; i++) {
            jFrameInputMaterialModel.jTextFieldParameter[i].setText(String.format("%7.4E", node.parameters[i]).replace(',', '.'));
            jFrameInputMaterialModel.jTextFieldParameter[i].setBackground(Color.white);
        }
        if(node.type.equals("NLMM105")) {
            if(node.parameters[12] == 0.0 && node.parameters[13] == 0.0) {
                jFrameInputMaterialModel.jRadioButton[0].setSelected(true);
                jFrameInputMaterialModel.jRadioButton[1].setSelected(false);
                for(i=12; i<14; i++) {
                    jFrameInputMaterialModel.jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                    jFrameInputMaterialModel.jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setText("0.0");
                    jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(false);
                }
            }
            else {
                jFrameInputMaterialModel.jRadioButton[1].setSelected(true);
                jFrameInputMaterialModel.jRadioButton[0].setSelected(false);
                for(i=12; i<14; i++) {
                    jFrameInputMaterialModel.jLabelParameterSymbol[i].setForeground(Color.black);
                    jFrameInputMaterialModel.jLabelParameterUnit[i].setForeground(Color.black);
                    //jFrameInputMaterialModel.jTextFieldParameter[i].setText("0.0");
                    jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(true);
                }
            }
        }
        jFrameInputMaterialModel.jButton[2].setEnabled(true);
    }
    
    void addMaterialModel() {
        
        int i;
        NonLinearMaterialModelType type = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelType('_' + String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7));      
        
        NonLinearMaterialModelList list = type.list;
     
        double parameters[];
        parameters = new double[type.numberParameters];
        
        for(i=0; i<type.numberParameters; i++)
            parameters[i] = Double.valueOf(jFrameInputMaterialModel.jTextFieldParameter[i].getText());
        
        NonLinearMaterialModel node = new NonLinearMaterialModel(String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7), jFrameInputMaterialModel.jTextFieldName.getText(), parameters);
        
        if(list == null)
            list = new NonLinearMaterialModelList();
        
        list.addNonLinearMaterialModel(node);
        type.list = list;
        
        jFrameInputMaterialModel.jComboBoxName.addItem(node.name);
        jFrameInputMaterialModel.jButtonNext.setEnabled(true);
        //jFrameInputMaterialModel.jFrameDocrosData.jMenuItemEdit[1].setEnabled(true);
        jFrameInputMaterialModel.jFrameDocrosData.jMenuItemEdit[2].setEnabled(true);
        if(jFrameInputMaterialModel.jFrameDocrosData.laminateGeoPropList != null && jFrameInputMaterialModel.jFrameDocrosData.laminateGeoPropList.first != null)
            jFrameInputMaterialModel.jFrameDocrosData.jMenuItemEdit[3].setEnabled(true);
        //jFrameInputMaterialModel.jFrameDocrosData.changedLists[jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeListIndex] = true; 
        jFrameInputMaterialModel.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputMaterialModel.jFrameDocrosData.dataFile != null)
            jFrameInputMaterialModel.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    Boolean hasError(Boolean jTextFieldNameVisible) {
        
        int i;
        String type = String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7);
        int numberParameters = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelType('_' + type).numberParameters;
        String text;
        
        if(jTextFieldNameVisible && jFrameInputMaterialModel.jTextFieldName.getText().equals("") || jFrameInputMaterialModel.jTextFieldName.getText().equals("Insert name")) {
           jFrameInputMaterialModel.jTextFieldName.setText("Insert name");
           jFrameInputMaterialModel.jTextFieldName.setBackground(Color.yellow);
           jFrameInputMaterialModel.jButton[2].setEnabled(false);
           return true;
        }
        for(i=0; i<numberParameters; i++) {
            if(jFrameInputMaterialModel.jTextFieldParameter[i].getText().equals("") || jFrameInputMaterialModel.jTextFieldParameter[i].getText().equals("Insert number")) {
                jFrameInputMaterialModel.jTextFieldParameter[i].setText("Insert number");
                jFrameInputMaterialModel.jTextFieldParameter[i].setBackground(Color.yellow);
                jFrameInputMaterialModel.jButton[2].setEnabled(false);
                return true;
            }
            try {
                Double.valueOf(jFrameInputMaterialModel.jTextFieldParameter[i].getText());
            }
            catch(Exception e) {
                text = jFrameInputMaterialModel.jTextFieldParameter[i].getText();
                jFrameInputMaterialModel.jTextFieldParameter[i].setText(text + " NOT A NUMBER");
                jFrameInputMaterialModel.jTextFieldParameter[i].setBackground(Color.yellow);
                jFrameInputMaterialModel.jButton[2].setEnabled(false);
                return true; 
            }
        }
        return false;
    }
     
    Boolean existsMaterialModelName() {
        
        NonLinearMaterialModelList list = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7));
        if(list == null)
            return false;
        NonLinearMaterialModel node = list.first;
        while(node != null) {
            if(node.name.equals(jFrameInputMaterialModel.jTextFieldName.getText())) 
                return true;
            node = node.next;
        }
        return false;
    }
    
    void setParametersVisible(Boolean visible) {
        
        int i, materialIndex, numberParameters, jPanelImageWidth, yIncrement;
        int xPosition1 = jFrameInputMaterialModel.jLabelName.getX();
        int xPosition2 = jFrameInputMaterialModel.jLabelMessage.getX();
        int yPosition = jFrameInputMaterialModel.jLabelMessage.getY() + jFrameInputMaterialModel.jFrameDocrosData.charHeight;      
        String type, symbol;
        Image imageScaled;
        ImageIcon imageIconMaterial;
        Dimension screenDim;
        
        jFrameInputMaterialModel.jLabelMessage.setVisible(false);
        jFrameInputMaterialModel.jTextFieldName.setText("");
        jFrameInputMaterialModel.jTextFieldName.setBackground(Color.white);
        jFrameInputMaterialModel.jComboBoxType.setEnabled(!visible);
        jFrameInputMaterialModel.jComboBoxName.setEnabled(!visible);
        jFrameInputMaterialModel.jComboBoxName.setVisible(!visible);
        jFrameInputMaterialModel.jTextFieldName.setVisible(visible);
        jFrameInputMaterialModel.jButtonNext.setVisible(!visible);
        jFrameInputMaterialModel.jButtonPrev.setVisible(!visible);
        jFrameInputMaterialModel.jButtonClose.setVisible(!visible);
        
        type = String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7);
        materialIndex = jFrameInputMaterialModel.jFrameDocrosData.getMaterialIndex(type);
        numberParameters = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelType('_' + type).numberParameters;
        
        if(visible) {            
            
            jFrameInputMaterialModel.jLabelParameterSymbol = new JLabel[numberParameters];
            jFrameInputMaterialModel.jLabelParameterUnit = new JLabel[numberParameters];
            jFrameInputMaterialModel.jTextFieldParameter = new JTextField[numberParameters];
            
            if(type.equals("NLMM105")) { 
                yIncrement = 22;
                jFrameInputMaterialModel.jLabelCrackModel.setVisible(true);                
                jFrameInputMaterialModel.jRadioButton[0].setVisible(true);
                jFrameInputMaterialModel.jRadioButton[1].setVisible(true);
            }
            else
                yIncrement = 24;
            
            for(i=0; i<numberParameters; i++) {
                
                symbol = jFrameInputMaterialModel.jFrameDocrosData.materialParametersSymbols[jFrameInputMaterialModel.jFrameDocrosData.getMaterialIndex(String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7))][i];
        
                if(i == 12 && type.equals("NLMM105")) { 
                    jFrameInputMaterialModel.jLabelCrackModel.setLocation(xPosition2, yPosition);
                    jFrameInputMaterialModel.jRadioButton[0].setLocation(xPosition2 + 80, yPosition);
                    jFrameInputMaterialModel.jRadioButton[1].setLocation(xPosition2 + 150, yPosition);
                    yPosition += yIncrement;
                }
                
                jFrameInputMaterialModel.jLabelParameterSymbol[i] = new JLabel(symbol);
                jFrameInputMaterialModel.jLabelParameterSymbol[i].setSize(jFrameInputMaterialModel.jFrameDocrosData.charWidth * jFrameInputMaterialModel.jLabelParameterSymbol[i].getText().length(), yIncrement);
                jFrameInputMaterialModel.jLabelParameterSymbol[i].setLocation(xPosition1, yPosition);
                jFrameInputMaterialModel.jLabelParameterSymbol[i].setVisible(visible);
                jFrameInputMaterialModel.add(jFrameInputMaterialModel.jLabelParameterSymbol[i]);

                switch (symbol.charAt(0)) {
                        case 'E':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[0] + "/" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[1] + "2]", SwingConstants.RIGHT);
                            break;
                        case 'f':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[0] + "/" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[1] + "2]", SwingConstants.RIGHT);
                            break;
                        case 'ε':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[-]", SwingConstants.RIGHT); 
                            break;
                        case 'ω':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[1] + "]", SwingConstants.RIGHT);
                            break;
                        case 'l':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[1] + "]", SwingConstants.RIGHT);
                            break;
                        default:
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[0-1]", SwingConstants.RIGHT);
                }
                jFrameInputMaterialModel.jLabelParameterUnit[i].setSize(xPosition2 - xPosition1 - jFrameInputMaterialModel.jLabelParameterSymbol[i].getWidth() - 10, yIncrement);
                jFrameInputMaterialModel.jLabelParameterUnit[i].setLocation(xPosition1 + jFrameInputMaterialModel.jLabelParameterSymbol[i].getWidth(), yPosition);
                jFrameInputMaterialModel.jLabelParameterUnit[i].setVisible(true);
                jFrameInputMaterialModel.add(jFrameInputMaterialModel.jLabelParameterUnit[i]);
                              
                jFrameInputMaterialModel.jTextFieldParameter[i] = new JTextField("");
                jFrameInputMaterialModel.jTextFieldParameter[i].setSize(jFrameInputMaterialModel.jTextFieldName.getWidth(), yIncrement);
                jFrameInputMaterialModel.jTextFieldParameter[i].setLocation(xPosition2, yPosition);
                jFrameInputMaterialModel.jTextFieldParameter[i].addKeyListener(new JTextFieldKeyListener());
                jFrameInputMaterialModel.jTextFieldParameter[i].addFocusListener(new JTextFieldFocusListener(jFrameInputMaterialModel.jButton, null, 0));
                jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(visible);
                jFrameInputMaterialModel.jTextFieldParameter[i].setVisible(visible);
                jFrameInputMaterialModel.add(jFrameInputMaterialModel.jTextFieldParameter[i]);
                
                yPosition += yIncrement;
                //yPosition += jFrameInputMaterialModel.jFrameDocrosData.charHeight;
            }
            if(type.equals("NLMM105")) {
                jFrameInputMaterialModel.jRadioButton[0].setSelected(visible);
                jFrameInputMaterialModel.jRadioButton[1].setSelected(!visible);
                for(i=12; i<14; i++) {
                    jFrameInputMaterialModel.jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                    jFrameInputMaterialModel.jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setText("0.0");
                    jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(!visible);
                }
            }
            //yPosition += jFrameInputMaterialModel.jFrameDocrosData.charHeight;
            yPosition += yIncrement;
            
            if(jFrameInputMaterialModel.bufferedImageMaterial[materialIndex] != null) {                
                screenDim = Toolkit.getDefaultToolkit().getScreenSize();
                jPanelImageWidth = jFrameInputMaterialModel.bufferedImageMaterial[materialIndex].getWidth() * (yPosition + 10) / jFrameInputMaterialModel.bufferedImageMaterial[materialIndex].getHeight() + 4;
                if(jFrameInputMaterialModel.getWidth() + jPanelImageWidth + 28 > screenDim.width)
                    jPanelImageWidth = screenDim.width - jFrameInputMaterialModel.getWidth() - 28;
                jFrameInputMaterialModel.jPanelImage.setSize(jPanelImageWidth, yPosition + 12);            
                jFrameInputMaterialModel.jLabelImage[materialIndex].setSize(jPanelImageWidth - 8, jFrameInputMaterialModel.jPanelImage.getHeight() - 8);
                imageScaled = jFrameInputMaterialModel.bufferedImageMaterial[materialIndex].getScaledInstance(jFrameInputMaterialModel.jLabelImage[materialIndex].getWidth(), jFrameInputMaterialModel.jLabelImage[materialIndex].getHeight(), Image.SCALE_SMOOTH);
                imageIconMaterial = new ImageIcon(imageScaled);
                jFrameInputMaterialModel.jLabelImage[materialIndex].setIcon(imageIconMaterial);
            }
            else
                jFrameInputMaterialModel.jPanelImage.setSize(0, 0);
        }
        else {
            jFrameInputMaterialModel.jLabelCrackModel.setVisible(visible);
            jFrameInputMaterialModel.jRadioButton[0].setVisible(visible);
            jFrameInputMaterialModel.jRadioButton[1].setVisible(visible);
            for(i=0; i<numberParameters; i++) {
                jFrameInputMaterialModel.jLabelParameterSymbol[i].setVisible(visible);
                jFrameInputMaterialModel.jLabelParameterUnit[i].setVisible(visible);
                jFrameInputMaterialModel.jTextFieldParameter[i].setVisible(visible);
            }
            jFrameInputMaterialModel.jButton[0].setVisible(visible);
            jFrameInputMaterialModel.jButton[1].setVisible(visible);
            jFrameInputMaterialModel.jButton[2].setText("New");
            jFrameInputMaterialModel.jComboBoxType.setSelectedIndex(0);
            jFrameInputMaterialModel.jComboBoxName.setSelectedIndex(0);
        }            
        for(i=0; i<3; i++) {
            jFrameInputMaterialModel.jButton[i].setLocation(jFrameInputMaterialModel.jButton[i].getX(), yPosition);
        }
        
        jFrameInputMaterialModel.jPanelImage.setVisible(visible);
        jFrameInputMaterialModel.jLabelImage[materialIndex].setVisible(visible);
        
        yPosition += jFrameInputMaterialModel.jFrameDocrosData.charHeight;
      
        if(visible) {                                   
            yPosition += 16 + jFrameInputMaterialModel.jFrameDocrosData.charHeight;
            if(jFrameInputMaterialModel.bufferedImageMaterial[materialIndex] != null)
                jFrameInputMaterialModel.setSize(jFrameInputMaterialModel.getWidth() + jFrameInputMaterialModel.jPanelImage.getWidth() + 28, yPosition);
            else
                jFrameInputMaterialModel.setSize(jFrameInputMaterialModel.getWidth(), yPosition);
        }
        else {
            yPosition += 34 + jFrameInputMaterialModel.jFrameDocrosData.charHeight;
            if(jFrameInputMaterialModel.bufferedImageMaterial[materialIndex] != null)
                jFrameInputMaterialModel.setSize(jFrameInputMaterialModel.getWidth() - (jFrameInputMaterialModel.jPanelImage.getWidth() + 28), yPosition);
            else
                jFrameInputMaterialModel.setSize(jFrameInputMaterialModel.getWidth(), yPosition);
        }       
    }
    
}
