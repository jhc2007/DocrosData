
package docrosdata;

    
public class StopConditionLayerStrainList {
    
    StopConditionLayerStrain first;
    StopConditionLayerStrain last;
    int count;

    public StopConditionLayerStrainList() {
        
        first = null;
        last = null;
        count = 0;
    }
    
    StopConditionLayerStrain getStopConditionLayerStrain(String nam) {
        
        StopConditionLayerStrain node = first;
        while(node != null) {
            if(node.name.equals(nam))
                return node;
            node = node.next;
        }
        return node;
    }
    
    void delStopConditionLayerStrain(String nam) {
        
        StopConditionLayerStrain node1, node2;
        node1 = first;
        node2 = node1.next;
        if(node1.name.equals(nam)) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && !node2.name.equals(nam)) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
    }
    
    void addStopConditionLayerStrain(StopConditionLayerStrain node) {
        
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
   
}
