
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class JComboBoxInputStopConditionLayerStrain implements ActionListener {

    JFrameInputStopConditionLayerStrain jFrameInputStopConditionLayerStrain;
    
    public JComboBoxInputStopConditionLayerStrain(JFrameInputStopConditionLayerStrain jf) {
        
        jFrameInputStopConditionLayerStrain = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;
    
        for(i=0; i<jFrameInputStopConditionLayerStrain.jLabelMessage.length; i++)
            jFrameInputStopConditionLayerStrain.jLabelMessage[i].setVisible(false);
        jFrameInputStopConditionLayerStrain.jButton[2].setEnabled(true);
        
        if(((JComboBox) ae.getSource()).getName() == null) {
           
            if(jFrameInputStopConditionLayerStrain.jComboBox[0].getSelectedIndex() == 0) {
                jFrameInputStopConditionLayerStrain.jTextField[1].setText("");
                jFrameInputStopConditionLayerStrain.jTextField[1].setBackground(Color.white);
                jFrameInputStopConditionLayerStrain.jTextField[1].setEnabled(false);
                jFrameInputStopConditionLayerStrain.jComboBox[1].setSelectedIndex(0);
                jFrameInputStopConditionLayerStrain.jComboBox[1].setEnabled(false);
                jFrameInputStopConditionLayerStrain.jComboBox[2].setSelectedIndex(0);
                jFrameInputStopConditionLayerStrain.jComboBox[2].setEnabled(false);
                jFrameInputStopConditionLayerStrain.jButton[0].setVisible(false);
                jFrameInputStopConditionLayerStrain.jButton[1].setVisible(false);
                jFrameInputStopConditionLayerStrain.jButton[2].setText("New");               
            }
            else {
                jFrameInputStopConditionLayerStrain.jTextField[1].setBackground(Color.white);
                jFrameInputStopConditionLayerStrain.jTextField[1].setEnabled(true);
                jFrameInputStopConditionLayerStrain.jComboBox[1].setEnabled(true);
                jFrameInputStopConditionLayerStrain.jComboBox[2].setEnabled(true);
                String name = String.valueOf(jFrameInputStopConditionLayerStrain.jComboBox[0].getSelectedItem());
                StopConditionLayerStrain node = jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList.getStopConditionLayerStrain(name);
                jFrameInputStopConditionLayerStrain.jComboBox[1].setSelectedItem(node.layerPatternsName);
                jFrameInputStopConditionLayerStrain.jComboBox[2].setSelectedItem(node.layerNumber);
                jFrameInputStopConditionLayerStrain.jTextField[1].setText(String.valueOf(node.strain));           
                jFrameInputStopConditionLayerStrain.jButton[0].setText("Delete");
                jFrameInputStopConditionLayerStrain.jButton[0].setVisible(true);
                jFrameInputStopConditionLayerStrain.jButton[1].setText("Reset");
                jFrameInputStopConditionLayerStrain.jButton[1].setVisible(true);
                jFrameInputStopConditionLayerStrain.jButton[2].setText("OK");                
            }
        }
        else {
            jFrameInputStopConditionLayerStrain.jComboBox[2].removeAllItems();
            jFrameInputStopConditionLayerStrain.jComboBox[2].addItem("Select");
            jFrameInputStopConditionLayerStrain.jComboBox[2].setSelectedIndex(0);
            if(jFrameInputStopConditionLayerStrain.jComboBox[1].getSelectedIndex() > 0) {
                String layerPatternsName = String.valueOf(jFrameInputStopConditionLayerStrain.jComboBox[1].getSelectedItem());
                int firstLayer, lastLayer;
                firstLayer = jFrameInputStopConditionLayerStrain.jFrameDocrosData.layerPatternsList.getFirstLayer(layerPatternsName);
                lastLayer = jFrameInputStopConditionLayerStrain.jFrameDocrosData.layerPatternsList.getLastLayer(layerPatternsName);
                for(i=firstLayer; i<=lastLayer; i++)
                    jFrameInputStopConditionLayerStrain.jComboBox[2].addItem(i);
            }
        }
    }
    
}

