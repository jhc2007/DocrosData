
package docrosdata;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

class JComboBoxNameInputMaterialModel implements ActionListener {
    
    JFrameInputMaterialModel jFrameInputMaterialModel;

    public JComboBoxNameInputMaterialModel(JFrameInputMaterialModel jf) {
        
        jFrameInputMaterialModel = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
              
        //if(jFrameInputMaterialModel.jComboBoxName.getSelectedItem() != null && !String.valueOf(jFrameInputMaterialModel.jComboBoxName.getSelectedItem()).equals("Select")) {
        if(jFrameInputMaterialModel.jComboBoxName.getSelectedIndex() > 0) {
            
            showParameters();
            jFrameInputMaterialModel.jButton[0].setText("Delete");
            jFrameInputMaterialModel.jButton[0].setVisible(true);
            jFrameInputMaterialModel.jButton[1].setText("Reset");
            jFrameInputMaterialModel.jButton[1].setVisible(true);
            jFrameInputMaterialModel.jButton[2].setText("OK");
            jFrameInputMaterialModel.jButton[2].setVisible(true);
            NonLinearMaterialModel node = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7), String.valueOf(jFrameInputMaterialModel.jComboBoxName.getSelectedItem()));
            if(node.countLayerPatterns > 0)
                jFrameInputMaterialModel.jButton[0].setEnabled(false);
        }
    }
    
    
    void showParameters() {
        
            int i, materialIndex, numberParameters, jPanelImageWidth, yIncrement;
            String type, symbol;
            Image imageScaled;
            ImageIcon imageIconMaterial;
            Dimension screenDim;
            
            int xPosition1 = jFrameInputMaterialModel.jLabelName.getX();
            int xPosition2 = jFrameInputMaterialModel.jComboBoxName.getX();
            int yPosition = jFrameInputMaterialModel.jComboBoxName.getY() + 2 * jFrameInputMaterialModel.jFrameDocrosData.charHeight;
            
            type = String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7);
            materialIndex = jFrameInputMaterialModel.jFrameDocrosData.getMaterialIndex(type);
            
            jFrameInputMaterialModel.jComboBoxType.setEnabled(false);
            jFrameInputMaterialModel.jComboBoxName.setEnabled(false);
            jFrameInputMaterialModel.jTextFieldName.setVisible(false);
            jFrameInputMaterialModel.jButton[0].setText("Reset");
            jFrameInputMaterialModel.jButton[0].setVisible(true);
            jFrameInputMaterialModel.jButton[1].setText("Save");
            jFrameInputMaterialModel.jButton[1].setEnabled(true);
            jFrameInputMaterialModel.jButtonNext.setVisible(false);
            jFrameInputMaterialModel.jButtonPrev.setVisible(false);
            jFrameInputMaterialModel.jButtonClose.setVisible(false);

            NonLinearMaterialModel node = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + type, String.valueOf(jFrameInputMaterialModel.jComboBoxName.getSelectedItem()));

            numberParameters = node.parameters.length;

            jFrameInputMaterialModel.jLabelParameterSymbol = new JLabel[numberParameters];
            jFrameInputMaterialModel.jLabelParameterUnit = new JLabel[numberParameters];
            jFrameInputMaterialModel.jTextFieldParameter = new JTextField[numberParameters];
            
            if(type.equals("NLMM105")) { 
                yIncrement = 22;
                jFrameInputMaterialModel.jLabelCrackModel.setVisible(true);                
                jFrameInputMaterialModel.jRadioButton[0].setVisible(true);
                jFrameInputMaterialModel.jRadioButton[1].setVisible(true);
            }
            else {
                yIncrement = 24;
                jFrameInputMaterialModel.jLabelCrackModel.setVisible(false);
                jFrameInputMaterialModel.jRadioButton[0].setVisible(false);
                jFrameInputMaterialModel.jRadioButton[1].setVisible(false);
            }

            for(i=0; i<numberParameters; i++) {
                
                symbol = jFrameInputMaterialModel.jFrameDocrosData.materialParametersSymbols[jFrameInputMaterialModel.jFrameDocrosData.getMaterialIndex(type)][i];
                
                if(i == 12 && type.equals("NLMM105")) { 
                    jFrameInputMaterialModel.jLabelCrackModel.setLocation(xPosition2, yPosition);
                    jFrameInputMaterialModel.jRadioButton[0].setLocation(xPosition2 + 80, yPosition);
                    jFrameInputMaterialModel.jRadioButton[1].setLocation(xPosition2 + 150, yPosition);
                    yPosition += yIncrement;
                }
                
                jFrameInputMaterialModel.jLabelParameterSymbol[i] = new JLabel(symbol);
                jFrameInputMaterialModel.jLabelParameterSymbol[i].setSize(jFrameInputMaterialModel.jFrameDocrosData.charWidth * jFrameInputMaterialModel.jLabelParameterSymbol[i].getText().length(), yIncrement);
                jFrameInputMaterialModel.jLabelParameterSymbol[i].setLocation(xPosition1, yPosition);
                jFrameInputMaterialModel.jLabelParameterSymbol[i].setVisible(true);
                jFrameInputMaterialModel.add(jFrameInputMaterialModel.jLabelParameterSymbol[i]);

                switch (symbol.charAt(0)) {
                        case 'E':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[0] + "/" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[1] + "2]", SwingConstants.RIGHT);
                            break;
                        case 'f':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[0] + "/" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[1] + "2]", SwingConstants.RIGHT);
                            break;
                        case 'ε':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[-]", SwingConstants.RIGHT); 
                            break;
                        case 'ω':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[1] + "]", SwingConstants.RIGHT);
                            break;
                        case 'l':
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[" + jFrameInputMaterialModel.jFrameDocrosData.unitsInputOutputData[1] + "]", SwingConstants.RIGHT);
                            break;
                        default:
                            jFrameInputMaterialModel.jLabelParameterUnit[i] = new JLabel("[0-1]", SwingConstants.RIGHT);
                }
                jFrameInputMaterialModel.jLabelParameterUnit[i].setSize(xPosition2 - xPosition1 - jFrameInputMaterialModel.jLabelParameterSymbol[i].getWidth() - 10, yIncrement);
                jFrameInputMaterialModel.jLabelParameterUnit[i].setLocation(xPosition1 + jFrameInputMaterialModel.jLabelParameterSymbol[i].getWidth(), yPosition);
                jFrameInputMaterialModel.jLabelParameterUnit[i].setVisible(true);
                jFrameInputMaterialModel.add(jFrameInputMaterialModel.jLabelParameterUnit[i]);
                
                jFrameInputMaterialModel.jTextFieldParameter[i] = new JTextField(String.format("%7.4E", node.parameters[i]).replace(',', '.'));
                jFrameInputMaterialModel.jTextFieldParameter[i].setSize(jFrameInputMaterialModel.jTextFieldName.getWidth(), yIncrement);
                jFrameInputMaterialModel.jTextFieldParameter[i].setLocation(xPosition2, yPosition);
                jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(true);
                jFrameInputMaterialModel.jTextFieldParameter[i].addKeyListener(new JTextFieldKeyListener());
                jFrameInputMaterialModel.jTextFieldParameter[i].addFocusListener(new JTextFieldFocusListener(jFrameInputMaterialModel.jButton, null, 0));
                jFrameInputMaterialModel.jTextFieldParameter[i].setVisible(true);
                jFrameInputMaterialModel.add(jFrameInputMaterialModel.jTextFieldParameter[i]);
                
                yPosition += yIncrement;
            }
            if(type.equals("NLMM105")) {
                if(node.parameters[12] == 0.0 && node.parameters[13] == 0.0) {
                    jFrameInputMaterialModel.jRadioButton[0].setSelected(true);
                    jFrameInputMaterialModel.jRadioButton[1].setSelected(false);
                    for(i=12; i<14; i++) {
                        jFrameInputMaterialModel.jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                        jFrameInputMaterialModel.jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                        jFrameInputMaterialModel.jTextFieldParameter[i].setText("0.0");
                        jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(false);
                    }
                }
                else {
                    jFrameInputMaterialModel.jRadioButton[1].setSelected(true);
                    jFrameInputMaterialModel.jRadioButton[0].setSelected(false);
                }
            }

            //yPosition += 2 + jFrameInputMaterialModel.jFrameDocrosData.charHeight;
            yPosition += yIncrement;

            if(jFrameInputMaterialModel.bufferedImageMaterial[materialIndex] != null) {
                jPanelImageWidth = jFrameInputMaterialModel.bufferedImageMaterial[materialIndex].getWidth() * (yPosition + 10) / jFrameInputMaterialModel.bufferedImageMaterial[materialIndex].getHeight() + 4;
                screenDim = Toolkit.getDefaultToolkit().getScreenSize();
                if(jFrameInputMaterialModel.getWidth() + jPanelImageWidth + 28 > screenDim.width)
                    jPanelImageWidth = screenDim.width - jFrameInputMaterialModel.getWidth() - 28;           
                jFrameInputMaterialModel.jPanelImage.setSize(jPanelImageWidth, yPosition + 12);            
                jFrameInputMaterialModel.jLabelImage[materialIndex].setSize(jPanelImageWidth - 8, jFrameInputMaterialModel.jPanelImage.getHeight() - 8);                
                imageScaled = jFrameInputMaterialModel.bufferedImageMaterial[materialIndex].getScaledInstance(jFrameInputMaterialModel.jLabelImage[materialIndex].getWidth(), jFrameInputMaterialModel.jLabelImage[materialIndex].getHeight(), Image.SCALE_SMOOTH);
                imageIconMaterial = new ImageIcon(imageScaled);
                jFrameInputMaterialModel.jLabelImage[materialIndex].setIcon(imageIconMaterial);
                jFrameInputMaterialModel.jLabelImage[jFrameInputMaterialModel.jFrameDocrosData.getMaterialIndex(type)].setVisible(true);
                jFrameInputMaterialModel.jPanelImage.setVisible(true);
            }
            else
                jFrameInputMaterialModel.jPanelImage.setSize(0, 0);
            for(i=0; i<3; i++)
                jFrameInputMaterialModel.jButton[i].setLocation(jFrameInputMaterialModel.jButton[i].getX(), yPosition);

            yPosition += 12 + 2 * jFrameInputMaterialModel.jFrameDocrosData.charHeight;
                        
            if(jFrameInputMaterialModel.bufferedImageMaterial[materialIndex] != null)
                jFrameInputMaterialModel.setSize(jFrameInputMaterialModel.getWidth() + jFrameInputMaterialModel.jPanelImage.getWidth() + 28, yPosition);
            else
                jFrameInputMaterialModel.setSize(jFrameInputMaterialModel.getWidth(), yPosition);
    }
    
}
