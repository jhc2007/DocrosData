
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputLayerPropertiesActioListener implements ActionListener {

    JFrameInputLayerProperties jFrameInputLayerProperties;
    
    public JButtonInputLayerPropertiesActioListener(JFrameInputLayerProperties jf) {
        
        jFrameInputLayerProperties = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;
        switch (ae.getActionCommand()) {
            case "Used":
                String layerPropertiesNames[];
                layerPropertiesNames = new String[jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.count];
                LayerProperties node = jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.first;
                for(i=0; i<jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.count; i++) {
                    layerPropertiesNames[i] = node.name;
                    node = node.next;
                }
                JFrameUsedNames jFrameUsedNames = new JFrameUsedNames("Used Layer Properties Names:", layerPropertiesNames, jFrameInputLayerProperties.getLocation());
                break;   
            case "New":
                jFrameInputLayerProperties.jTextFieldName.setText("");
                jFrameInputLayerProperties.jTextFieldName.setBackground(Color.white);
                jFrameInputLayerProperties.jTextFieldName.setVisible(true);
                jFrameInputLayerProperties.jButtonUsedName.setVisible(true);
                jFrameInputLayerProperties.jComboBox[0].setVisible(false);
                if(jFrameInputLayerProperties.jComboBox[0].getItemCount() > 1) {
                    jFrameInputLayerProperties.jButton[1].setText("Back");
                    jFrameInputLayerProperties.jButton[1].setEnabled(true);
                    jFrameInputLayerProperties.jButton[1].setVisible(true);
                    jFrameInputLayerProperties.jButtonUsedName.setEnabled(true);
                }
                else
                    jFrameInputLayerProperties.jButtonUsedName.setEnabled(false);
                jFrameInputLayerProperties.jButton[2].setText("OK");
                break; 
            case "OK":
                if(jFrameInputLayerProperties.jTextFieldName.isVisible()) {
                    if(existsLayerPropertiesName()) {
                        jFrameInputLayerProperties.jLabelMessage[0].setVisible(true);
                        jFrameInputLayerProperties.jTextFieldName.setBackground(Color.yellow);
                        jFrameInputLayerProperties.jButton[2].setEnabled(false);
                    }
                    else {
                        if(jFrameInputLayerProperties.jTextFieldName.getText().equals("") || jFrameInputLayerProperties.jTextFieldName.getText().equals("Insert name")) {
                            jFrameInputLayerProperties.jTextFieldName.setText("Insert name");
                            jFrameInputLayerProperties.jTextFieldName.setBackground(Color.yellow);
                            //jFrameInputLayerProperties.jButton[0].setEnabled(false);
                            jFrameInputLayerProperties.jButton[2].setEnabled(false);
                        }
                        else {
                            if(laminateGeoPropSelected() && materialTypeSelected() && materialNameSelected()) {
                                addLayerProperties();
                                jFrameInputLayerProperties.jTextFieldName.setVisible(false);
                                jFrameInputLayerProperties.jButtonUsedName.setVisible(false);
                                jFrameInputLayerProperties.jComboBox[0].setVisible(true);
                                jFrameInputLayerProperties.jButton[2].setText("New");
                            }
                        }
                    }
                }
                else {
                    if(laminateGeoPropSelected() && materialTypeSelected() && materialNameSelected()) 
                        updateLayerProperties();
                }
                break;
            case "Reset":
                resetLayerProperties();
                break;
            case "Back":
                jFrameInputLayerProperties.jTextFieldName.setVisible(false);
                jFrameInputLayerProperties.jButtonUsedName.setVisible(false);
                jFrameInputLayerProperties.jComboBox[0].setVisible(true);
                jFrameInputLayerProperties.jComboBox[0].setSelectedIndex(0);
                break;
            case "Delete":
                deleteLayerProperties();
                break;
            case "Next":
                jFrameInputLayerProperties.dispose();
                JFrameInputLayerPatterns jFrameInputLayerPatterns = new JFrameInputLayerPatterns(jFrameInputLayerProperties.jFrameDocrosData, jFrameInputLayerProperties.getLocation());
                break;
            case "Previous":
                jFrameInputLayerProperties.dispose();
                JFrameInputLaminateGeoProp jFrameInputLaminateGeoProp = new JFrameInputLaminateGeoProp(jFrameInputLayerProperties.jFrameDocrosData, jFrameInputLayerProperties.getLocation());
                break;
            case "Close":
                jFrameInputLayerProperties.dispose();
                break;
        }
    
    }
    
    void deleteLayerProperties() {
        //int i;
        String name = String.valueOf(jFrameInputLayerProperties.jComboBox[0].getSelectedItem());
        jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.delLayerProperties(name);
        //jFrameInputLayerProperties.jFrameDocrosData.changedLists[jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesListIndex] = true;
        //jFrameInputLayerProperties.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        jFrameInputLayerProperties.jComboBox[0].removeItem(name);
        jFrameInputLayerProperties.jComboBox[0].setSelectedIndex(0);
        jFrameInputLayerProperties.jButton[0].setVisible(false);
        if(jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList == null || jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.first == null) {
            jFrameInputLayerProperties.jButtonNext.setEnabled(false);
            jFrameInputLayerProperties.jTextFieldName.setText("");
            jFrameInputLayerProperties.jTextFieldName.setBackground(Color.white);
            jFrameInputLayerProperties.jTextFieldName.setVisible(true);
            jFrameInputLayerProperties.jButtonUsedName.setVisible(true);
            jFrameInputLayerProperties.jComboBox[0].setVisible(false);
            if(jFrameInputLayerProperties.jComboBox[0].getItemCount() > 1) {
                jFrameInputLayerProperties.jButton[1].setText("Back");
                jFrameInputLayerProperties.jButton[1].setEnabled(true);
                jFrameInputLayerProperties.jButton[1].setVisible(true);
            }
            else
                jFrameInputLayerProperties.jButtonUsedName.setEnabled(false);
            jFrameInputLayerProperties.jButton[2].setText("OK");
            jFrameInputLayerProperties.jFrameDocrosData.jMenuItemEdit[4].setEnabled(false);
        }
        //jFrameInputLayerProperties.jFrameDocrosData.changedLists[jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesListIndex] = true;
        jFrameInputLayerProperties.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputLayerProperties.jFrameDocrosData.dataFile != null)
            jFrameInputLayerProperties.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }

        
    void resetLayerProperties() {
        
        LayerProperties node = jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.getLayerProperties(String.valueOf(jFrameInputLayerProperties.jComboBox[0].getSelectedItem()));
        jFrameInputLayerProperties.jComboBox[1].setSelectedItem(node.geoProp.name);
        jFrameInputLayerProperties.jComboBox[2].setSelectedItem(node.materialModel.type);
        jFrameInputLayerProperties.jComboBox[3].setSelectedItem(node.materialModel.name);
    }
    
    void updateLayerProperties() {
        //int i;
        LayerProperties node = jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.getLayerProperties(String.valueOf(jFrameInputLayerProperties.jComboBox[0].getSelectedItem()));
        LaminateGeoProp laminateGeoProp = jFrameInputLayerProperties.jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(String.valueOf(jFrameInputLayerProperties.jComboBox[1].getSelectedItem()));
        NonLinearMaterialModel materialModel = jFrameInputLayerProperties.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jFrameInputLayerProperties.jComboBox[2].getSelectedItem()), String.valueOf(jFrameInputLayerProperties.jComboBox[3].getSelectedItem()));
        node.geoProp = laminateGeoProp;
        node.materialModel = materialModel;
        jFrameInputLayerProperties.jComboBox[0].setSelectedIndex(0);
        jFrameInputLayerProperties.jButton[0].setVisible(false);
        jFrameInputLayerProperties.jButton[1].setVisible(false);
        jFrameInputLayerProperties.jButton[2].setText("New");
        //jFrameInputLayerProperties.jFrameDocrosData.changedLists[jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesListIndex] = true;
        jFrameInputLayerProperties.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputLayerProperties.jFrameDocrosData.dataFile != null)
            jFrameInputLayerProperties.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void addLayerProperties() {
        
        int i;
        //LayerPropertiesList list = ;
        LayerProperties node = new LayerProperties(jFrameInputLayerProperties.jTextFieldName.getText());
        LaminateGeoProp laminateGeoProp = jFrameInputLayerProperties.jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(String.valueOf(jFrameInputLayerProperties.jComboBox[1].getSelectedItem()));
        NonLinearMaterialModel materialModel = jFrameInputLayerProperties.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jFrameInputLayerProperties.jComboBox[2].getSelectedItem()), String.valueOf(jFrameInputLayerProperties.jComboBox[3].getSelectedItem()));
        node.geoProp = laminateGeoProp;
        node.materialModel = materialModel;
        if(jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList == null)
            jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList = new LayerPropertiesList();
        jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.addLayerProperties(node);
        jFrameInputLayerProperties.jComboBox[0].addItem(node.name);
        jFrameInputLayerProperties.jButtonNext.setEnabled(true);
        jFrameInputLayerProperties.jComboBox[0].setSelectedIndex(0);
        jFrameInputLayerProperties.jFrameDocrosData.jMenuItemEdit[4].setEnabled(true);
        
        //jFrameInputLayerProperties.jFrameDocrosData.changedLists[jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesListIndex] = true;
        jFrameInputLayerProperties.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputLayerProperties.jFrameDocrosData.dataFile != null)
            jFrameInputLayerProperties.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
 
    Boolean laminateGeoPropSelected() {
        
        if(jFrameInputLayerProperties.jComboBox[1].getSelectedIndex() == 0) {
            jFrameInputLayerProperties.jLabelMessage[1].setVisible(true);
            jFrameInputLayerProperties.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean materialTypeSelected() {
           
        if(jFrameInputLayerProperties.jComboBox[2].getSelectedIndex() == 0) {
            jFrameInputLayerProperties.jLabelMessage[2].setVisible(true);
            jFrameInputLayerProperties.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean materialNameSelected() {
        
        if(jFrameInputLayerProperties.jComboBox[3].getSelectedIndex() == 0) {
            jFrameInputLayerProperties.jLabelMessage[3].setVisible(true);
            jFrameInputLayerProperties.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean existsLayerPropertiesName() {
        
        LayerPropertiesList list = jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList;
        if(list == null)
            return false;
        LayerProperties node = list.first;
        while(node != null) {
            if(node.name.equals(jFrameInputLayerProperties.jTextFieldName.getText()))
                return true;
            node = node.next;
        }
        return false;
    }
}
