
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class JFrameInputLaminateGeoProp extends JFrame {
    
    JFrameDocrosData jFrameDocrosData;
    Point location;
    JLabel jLabel[];
    JLabel jLabelMessage;
    JTextField jTextField[];
    JComboBox jComboBoxName;
    JButton jButton[];
    JButton jButtonNext, jButtonPrev, jButtonClose, jButtonUsedName;
    JButton jButtonHelp;
    String jLabelText[] = {"Geometry Name", "Thickness", "Width"};
    String jButtonText[] = {"Delete", "Reset", "OK"};
    
    public JFrameInputLaminateGeoProp(JFrameDocrosData jfdd, Point loc) {
        
        super("<LAMINATEGEOPROP>");
        jFrameDocrosData = jfdd;
        location = loc;
        init();
    }
    
    private void init() {
     
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        //this.setAlwaysOnTop(true);             
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, jLabelWidth, xPosition = 10, yPosition = 10;
                
        jLabelWidth = jFrameDocrosData.charWidth * JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText);
        
        JLabel jLabelTitle = new JLabel("Layer Geometry Properties");
        jLabelTitle.setSize(jFrameDocrosData.charWidth * (jLabelTitle.getText().length() + 2), jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(jLabelWidth + xPosition, yPosition);
        this.add(jLabelTitle);
        
        if(getClass().getResourceAsStream("/help/helpLaminateGeoProp.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("LaminateGeoProp"));//, "Laminate Geo Prop"));
            this.add(jButtonHelp);
        }
        yPosition += 30;
                
        jLabel = new JLabel[3];
        jTextField = new JTextField[3];
                
        for(i=0; i<3; i++) {
            if(jFrameDocrosData.unitsInputOutputData == null || i == 0)
                jLabel[i] = new JLabel(jLabelText[i]);
            else
                jLabel[i] = new JLabel(jLabelText[i] + " (" + jFrameDocrosData.unitsInputOutputData[1] + ")");
            jLabel[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
            jLabel[i].setLocation(xPosition, yPosition);
            this.add(jLabel[i]);
            jTextField[i] = new JTextField();
            if(i == 0) {
                jTextField[i].setSize(jLabelTitle.getWidth() - 64, jLabelTitle.getHeight());
                jButtonUsedName = new JButton("Used");
                jButtonUsedName.setSize(63, 26);
                jButtonUsedName.setLocation(xPosition + jLabelWidth + jLabelTitle.getWidth() - 64, yPosition);
                jButtonUsedName.addActionListener(new JButtonInputLaminateGeoPropActioListener(this));
                this.add(jButtonUsedName);
            }
            else
                jTextField[i].setSize(jLabelTitle.getSize());
            jTextField[i].setLocation(xPosition + jLabelWidth, yPosition);
           
            yPosition += 30;
        }
        
        jLabelMessage = new JLabel("This name is already used");
        jLabelMessage.setSize(jTextField[0].getSize());
        jLabelMessage.setLocation(jTextField[0].getX(), yPosition);
        jLabelMessage.setForeground(Color.red);
        jLabelMessage.setVisible(false);
        this.add(jLabelMessage);
        
        jComboBoxName = new JComboBox();
        jComboBoxName.setSize(jLabelTitle.getSize());
        jComboBoxName.setLocation(jTextField[0].getLocation());
        jComboBoxName.addItem("Select");
        jComboBoxName.setSelectedIndex(0);
        if(jFrameDocrosData.laminateGeoPropList != null && jFrameDocrosData.laminateGeoPropList.first != null) {
            LaminateGeoProp node = jFrameDocrosData.laminateGeoPropList.first;
            while(node != null) {
                jComboBoxName.addItem(node.name);
                node = node.next;
            }
            jTextField[0].setVisible(false);
            for(i=1; i<3; i++) {
                jLabel[i].setVisible(false);
                jTextField[i].setVisible(false);
                jButtonUsedName.setVisible(false);
            }    
        }
        else {
            jComboBoxName.setVisible(false);
            jButtonUsedName.setEnabled(false);
        }
        jComboBoxName.addActionListener(new JComboBoxInputLaminateGeoProp(this));
        this.add(jComboBoxName);
        
        yPosition += 30;
        
        jButton = new JButton[3];
        
        for(i=0; i<3; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jComboBoxName.getWidth()) / 3, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * jButton[i].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputLaminateGeoPropActioListener(this));
            this.add(jButton[i]);
        }
        jButton[0].setVisible(false);
        jButton[1].setVisible(false);
        if(jFrameDocrosData.laminateGeoPropList != null)
            jButton[2].setText("New");
        
        for(i=0; i<3; i++) {
            if(i == 0) {
                jTextField[i].setName("NameLaminateGeoProp");
                jTextField[i].addFocusListener(new JTextFieldNameFocusListener(null, 0, jLabelMessage, jButton[2]));
            }
            else {
                jTextField[i].setName("DoublePositive");
                jTextField[i].addFocusListener(new JTextFieldFocusListener(jButton, null, 0));
            }
            jTextField[i].addKeyListener(new JTextFieldKeyListener());    
            this.add(jTextField[i]);
        }
        
        yPosition += jFrameDocrosData.charHeight + 10;
        
        jButtonPrev = new JButton("Previous");
        jButtonPrev.setSize((jLabelWidth + jComboBoxName.getWidth()) / 3, 18);
        jButtonPrev.setLocation(xPosition, yPosition);
        jButtonPrev.addActionListener(new JButtonInputLaminateGeoPropActioListener(this));
        this.add(jButtonPrev);
        
        jButtonClose = new JButton("Close");
        jButtonClose.setSize((jLabelWidth + jComboBoxName.getWidth()) / 3, 18);
        jButtonClose.setLocation(xPosition + jButtonPrev.getWidth(), yPosition);
        jButtonClose.addActionListener(new JButtonInputLaminateGeoPropActioListener(this));
        this.add(jButtonClose);
        
        jButtonNext = new JButton("Next");
        jButtonNext.setSize((jLabelWidth + jComboBoxName.getWidth()) / 3, 18);
        jButtonNext.setLocation(xPosition + 2 * jButtonPrev.getWidth(), yPosition);
        jButtonNext.addActionListener(new JButtonInputLaminateGeoPropActioListener(this));
        this.add(jButtonNext);
        if(jFrameDocrosData.laminateGeoPropList == null || jFrameDocrosData.laminateGeoPropList.first == null || !jFrameDocrosData.nonLinearMaterialModelTypeList.existsMaterialModel())
            jButtonNext.setEnabled(false);
        
        yPosition += 2 * jFrameDocrosData.charHeight;
        
        this.setSize(jLabelWidth + jLabelTitle.getWidth() + 26, yPosition);
        this.setVisible(true);
        
    }
    
}
