/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package docrosdata;

/**
 *
 * @author Moi
 */
public class LayerPropertiesList {

    LayerProperties first;
    LayerProperties last;
    int count;
    
    public LayerPropertiesList() {

        first = null;
        last = null;
        count = 0;
    }
    
    void addLayerProperties(LayerProperties node) {
        
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
    
     void delLayerProperties() {
        
        LayerProperties node1, node2;
        node1 = first;
        node2 = first.next;
        if(node2 == null) {
            first = null;
            last = null;
        }
        else {
            while(node2.next != null) {
                node1 = node2;
                node2 = node2.next;
            }           
            node1.next = null;
            last = node1;
        }
        count--;
    }

    void delLayerProperties(String n) {
        
        LayerProperties node1, node2;
        node1 = first;
        node2 = first.next;
        if(node1.name.equals(n)) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && !node2.name.equals(n)) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
    }
     
    LayerProperties getLayerProperties(String n) {
        
        LayerProperties temp;
        temp = first;
        while(temp != null && !temp.name.equals(n))
            temp = temp.next;
        if(temp == null)
            System.out.println("Layer Properties "+n+" doesn't exist");
        return temp;
        
    }
    
    int maxNumberCharacterName() {
        
        int max = 0;
        LayerProperties layer;
        layer = first;
        while(layer != null) {
            if(layer.name.length() > max)
                max = layer.name.length();
            layer = layer.next;
        }
        return max;
    }
    
    Boolean existsLayerPropertiesName(String name) {
        
        LayerProperties temp;
        temp = first;
        while(temp != null) {
            if(temp.name.equals(name))
                return true;
            temp = temp.next;
        }
        return false;
    }
    
    void printLayerPropertiesList() {
        System.out.println("LayerPropertiesList:"+'\n');
        LayerProperties temp;
        temp = first;
        while(temp != null) {
            System.out.println("Layer Name: "+temp.name);
            System.out.println("Material Type: "+temp.materialModel.type);
            System.out.println("Material Name: "+temp.materialModel.name);
            System.out.println("GeoProp Name: "+temp.geoProp.name+'\n');
            System.out.println("CountLayerPatterns: "+temp.countLayerPatterns);
            temp = temp.next;
        }
    }
    
}
