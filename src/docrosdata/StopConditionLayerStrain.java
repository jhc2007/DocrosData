
package docrosdata;

public class StopConditionLayerStrain {
    
    String name;
    int layerNumber;
    String layerPatternsName;
    double strain;
    StopConditionLayerStrain next;
    
    public StopConditionLayerStrain(String nam, int layer, String lpn, double str) {
        
        name = nam;
        layerNumber = layer;
        layerPatternsName = lpn;
        strain = str;
        next = null;
    }
}
