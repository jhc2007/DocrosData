
package docrosdata;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

class JButtonCrossSectionGraphicsActionListener implements ActionListener {
    
    JFrameCrossSectionGraphics jFrameCrossSectionGraphics;

    public JButtonCrossSectionGraphicsActionListener(JFrameCrossSectionGraphics jf) {
        jFrameCrossSectionGraphics = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
                
        switch(ae.getActionCommand()) {
            case "Open":
                String xLabel = removeHtmlCode(jFrameCrossSectionGraphics.jComboBox[0].getSelectedItem().toString());
                String yLabel = removeHtmlCode(jFrameCrossSectionGraphics.jComboBox[1].getSelectedItem().toString());
                createCrossSectionFile(xLabel, yLabel);
                break;
            case "Close":
                jFrameCrossSectionGraphics.dispose();
                break;
        }        
    }
    
    String removeHtmlCode(String code) {

        code = code.replaceAll("html>", "");
        code = code.replaceAll("u>", "");
        code = code.replaceAll("<", "");
        code = code.replaceAll("/", "");
        
        return code;
        
    }
    
    void createCrossSectionFile(String xAxis, String yAxis) {
        
        BufferedWriter bufferedWriter;
        File tempDataFile;
        
        tempDataFile = new File(jFrameCrossSectionGraphics.jFrameDocrosData.dataFile.getPath().substring(0, jFrameCrossSectionGraphics.jFrameDocrosData.dataFile.getPath().length() - jFrameCrossSectionGraphics.jFrameDocrosData.dataFile.getName().length()) + "tempCrossSection.txt");
        
        if(!tempDataFile.exists()) {
            try {
                tempDataFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(JButtonCrossSectionGraphicsActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
            tempDataFile.deleteOnExit();    
        }
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(tempDataFile.getAbsoluteFile()));
            bufferedWriter.write(jFrameCrossSectionGraphics.jFrameDocrosData.dataFile.getName().substring(0, jFrameCrossSectionGraphics.jFrameDocrosData.dataFile.getName().length() - 6));
            bufferedWriter.newLine();
            bufferedWriter.write(xAxis);
            bufferedWriter.newLine();
            bufferedWriter.write(getUnit(xAxis));
            bufferedWriter.newLine();
            bufferedWriter.write(yAxis);
            bufferedWriter.newLine();
            bufferedWriter.write(getUnit(yAxis));
            bufferedWriter.newLine();
            bufferedWriter.close();           
        } catch (IOException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        BufferedInputStream input;
        FileOutputStream output;
        File tempCreateFile;
        byte[] buffer = new byte[256];
        int bytesRead;            
        
        try {   
            tempCreateFile = new File(jFrameCrossSectionGraphics.jFrameDocrosData.dataFile.getPath().substring(0, jFrameCrossSectionGraphics.jFrameDocrosData.dataFile.getPath().length() - jFrameCrossSectionGraphics.jFrameDocrosData.dataFile.getName().length()) + "tempCreateCrossSectionGraphic.xlsm");
            if(!tempCreateFile.exists()) {
                tempCreateFile.createNewFile();
                tempCreateFile.deleteOnExit(); 
                input = new BufferedInputStream(getClass().getResourceAsStream("/graphics/createCrossSectionGraphic.xlsm"));            
                try {
                    output = new FileOutputStream(tempCreateFile);
                    while((bytesRead = input.read(buffer)) != -1)
                        output.write(buffer, 0, bytesRead);
                    output.close();
                }
                catch (IOException e) {
                    Logger.getLogger(JButtonHelpActionListener.class.getName()).log(Level.SEVERE, null, e);
                }
                input.close();
            }
            Desktop.getDesktop().open(tempCreateFile);

        } catch (IOException ex) {
            Logger.getLogger(JButtonHelpActionListener.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    String getUnit(String axis) {
        
        switch(axis.substring(0, 3)) {
            case "Cur":
                return " (1/" + jFrameCrossSectionGraphics.jFrameDocrosData.unitsInputOutputData[1] + ")";
                //break;
            case "Mom":
                return " (" + jFrameCrossSectionGraphics.jFrameDocrosData.unitsInputOutputData[0] + "." + jFrameCrossSectionGraphics.jFrameDocrosData.unitsInputOutputData[1] + ")";
                //break;
            default:
                return " (" + jFrameCrossSectionGraphics.jFrameDocrosData.unitsInputOutputData[1] + ")";
                //break;
        }
    }
}
