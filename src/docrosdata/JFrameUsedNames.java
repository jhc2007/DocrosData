
package docrosdata;

import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class JFrameUsedNames extends JFrame {
    
    String title;
    String usedNames[];
    Point location;
    
    public JFrameUsedNames(String tit, String names[], Point loc) {
    
        super("USED");
        title = tit;
        usedNames = names;
        location = loc;
        init();
    }
    
    private void init() {
        
        int i, jLabelWidth, yPosition = 8;
        JLabel jLabelTitle, jLabelName[];
        JButton jButtonOK;
        Font font;
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        this.setResizable(false);
        this.setLayout(null);
        
        jLabelWidth = getMaxWidth(usedNames);
        if(title.length() * 9 > jLabelWidth)
            jLabelWidth = title.length() * 9;

        jLabelTitle = new JLabel(title, SwingConstants.CENTER);
        font = jLabelTitle.getFont();
        jLabelTitle.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        jLabelTitle.setBackground(Color.yellow);
        jLabelTitle.setSize(jLabelWidth, 20);
        jLabelTitle.setLocation(0, yPosition);
        this.add(jLabelTitle);
        yPosition += 30;

        jLabelName = new JLabel[usedNames.length];
        for(i=0; i<usedNames.length; i++) {
            jLabelName[i] = new JLabel(usedNames[i], SwingConstants.CENTER);
            jLabelName[i].setFont(font.deriveFont(font.getStyle() & ~Font.BOLD));
            jLabelName[i].setSize(jLabelWidth, 20);
            jLabelName[i].setLocation(0, yPosition);
            this.add(jLabelName[i]);
            yPosition += 20;
        }
        yPosition += 15;
        
        this.setSize(jLabelWidth + 10, (usedNames.length + 5) * 20 + 10);
        
        jButtonOK = new JButton("OK");
        jButtonOK.setSize(60, 20);
        jButtonOK.setLocation((jLabelWidth - jButtonOK.getWidth()) / 2, yPosition);
        jButtonOK.addActionListener(new JButtonOKActionListener(this));
        this.add(jButtonOK);
        
        this.setVisible(true);
    }
    
    int getMaxWidth(String names[]) {
        
        int i, max = 0;
        for(i=0; i<names.length; i++) {
            if(names[i].length() > max)
                max = names[i].length(); 
        }
        return max * 9;
    }

    private static class JButtonOKActionListener implements ActionListener {

        JFrameUsedNames jFrameUsedNames;
        
        public JButtonOKActionListener(JFrameUsedNames jf) {
            jFrameUsedNames = jf;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            jFrameUsedNames.dispose();
        }
    }
}
