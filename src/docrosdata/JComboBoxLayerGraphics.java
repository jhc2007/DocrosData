
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class JComboBoxLayerGraphics implements ActionListener {
    
    JFrameLayerGraphics jFrameStressStrainGraphics;

    public JComboBoxLayerGraphics(JFrameLayerGraphics jf) {
        
        jFrameStressStrainGraphics = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(((JComboBox) ae.getSource()).getName() == null) {
            if(jFrameStressStrainGraphics.jComboBox[1].getSelectedIndex() > 0)
                jFrameStressStrainGraphics.jButton[0].setEnabled(true);
            else
                jFrameStressStrainGraphics.jButton[0].setEnabled(false);
        }
        else {//JComboBox[0] Pattern Name
            int i, firstLayer, lastLayer;
            String patternName;
            jFrameStressStrainGraphics.jComboBox[1].removeAllItems();
            jFrameStressStrainGraphics.jComboBox[1].addItem("Select");
            jFrameStressStrainGraphics.jComboBox[1].setSelectedIndex(0);
            if(jFrameStressStrainGraphics.jComboBox[0].getSelectedIndex() > 0) {                   
                patternName = String.valueOf(jFrameStressStrainGraphics.jComboBox[0].getSelectedItem());
                firstLayer = jFrameStressStrainGraphics.jFrameDocrosData.layerPatternsList.getFirstLayer(patternName);     
                lastLayer = jFrameStressStrainGraphics.jFrameDocrosData.layerPatternsList.getLastLayer(patternName);
                for(i=firstLayer; i<=lastLayer; i++)
                    jFrameStressStrainGraphics.jComboBox[1].addItem(i);
            }
        }
    }
    
}
