
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

public class JPopupMenuItemActionListener implements ActionListener {

        JPanelLayerPatterns jPanelLayerPatterns;
        JPopupMenu jPopupMenu;
        JFrameDocrosData jFrameDocrosData;
        JPanel jPanelMainProperties;
    
    public JPopupMenuItemActionListener(JPanelLayerPatterns jplp, JPopupMenu jpm, JFrameDocrosData jfdd, JPanel jpmp) {
 
        jPanelLayerPatterns = jplp;
        jPopupMenu = jpm;
        jFrameDocrosData = jfdd;
        jPanelMainProperties = jpmp;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
                
        jPanelMainProperties.setVisible(false);
        jPopupMenu.setVisible(false);
        switch(ae.getActionCommand()) {
            case "Material":
                createJFrameMaterial();
                break;
            case "Geometry":
                createJFrameGeometry();
                break;
        }    
    }
            
    void createJFrameMaterial() {

        int i, jFrameMaterialWidth = 20, yPosition = 6, xPosition, jComboBoxWidth;
        
        String jLabelWestText[] = {"Layer Properties Name", "Material Type", "Material Name"};
        String jButtonLabel[] = {"Show Parameters", "View", "OK", "Close"};
        
        JFrame jFrameMaterial;       
        JLabel jLabelLayerName;
        JLabel jLabelWest[];
        JComboBox jComboBoxLawType, jComboBoxLawName;
        JButton jButtonMaterial[];
        
        jFrameMaterial = new JFrame("Material > " + jPanelLayerPatterns.layerPatterns.name/* + " > " + jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].name*/);
        jFrameMaterialWidth += jFrameDocrosData.charWidth * jFrameMaterial.getTitle().length();
        jFrameMaterial.setLocation(15, 45);
        jFrameMaterial.setResizable(false);
        jFrameMaterial.getContentPane().setLayout(null);

        xPosition = 10 + jFrameDocrosData.charWidth * maxNumberCharacterStringArray(jLabelWestText);        
        
        JLabel jLabelNorth = new JLabel("Material Properties");
        jLabelNorth.setSize(jFrameDocrosData.charWidth * jLabelNorth.getText().length(), jFrameDocrosData.charHeight);
        jLabelNorth.setLocation(xPosition, yPosition);
        jFrameMaterial.add(jLabelNorth);
        
        if(xPosition + jLabelNorth.getWidth() > jFrameMaterialWidth) 
            jFrameMaterialWidth = xPosition + jLabelNorth.getWidth();
     
        //yPosition += jFrameDocrosData.charHeight;
        yPosition += 25;
                        
        jLabelWest = new JLabel[jLabelWestText.length];  
        
        for(i=0; i<jLabelWest.length; i++) {
            jLabelWest[i] = new JLabel(jLabelWestText[i]);
            jLabelWest[i].setSize(xPosition - 10, jFrameDocrosData.charHeight);
            jLabelWest[i].setLocation(12, yPosition);
            jFrameMaterial.add(jLabelWest[i]);
            yPosition += 25;
        }
        
        //yPosition = 10 + jFrameDocrosData.charHeight;
        //yPosition = 33;
        yPosition = jLabelWest[0].getY();
                
        jLabelLayerName = new JLabel(jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].name);
        if(jLabelNorth.getText().length() > jLabelLayerName.getText().length()) 
            jLabelLayerName.setSize(jLabelNorth.getWidth(), jFrameDocrosData.charHeight);
        else {
            jLabelLayerName.setSize(jFrameDocrosData.charWidth * jLabelLayerName.getText().length(), jFrameDocrosData.charHeight);
            jFrameMaterialWidth = xPosition + jLabelLayerName.getWidth();
        }
        jLabelLayerName.setLocation(xPosition, yPosition);
        jFrameMaterial.add(jLabelLayerName);

        //yPosition += jFrameDocrosData.charHeight;
        yPosition += 25;
        
        jComboBoxLawType = new JComboBox();
        jComboBoxLawName = new JComboBox();
        
        jComboBoxWidth = 40 + jFrameDocrosData.charWidth * maxInt(jFrameDocrosData.nonLinearMaterialModelTypeList.maxNumberCharacterTypeName(), jFrameDocrosData.nonLinearMaterialModelTypeList.maxNumberCharacterModelName());
        
        if(jLabelLayerName.getWidth() > jComboBoxWidth)
            jComboBoxWidth = jLabelLayerName.getWidth();
        else
            jFrameMaterialWidth = xPosition + jComboBoxWidth;
        
        String strModelType;
        strModelType = jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].materialModel.type;
    
        NonLinearMaterialModelType type;
        type = jFrameDocrosData.nonLinearMaterialModelTypeList.first;
        
        while(type != null) {
            jComboBoxLawType.addItem(type.name.substring(1));            
            type = type.next;
        }
        
        jComboBoxLawType.setSelectedItem(strModelType);
        jComboBoxLawType.setSize(jComboBoxWidth, jFrameDocrosData.charHeight);
        jComboBoxLawType.setLocation(xPosition, yPosition);
        jFrameMaterial.add(jComboBoxLawType);
        
        //yPosition += jFrameDocrosData.charHeight;
        yPosition += 25;
        
        NonLinearMaterialModel model;
        model = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + strModelType).first;
  
        String modelName;
        modelName = jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].materialModel.name; 
        
        while(model != null) {
            jComboBoxLawName.addItem(model.name);
            model = model.next;
        }
        
        jComboBoxLawName.setSelectedItem(modelName);     
        //jComboBoxLawType.addItemListener(new JComboBoxLawTypeItemListener(jFrameDocrosData, jPanelLayerPatterns, jComboBoxLawName));
        jComboBoxLawName.setSize(jComboBoxWidth, jFrameDocrosData.charHeight);
        jComboBoxLawName.setLocation(xPosition, yPosition);
       
        //jLabelParameters = new JLabel[4];
        
        
        //jComboBoxLawName.addActionListener(new JComboBoxLawNameActionListener(jFrameDocrosData, jComboBoxLawType, jLabelParameters, jTextFieldParameters));
        
        //System.out.println("Parameters: " + jLabelParameters.length);
        
        //yPosition += 2 * jFrameDocrosData.charHeight;
        yPosition += 35;
        
        jFrameMaterial.add(jComboBoxLawName);
        
        jButtonMaterial = new JButton[jButtonLabel.length];
        
        jButtonMaterial[0] = new JButton(jButtonLabel[0]);
        jButtonMaterial[0].setSize(xPosition + jComboBoxWidth - 10, 18);
        jButtonMaterial[0].setLocation(10, yPosition);
        
        jComboBoxLawType.addActionListener(new JComboBoxLawTypeActionListener(jFrameDocrosData, jPanelLayerPatterns, jComboBoxLawName, jButtonMaterial));
        
        //yPosition += jFrameDocrosData.charHeight + 5;   
        yPosition += 24;   
        
        jFrameMaterial.add(jButtonMaterial[0]);   
        
        for(i=1; i<jButtonLabel.length; i++) {
            
            jButtonMaterial[i] = new JButton(jButtonLabel[i]);
            jButtonMaterial[i].setSize(jButtonMaterial[0].getWidth() / 3, 25);
            jButtonMaterial[i].setLocation(10 + (i - 1) * jButtonMaterial[i].getWidth(), yPosition);
            jButtonMaterial[i].addActionListener(new JButtonMaterialActionListener(jFrameDocrosData, jPanelLayerPatterns, jFrameMaterial, jComboBoxLawType, jComboBoxLawName, jButtonMaterial, jLabelLayerName));
            jFrameMaterial.add(jButtonMaterial[i]);                                      
        }
        //System.out.println("NumComp: " + jFrameMaterial.getComponentCount());    
        yPosition += jFrameDocrosData.charHeight;
        //yPosition += 22;
        
        jFrameMaterial.setSize(jFrameMaterialWidth + 17, yPosition + 36);
        
        jButtonMaterial[0].addActionListener(new JButtonShowParametersActionListener(jFrameDocrosData, jPanelLayerPatterns, jFrameMaterial, jComboBoxLawType, jComboBoxLawName, jButtonMaterial, jLabelLayerName)); //, jFrameMaterial.getWidth(), jFrameMaterial.getHeight()));       
        jFrameMaterial.setVisible(true);
        
    }
    
    void createJFrameGeometry() {
        
        int i, jFrameGeometryWidth = 20, xPosition = 10, yPosition = 10, textFieldCenterWidth;
        
        JFrame jFrameGeometry;
        JLabel jLabelNorth, jLabelWest[];
        JTextField jTextFieldCenter[];
        JButton jButtonGeometry[];
        
        String jLabelWestText[] = {"Name", "Thickness", "Width"};
        String jButtonLabel[] = {"View", "OK", "Close"};
        
        jFrameGeometry = new JFrame("Geometry > " + jPanelLayerPatterns.layerPatterns.name /* + " > " + jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].name*/ );
        
        jFrameGeometryWidth += jFrameDocrosData.charWidth * jFrameGeometry.getTitle().length();
        
        jFrameGeometry.setLocation(15, 45);
        //jFrameGeometry.setAlwaysOnTop(true);
        jFrameGeometry.setResizable(false);
        //jFrameGeometry.setLayout(new GridLayout(4, 2));
        jFrameGeometry.getContentPane().setLayout(null);
        
        jLabelNorth = new JLabel("Geometry Properties");
        
        jLabelWest = new JLabel[jLabelWestText.length];
        
        jTextFieldCenter = new JTextField[jLabelWestText.length];
               
        jButtonGeometry = new JButton[jButtonLabel.length];
        
        xPosition += jFrameDocrosData.charWidth * maxNumberCharacterStringArray(jLabelWestText);
        
        jLabelNorth.setSize(jFrameDocrosData.charWidth * jLabelNorth.getText().length(), jFrameDocrosData.charHeight);
        jLabelNorth.setLocation(xPosition, yPosition);
        jFrameGeometry.add(jLabelNorth);
        
        yPosition += jFrameDocrosData.charHeight;
        
        textFieldCenterWidth = maxInt(jFrameDocrosData.laminateGeoPropList.maxNumberCharacterProperty(), jLabelNorth.getWidth());

        jFrameGeometryWidth = maxInt(jFrameGeometryWidth, xPosition + textFieldCenterWidth + 10);
        
        for(i=0; i<jLabelWest.length; i++) {
            
            jLabelWest[i] = new JLabel(jLabelWestText[i]);
            jLabelWest[i].setSize(xPosition - 10, jFrameDocrosData.charHeight);
            jLabelWest[i].setLocation(10, yPosition);
            jFrameGeometry.add(jLabelWest[i]);     
               
            jTextFieldCenter[i] = new JTextField(jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].geoProp.getPropertyValue(jLabelWestText[i]));
            //jTextFieldCenter[i].setName(jLabelWestText[i]);//.toLowerCase());
            jTextFieldCenter[i].setSize(textFieldCenterWidth, jFrameDocrosData.charHeight);
            jTextFieldCenter[i].setLocation(xPosition, yPosition);
            if(i == 0)
                jTextFieldCenter[i].addFocusListener(new JTextFieldNameFocusListener(null, 0, null, jButtonGeometry[1]));
            else//(i != 0)
                jTextFieldCenter[i].addFocusListener(new JTextFieldFocusListener(jButtonGeometry, null, 0));
            jTextFieldCenter[i].addKeyListener(new JTextFieldKeyListener());
            jFrameGeometry.add(jTextFieldCenter[i]);
        
            yPosition += jFrameDocrosData.charHeight;
        
        }
        
        yPosition += jFrameDocrosData.charHeight;
        
        for(i=0; i<jButtonLabel.length; i++) {
        
            jButtonGeometry[i] = new JButton(jButtonLabel[i]);
            jButtonGeometry[i].setSize((xPosition - 10 + textFieldCenterWidth) / 3, jFrameDocrosData.charHeight);
            jButtonGeometry[i].setLocation(10 + i * jButtonGeometry[i].getWidth(), yPosition);
            jButtonGeometry[i].addActionListener(new JButtonGeometryActionListener(jFrameDocrosData, jPanelLayerPatterns, jFrameGeometry, jTextFieldCenter, jButtonGeometry));
            jFrameGeometry.add(jButtonGeometry[i]);
        }
        yPosition += jFrameDocrosData.charHeight;
        
        jFrameGeometry.setSize(jFrameGeometryWidth + 7, yPosition + 38);
        jFrameGeometry.setVisible(true);
    }
    
    public static int maxNumberCharacterStringArray(String array[]) {
 
        int i, max = 0;
        for(i=0; i<array.length; i++)
            if(array[i].length() > max)
                max = array[i].length();
        return max;
    }
    
    int maxInt(int a, int b) {
        //System.out.println("A: " + a);
        //System.out.println("B: " + b);
        if(a > b)
            return a;
        return b;
    }

    private static class jButtonCloseActionListener implements ActionListener {

        JFrame jFrameStrainStress;
        
        public jButtonCloseActionListener(JFrame jf) {
            jFrameStrainStress = jf;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            jFrameStrainStress.dispose();
        }
    }
}
