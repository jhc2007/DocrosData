
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputStopConditionLayerStrainActionListener implements ActionListener {

    JFrameInputStopConditionLayerStrain jFrameInputStopConditionLayerStrain;
    
    public JButtonInputStopConditionLayerStrainActionListener(JFrameInputStopConditionLayerStrain jf) {
        
        jFrameInputStopConditionLayerStrain = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        int i;
        switch (ae.getActionCommand()) {
            case "Used":
                String stopConditionNames[];
                stopConditionNames = new String[jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList.count];
                StopConditionLayerStrain node = jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList.first;
                for(i=0; i<stopConditionNames.length; i++) {
                    stopConditionNames[i] = node.name;
                    node = node.next;
                }
                JFrameUsedNames jFrameUsedNames = new JFrameUsedNames("Used Stop Condition Names:", stopConditionNames, jFrameInputStopConditionLayerStrain.getLocation());
                break;
            case "OK":
                if(jFrameInputStopConditionLayerStrain.jTextField[0].isVisible()) {
                    if(stopConditionNameInserted() && layerPatternsNameSelected() && layerNumberSelected() && strainInserted())
                        addStopConditionLayerStrain();
                    //jFrameInputStopConditionLayerStrain.jButton[5].setEnabled(true);
                }
                else{
                    if(layerPatternsNameSelected() && layerNumberSelected() && strainInserted())
                        updateStopConditionLayerStrain(); 
                }
                break;
            case "New":
                for(i=0; i<jFrameInputStopConditionLayerStrain.jTextField.length; i++) {
                    jFrameInputStopConditionLayerStrain.jTextField[i].setText(null);
                    jFrameInputStopConditionLayerStrain.jTextField[i].setBackground(Color.white);
                    jFrameInputStopConditionLayerStrain.jTextField[i].setEnabled(true);
                }
                jFrameInputStopConditionLayerStrain.jTextField[0].setVisible(true);
                jFrameInputStopConditionLayerStrain.jButtonUsedName.setVisible(true);
                if(jFrameInputStopConditionLayerStrain.jComboBox[0].getItemCount() == 1)
                    jFrameInputStopConditionLayerStrain.jButtonUsedName.setEnabled(false);
                else
                    jFrameInputStopConditionLayerStrain.jButtonUsedName.setEnabled(true);
                jFrameInputStopConditionLayerStrain.jComboBox[0].setVisible(false);
                jFrameInputStopConditionLayerStrain.jComboBox[1].setEnabled(true);
                jFrameInputStopConditionLayerStrain.jComboBox[2].setEnabled(true);
                jFrameInputStopConditionLayerStrain.jButton[1].setText("Back");
                jFrameInputStopConditionLayerStrain.jButton[1].setVisible(true);
                jFrameInputStopConditionLayerStrain.jButton[2].setText("OK");
                break;
            case "Back":
                jFrameInputStopConditionLayerStrain.jTextField[0].setVisible(false);
                jFrameInputStopConditionLayerStrain.jButtonUsedName.setVisible(false);
                jFrameInputStopConditionLayerStrain.jComboBox[0].setVisible(true);
                jFrameInputStopConditionLayerStrain.jComboBox[0].setSelectedIndex(0);
                break;
            case "Reset":
                jFrameInputStopConditionLayerStrain.jComboBox[0].setSelectedIndex(jFrameInputStopConditionLayerStrain.jComboBox[0].getSelectedIndex());               
                break;
            case "Delete":
                deleteStopConditionLayerStrain();
                break;
            case "Previous":
                jFrameInputStopConditionLayerStrain.dispose();
                JFrameInputMonotonicLoadTypeData jFrameInputMonotonicLoadTypeData = new JFrameInputMonotonicLoadTypeData(jFrameInputStopConditionLayerStrain.jFrameDocrosData, jFrameInputStopConditionLayerStrain.getLocation());
                //JFrameInputLayerPatterns jFrameInputLayerPatterns = new JFrameInputLayerPatterns(jFrameInputStopConditionLayerStrain.jFrameDocrosData, jFrameInputStopConditionLayerStrain.getLocation());
                break;            
            case "Next":
                jFrameInputStopConditionLayerStrain.dispose();
                JFrameInputCyclicLoadTypeData jFrameCyclicLoadTypeData = new JFrameInputCyclicLoadTypeData(jFrameInputStopConditionLayerStrain.jFrameDocrosData, jFrameInputStopConditionLayerStrain.getLocation());
                break;
            case "Close":
                jFrameInputStopConditionLayerStrain.dispose();
                break;
        }
    }
    
    void deleteStopConditionLayerStrain() {
        
        int i;
        String stopConditionName = String.valueOf(jFrameInputStopConditionLayerStrain.jComboBox[0].getSelectedItem());
        jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList.delStopConditionLayerStrain(stopConditionName);
        jFrameInputStopConditionLayerStrain.jComboBox[0].removeItem(stopConditionName);
        jFrameInputStopConditionLayerStrain.jComboBox[0].setSelectedIndex(0);
        if(jFrameInputStopConditionLayerStrain.jComboBox[0].getItemCount() == 1) {
            jFrameInputStopConditionLayerStrain.jComboBox[0].setVisible(false);
            for(i=0; i<jFrameInputStopConditionLayerStrain.jTextField.length; i++) {
                jFrameInputStopConditionLayerStrain.jTextField[i].setText(null);
                jFrameInputStopConditionLayerStrain.jTextField[i].setBackground(Color.white);
                jFrameInputStopConditionLayerStrain.jTextField[i].setEnabled(true);
            }
            jFrameInputStopConditionLayerStrain.jTextField[0].setVisible(true);
            jFrameInputStopConditionLayerStrain.jButtonUsedName.setVisible(true);
            jFrameInputStopConditionLayerStrain.jButtonUsedName.setEnabled(false);
            jFrameInputStopConditionLayerStrain.jComboBox[1].setEnabled(true);
            jFrameInputStopConditionLayerStrain.jComboBox[2].setEnabled(true);
            jFrameInputStopConditionLayerStrain.jComboBox[1].setSelectedIndex(0);
            jFrameInputStopConditionLayerStrain.jComboBox[2].setSelectedIndex(0);
            jFrameInputStopConditionLayerStrain.jButton[1].setVisible(false);
            jFrameInputStopConditionLayerStrain.jButton[2].setText("OK");
            //jFrameInputStopConditionLayerStrain.jButton[5].setEnabled(false);
        }
        if(jFrameInputStopConditionLayerStrain.jFrameDocrosData.phaseDataList != null && jFrameInputStopConditionLayerStrain.jFrameDocrosData.phaseDataList.first != null) {
            PhaseData node = jFrameInputStopConditionLayerStrain.jFrameDocrosData.phaseDataList.first;
            while(node != null) {
                if(node.loadType.equals("_MONOTONIC") && node.stopConditionName.equals(stopConditionName)) {
                    jFrameInputStopConditionLayerStrain.jFrameDocrosData.phaseDataList.delPhaseData(node);
                    node = null;
                }
                else
                    node = node.next;
            }
        }

        if((jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList == null || jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList.first == null) 
                && (jFrameInputStopConditionLayerStrain.jFrameDocrosData.cyclicLoadTypeDataList == null || jFrameInputStopConditionLayerStrain.jFrameDocrosData.cyclicLoadTypeDataList.first == null)) {
            jFrameInputStopConditionLayerStrain.jFrameDocrosData.jMenuItemEdit[10].setEnabled(false);
        }
                
        jFrameInputStopConditionLayerStrain.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputStopConditionLayerStrain.jFrameDocrosData.dataFile != null)
            jFrameInputStopConditionLayerStrain.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);

    }
    
    void updateStopConditionLayerStrain() {
        
        String stopConditionName = String.valueOf(jFrameInputStopConditionLayerStrain.jComboBox[0].getSelectedItem());
        String layerPatternsName = String.valueOf(jFrameInputStopConditionLayerStrain.jComboBox[1].getSelectedItem());
        int layerNumber = Integer.valueOf(String.valueOf(jFrameInputStopConditionLayerStrain.jComboBox[2].getSelectedItem()));
        double strain = Double.valueOf(jFrameInputStopConditionLayerStrain.jTextField[1].getText());
        StopConditionLayerStrain node = jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList.getStopConditionLayerStrain(stopConditionName);
        node.layerPatternsName = layerPatternsName;
        node.layerNumber = layerNumber;
        node.strain = strain;
        jFrameInputStopConditionLayerStrain.jComboBox[0].setSelectedIndex(0);
        jFrameInputStopConditionLayerStrain.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputStopConditionLayerStrain.jFrameDocrosData.dataFile != null)
            jFrameInputStopConditionLayerStrain.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void addStopConditionLayerStrain() {
                
        String stopConditionName = jFrameInputStopConditionLayerStrain.jTextField[0].getText();
        String layerPatternsName = String.valueOf(jFrameInputStopConditionLayerStrain.jComboBox[1].getSelectedItem());
        int layerNumber = Integer.valueOf(String.valueOf(jFrameInputStopConditionLayerStrain.jComboBox[2].getSelectedItem()));
        double strain = Double.valueOf(jFrameInputStopConditionLayerStrain.jTextField[1].getText());
        StopConditionLayerStrain node = new StopConditionLayerStrain(stopConditionName, layerNumber, layerPatternsName, strain);
        if(jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList == null)
            jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList = new StopConditionLayerStrainList();
        jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList.addStopConditionLayerStrain(node);

        jFrameInputStopConditionLayerStrain.jComboBox[0].addItem(node.name);
        jFrameInputStopConditionLayerStrain.jComboBox[0].setSelectedIndex(0);
        jFrameInputStopConditionLayerStrain.jComboBox[0].setVisible(true);
        jFrameInputStopConditionLayerStrain.jTextField[0].setVisible(false);
        jFrameInputStopConditionLayerStrain.jButtonUsedName.setVisible(false);
        jFrameInputStopConditionLayerStrain.jButton[2].setText("New");
        if(jFrameInputStopConditionLayerStrain.jFrameDocrosData.monotonicLoadTypeDataList != null && jFrameInputStopConditionLayerStrain.jFrameDocrosData.monotonicLoadTypeDataList.first != null)
            jFrameInputStopConditionLayerStrain.jFrameDocrosData.jMenuItemEdit[10].setEnabled(true);
        jFrameInputStopConditionLayerStrain.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputStopConditionLayerStrain.jFrameDocrosData.dataFile != null)
            jFrameInputStopConditionLayerStrain.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }

    Boolean existsStopConditionName() {
        
        StopConditionLayerStrainList list = jFrameInputStopConditionLayerStrain.jFrameDocrosData.stopConditionLayerStrainList;
        if(list == null)
            return false;
        StopConditionLayerStrain node = list.first;
        while(node != null) {
            if(node.name.equals(jFrameInputStopConditionLayerStrain.jTextField[0].getText()))
                return true;
            node = node.next;
        }
        return false;
    }
    
    Boolean strainInserted() {
        
        if(jFrameInputStopConditionLayerStrain.jTextField[1].getText().equals("")) {
            jFrameInputStopConditionLayerStrain.jTextField[1].setBackground(Color.yellow);
            jFrameInputStopConditionLayerStrain.jLabelMessage[4].setVisible(true);
            jFrameInputStopConditionLayerStrain.jButton[2].setEnabled(false);    
            return false;    
        }
        return true;
    }
    
    Boolean layerNumberSelected() {
        
        if(jFrameInputStopConditionLayerStrain.jComboBox[2].getSelectedIndex() == 0) {
            jFrameInputStopConditionLayerStrain.jLabelMessage[3].setVisible(true);
            jFrameInputStopConditionLayerStrain.jButton[2].setEnabled(false);    
            return false;
        }
        return true;
    }
          
    Boolean layerPatternsNameSelected() {
        
        if(jFrameInputStopConditionLayerStrain.jComboBox[1].getSelectedIndex() == 0) {
            jFrameInputStopConditionLayerStrain.jLabelMessage[2].setVisible(true);
            jFrameInputStopConditionLayerStrain.jButton[2].setEnabled(false);    
            return false;
        }
        return true;
    }
    
    Boolean stopConditionNameInserted() {
              
        if(jFrameInputStopConditionLayerStrain.jTextField[0].getText().equals("")) {
            jFrameInputStopConditionLayerStrain.jTextField[0].setBackground(Color.yellow);
            jFrameInputStopConditionLayerStrain.jLabelMessage[0].setVisible(true);
            jFrameInputStopConditionLayerStrain.jButton[2].setEnabled(false);
            return false;
        }
        if(existsStopConditionName()) {
            jFrameInputStopConditionLayerStrain.jTextField[0].setBackground(Color.yellow);
            jFrameInputStopConditionLayerStrain.jLabelMessage[1].setVisible(true);
            jFrameInputStopConditionLayerStrain.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    
}

