
package docrosdata;

public class NonLinearMaterialModelTypeList {
    
    NonLinearMaterialModelType first;
    NonLinearMaterialModelType last;
    int count;
    
    public NonLinearMaterialModelTypeList() {
       
        first = null;
        last = null;
        count = 0;
    }
    
    void addNonLinearMaterialModelType(NonLinearMaterialModelType node) {
  
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
    
    NonLinearMaterialModelType getNonLinearMaterialModelType(String type) {
        
        NonLinearMaterialModelType mtype;
        mtype = first;
        while(mtype != null && !mtype.name.equals(type))
            mtype = mtype.next;
        return mtype;
    }
    
    NonLinearMaterialModel getNonLinearMaterialModel(String t, String n) {
        
        NonLinearMaterialModelType mtype;
        mtype = first;
        while(mtype != null && !mtype.name.equals(t))
            mtype = mtype.next;
        if(mtype == null) { 
            //System.out.println(t+"doesn't exist");
            return null;
        }
        NonLinearMaterialModel mmodel = mtype.list.first;
        while(mmodel != null && !mmodel.name.equals(n))
            mmodel = mmodel.next;
        //if(mmodel == null)
            //System.out.println(n+"doesn't exist");
        return mmodel;
    }
    
    NonLinearMaterialModelList getNonLinearMaterialModelList(String type) {
        
        //System.out.println("Type: " + type);
        NonLinearMaterialModelType nlmmt;
        nlmmt = first;
        while(nlmmt != null && !nlmmt.name.equals(type))
            nlmmt = nlmmt.next;
        //System.out.println("NLMMT: " + nlmmt);
        if(nlmmt == null)
            return null;
        return nlmmt.list;
    }
    
    int maxNumberCharacterTypeName() {
        
        int max = 0;
        NonLinearMaterialModelType type;
        type = first;
        while(type != null) {
            if(type.name.length() > max)
                max = type.name.length();
            type = type.next;
        }
        return max - 1;
    }
    
    int maxNumberCharacterModelName() {
        
        int max = 0;
        NonLinearMaterialModelType type;
        NonLinearMaterialModel model;
        type = first;
        while(type != null) {
            model = type.list.first;
            while(model != null) {
                if(model.name.length() > max)
                    max = model.name.length();
                model = model.next;
            }
            type = type.next;
        }
        return max;    
    }
       
    void delNonLinearMaterialModel(NonLinearMaterialModel model) {
        
        NonLinearMaterialModelType type = first;
        while(!type.name.equals('_' + model.type))
            type = type.next;
        NonLinearMaterialModel node1 = type.list.first;
        NonLinearMaterialModel node2 = node1.next;
        if(node1 == model) {
            type.list.first = node2;
            if(node2 == null)
                last = null;
        }
        else {
            while(node2 != null && node2 != model) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node1.next == null)
                type.list.last = node1;
        }
        type.list.count--;
    }

    Boolean existsMaterialModel() {
        
        NonLinearMaterialModelType type;
        //NonLinearMaterialModel model;
        type = first;
        while(type != null) {
            if(type.list != null && type.list.first != null)
                return true;
            type = type.next;
        }
        return false;        
    }
    
    Boolean existsMaterialModelName(String t, String n) {
        
        NonLinearMaterialModelType type;
        NonLinearMaterialModel model;
        
        type = first;
        
        while(type != null) {
            if(type.name.equals(t)) {
                model = type.list.first;
                while(model != null) {
                    if(model.name.equals(n))
                        return true;
                    model = model.next;
                }
                return false;
            }
            type = type.next;
        }
        return false;
    }
    
    void printNonLinearMaterialModelTypeList() {
        
        int i;
        NonLinearMaterialModelType t = first;
        while(t != null) {
            System.out.println("Name: " + t.name+'\n');
            NonLinearMaterialModelList list = t.list;
            NonLinearMaterialModel m = list.first;
            while(m != null) {
                System.out.println("Type: "+m.type);
                System.out.println("Name: "+m.name+'\n');
                for(i=0; i<m.parameters.length; i++)
                    System.out.println("parameter["+i+"]="+m.parameters[i]);
                m = m.next;
            }
            t = t.next;
        }
            
        
    }
}
