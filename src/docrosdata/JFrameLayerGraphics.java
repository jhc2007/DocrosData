
package docrosdata;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class JFrameLayerGraphics extends JFrame {
    
    JFrameDocrosData jFrameDocrosData;
    JLabel jLabel[];
    JComboBox jComboBox[];
    JButton jButtonHelp;
    JButton jButton[];
    String jLabelText[] = {"Pattern Name", "Layer Number"};
    String jButtonText[] = {"Open", "Close"};
    
    public JFrameLayerGraphics(JFrameDocrosData jf) {
        
        super("Layer Graphics");
        jFrameDocrosData = jf;
        init();
    }
    
    private void init() {

        this.setLocation(15, 45);             
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, jLabelWidth, xPosition = 12, yPosition = 12;
        
        jLabelWidth = jFrameDocrosData.charWidth * (JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText));        
        
        JLabel jLabelTitle = new JLabel("Layer Graphics");
        jLabelTitle.setSize(jFrameDocrosData.charWidth * (jLabelTitle.getText().length() + 4), jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(xPosition + jLabelWidth, yPosition);
        this.add(jLabelTitle);
        
        yPosition += 33;
        
        if(getClass().getResourceAsStream("/help/helpLayerGraphics.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("LayerGraphics"));
            this.add(jButtonHelp);
        }
        
        jLabel = new JLabel[2];
        jComboBox = new JComboBox[2]; 
        for(i=0; i<2; i++) {
            jLabel[i] = new JLabel(jLabelText[i]);
            jLabel[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
            jLabel[i].setLocation(xPosition, yPosition);
            this.add(jLabel[i]);
            jComboBox[i] = new JComboBox();
            jComboBox[i].setSize(jLabelTitle.getWidth(), jFrameDocrosData.charHeight);
            jComboBox[i].setLocation(xPosition + jLabelWidth, yPosition);
            jComboBox[i].addItem("Select");
            if(i == 0 && jFrameDocrosData.layerPatternsList != null && jFrameDocrosData.layerPatternsList.first != null) {
                LayerPatterns node = jFrameDocrosData.layerPatternsList.first;
                while(node != null) {
                    jComboBox[i].addItem(node.name);
                    node = node.next;
                }
            }
            jComboBox[i].setSelectedIndex(0);
            this.add(jComboBox[i]);
            yPosition += 33;
        }
        jComboBox[0].setName("PatternName");
        
        yPosition += 33;
                
        jButton = new JButton[2];       
        for(i=0; i<2; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 2, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * jButton[0].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonLayerGraphicsActionListener(this));
            this.add(jButton[i]);
        }
        jButton[0].setEnabled(false);
        
        jComboBox[0].addActionListener(new JComboBoxLayerGraphics(this));
        jComboBox[1].addActionListener(new JComboBoxLayerGraphics(this));
                                         
        //jButton[0].addActionListener(new JButtonStressStrainGraphicsActionListener(this));
        //jButton[1].addActionListener(new JButtonStressStrainGraphicsActionListener(this));
        
        yPosition += 63;
        
        this.setSize(jLabelTitle.getWidth() + jLabelWidth + 26, yPosition);
        this.setVisible(true);
   }

}
