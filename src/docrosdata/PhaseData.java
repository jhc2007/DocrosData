
package docrosdata;

public class PhaseData {
    
    String loadType; //_MONOTONIC || _CYCLIC
    String loadName;
    String stopConditionName;
    PhaseData next;
    
    public PhaseData(String type, String name, String stop) {
        
        loadType = type;
        loadName = name;
        stopConditionName = stop;
    }
}
