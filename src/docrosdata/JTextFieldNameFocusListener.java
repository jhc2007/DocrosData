
package docrosdata;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

class JTextFieldNameFocusListener implements FocusListener {

    JLabel jLabelMessage[];//, jLabelMessage2;
    int messageIndex;
    JLabel jLabelMessage0;
    JButton jButton;
    
    public JTextFieldNameFocusListener(JLabel jl[], int index, JLabel jl0, JButton jb) {
        
        jLabelMessage = jl;
        messageIndex = index;
        jLabelMessage0 = jl0;
        jButton = jb;
        
    }

    @Override
    public void focusGained(FocusEvent fe) {

        String name = ((JTextField) fe.getSource()).getName();
        
        int i;
        if(jLabelMessage != null && (
                name.equals("NameLaminateGeoProp") || 
                /* name.equals("NameMaterialModel") || */
                name.equals("NameLayerProperties") || 
                name.equals("NameLayerPatterns")))
            for(i=0; i<jLabelMessage.length; i++)
                jLabelMessage[i].setVisible(false);
        //if(!((JTextField) fe.getSource()).getBackground().equals(Color.WHITE)) {
       
        ((JTextField) fe.getSource()).setBackground(Color.white);
        if(((JTextField) fe.getSource()).getText().equals("Insert name"))
            ((JTextField) fe.getSource()).setText("");
        if(jButton != null)
            jButton.setEnabled(true);
        if(jLabelMessage != null && messageIndex != -1)
            jLabelMessage[messageIndex].setVisible(false);       
        if(jLabelMessage0 != null)
            jLabelMessage0.setVisible(false);
        //}
    }

    @Override
    public void focusLost(FocusEvent fe) {

        String name = ((JTextField) fe.getSource()).getName();
        
        int i;
        if(jLabelMessage != null)
            for(i=0; i<jLabelMessage.length; i++)
                jLabelMessage[i].setVisible(false);
        if(((JTextField) fe.getSource()).getText().equals("")) {// && !((JTextField) fe.getSource()).getName().equals("NameLayerPatterns")) {
            if(!name.equals("NameCyclicLoad") && !name.equals("NameMonotonicLoad") && !name.equals("NameStopCondition") && !name.equals("NameMainParameters"))
                ((JTextField) fe.getSource()).setText("Insert name");
            ((JTextField) fe.getSource()).setBackground(Color.yellow);
            if(jButton != null)
                jButton.setEnabled(false);
            if(jLabelMessage != null && messageIndex != -1)
                jLabelMessage[messageIndex].setVisible(true);
            if(jLabelMessage0 != null && 
                    !name.equals("NameLaminateGeoProp") && 
                    !name.equals("NameMaterialModel") && 
                    !name.equals("NameCyclicLoad") &&
                    !name.equals("NameMonotonicLoad") &&
                    !name.equals("NameStopCondition"))
                    jLabelMessage0.setVisible(true);
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
