/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package docrosdata;

import javax.swing.JPanel;

/**
 *
 * @author Moi
 */
public class JPanelLayerPatterns extends JPanel {
    
    LayerPatterns layerPatterns;
    int indexLayerProperties;
    double zoomFactor;
    
    public JPanelLayerPatterns(LayerPatterns lp, int ilp, double zoom) {
        super();
        layerPatterns = lp;
        indexLayerProperties = ilp;
        zoomFactor = zoom;
        init();
    }
    
    private void init() {
        //this.addMouseListener(new JPanelLayerPatternsMouseListener(temp, jPanelLayerPatterns[i], this));//jPanelMain));
        //jLabel = new JLabel(layerPatterns.name);
        if(indexLayerProperties == 0)
            this.setSize(JFrameDocrosData.doubleToInt(zoomFactor * layerPatterns.layerProperties[indexLayerProperties].geoProp.width), JFrameDocrosData.doubleToInt(zoomFactor * layerPatterns.numLayers * layerPatterns.layerProperties[indexLayerProperties].geoProp.thickness));
        else
            this.setSize(JFrameDocrosData.doubleToInt(zoomFactor * layerPatterns.layerProperties[indexLayerProperties].geoProp.width / 2), JFrameDocrosData.doubleToInt(zoomFactor * layerPatterns.numLayers * layerPatterns.layerProperties[indexLayerProperties].geoProp.thickness));
        this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        //this.add(jLabel);
    }
    
//        public int doubleToInt(double x) {
//
//        int i = (int) x;
//        double d = x - i;
//        if(d >= 0.5)
//            i++;
//        return i;
//    }
    
}
