
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class JComboBoxInputLaminateGeoProp implements ActionListener {

    JFrameInputLaminateGeoProp jFrameInputLaminateGeoProp;
    
    public JComboBoxInputLaminateGeoProp(JFrameInputLaminateGeoProp jf) {
        
        jFrameInputLaminateGeoProp = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
 
        int i;
        String name = String.valueOf(((JComboBox) ae.getSource()).getSelectedItem());
        
        if(!name.equals("Select")) {
            showParameters();
            //jFrameInputLaminateGeoProp.jButton[0].setText("Show");
            //jFrameInputLaminateGeoProp.jButton[0].setVisible(true);
            //jFrameInputLaminateGeoProp.jButton[1].setText("Delete");
            //jFrameInputLaminateGeoProp.jButton[1].setVisible(true);
            LaminateGeoProp node = jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(name);
            if(node.countLayerPatterns > 0)
                jFrameInputLaminateGeoProp.jButton[0].setEnabled(false);
        }
        else {
            if(jFrameInputLaminateGeoProp.jComboBoxName.isVisible()) {
                for(i=1; i<3; i++) {
                    jFrameInputLaminateGeoProp.jLabel[i].setVisible(false);
                    jFrameInputLaminateGeoProp.jTextField[i].setVisible(false);               
                }
                //jFrameInputLaminateGeoProp.jComboBoxName.setEnabled(false);
                jFrameInputLaminateGeoProp.jButton[0].setVisible(false);
                jFrameInputLaminateGeoProp.jButton[1].setVisible(false);
                jFrameInputLaminateGeoProp.jButton[2].setText("New");
                jFrameInputLaminateGeoProp.jButton[2].setEnabled(true);
            }
        }
    }
    
    void showParameters() {
       
        int i;
        String name = String.valueOf(jFrameInputLaminateGeoProp.jComboBoxName.getSelectedItem());
        LaminateGeoProp node = jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(name);
        //System.out.println("Node Name: " + node.name);
        for(i=1; i<3; i++) {
            jFrameInputLaminateGeoProp.jTextField[i].setText(node.getPropertyValue(i));
            jFrameInputLaminateGeoProp.jTextField[i].setBackground(Color.white);
            jFrameInputLaminateGeoProp.jTextField[i].setVisible(true);
            jFrameInputLaminateGeoProp.jLabel[i].setVisible(true);               
        }
        //jFrameInputLaminateGeoProp.jComboBoxName.setEnabled(false);
        //jFrameInputLaminateGeoProp.jButton[1].setText("Save");
        
        //jFrameInputLaminateGeoProp.jButton[0].setText("Reset");
        jFrameInputLaminateGeoProp.jButton[0].setVisible(true);
        if(node.countLayerPatterns > 0)
            jFrameInputLaminateGeoProp.jButton[0].setEnabled(false);
        else
            jFrameInputLaminateGeoProp.jButton[0].setEnabled(true);
        jFrameInputLaminateGeoProp.jButton[1].setText("Reset");
        jFrameInputLaminateGeoProp.jButton[1].setVisible(true);
        jFrameInputLaminateGeoProp.jButton[1].setEnabled(true);
        jFrameInputLaminateGeoProp.jButton[2].setText("OK");
        jFrameInputLaminateGeoProp.jButton[2].setEnabled(true);
    }
    
}
