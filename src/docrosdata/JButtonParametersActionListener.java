
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

class JButtonParametersActionListener implements ActionListener {

    JFrameDocrosData jFrameDocrosData;
    JPanelLayerPatterns jPanelLayerPatterns;
    JFrame jFrameMaterial;
    JComboBox jComboBoxLawType;
    JComboBox jComboBoxLawName;
    JLabel jLabelParameterSymbol[];
    JLabel jLabelParameterUnit[];
    JTextField jTextFieldParameter[];
    JButton jButtonShow;
    JButton jButtonParameters[];
    JLabel jLabelLayerPropertiesName;
    JRadioButton jRadioButton[];
    
    NonLinearMaterialModel materialModel;
    NonLinearMaterialModel modelSelected;
    LayerProperties layerProperties;
    JTextField jTextFieldMaterialModelName, jTextFieldLayerPropertiesName;
    Boolean newMaterialModelName, newLayerPropertiesName;
    JLabel jLabel1, jLabel2;//, jLabel3;//, jLabelLayerPropertiesName;
    int jFrameMaterialWidth, jFrameMaterialHeight;
    //JLabel jLabel3, jLabel4;
    
    public JButtonParametersActionListener(JFrameDocrosData jfdd, JPanelLayerPatterns jplp, JFrame jfm, JComboBox jcblt, JComboBox jcbln, JLabel jlps[], JLabel jlpu[], JTextField jtfp[], JButton jbshow, JButton jbp[], JLabel jllpn, int jfmw, int jfmh, JRadioButton jrb[]) {
        
        jFrameDocrosData = jfdd;
        jPanelLayerPatterns = jplp;
        jFrameMaterial = jfm;
        jComboBoxLawType = jcblt;
        jComboBoxLawName = jcbln;
        jLabelParameterSymbol = jlps;
        jLabelParameterUnit = jlpu;
        jTextFieldParameter = jtfp;
        jButtonShow = jbshow;
        jButtonParameters = jbp;
        jLabelLayerPropertiesName = jllpn;
        jFrameMaterialWidth = jfmw;
        jFrameMaterialHeight = jfmh;
        jRadioButton = jrb;
        
        materialModel = jplp.layerPatterns.layerProperties[jplp.indexLayerProperties].materialModel;
        //modelSelected = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jcblt.getSelectedItem()), String.valueOf(jcbln.getSelectedItem()));
        layerProperties = jplp.layerPatterns.layerProperties[jplp.indexLayerProperties];
        jTextFieldMaterialModelName = new JTextField("");
        jTextFieldMaterialModelName.setVisible(false);
        jTextFieldLayerPropertiesName = new JTextField("");
        jTextFieldLayerPropertiesName.setVisible(false);
        newMaterialModelName = false;
        newLayerPropertiesName = false;
        jLabel1 = new JLabel("");
        jLabel2 = new JLabel("This name is already used");
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        modelSelected = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jComboBoxLawType.getSelectedItem()), String.valueOf(jComboBoxLawName.getSelectedItem()));
        
        switch (ae.getActionCommand()) {            
            case "Reset":             
                resetParameters();
                break;         
            case "OK":
                if(modelSelected == null) {                   
                    if(!hasError() && newMaterialModelNameInserted()) {                        
                        int i;
                        double parameters[];
                        parameters = new double[jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelType('_' + String.valueOf(jComboBoxLawType.getSelectedItem())).numberParameters];
                        for(i=0; i<parameters.length; i++)
                            parameters[i] = Double.valueOf(jTextFieldParameter[i].getText());
                        modelSelected = new NonLinearMaterialModel(String.valueOf(jComboBoxLawType.getSelectedItem()), jTextFieldMaterialModelName.getText(), parameters);
                        jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + String.valueOf(jComboBoxLawType.getSelectedItem())).addNonLinearMaterialModel(modelSelected);
                    }
                }
                if(modelSelected != null) {                    
                    if(!parametersChanged()) { 
                        if(!materialModel.equals(modelSelected)) {  
                            if(newLayerPropertiesNameInserted()) {
                                changeMaterialModel();
                                jFrameDocrosData.drawFigure(jFrameDocrosData.phase); 
                                jFrameMaterial.dispose();                        
                            }                       
                        }
                        else {
                            jFrameDocrosData.drawFigure(jFrameDocrosData.phase); 
                            jFrameMaterial.dispose();
                        }

                    }
                    else {
                        if(!hasError() && newLayerPropertiesNameInserted() && newMaterialModelNameInserted()) { 
                            changeLists();
                            jFrameDocrosData.drawFigure(jFrameDocrosData.phase); 
                            jFrameMaterial.dispose();
                         }
                    }
                }
                break;
            case "Close":
                jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                jFrameMaterial.dispose();//.setVisible(false);
                break;
        }              
    }
    
    Boolean newLayerPropertiesNameInserted() {
                    
        if(newLayerPropertiesName || layerProperties.countLayerPatterns <= 1)
            return true;
        int i;
        jLabelLayerPropertiesName.setVisible(false);
        jTextFieldLayerPropertiesName.setEnabled(true);
        jTextFieldMaterialModelName.setEnabled(false);
        jButtonShow.setVisible(false);
        jButtonParameters[0].setVisible(false);

        if(jTextFieldLayerPropertiesName.isVisible()) {

            if(jTextFieldLayerPropertiesName.getText().equals("") || jTextFieldLayerPropertiesName.getText().equals("Inser a name"))
                return false;

            if(jFrameDocrosData.layerPropertiesList.existsLayerPropertiesName(jTextFieldLayerPropertiesName.getText())) {
                jTextFieldLayerPropertiesName.setBackground(Color.yellow);
                jLabel2.setVisible(true);
            }
            else {

                newLayerPropertiesName = true;
                return true;
            }
        }
        else {
            
            for(i=0; i<jLabelParameterSymbol.length; i++) {
                jLabelParameterSymbol[i].setVisible(false);
                jLabelParameterUnit[i].setVisible(false);
                jTextFieldParameter[i].setVisible(false);
            }
            

            //jLabel3.setVisible(false);

            jLabel1.setText("Insert a New Layer Properties Name");
            jLabel1.setSize(jFrameDocrosData.charWidth * jLabel1.getText().length(), jFrameDocrosData.charHeight);
            jLabel1.setLocation(jButtonShow.getLocation());
            jFrameMaterial.add(jLabel1);

/*
            jLabelLayerPropertiesName.setSize(jFrameDocrosData.charWidth * jLabelLayerPropertiesName.getText().length(), jFrameDocrosData.charHeight);
            jLabelLayerPropertiesName.setLocation(jLabel1.getX(), jLabel1.getY() + 2 * jFrameDocrosData.charHeight);
            jFrameMaterial.add(jLabelLayerPropertiesName);
*/
            jTextFieldLayerPropertiesName.setSize(jComboBoxLawName.getWidth(), jFrameDocrosData.charHeight);
            jTextFieldLayerPropertiesName.setLocation(jLabelLayerPropertiesName.getLocation());
            jTextFieldLayerPropertiesName.setVisible(true);
            //jTextFieldLayerPropertiesName.setBackground(Color.LIGHT_GRAY);
            jTextFieldLayerPropertiesName.addFocusListener(new JTextFieldNameFocusListener(null, 0, jLabel2, jButtonParameters[1]));
            jFrameMaterial.add(jTextFieldLayerPropertiesName);

            jLabel2.setSize(jFrameDocrosData.charWidth * jLabel2.getText().length(), jFrameDocrosData.charHeight);
            jLabel2.setLocation(jComboBoxLawName.getX(), jComboBoxLawName.getY() + jFrameDocrosData.charHeight);
            jLabel2.setVisible(false);
            jFrameMaterial.add(jLabel2);

            for(i=0; i<jButtonParameters.length; i++)
                jButtonParameters[i].setLocation(jButtonParameters[i].getX(), jLabel1.getY() + jFrameDocrosData.charHeight);
            
            jFrameMaterial.setSize(jFrameMaterialWidth, jFrameMaterialHeight);
            //jFrameMaterial.setSize(jFrameMaterial.getWidth(), jButtonParameters[0].getY() + 2 * jFrameDocrosData.charHeight + 15);
            //jFrameMaterial.setSize(jFrameMaterial.getWidth(), jFrameMaterial.getHeight() + jFrameDocrosData.charHeight);
        }

        return false;
    }
    
    void changeMaterialModel() {
       
         if(materialModel.countLayerPatterns == 1) {
            
            materialModel.countLayerPatterns--;
            modelSelected.countLayerPatterns++;
            layerProperties.materialModel = modelSelected;
            if(materialModel.countLayerPatterns == 0)
                jFrameDocrosData.nonLinearMaterialModelTypeList.delNonLinearMaterialModel(materialModel);   
        }
        else {
            
            if(layerProperties.countLayerPatterns > 1) {
                
                LayerProperties copy = new LayerProperties(jTextFieldLayerPropertiesName.getText());//, layerProperties);
                jFrameDocrosData.layerPropertiesList.addLayerProperties(copy);
                layerProperties.countLayerPatterns--;
                //materialModel.countLayerPatterns--;
                copy.countLayerPatterns++;
                //modelSelected.countLayerPatterns++;
                copy.geoProp = layerProperties.geoProp;
                copy.materialModel = modelSelected;
                jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties] = copy;
                //jFrameDocrosData.changedLists[jFrameDocrosData.layerPatternsListIndex] = true;
            }
            else 
                layerProperties.materialModel = modelSelected;
            materialModel.countLayerPatterns--;
            modelSelected.countLayerPatterns++;                  
            //jFrameDocrosData.changedLists[jFrameDocrosData.nonLinearMaterialModelTypeListIndex] = true;
        }
        //jFrameDocrosData.changedLists[jFrameDocrosData.layerPropertiesListIndex] = true;
        jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameDocrosData.dataFile != null)
            jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
   }
    
    Boolean hasError() {
        
        int i;
        String text;
        Boolean error = false;
        for(i=0; i<jTextFieldParameter.length; i++) {
            try {
                Double.valueOf(jTextFieldParameter[i].getText());
            }
            catch(Exception e) {
                error = true;
                text = jTextFieldParameter[i].getText();
                jTextFieldParameter[i].setText(text + " NOT A NUMBER");
                jTextFieldParameter[i].setBackground(Color.yellow);
             }
        }
        return error;
    }
    
    void resetParameters() {
        
        int i;
        jButtonParameters[1].setEnabled(true);//.setVisible(true);
        jButtonShow.setEnabled(true);
        for(i=0; i<jTextFieldParameter.length; i++) {
            jTextFieldParameter[i].setBackground(Color.white);
            jTextFieldParameter[i].setText(String.format("%7.4E", modelSelected.parameters[i]).replace(',', '.'));
        }
        if(modelSelected.type.equals("NLMM105")) {
            if(modelSelected.parameters[12] == 0.0 && modelSelected.parameters[13] == 0.0) {
                jRadioButton[0].setSelected(true);
                jRadioButton[1].setSelected(false);
                for(i=12; i<14; i++) {
                    jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                    jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                    jTextFieldParameter[i].setText("0.0");
                    jTextFieldParameter[i].setEnabled(false);
                }
            }
            else {
                jRadioButton[1].setSelected(true);
                jRadioButton[0].setSelected(false);
                for(i=12; i<14; i++) {
                    jLabelParameterSymbol[i].setForeground(Color.black);
                    jLabelParameterUnit[i].setForeground(Color.black);
                    jTextFieldParameter[i].setEnabled(true);
                }
            }
        }
    }
    
    Boolean parametersChanged() {

        int i;
        for(i=0; i<jTextFieldParameter.length; i++)
            if(modelSelected.parameters[i] != Double.valueOf(jTextFieldParameter[i].getText()))
                return true;
        return false;
    }
    
    void createNewMaterialModel(String type, LayerProperties layer) {
        
        int i;
        NonLinearMaterialModel newModel;
        double parameters[];
         
        parameters = new double[jTextFieldParameter.length];
        
        for(i=0; i<parameters.length; i++)
            parameters[i] = Double.valueOf(jTextFieldParameter[i].getText());
        
        newModel = new NonLinearMaterialModel(type, jTextFieldMaterialModelName.getText(), parameters);
                     
        jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + type).addNonLinearMaterialModel(newModel);
        layer.materialModel = newModel;
        materialModel.countLayerPatterns--; 
        newModel.countLayerPatterns++;
        
        if(materialModel.countLayerPatterns == 0)
            jFrameDocrosData.nonLinearMaterialModelTypeList.delNonLinearMaterialModel(materialModel);  
        
    }
    
    LayerProperties createNewLayerProperties() {
        
        LayerProperties copy = new LayerProperties(jTextFieldLayerPropertiesName.getText());//, layerProperties);
        jFrameDocrosData.layerPropertiesList.addLayerProperties(copy);
        layerProperties.countLayerPatterns--;
        //materialModel.countLayerPatterns--;
        copy.countLayerPatterns++;
                //modelSelected.countLayerPatterns++;
        copy.geoProp = layerProperties.geoProp;
        //copy.materialModel = //modelSelected;
        jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties] = copy;
        return copy;
    
    }
    
    void updateParameters(NonLinearMaterialModel model) {
        
        int i;
        for(i=0; i<jTextFieldParameter.length; i++)
            model.parameters[i] = Double.valueOf(jTextFieldParameter[i].getText());
        
    }
    
    void changeLists() {
        
        if(materialModel == modelSelected) {
            if(materialModel.countLayerPatterns == 1)
                updateParameters(materialModel);
            else {
                if(layerProperties.countLayerPatterns == 1) {
                    createNewMaterialModel(materialModel.type, layerProperties);
                }
                else {
                    //createNewLayerProperties();
                    createNewMaterialModel(materialModel.type, createNewLayerProperties());
                    //jFrameDocrosData.changedLists[jFrameDocrosData.layerPatternsListIndex] = true;
                }
            }    
        
        }
        else {
            if(layerProperties.countLayerPatterns == 1) {
                createNewMaterialModel(modelSelected.type, layerProperties);
            }
            else {
                createNewMaterialModel(modelSelected.type, createNewLayerProperties());
                //jFrameDocrosData.changedLists[jFrameDocrosData.layerPatternsListIndex] = true;
            }
        }
        //jFrameDocrosData.changedLists[jFrameDocrosData.layerPropertiesListIndex] = true;
        //jFrameDocrosData.changedLists[jFrameDocrosData.nonLinearMaterialModelTypeListIndex] = true;
        jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameDocrosData.dataFile != null)
            jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    Boolean newMaterialModelNameInserted() {
             
        if(newMaterialModelName)
            return true;
        
        if(modelSelected == null) {
            
            if(!jTextFieldMaterialModelName.isVisible())
                insertJTextFieldMaterialModelName();
            else {
                if(jTextFieldMaterialModelName.getText().equals("") || jTextFieldMaterialModelName.getText().equals("Insert a name"))
                    return false;
                else {
                    newMaterialModelName = true;
                    return true;
                }
            }
        }
        else {

            if(materialModel.equals(modelSelected)) {

                if(materialModel.countLayerPatterns == 1) {
                    newMaterialModelName = true;
                    return true;
                }
                else {
                    if(!jTextFieldMaterialModelName.isVisible()) {
                        insertJTextFieldMaterialModelName();
                    }
                    else {
                        if(jTextFieldMaterialModelName.getText().equals("") || jTextFieldMaterialModelName.getText().equals("Insert a name"))
                            return false;
                        if(!jFrameDocrosData.nonLinearMaterialModelTypeList.existsMaterialModelName('_' + materialModel.type, jTextFieldMaterialModelName.getText())) {
                            newMaterialModelName = true;
                            return true;
                        }
                        else {
                            jLabel2.setVisible(true);
                            jTextFieldMaterialModelName.setBackground(Color.yellow);
                        }
                    }
                }
            }
            else {
                    if(!jTextFieldMaterialModelName.isVisible()) {
                        insertJTextFieldMaterialModelName();
                        //return false;
                    }
                    else {

                        if(jTextFieldMaterialModelName.getText() == "" || jTextFieldMaterialModelName.getText() == "Insert a name")
                            return false;
                        if(!jFrameDocrosData.nonLinearMaterialModelTypeList.existsMaterialModelName('_' + materialModel.type, jTextFieldMaterialModelName.getText()))
                            return true;
                        else {
                            jLabel2.setVisible(true);
                            jTextFieldMaterialModelName.setBackground(Color.yellow);
                        }

                    }
                }
        }
        return false;
    }
    
    void insertJTextFieldMaterialModelName() {
        
        int i;
        
        for(i=0; i<jLabelParameterSymbol.length; i++) {
            jLabelParameterSymbol[i].setVisible(false);
            jLabelParameterUnit[i].setVisible(false);
            jTextFieldParameter[i].setVisible(false);
        }
            
        jButtonShow.setVisible(false);
        jButtonParameters[0].setVisible(false);
        
        //jLabel1.setVisible(false);
        jTextFieldLayerPropertiesName.setEnabled(false);
        jTextFieldMaterialModelName.setEnabled(true);
        jComboBoxLawName.setVisible(false);
        
        jLabel1.setText("Insert a New Material Model Name");
        jLabel1.setSize(jFrameDocrosData.charWidth * jLabel1.getText().length(), jFrameDocrosData.charHeight);
        jLabel1.setLocation(jButtonShow.getLocation());
        //jLabel3.setVisible(true);      
        jFrameMaterial.add(jLabel1);
        
        
        //jLabel4.setSize(jFrameDocrosData.charWidth * jLabel4.getText().length(), jFrameDocrosData.charHeight);
        //jLabel4.setLocation(12/*jButtonShow.getX()*/, jButtonShow.getY() + jFrameDocrosData.charHeight);
        //jFrameMaterial.add(jLabel4);
        
                

        
        jTextFieldMaterialModelName.setSize(jComboBoxLawName.getWidth(), jFrameDocrosData.charHeight);
        jTextFieldMaterialModelName.setLocation(jComboBoxLawName.getLocation());//getX(), jLabel4.getY());
        jTextFieldMaterialModelName.addFocusListener(new JTextFieldNameFocusListener(null, 0, jLabel2, jButtonParameters[1]));
        jTextFieldMaterialModelName.setVisible(true);
        jFrameMaterial.add(jTextFieldMaterialModelName);
    
        jLabel2.setSize(jFrameDocrosData.charWidth * jLabel2.getText().length(), jFrameDocrosData.charHeight);
        jLabel2.setLocation(jTextFieldMaterialModelName.getX(), jTextFieldMaterialModelName.getY() + jFrameDocrosData.charHeight);
        //jLabel2.setVisible(true);
        jLabel2.setVisible(false);
        jFrameMaterial.add(jLabel2);
        
        for(i=0; i<jButtonParameters.length; i++)
            jButtonParameters[i].setLocation(jButtonParameters[i].getX(), jLabel1.getY() + jFrameDocrosData.charHeight);
        
        jFrameMaterial.setSize(jFrameMaterialWidth, jFrameMaterialHeight);
        //jFrameMaterial.setSize(jFrameMaterial.getWidth(), jButtonParameters[0].getY() + 2 * jFrameDocrosData.charHeight + 15);
    }
}
