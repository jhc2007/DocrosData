
package docrosdata;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JTextField;

class JTextFieldFocusListener implements FocusListener {

    JButton jButton[];
    //int jButtonNotVisible; 
    JButton jButtonShow;
    double parameterValue;
    
    String text;
    
    public JTextFieldFocusListener(JButton[] jbutton, JButton jbshow, double pvalue) {
        
        jButton = jbutton;
        //jButtonNotVisible = jbnv;
        jButtonShow = jbshow;
        parameterValue = pvalue;
        //jLabelMessage = jlabel;
        text = ""; 
   }

    @Override
    public void focusGained(FocusEvent fe) {
        
        ((JTextField) fe.getSource()).selectAll();
        
        if(((JTextField) fe.getSource()).getName() == null || ((JTextField) fe.getSource()).getName().equals("DoublePositive")) {
           
            if(!((JTextField) fe.getSource()).getBackground().equals(Color.WHITE)) {
                ((JTextField) fe.getSource()).setBackground(Color.WHITE);
                if(text.equals("Insert number"))
                    ((JTextField) fe.getSource()).setText("");
                else
                    ((JTextField) fe.getSource()).setText(text);
                if(jButtonShow == null && ((JTextField) fe.getSource()).getName() != null)
                    jButton[0].setEnabled(true);
                    //jButton[0].setVisible(true);
                if(jButton[2].getText().equals("Cancel"))
                    jButton[1].setEnabled(true);
                else
                    jButton[2].setEnabled(true);
            }
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent fe) {
        //System.out.println(((JTextField) fe.getSource()).getName());
        //if(!((JTextField) fe.getSource()).getName().equals("Name")) {
            //double value = 0;
            //System.out.println(((JTextField) ke.getSource()).getText() + ke.getKeyChar());
        
        text = ((JTextField) fe.getSource()).getText();

        try {
            double jTextFieldValue = Double.valueOf(text);
            //System.out.println("Value is a double: " + value);
            if(jButtonShow != null && jTextFieldValue != parameterValue) {
                jButtonShow.setEnabled(false);
            }

        }
        catch(Exception e) {
            //System.out.println(((JTextField) fe.getSource()).getName()+ " value is not a double: ");
            if(text.equals("")) 
                ((JTextField) fe.getSource()).setText("Insert number");
            else
                ((JTextField) fe.getSource()).setText(text + " NOT A NUMBER");
            ((JTextField) fe.getSource()).setBackground(Color.yellow);
            if(jButtonShow != null)
                jButtonShow.setEnabled(false);
            else
                if(((JTextField) fe.getSource()).getName() == null || !((JTextField) fe.getSource()).getName().equals("DoublePositive"))
                    jButton[0].setEnabled(false);
            if(jButton[2].getText().equals("Cancel"))
                jButton[1].setEnabled(false);
            else
                jButton[2].setEnabled(false);                
        }
     }
}

