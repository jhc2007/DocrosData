
package docrosdata;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

class JTextFieldDoubleFocusListener implements FocusListener {

    JLabel jLabelMessage[];
    int messageIndex;
    JButton jButton0;
    JButton jButton1;
    String text;
    
    public JTextFieldDoubleFocusListener(JLabel[] jl, int index, JButton jb0, JButton jb1) {
        
        jLabelMessage = jl;
        messageIndex = index;
        jButton0 = jb0;
        jButton1 = jb1;
        text = "";
    }

    @Override
    public void focusGained(FocusEvent fe) {
        if(!((JTextField) fe.getSource()).getBackground().equals(Color.WHITE)) {
            ((JTextField) fe.getSource()).setBackground(Color.WHITE);
            ((JTextField) fe.getSource()).setText(text);
            if(jLabelMessage != null)
                jLabelMessage[messageIndex].setVisible(false);
            if(jButton0 != null)
                jButton0.setEnabled(true);
            if(jButton1 != null)
                jButton1.setEnabled(true);
       }
    }

    @Override
    public void focusLost(FocusEvent fe) {
        
        int i;
        for(i=0; i<jLabelMessage.length; i++)
            jLabelMessage[i].setVisible(false);
        text = ((JTextField) fe.getSource()).getText();
        try {
            //double value = 
            Double.valueOf(text);
        }
        catch(Exception e) {
            ((JTextField) fe.getSource()).setBackground(Color.yellow);
            if(jLabelMessage != null)
                jLabelMessage[messageIndex].setVisible(true);
            if(jButton0 != null)
                jButton0.setEnabled(false);
            if(jButton1 != null)
                jButton1.setEnabled(false);
            if(!text.equals("") && !((JTextField) fe.getSource()).getName().equals("AxialLoad") && !((JTextField) fe.getSource()).getName().equals("Tolerance")) 
                ((JTextField) fe.getSource()).setText(text + " NOT A NUMBER");

        }
     }
}