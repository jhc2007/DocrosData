
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class JFrameInputMainParameters extends JFrame {
    
    JFrameDocrosData jFrameDocrosData;
    Point location;
    JLabel jLabel[];
    JLabel jLabelMessage[];
    JTextField jTextField[];
    JComboBox jComboBox[];
    JButton jButton[];
    JButton jButtonHelp;
                                                        
    String jLabelText[] = {"Job Title", "SxS Graphics", "Axial Load", "Force Unit", "Lenght Unit", "Tolerance"};//, "Number of Layers", "Number of Layers to Analyse", "Layer Number"};
    String jButtonText[] = {"OK", "Close", "Next"};
    String jLabelMessageText[] = {"Insert title", "Insert axial load", "Insert tolerance"};//, "Insert number of layers", "Insert layer number"};
            
    public JFrameInputMainParameters(JFrameDocrosData jf, Point loc) {

        super("<MAIN_PARAMETERS>");
        jFrameDocrosData = jf;
        location = loc;
        init();
    }
    
    private void init() {
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        //this.setAlwaysOnTop(true);             
        this.setResizable(false);
        this.getContentPane().setLayout(null);
        
        int i, jLabelWidth, xPosition = 10, yPosition = 10;
        
        jLabelWidth = jFrameDocrosData.charWidth * (JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText));
        
        JLabel jLabelTitle = new JLabel("Main Parameters");
        //JLabel jLabelTitle = new JLabel("ρm ξ1");
        //jLabelTitle.setSize(jFrameDocrosData.charWidth * (jLabelTitle.getText().length() + 10), jFrameDocrosData.charHeight);
        jLabelTitle.setSize(2 * jLabelWidth + 40, jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(xPosition + jLabelWidth, yPosition);
        this.add(jLabelTitle);
        
        if(getClass().getResourceAsStream("/help/helpMainParameters.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("MainParameters"));//, "Main Parameters"));
            this.add(jButtonHelp);
        }
        yPosition += 30;
        
        jLabel = new JLabel[jLabelText.length];
        jTextField = new JTextField[3];
        jComboBox = new JComboBox[4];
        jLabelMessage = new JLabel[jLabelMessageText.length];
        jButton = new JButton[jButtonText.length];

        for(i=0; i<jLabelText.length; i++) {
            jLabel[i] = new JLabel(jLabelText[i]);
            jLabel[i].setSize(jFrameDocrosData.charWidth * jLabel[i].getText().length(), jFrameDocrosData.charHeight);
            if(i == 4)
                jLabel[i].setLocation(jLabelTitle.getX() + 100, yPosition);
            else {
                jLabel[i].setLocation(xPosition, yPosition);
            }
            if(i != 3)
                yPosition += 6 + jFrameDocrosData.charHeight;
            this.add(jLabel[i]);
        }
               
        yPosition = jLabel[0].getY();
                
        for(i=0; i<3; i++) {
            jTextField[i] = new JTextField();
            if(i == 0) {
                jTextField[i].setSize(jLabelTitle.getSize());
                jTextField[i].setName("NameMainParameters");
            }
            else {
                jTextField[i].setSize(7 * jFrameDocrosData.charWidth, jFrameDocrosData.charHeight);
                if(i == 1)
                    jTextField[i].setText("0.0");
                else {
                    jTextField[i].setText("10.0");
                    jTextField[i].setName("Tolerance");
                }
                //    jTextField[i].setName("AxialLoad");
            }
            jTextField[i].setLocation(jLabelTitle.getX(), yPosition);
            if(i < 2)
                yPosition += 6 + jFrameDocrosData.charHeight;
            jTextField[i].addKeyListener(new JTextFieldKeyListener());
            this.add(jTextField[i]);
            yPosition += 6 + jFrameDocrosData.charHeight;
        }      
                
        yPosition = jLabel[1].getY();
        
        for(i=0; i<3; i++) {
            jComboBox[i] = new JComboBox();
            jComboBox[i].setSize(7 * jFrameDocrosData.charWidth - 1, jFrameDocrosData.charHeight);
            if(i == 0 || i == 1) {
       
            }
            else {
   
                
            }
            
            if(i == 0) {
                jComboBox[i].setLocation(jLabelTitle.getX(), yPosition);
                yPosition += 12 + 2 * jFrameDocrosData.charHeight;
                jComboBox[i].addItem("Yes");
                jComboBox[i].addItem("No");
                jComboBox[i].setSelectedIndex(0);
            }
            else {
                if(i == 1) {
                    jComboBox[i].setLocation(jLabelTitle.getX(), yPosition);
                    jComboBox[i].addItem("N");
                    jComboBox[i].addItem("kN");
                    jComboBox[i].addItem("MN");
                    jComboBox[i].setSelectedIndex(0);
                }
                else {
                    jComboBox[i].setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - jComboBox[i].getWidth() - 1, yPosition);
                    yPosition += 12 + 2 * jFrameDocrosData.charHeight;
                    jComboBox[i].addItem("mm");
                    jComboBox[i].addItem("cm");
                    jComboBox[i].addItem("m");
                    jComboBox[i].setSelectedIndex(0);
                }
            }
            this.add(jComboBox[i]);
        }
        
        if(jFrameDocrosData.mainParameters != null) {
            jTextField[0].setText(jFrameDocrosData.mainParameters.title);
            jTextField[1].setText(String.valueOf(jFrameDocrosData.mainParameters.axialLoad));
            if(jFrameDocrosData.mainParameters.sxsGraphics.equals("_YES"))
                jComboBox[0].setSelectedIndex(0);
            else
                jComboBox[0].setSelectedIndex(1);
        }
                              
        if(jFrameDocrosData.unitsInputOutputData != null) {
            for(i=0; i<2; i++)
                jComboBox[i + 1].setSelectedItem(jFrameDocrosData.unitsInputOutputData[i]);
        }
        
        if(jFrameDocrosData.tolerances >= 0)
            jTextField[2].setText(String.valueOf(jFrameDocrosData.tolerances));
        
        yPosition += 6;// + jFrameDocrosData.charHeight;
        
        for(i=0; i<jLabelMessageText.length; i++) {
            jLabelMessage[i] = new JLabel(jLabelMessageText[i]);
            jLabelMessage[i].setSize(jLabelTitle.getSize());
            jLabelMessage[i].setLocation(xPosition + 2 * (jLabelWidth + jLabelTitle.getWidth()) / 3, yPosition);
            jLabelMessage[i].setForeground(Color.red);
            jLabelMessage[i].setVisible(false);
            this.add(jLabelMessage[i]);
        }
        
        yPosition += jFrameDocrosData.charHeight;
        
        for(i=0; i<jButtonText.length; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            if(i == 0) {
                jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, jFrameDocrosData.charHeight);
                jButton[i].setLocation(xPosition + 2 * jButton[i].getWidth(), yPosition);
            }
            else {
                if(i == 1)
                    yPosition += jFrameDocrosData.charHeight + 10;
                jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 2, 18);
                jButton[i].setLocation(xPosition + (i - 1) * jButton[1].getWidth(), yPosition);
            }
            this.add(jButton[i]);
        }
        if(jFrameDocrosData.mainParameters == null || jFrameDocrosData.unitsInputOutputData == null || jFrameDocrosData.tolerances < 0)
            jButton[2].setEnabled(false);
                 
        for(i = 0; i<jButtonText.length; i++)
            jButton[i].addActionListener(new JButtonInputMainParametersActionListener(this));            
   
        jTextField[0].addFocusListener(new JTextFieldNameFocusListener(jLabelMessage, 0, null, jButton[0]));
        jTextField[1].addFocusListener(new JTextFieldDoubleFocusListener(jLabelMessage, 1, jButton[0], null));
        jTextField[2].addFocusListener(new JTextFieldDoubleFocusListener(jLabelMessage, 2, jButton[0], null));
        
        
        yPosition += 2 * jFrameDocrosData.charHeight;
        
        this.setSize(jLabelWidth + jLabelTitle.getWidth() + 26, yPosition);
        this.setVisible(true);
    }
    
}
