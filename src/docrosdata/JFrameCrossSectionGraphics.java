
package docrosdata;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class JFrameCrossSectionGraphics extends JFrame {
    
    JFrameDocrosData jFrameDocrosData;
    JLabel jLabel[];
    JComboBox jComboBox[];
    JButton jButtonHelp;
    JButton jButton[];
    String jLabelText[] = {"X axis", "Y axis"};
    String jComboText[] = {"Select", "<html><u>C</u>urvature</html>", "<html><u>M</u>oment</html>", "<html><u>N</u>eutral Axis</html>", "<html>Crack <u>W</u>idth</html>"};
    String jButtonText[] = {"Open", "Close"};
    
    public JFrameCrossSectionGraphics(JFrameDocrosData jf) {
        
        super("Cross Section Graphics");
        jFrameDocrosData = jf;
        init();
    }
    
     private void init() {

        this.setLocation(15, 45);             
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, j, jLabelWidth, xPosition = 12, yPosition = 12;
        
        jLabelWidth = jFrameDocrosData.charWidth * (JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText));        
        
        JLabel jLabelTitle = new JLabel("Cross Section Graphics");
        jLabelTitle.setSize(jFrameDocrosData.charWidth * (jLabelTitle.getText().length() + 4), jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(xPosition + jLabelWidth, yPosition);
        this.add(jLabelTitle);
        
        yPosition += 33;
        
        if(getClass().getResourceAsStream("/help/helpCrossSectionGraphics.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("CrossSectionGraphics"));
            this.add(jButtonHelp);
        }
        
        jLabel = new JLabel[2];
        jComboBox = new JComboBox[2]; 
        for(i=0; i<2; i++) {
            jLabel[i] = new JLabel(jLabelText[i]);
            jLabel[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
            jLabel[i].setLocation(xPosition, yPosition);
            this.add(jLabel[i]);
            jComboBox[i] = new JComboBox();
            jComboBox[i].setName(jLabelText[i]);
            jComboBox[i].setSize(jLabelTitle.getWidth(), jFrameDocrosData.charHeight);
            jComboBox[i].setLocation(xPosition + jLabelWidth, yPosition);
            for(j=0; j<jComboText.length; j++) {
                jComboBox[i].addItem(jComboText[j]);
            }
            jComboBox[i].setSelectedIndex(0);
            this.add(jComboBox[i]);
            yPosition += 33;
        }
                
        yPosition += 33;
                
        jButton = new JButton[2];       
        for(i=0; i<2; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 2, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * jButton[0].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonCrossSectionGraphicsActionListener(this));
            this.add(jButton[i]);
        }
        jButton[0].setEnabled(false);
        
        jComboBox[0].addActionListener(new JComboBoxCrossSectionGraphics(this));
        jComboBox[1].addActionListener(new JComboBoxCrossSectionGraphics(this));
                                         
        //jButton[0].addActionListener(new JButtonStressStrainGraphicsActionListener(this));
        //jButton[1].addActionListener(new JButtonStressStrainGraphicsActionListener(this));
        
        yPosition += 63;
        
        this.setSize(jLabelTitle.getWidth() + jLabelWidth + 26, yPosition);
        this.setVisible(true);
   }

}
