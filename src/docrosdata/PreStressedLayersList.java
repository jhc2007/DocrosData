
package docrosdata;

public class PreStressedLayersList {
    
    PreStressedLayers first;
    PreStressedLayers last;
    int count;
    
    public PreStressedLayersList() {
        
        first = null;
        last = null;
        count = 0;
    }
    
    void delPreStressedLayers(int rangeNumber) {
        
        PreStressedLayers node = getPreStressedLayers(rangeNumber);
        PreStressedLayers node1, node2;
        node1 = first;
        node2 = node1.next;
        if(node1 == node) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && node2 != node) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
    }
    
    PreStressedLayers getPreStressedLayers(int rangeNumber) {
                
        int number = 1;
        PreStressedLayers node = first;
        while(node != null && number != rangeNumber) {
            node = node.next;
            number++;
        }
        return node;
    }
    
    void addPreStressedLayers(PreStressedLayers node) {
        
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
    
}
