
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

class JFrameInputMaterialModel extends JFrame {
    
    JFrameDocrosData jFrameDocrosData;
    Point location;
    
    JLabel jLabelType, jLabelName, jLabelMessage, jLabelCrackModel;
    JLabel jLabelParameterSymbol[], jLabelParameterUnit[];
        
    JTextField jTextFieldName;
    JTextField jTextFieldParameter[];
    
    JComboBox jComboBoxType;
    JComboBox jComboBoxName;
    
    JButton jButtonHelp;
    JButton jButton[];
    JRadioButton jRadioButton[];
    JButton jButtonPrev, jButtonNext, jButtonClose;
    
    JPanel jPanelImage;
    BufferedImage bufferedImageMaterial[];
    JLabel jLabelImage[];
    
    String jLabelText[] = {"Type", "Name", "Parameter   "};
    String jButtonText[] = {"Reset", "Add", "New"};
    String jRadioButtonText[] = {"Trilinear", "Quadrilinear"};
    String loadType[] = {" (Cyclic)", " (Cyclic)", " (Monotonic)", " (Monotonic)", " (Cyclic)", " (Cyclic)", " (Monotonic)", " (Cyclic/Monotonic)"};
    
    public JFrameInputMaterialModel(JFrameDocrosData jf, Point loc) {
        
        super("<NLMM>");
        jFrameDocrosData = jf;
        location = loc;
        init();      
    }
    
    private void init() {
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        this.setResizable(false);
        this.getContentPane().setLayout(null);
        
        int i, jLabelWidth, xPosition = 10, yPosition = 10;
        BufferedInputStream input;
        FileOutputStream output;
        File imageFile;
        byte[] buffer = new byte[256];
        int bytesRead;    
        
        jLabelWidth = jFrameDocrosData.charWidth * JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText);
        
        JLabel jLabelTitle = new JLabel("Material Properties");
        jLabelTitle.setSize(jFrameDocrosData.charWidth * (jLabelTitle.getText().length() + 8), jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(jLabelWidth + xPosition, yPosition);
        this.add(jLabelTitle);    
        
        jButtonHelp = new JButton("Help");
        jButtonHelp.setSize(63, 18);
        jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
        jButtonHelp.addActionListener(new JButtonHelpActionListener("MaterialModel"));//, "Material Model"));
        if(getClass().getResourceAsStream("/help/helpMaterialModel.pdf") != null)
            jButtonHelp.setVisible(true);
        else
            jButtonHelp.setVisible(false);
        this.add(jButtonHelp);
        
        yPosition += jFrameDocrosData.charHeight;
        
        jLabelType = new JLabel("Material Type");
        jLabelType.setSize(jLabelWidth, jFrameDocrosData.charHeight);
        jLabelType.setLocation(xPosition, yPosition);
        this.add(jLabelType);
        
        jLabelCrackModel = new JLabel("Crack Model:");
        jLabelCrackModel.setSize(jLabelWidth, 22);
        jLabelCrackModel.setVisible(false);
        this.add(jLabelCrackModel);
        
        jRadioButton = new JRadioButton[jRadioButtonText.length];
        for(i=0; i<jRadioButtonText.length; i++) {
            jRadioButton[i] = new JRadioButton(jRadioButtonText[i]);
            jRadioButton[i].setName(jRadioButtonText[i]);
            jRadioButton[i].setSize(jFrameDocrosData.charWidth * (jRadioButtonText[i].length() - 1), 22);
            //jRadioButton[i].setLocation(xPosition + jLabelLeftWidth + j * jRadioButton[0].getWidth(), yPosition);
            jRadioButton[i].addActionListener(new JRadioButtonIntupMaterialModelActionListener(this));
            jRadioButton[i].setVisible(false);
            this.add(jRadioButton[i]);
        }
        
        jComboBoxType = new JComboBox();
        jComboBoxType.setSize(jLabelTitle.getSize());
        jComboBoxType.setLocation(xPosition + jLabelWidth, yPosition);
        
        jComboBoxType.addItem("Select");
        //NonLinearMaterialModelType node = jFrameDocrosData.nonLinearMaterialModelTypeList.first;
        //while(node != null) {
        for(i=0; i<jFrameDocrosData.materials.length; i++)
            jComboBoxType.addItem(jFrameDocrosData.materials[i] + loadType[i]);
        jComboBoxType.setSelectedIndex(0);
        this.add(jComboBoxType);
        
        yPosition += jFrameDocrosData.charHeight;
        
        jLabelName = new JLabel("Material Name");
        jLabelName.setSize(jLabelWidth, jFrameDocrosData.charHeight);
        jLabelName.setLocation(xPosition, yPosition);
        this.add(jLabelName);
        
        jComboBoxName = new JComboBox();
        jComboBoxName.setSize(jLabelTitle.getSize());
        jComboBoxName.setLocation(xPosition + jLabelWidth, yPosition);
        jComboBoxName.addItem("Select");
        jComboBoxName.setSelectedIndex(0);
        //jComboBoxName.setVisible(false);
        this.add(jComboBoxName);
               
        jTextFieldName = new JTextField("");
        jTextFieldName.setSize(jComboBoxName.getSize());
        jTextFieldName.setLocation(jComboBoxName.getLocation());
        jTextFieldName.setName("NameMaterialModel");
        
        jTextFieldName.addKeyListener(new JTextFieldKeyListener());    
        jTextFieldName.setVisible(false);
        this.add(jTextFieldName);
        
        yPosition += jFrameDocrosData.charHeight;
        
        jLabelMessage = new JLabel("This name is already used");
        jLabelMessage.setSize(jTextFieldName.getSize());
        jLabelMessage.setLocation(jTextFieldName.getX(), yPosition);
        jLabelMessage.setVisible(false);
        this.add(jLabelMessage);      
        
        yPosition += jFrameDocrosData.charHeight;
        
        jButton = new JButton[3];
        
        for(i=0; i<3; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jComboBoxName.getWidth()) / 3, 25);
            jButton[i].setLocation(xPosition + i * jButton[i].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputMaterialModelActionListener(this));
            this.add(jButton[i]);
        }
 
        //yPosition += jFrameDocrosData.charHeight + 10;
        yPosition += 35;
        
        jButton[0].setVisible(false);
        jButton[1].setVisible(false);
        jButton[2].setEnabled(false);

        jButtonPrev = new JButton("Previous");
        jButtonPrev.setSize((jLabelWidth + jComboBoxName.getWidth()) / 3, 18);
        jButtonPrev.setLocation(xPosition, yPosition);
        jButtonPrev.addActionListener(new JButtonInputMaterialModelActionListener(this));
        this.add(jButtonPrev);
        
        jButtonClose = new JButton("Close");
        jButtonClose.setSize((jLabelWidth + jComboBoxName.getWidth()) / 3, 18);
        jButtonClose.setLocation(xPosition + jButtonPrev.getWidth(), yPosition);
        jButtonClose.addActionListener(new JButtonInputMaterialModelActionListener(this));
        this.add(jButtonClose);
        
        jButtonNext = new JButton("Next");
        jButtonNext.setSize((jLabelWidth + jComboBoxName.getWidth()) / 3, 18);
        jButtonNext.setLocation(xPosition + 2 * jButtonPrev.getWidth(), yPosition);
        jButtonNext.addActionListener(new JButtonInputMaterialModelActionListener(this));
        this.add(jButtonNext);
        if(!jFrameDocrosData.nonLinearMaterialModelTypeList.existsMaterialModel())
            jButtonNext.setEnabled(false);
        
        yPosition += 2 * jFrameDocrosData.charHeight;
        
        jPanelImage = new JPanel();
        jPanelImage.setLocation(20 + jTextFieldName.getX() + jTextFieldName.getWidth(), jButtonHelp.getY());
        jPanelImage.setBackground(Color.white);
        jPanelImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        this.add(jPanelImage);
        
        bufferedImageMaterial = new BufferedImage[jFrameDocrosData.materials.length];
        jLabelImage = new JLabel[jFrameDocrosData.materials.length];
        for(i=0; i<jFrameDocrosData.materials.length; i++) {
            if(getClass().getResourceAsStream("/images/image" + jFrameDocrosData.materials[i] + ".jpg") != null) {
                try {
                    input = new BufferedInputStream(getClass().getResourceAsStream("/images/image" + jFrameDocrosData.materials[i] + ".jpg"));
                    //imageFile = new File("C:" + File.separator + "Windows" + File.separator + "Temp" + File.separator + "image" + jFrameDocrosData.materials[i] + ".jpg");
                    imageFile = new File(System.getProperty("java.io.tmpdir") + "image" + jFrameDocrosData.materials[i] + ".jpg");
                    if(!imageFile.exists())
                        imageFile.createNewFile();
                    imageFile.deleteOnExit();
                    output = new FileOutputStream(imageFile);
                    while((bytesRead = input.read(buffer)) != -1)
                        output.write(buffer, 0, bytesRead);
                    output.close();
                    bufferedImageMaterial[i] = ImageIO.read(imageFile);
                } catch (IOException ex) {
                    Logger.getLogger(JFrameInputMaterialModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else
                bufferedImageMaterial[i] = null;
            jLabelImage[i] = new JLabel();
            jLabelImage[i].setVisible(false);
            jPanelImage.add(jLabelImage[i]);
        }                   
        jComboBoxType.addActionListener(new JComboBoxTypeInputMaterialModel(this));
        jComboBoxName.addActionListener(new JComboBoxNameInputMaterialModel(this));
        jTextFieldName.addFocusListener(new JTextFieldNameFocusListener(null, 0, jLabelMessage, jButton[2]));
        this.setSize(jLabelWidth + jLabelTitle.getWidth() + 26, yPosition);
        this.setVisible(true);
        
    }
    

}
