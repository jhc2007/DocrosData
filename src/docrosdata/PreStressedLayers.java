
package docrosdata;

public class PreStressedLayers {
    
    String layerPatternsName;
    int firstLayer;
    int lastLayer;
    double strain;
    PreStressedLayers next;
    
    public PreStressedLayers(String lpn, int first, int last, double str) {
        
        layerPatternsName = lpn;
        firstLayer = first;
        lastLayer = last;
        strain = str;
        next = null;
    }
    
}
