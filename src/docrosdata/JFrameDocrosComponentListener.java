
package docrosdata;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

class JFrameDocrosComponentListener implements ComponentListener {

    JFrameDocrosData jFrameDocrosData;
    
    public JFrameDocrosComponentListener(JFrameDocrosData jf) {
        
        jFrameDocrosData = jf;
    }

    @Override
    public void componentResized(ComponentEvent ce) {
        
        if(jFrameDocrosData.figureIsDrawn)
            jFrameDocrosData.drawFigure(jFrameDocrosData.phase);       
    }

    @Override
    public void componentMoved(ComponentEvent ce) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void componentShown(ComponentEvent ce) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void componentHidden(ComponentEvent ce) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
