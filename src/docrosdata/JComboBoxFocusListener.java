/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package docrosdata;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author Moi
 */
class JComboBoxFocusListener implements FocusListener {

    //JFrameInputLayerProperties jFrameInputLayerProperties;
    JLabel jLabelMessage[];
    int messageIndex;
    JButton jButton;
    
    public JComboBoxFocusListener(JLabel jl[], int index, JButton jb) {
        
        jLabelMessage = jl;
        messageIndex = index;
        jButton = jb;
    }

    @Override
    public void focusGained(FocusEvent fe) {
        
        int i;
        jButton.setEnabled(true);      
        if(messageIndex == -1) {
            for(i=0; i<jLabelMessage.length; i++)
                jLabelMessage[i].setVisible(false);
        }
        else
            jLabelMessage[messageIndex].setVisible(false);
    }

    @Override
    public void focusLost(FocusEvent fe) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
