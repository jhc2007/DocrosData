
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;

class JRadioButtonIntupMaterialModelActionListener implements ActionListener {
    
    JFrameInputMaterialModel jFrameInputMaterialModel;

    public JRadioButtonIntupMaterialModelActionListener(JFrameInputMaterialModel jf) {
        jFrameInputMaterialModel = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        jFrameInputMaterialModel.jButton[2].setEnabled(true);
        int i;
        if(((JRadioButton) ae.getSource()).getName().equals(jFrameInputMaterialModel.jRadioButtonText[0])) {
            if(jFrameInputMaterialModel.jRadioButton[0].isSelected()) {
                jFrameInputMaterialModel.jRadioButton[1].setSelected(false);
                for(i=12; i<14; i++) {
                    jFrameInputMaterialModel.jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                    jFrameInputMaterialModel.jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setText("0.0");
                    jFrameInputMaterialModel.jTextFieldParameter[i].setBackground(Color.white);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(false);
                }
            }
            else {
                jFrameInputMaterialModel.jRadioButton[1].setSelected(true);
                for(i=12; i<14; i++) {
                    jFrameInputMaterialModel.jLabelParameterSymbol[i].setForeground(Color.black);
                    jFrameInputMaterialModel.jLabelParameterUnit[i].setForeground(Color.black);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setText(null);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(true);
                }
             }
        }
        else {
            if(jFrameInputMaterialModel.jRadioButton[1].isSelected()) {
                jFrameInputMaterialModel.jRadioButton[0].setSelected(false); 
                for(i=12; i<14; i++) {
                    jFrameInputMaterialModel.jLabelParameterSymbol[i].setForeground(Color.black);
                    jFrameInputMaterialModel.jLabelParameterUnit[i].setForeground(Color.black);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setText(null);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(true);
                }
            }
            else {
                jFrameInputMaterialModel.jRadioButton[0].setSelected(true);
                for(i=12; i<14; i++) {
                    jFrameInputMaterialModel.jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                    jFrameInputMaterialModel.jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setText("0.0");
                    jFrameInputMaterialModel.jTextFieldParameter[i].setBackground(Color.white);
                    jFrameInputMaterialModel.jTextFieldParameter[i].setEnabled(false);
                }
            }
        } 
    }
    
}
