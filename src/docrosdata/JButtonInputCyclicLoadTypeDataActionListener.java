
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

class JButtonInputCyclicLoadTypeDataActionListener implements ActionListener {

    JFrameInputCyclicLoadTypeData jFrameInputCyclicLoadTypeData;
    
    public JButtonInputCyclicLoadTypeDataActionListener(JFrameInputCyclicLoadTypeData jf) {
        
        jFrameInputCyclicLoadTypeData = jf;
        //step = 0;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        int i;
        switch (ae.getActionCommand()) {
            case "Used":
                String cyclicLoadNames[];
                cyclicLoadNames = new String[jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList.count];
                CyclicLoadTypeData node = jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList.first;
                for(i=0; i<cyclicLoadNames.length; i++) {
                    cyclicLoadNames[i] = node.name;
                    node = node.next;
                }
                JFrameUsedNames jFrameUsedNames = new JFrameUsedNames("Used Cyclic Load Names:", cyclicLoadNames, jFrameInputCyclicLoadTypeData.getLocation());
                break;
            case "Reset":
                jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setSelectedIndex(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].getSelectedIndex());
                jFrameInputCyclicLoadTypeData.hideJPanelProp();
                break;
            case "Back":
                jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setSelectedIndex(0);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setVisible(true);
                jFrameInputCyclicLoadTypeData.jTextFieldMain[0].setVisible(false);
                jFrameInputCyclicLoadTypeData.jButtonUsedName.setVisible(false);
                jFrameInputCyclicLoadTypeData.jButtonMain[0].setVisible(false);
                break;
            case "New":
                for(i=0; i<jFrameInputCyclicLoadTypeData.jTextFieldMain.length; i++)
                    jFrameInputCyclicLoadTypeData.jTextFieldMain[i].setEnabled(true);
                jFrameInputCyclicLoadTypeData.jTextFieldMain[0].setVisible(true);
                jFrameInputCyclicLoadTypeData.jButtonUsedName.setVisible(true);
                if(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].getItemCount() == 1)
                    jFrameInputCyclicLoadTypeData.jButtonUsedName.setEnabled(false);
                else
                    jFrameInputCyclicLoadTypeData.jButtonUsedName.setEnabled(true);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setVisible(false);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[1].setEnabled(true);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[1].setSelectedIndex(0);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setEnabled(true);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setSelectedIndex(0);
                jFrameInputCyclicLoadTypeData.jButtonAddSteps.setEnabled(true);
                jFrameInputCyclicLoadTypeData.jButtonMain[1].setText("Back");
                jFrameInputCyclicLoadTypeData.jButtonMain[1].setVisible(true);
                jFrameInputCyclicLoadTypeData.jButtonMain[2].setText("OK");
                jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(true);
                break;
            case "Add":
                for(i=0; i<jFrameInputCyclicLoadTypeData.jLabelMessageMain.length; i++)
                    jFrameInputCyclicLoadTypeData.jLabelMessageMain[i].setVisible(false);
                if(jFrameInputCyclicLoadTypeData.jTextFieldMain[4].getText().equals("")) {// || jFrameInputCyclicLoadTypeData.jTextFieldMain[3].getText().equals("0")) {
                    jFrameInputCyclicLoadTypeData.jLabelMessageMain[1].setVisible(true);
                    jFrameInputCyclicLoadTypeData.jButtonAddSteps.setEnabled(false);
                }
                else {
                    jFrameInputCyclicLoadTypeData.showJPanelProp();
                    int numSteps = Integer.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldMain[4].getText());
                    jFrameInputCyclicLoadTypeData.cycles = new int[numSteps];
                    jFrameInputCyclicLoadTypeData.amplitude = new double[numSteps];
                    if(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].isVisible()) {
                        String name = String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].getSelectedItem());
                        CyclicLoadTypeData cyclicLoad = jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList.getCyclicLoadTypeData(name);
                        if(cyclicLoad.steps == numSteps) {
                            jFrameInputCyclicLoadTypeData.cycles = cyclicLoad.cycles;
                            jFrameInputCyclicLoadTypeData.amplitude = cyclicLoad.amplitude;
                            jFrameInputCyclicLoadTypeData.jTextFieldProp[0].setText(String.valueOf(cyclicLoad.cycles[0]));
                            jFrameInputCyclicLoadTypeData.jTextFieldProp[1].setText(String.valueOf(cyclicLoad.amplitude[0]));
                        }
                        else
                            for(i=0; i<numSteps; i++)
                                jFrameInputCyclicLoadTypeData.cycles[i] = -1;
                    }
                    else
                        for(i=0; i<numSteps; i++)
                            jFrameInputCyclicLoadTypeData.cycles[i] = -1;
                    jFrameInputCyclicLoadTypeData.jButtonProp[0].setVisible(false);
                    jFrameInputCyclicLoadTypeData.jLabelStepProp.setText("Step " + jFrameInputCyclicLoadTypeData.step + " / " + numSteps);
                    if(numSteps == 1)
                         jFrameInputCyclicLoadTypeData.jButtonProp[1].setText("Insert");          
                }
                break;
            case "Delete":
                deleteCyclicLoadTypeData();
                if(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].getItemCount() == 1) {
                    for(i=0; i<jFrameInputCyclicLoadTypeData.jTextFieldMain.length; i++)
                        jFrameInputCyclicLoadTypeData.jTextFieldMain[i].setEnabled(true);
                    jFrameInputCyclicLoadTypeData.jTextFieldMain[0].setVisible(true);
                    jFrameInputCyclicLoadTypeData.jButtonUsedName.setVisible(true);
                    if(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].getItemCount() == 1)
                        jFrameInputCyclicLoadTypeData.jButtonUsedName.setEnabled(false);
                    else
                        jFrameInputCyclicLoadTypeData.jButtonUsedName.setEnabled(true);
                    jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setVisible(false);
                    jFrameInputCyclicLoadTypeData.jComboBoxMain[1].setSelectedIndex(0);
                    jFrameInputCyclicLoadTypeData.jComboBoxMain[1].setEnabled(true);
                    jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setSelectedIndex(0);
                    jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setEnabled(true);
                    jFrameInputCyclicLoadTypeData.jButtonAddSteps.setEnabled(true);
                    //jFrameInputCyclicLoadTypeData.hideJPanelProp();
                    //jFrameInputCyclicLoadTypeData.jButtonMain[1].setText("Back");
                    jFrameInputCyclicLoadTypeData.jButtonMain[0].setVisible(false);
                    jFrameInputCyclicLoadTypeData.jButtonMain[1].setVisible(false);
                    //jFrameInputCyclicLoadTypeData.jButtonMain[0].setVisible(true);
                    jFrameInputCyclicLoadTypeData.jButtonMain[2].setText("OK");
                    jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(true);
                }
                break;
            case "Previous":
                if(((JButton) ae.getSource()).getName().equals("PrevProp")) {
                    
                    int numSteps = Integer.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldMain[4].getText());
                    for(i=0; i<jFrameInputCyclicLoadTypeData.jLabelMessagePropText.length; i++)
                        jFrameInputCyclicLoadTypeData.jLabelMessageProp[i].setVisible(false);
                    jFrameInputCyclicLoadTypeData.step--;
                    jFrameInputCyclicLoadTypeData.jButtonProp[1].setText("Next");
                    jFrameInputCyclicLoadTypeData.jButtonProp[1].setEnabled(true);
                    jFrameInputCyclicLoadTypeData.jLabelStepProp.setText("Step " + jFrameInputCyclicLoadTypeData.step + " / " + numSteps);
                    for(i=0; i<2; i++)
                        jFrameInputCyclicLoadTypeData.jTextFieldProp[i].setBackground(Color.white);
                    jFrameInputCyclicLoadTypeData.jTextFieldProp[0].setText(String.valueOf(jFrameInputCyclicLoadTypeData.cycles[jFrameInputCyclicLoadTypeData.step - 1]));
                    jFrameInputCyclicLoadTypeData.jTextFieldProp[1].setText(String.valueOf(jFrameInputCyclicLoadTypeData.amplitude[jFrameInputCyclicLoadTypeData.step - 1]));
                    //if(jFrameInputCyclicLoadTypeData.step == numSteps)
                    if(jFrameInputCyclicLoadTypeData.step == 1)
                        jFrameInputCyclicLoadTypeData.jButtonProp[0].setVisible(false);
                } 
                else {
                    jFrameInputCyclicLoadTypeData.dispose();
                    JFrameInputMonotonicLoadTypeData jFrameInputMonotonicLoadTypeData = new JFrameInputMonotonicLoadTypeData(jFrameInputCyclicLoadTypeData.jFrameDocrosData, jFrameInputCyclicLoadTypeData.getLocation());
                }
                break;
            case "Next":
                if(((JButton) ae.getSource()).getName().equals("NextProp")) {
                    if(cyclesInserted() && amplitudeInserted())
                        addCyclesAndAmplitude();
                }
                else {
                    jFrameInputCyclicLoadTypeData.dispose();
                    JFrameInputPhaseData jFrameInputPhaseData = new JFrameInputPhaseData(jFrameInputCyclicLoadTypeData.jFrameDocrosData, jFrameInputCyclicLoadTypeData.getLocation());
                }
                break;
            case "Close":
                jFrameInputCyclicLoadTypeData.dispose();
                break;
            case "Insert":
                if(cyclesInserted() && amplitudeInserted()) {
                    addCyclesAndAmplitude();
                    jFrameInputCyclicLoadTypeData.jButtonMain[0].setEnabled(true);
                    jFrameInputCyclicLoadTypeData.jButtonMain[1].setEnabled(true);
                    jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(true);
                }
                break;
            case "Cancel":
                jFrameInputCyclicLoadTypeData.hideJPanelProp();
                break;
            case "OK":
                if(jFrameInputCyclicLoadTypeData.jTextFieldMain[0].isVisible()) {
                    if(loadNameInserted() && layerPatternsNameSelected() && layerNumberSelected() && strainIncrementInserted() && cyclesAmplitudeDefined()) {
                        addCyclicLoadTypeData();
                        jFrameInputCyclicLoadTypeData.cycles = null;
                        jFrameInputCyclicLoadTypeData.amplitude = null;
                        jFrameInputCyclicLoadTypeData.jTextFieldMain[0].setVisible(false);
                        jFrameInputCyclicLoadTypeData.jButtonUsedName.setVisible(false);
                        jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setVisible(true);
                        jFrameInputCyclicLoadTypeData.jButtonMain[2].setText("New");
                        jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(true);
                    }
                }
                else {
                    if(layerPatternsNameSelected() && layerNumberSelected() && strainIncrementInserted() && cyclesAmplitudeDefined()) {
                        updateCyclicLoadTypeData();
                    }
                }
                break;
        }   
    }
    
    void deleteCyclicLoadTypeData() {
        
        String cyclicLoadName = String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].getSelectedItem());
        jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList.delCyclicLoadTypeData(cyclicLoadName);
        jFrameInputCyclicLoadTypeData.jComboBoxMain[0].removeItem(cyclicLoadName);
        jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setSelectedIndex(0);
        if(jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataList != null && jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataList.first != null) {
            PhaseData node = jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataList.first;
            while(node != null) {
                if(node.loadType.equals("_CYCLIC") && node.loadName.equals(cyclicLoadName)) {
                    jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataList.delPhaseData(node);
                    //jFrameInputCyclicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataListIndex] = true;
                    node = null;
                }
                else
                    node = node.next;
            }
        }
        if((jFrameInputCyclicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList == null || jFrameInputCyclicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.first == null) 
                && (jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList == null || jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList.first == null)) {
            jFrameInputCyclicLoadTypeData.jFrameDocrosData.jMenuItemEdit[10].setEnabled(false);
            jFrameInputCyclicLoadTypeData.jButtonMainNext.setEnabled(false);
        }
        //jFrameInputCyclicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataListIndex] = true;
        jFrameInputCyclicLoadTypeData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputCyclicLoadTypeData.jFrameDocrosData.dataFile != null)
            jFrameInputCyclicLoadTypeData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);

    }

    void updateCyclicLoadTypeData() {
        
        String cyclicLoadName = String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].getSelectedItem());
        CyclicLoadTypeData node = jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList.getCyclicLoadTypeData(cyclicLoadName);
        String layerPatternsName = String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[1].getSelectedItem());       
        int layerNumber = Integer.valueOf(String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[2].getSelectedItem()));
        double strain = Double.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldMain[3].getText());
        int steps = Integer.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldMain[4].getText());
        node.layerNumber = layerNumber;
        node.steps = steps;
        node.strainIncrement = strain;
        node.cycles = jFrameInputCyclicLoadTypeData.cycles;
        node.amplitude = jFrameInputCyclicLoadTypeData.amplitude;
        jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setSelectedIndex(0);
        //jFrameInputCyclicLoadTypeData.jButtonMain[0].setVisible(false);
        //if(jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataList != null && jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataList.getPhaseData("_CYCLIC", cyclicLoadName) != null)
        //    jFrameInputCyclicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataListIndex] = true;
        //jFrameInputCyclicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataListIndex] = true;
        jFrameInputCyclicLoadTypeData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputCyclicLoadTypeData.jFrameDocrosData.dataFile != null)
            jFrameInputCyclicLoadTypeData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    
    }
    
    void addCyclicLoadTypeData() {
        
        String cyclicLoadName = jFrameInputCyclicLoadTypeData.jTextFieldMain[0].getText();
        String layerPatternsName = String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[1].getSelectedItem());
        int layerNumber = Integer.valueOf(String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[2].getSelectedItem()));
        double strain = Double.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldMain[3].getText());
        int steps = Integer.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldMain[4].getText());
        CyclicLoadTypeData node = new CyclicLoadTypeData(cyclicLoadName, layerNumber, steps, strain);
        node.cycles = jFrameInputCyclicLoadTypeData.cycles;
        node.amplitude = jFrameInputCyclicLoadTypeData.amplitude;
        CyclicLoadTypeDataList list = jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList;
        if(list == null) {
            list = new CyclicLoadTypeDataList();
            jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList = list;
        }
        list.addCyclicLoadTypeData(node);
        jFrameInputCyclicLoadTypeData.jComboBoxMain[0].addItem(cyclicLoadName);
        jFrameInputCyclicLoadTypeData.jComboBoxMain[0].setSelectedIndex(0);
        jFrameInputCyclicLoadTypeData.jButtonMainNext.setEnabled(true);
        //if(jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataList != null && jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataList.getPhaseData("_CYCLIC", cyclicLoadName) != null)
        //    jFrameInputCyclicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputCyclicLoadTypeData.jFrameDocrosData.phaseDataListIndex] = true;
        jFrameInputCyclicLoadTypeData.jFrameDocrosData.jMenuItemEdit[10].setEnabled(true);
        //jFrameInputCyclicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataListIndex] = true;
        jFrameInputCyclicLoadTypeData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputCyclicLoadTypeData.jFrameDocrosData.dataFile != null)
            jFrameInputCyclicLoadTypeData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    Boolean cyclesAmplitudeDefined() {
        int i;
        if(jFrameInputCyclicLoadTypeData.jTextFieldMain[4].getText().equals("")) {// || jFrameInputCyclicLoadTypeData.jTextFieldMain[3].getText().equals("0")) {
            jFrameInputCyclicLoadTypeData.jLabelMessageMain[1].setVisible(true);
            jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(false);    
            return false;  
        }
        int numSteps = Integer.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldMain[4].getText());
        if(jFrameInputCyclicLoadTypeData.cycles == null || numSteps != jFrameInputCyclicLoadTypeData.cycles.length) {
            jFrameInputCyclicLoadTypeData.jLabelMessageMain[3].setVisible(true);
            jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(false);    
            return false;  
        }
        else {
            for(i=0; i<numSteps; i++) {
                if(jFrameInputCyclicLoadTypeData.cycles[i] == -1) {
                    jFrameInputCyclicLoadTypeData.jLabelMessageMain[3].setVisible(true);
                    jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(false);    
                    return false;
                }
            }
        }
        return true;
        /*
        else {
            for(i=0; i<numSteps; i++)
                if(jFrameInputCyclicLoadTypeData.cycles[i] < 0) {      
                    jFrameInputCyclicLoadTypeData.jLabelMessageMain[3].setVisible(true);
                    jFrameInputCyclicLoadTypeData.jButtonMain[1].setEnabled(false);    
                    return false; 
                }
            return true;
        }
        */
    }
 
    Boolean strainIncrementInserted() {
        
        if(jFrameInputCyclicLoadTypeData.jTextFieldMain[3].getText().equals("")) {
            jFrameInputCyclicLoadTypeData.jTextFieldMain[3].setBackground(Color.yellow);
            jFrameInputCyclicLoadTypeData.jLabelMessageMain[5].setVisible(true);
            jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(false);    
            return false;    
        }
        return true;
    }
    
    Boolean layerNumberSelected() {
        
        if(jFrameInputCyclicLoadTypeData.jComboBoxMain[2].getSelectedIndex() == 0) {
            jFrameInputCyclicLoadTypeData.jLabelMessageMain[6].setVisible(true);
            jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(false);    
            return false;
        }
        return true;
    }
    
    Boolean layerPatternsNameSelected() {
        
        if(jFrameInputCyclicLoadTypeData.jComboBoxMain[1].getSelectedIndex() == 0) {
            jFrameInputCyclicLoadTypeData.jLabelMessageMain[2].setVisible(true);
            jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(false);    
            return false;
        }
        return true;
    }
    
    Boolean loadNameInserted() {
        
        if(jFrameInputCyclicLoadTypeData.jTextFieldMain[0].getText().equals("")) {
            jFrameInputCyclicLoadTypeData.jTextFieldMain[0].setBackground(Color.yellow);
            jFrameInputCyclicLoadTypeData.jLabelMessageMain[4].setVisible(true);
            jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(false);
            return false;
        }
        if(existsCyclicLoadTypeName()) {
            jFrameInputCyclicLoadTypeData.jTextFieldMain[0].setBackground(Color.yellow);
            jFrameInputCyclicLoadTypeData.jLabelMessageMain[0].setVisible(true);
            jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean existsCyclicLoadTypeName() {
        
        CyclicLoadTypeDataList list = jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList;
        if(list == null)
            return false;
        CyclicLoadTypeData node = list.first;
        while(node != null) {
            if(node.name.equals(jFrameInputCyclicLoadTypeData.jTextFieldMain[0].getText()))
                return true;
            node = node.next;
        }
        return false;
    }
    
    Boolean cyclesInserted() {
        
        if(jFrameInputCyclicLoadTypeData.jTextFieldProp[0].getText().equals("")) {// || jFrameInputCyclicLoadTypeData.jTextFieldProp[0].getText().equals("0")) {
            jFrameInputCyclicLoadTypeData.jLabelMessageProp[0].setVisible(true);
            jFrameInputCyclicLoadTypeData.jTextFieldProp[0].setBackground(Color.yellow);
            //jFrameInputCyclicLoadTypeData.jButtonProp[0].setEnabled(false);
            jFrameInputCyclicLoadTypeData.jButtonProp[1].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean amplitudeInserted() {
        
        if(jFrameInputCyclicLoadTypeData.jTextFieldProp[1].getText().equals("")) {
           jFrameInputCyclicLoadTypeData.jLabelMessageProp[1].setVisible(true);
           jFrameInputCyclicLoadTypeData.jTextFieldProp[1].setBackground(Color.yellow);
           //jFrameInputCyclicLoadTypeData.jButtonProp[0].setEnabled(false);
           jFrameInputCyclicLoadTypeData.jButtonProp[1].setEnabled(false);
           return false;
        }
        return true;
    }
    
    void addCyclesAndAmplitude() {
        //int i;
        int numSteps = Integer.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldMain[4].getText());
        jFrameInputCyclicLoadTypeData.cycles[jFrameInputCyclicLoadTypeData.step - 1] = Integer.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldProp[0].getText());
        jFrameInputCyclicLoadTypeData.amplitude[jFrameInputCyclicLoadTypeData.step - 1] = Double.valueOf(jFrameInputCyclicLoadTypeData.jTextFieldProp[1].getText());
        jFrameInputCyclicLoadTypeData.jButtonProp[0].setVisible(true);
        jFrameInputCyclicLoadTypeData.step++;
        if(jFrameInputCyclicLoadTypeData.step <= numSteps && jFrameInputCyclicLoadTypeData.cycles[jFrameInputCyclicLoadTypeData.step - 1] > 0) {
            jFrameInputCyclicLoadTypeData.jTextFieldProp[0].setText(String.valueOf(jFrameInputCyclicLoadTypeData.cycles[jFrameInputCyclicLoadTypeData.step - 1]));
            jFrameInputCyclicLoadTypeData.jTextFieldProp[1].setText(String.valueOf(jFrameInputCyclicLoadTypeData.amplitude[jFrameInputCyclicLoadTypeData.step - 1]));
        }
        else {
            jFrameInputCyclicLoadTypeData.jTextFieldProp[0].setText("");
            jFrameInputCyclicLoadTypeData.jTextFieldProp[1].setText("");
        }
        jFrameInputCyclicLoadTypeData.jLabelStepProp.setText("Step " + jFrameInputCyclicLoadTypeData.step + " / " + numSteps);
        if(jFrameInputCyclicLoadTypeData.step == numSteps)
            jFrameInputCyclicLoadTypeData.jButtonProp[1].setText("Insert");
        if(jFrameInputCyclicLoadTypeData.step > numSteps)
            jFrameInputCyclicLoadTypeData.hideJPanelProp();
    }


}