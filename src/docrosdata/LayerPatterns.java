
package docrosdata;

public class LayerPatterns {
    
    String name;
    int numLayers;
    //int numPhases; // ???
    //int numProp; // ???
    int layerPropertiesPhase[];
    LayerProperties layerProperties[];
    LayerPatterns next;
    
    public LayerPatterns(String n, int nl, int nlp) {
        name = n;
        numLayers = nl;
        //numPhases = 0;
        //numProp = 0;
        layerProperties = new LayerProperties[nlp];
        layerPropertiesPhase = new int[nlp];
        next = null;
    }
    
    double widthLayerPatterns(int phase) {
        double width = 0;
        int i;
        for(i=0; i<layerProperties.length; i++)
            if(layerPropertiesPhase[i] <= phase)
                width += layerProperties[i].geoProp.width;
        return width;
    }
    
    double heightLayerPatterns(int phase) {
        double height, max = 0;
        int i;
        for(i=0; i<layerProperties.length; i++) {
            if(layerPropertiesPhase[i] <= phase) {
                height = numLayers * layerProperties[i].geoProp.thickness;
                if(height > max)
                    max = height;
            }
        }
        return max;
        //numLayers * layerProperties[i].geoProp.thickness
        //return (numLayers * layerProperties[indexLayerProperties].geoProp.thickness);
    }
}
