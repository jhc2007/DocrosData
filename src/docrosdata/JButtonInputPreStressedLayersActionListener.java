
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputPreStressedLayersActionListener implements ActionListener {
    
    JFrameInputPreStressedLayers jFrameInputPreStressedLayers;

    public JButtonInputPreStressedLayersActionListener(JFrameInputPreStressedLayers jf) {
        
        jFrameInputPreStressedLayers = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;
        switch (ae.getActionCommand()) {
            case "OK":
                if(jFrameInputPreStressedLayers.jComboBox[0].isEnabled()) {
                    if(patternNameSelected() && firstLayerSelected() && lastLayerSelected() && strainInserted())
                        updatePreStressedLayers();
                }
                else{
                    if(patternNameSelected() && firstLayerSelected() && lastLayerSelected() && strainInserted())
                        addPreStressedLayers(); 
                    jFrameInputPreStressedLayers.jButton[5].setEnabled(true);
                }
                break;
            case "New":
                //jFrameInputPreStressedLayers.jComboBox[0].setSelectedIndex(jFrameInputPreStressedLayers.jComboBox[0].getItemCount() - 1);
                jFrameInputPreStressedLayers.jComboBox[0].setSelectedIndex(0);
                jFrameInputPreStressedLayers.jComboBox[0].setEnabled(false);
                for(i=1; i<4; i++) {
                    jFrameInputPreStressedLayers.jComboBox[i].setEnabled(true);
                    jFrameInputPreStressedLayers.jComboBox[i].setSelectedIndex(0);
                }
                jFrameInputPreStressedLayers.jTextField.setEnabled(true);              
                jFrameInputPreStressedLayers.jButton[1].setText("Back");
                jFrameInputPreStressedLayers.jButton[1].setVisible(true);
                jFrameInputPreStressedLayers.jButton[2].setText("OK");
                break;
            case "Back":
                jFrameInputPreStressedLayers.jComboBox[0].setEnabled(true);
                jFrameInputPreStressedLayers.jComboBox[0].setSelectedIndex(0);
                break;
            case "Reset":
                jFrameInputPreStressedLayers.jComboBox[0].setSelectedIndex(jFrameInputPreStressedLayers.jComboBox[0].getSelectedIndex());               
                break;
            case "Delete":
                deletePreStressedLayers();
                break;
            case "Previous":
                jFrameInputPreStressedLayers.dispose();
                JFrameInputLayersToAnalyze jFrameInputLayersToAnalyze = new JFrameInputLayersToAnalyze(jFrameInputPreStressedLayers.jFrameDocrosData, jFrameInputPreStressedLayers.getLocation());                
                break;            
            case "Next":
                jFrameInputPreStressedLayers.dispose();
                JFrameInputMonotonicLoadTypeData jFrameInputMonotonicLoadTypeData = new JFrameInputMonotonicLoadTypeData(jFrameInputPreStressedLayers.jFrameDocrosData, jFrameInputPreStressedLayers.getLocation());
                break;
            case "Close":
                jFrameInputPreStressedLayers.dispose();
                break;
        }
    }
    
    void deletePreStressedLayers() {
        
        int i, range;
        range = jFrameInputPreStressedLayers.jComboBox[0].getSelectedIndex();
        //PreStressedLayers node = jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList.getPreStressedLayers(range);
        jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList.delPreStressedLayers(range);
        jFrameInputPreStressedLayers.jComboBox[0].removeAllItems();
        jFrameInputPreStressedLayers.jComboBox[0].addItem("Select");
        for(i=1; i<=jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList.count; i++)
            jFrameInputPreStressedLayers.jComboBox[0].addItem(i);           
        jFrameInputPreStressedLayers.jComboBox[0].setSelectedIndex(0);
        if(jFrameInputPreStressedLayers.jComboBox[0].getItemCount() == 1) {
            jFrameInputPreStressedLayers.jComboBox[0].setEnabled(false);
            for(i=1; i<4; i++) {
                jFrameInputPreStressedLayers.jComboBox[i].setEnabled(true);
                jFrameInputPreStressedLayers.jComboBox[i].setSelectedIndex(0);
            }
            jFrameInputPreStressedLayers.jTextField.setText(null);
            jFrameInputPreStressedLayers.jTextField.setBackground(Color.white);
            jFrameInputPreStressedLayers.jTextField.setEnabled(true);
            jFrameInputPreStressedLayers.jButton[2].setText("OK");
        }
        jFrameInputPreStressedLayers.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputPreStressedLayers.jFrameDocrosData.dataFile != null)
            jFrameInputPreStressedLayers.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void updatePreStressedLayers() {
        
        PreStressedLayers node = jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList.getPreStressedLayers(jFrameInputPreStressedLayers.jComboBox[0].getSelectedIndex());
        String patternName = String.valueOf(jFrameInputPreStressedLayers.jComboBox[1].getSelectedItem());
        int firstLayer = Integer.valueOf(String.valueOf(jFrameInputPreStressedLayers.jComboBox[2].getSelectedItem()));
        int lastLayer = Integer.valueOf(String.valueOf(jFrameInputPreStressedLayers.jComboBox[3].getSelectedItem()));
        double strain = Double.valueOf(jFrameInputPreStressedLayers.jTextField.getText());
        node.layerPatternsName = patternName;
        node.firstLayer = firstLayer;
        node.lastLayer = lastLayer;
        node.strain = strain;
        jFrameInputPreStressedLayers.jComboBox[0].setSelectedIndex(0);
        jFrameInputPreStressedLayers.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputPreStressedLayers.jFrameDocrosData.dataFile != null)
            jFrameInputPreStressedLayers.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void addPreStressedLayers() {
                
        String patternName = String.valueOf(jFrameInputPreStressedLayers.jComboBox[1].getSelectedItem());
        int firstLayer = Integer.valueOf(String.valueOf(jFrameInputPreStressedLayers.jComboBox[2].getSelectedItem()));
        int lastLayer = Integer.valueOf(String.valueOf(jFrameInputPreStressedLayers.jComboBox[3].getSelectedItem()));
        double strain = Double.valueOf(jFrameInputPreStressedLayers.jTextField.getText());
        PreStressedLayers node = new PreStressedLayers(patternName, firstLayer, lastLayer, strain);
        if(jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList == null)
            jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList = new PreStressedLayersList();
        jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList.addPreStressedLayers(node);
        jFrameInputPreStressedLayers.jComboBox[0].addItem(jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList.count);
        jFrameInputPreStressedLayers.jComboBox[0].setSelectedIndex(0);
        jFrameInputPreStressedLayers.jComboBox[0].setEnabled(true);
        jFrameInputPreStressedLayers.jButton[2].setText("New");
        jFrameInputPreStressedLayers.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputPreStressedLayers.jFrameDocrosData.dataFile != null)
            jFrameInputPreStressedLayers.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    Boolean strainInserted() {
        
        if(jFrameInputPreStressedLayers.jTextField.getText().equals("")) {
            jFrameInputPreStressedLayers.jTextField.setBackground(Color.yellow);
            jFrameInputPreStressedLayers.jLabelMessage[3].setVisible(true);
            jFrameInputPreStressedLayers.jButton[2].setEnabled(false);    
            return false;    
        }
        return true;
    }
    
    Boolean patternNameSelected() {
        
        if(jFrameInputPreStressedLayers.jComboBox[1].getSelectedIndex() == 0) {
            jFrameInputPreStressedLayers.jLabelMessage[0].setVisible(true);
            jFrameInputPreStressedLayers.jButton[2].setEnabled(false);    
            return false;
        }
        return true;
    }
    
    Boolean firstLayerSelected() {
        
        if(jFrameInputPreStressedLayers.jComboBox[2].getSelectedIndex() == 0) {
            jFrameInputPreStressedLayers.jLabelMessage[1].setVisible(true);
            jFrameInputPreStressedLayers.jButton[2].setEnabled(false);    
            return false;
        }
        return true;
    }
    
    Boolean lastLayerSelected() {
        
        if(jFrameInputPreStressedLayers.jComboBox[3].getSelectedIndex() == 0) {
            jFrameInputPreStressedLayers.jLabelMessage[2].setVisible(true);
            jFrameInputPreStressedLayers.jButton[2].setEnabled(false);    
            return false;
        }
        return true;
    }
}
