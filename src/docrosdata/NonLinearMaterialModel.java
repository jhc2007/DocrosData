/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package docrosdata;

/**
 *
 * @author Moi
 */
public class NonLinearMaterialModel {
   
    String type; //Keyword without '_'
    String name;
    double parameters[];
    int countLayerPatterns;
    NonLinearMaterialModel next;
    
    public NonLinearMaterialModel(String t, String n, double p[]) {
        
        type = t;
        name = n;
        parameters = p;
        countLayerPatterns = 0;
        next = null;
    }
    
}
