
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JMenuItemViewActionListener implements ActionListener {

    JFrameDocrosData jFrameDocrosData;
    int viewPhase;
    
    public JMenuItemViewActionListener(JFrameDocrosData jfdd, int phase) {
        
        jFrameDocrosData = jfdd;
        viewPhase = phase;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
       
        jFrameDocrosData.phase = viewPhase;
        jFrameDocrosData.drawFigure(viewPhase);
    }
    
}
