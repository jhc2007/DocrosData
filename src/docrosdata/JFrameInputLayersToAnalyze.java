
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

public class JFrameInputLayersToAnalyze extends JFrame {
    
    JFrameDocrosData jFrameDocrosData;
    Point location;
    JList jList1;
    JList jList2;
    JScrollPane jScrollPane1;
    JScrollPane jScrollPane2;
    JButton jButton[];
    JButton jButtonHelp;
    JLabel jLabelMessage[];
    
    String jLabelText[] = {"Layers", "Number", "Layers", "Analyzed"};
    String jButtonText[] = {"Add", "Remove", "OK", "Previous", "Close", "Next"};
    String jLabelMessageText[] = {"Select layers to add", "Select layers to remove"};
    
    public JFrameInputLayersToAnalyze(JFrameDocrosData jf, Point loc) {
        
        super("<LAYER-STRESS-STRAIN-GRAPHICS>");
        jFrameDocrosData = jf;
        location = loc;
        init();  
    }
    
    private void init() {
     
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        this.setResizable(false);
        this.getContentPane().setLayout(null);
        
        
        int i, xPosition = 15, yPosition = 10;
        
        JLabel jLabelTitle = new JLabel("Layers to Stress-Strain Graphics");
        jLabelTitle.setSize(jLabelTitle.getText().length() * jFrameDocrosData.charWidth, jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(xPosition, yPosition);
        this.add(jLabelTitle);
        
        if(getClass().getResourceAsStream("/help/helpLayersToAnalyze.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(6 + jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("LayersToAnalyze"));//, "Layers to Stress-Strain Graphics"));
            this.add(jButtonHelp);
        }
        yPosition += 2 * jFrameDocrosData.charHeight;
        
        JLabel jLabel[];
        jLabel = new JLabel[4];
        for(i=0; i<4; i++) {
            jLabel[i] = new JLabel(jLabelText[i]);
            jLabel[i].setSize(60, jFrameDocrosData.charHeight);
            switch (i) {
                case 0:
                    jLabel[i].setLocation(xPosition, yPosition);
                    break;
                case 1:
                    jLabel[i].setLocation(xPosition, yPosition + 20);
                    break;
                case 2:
                    jLabel[i].setLocation(188, yPosition);
                    break;
                case 3:
                    jLabel[i].setLocation(188, yPosition + 20);
                    break;
            }
            this.add(jLabel[i]);
        }
               
        yPosition += 8;
        
        jScrollPane1 = new JScrollPane();
        jScrollPane1.setSize(50, 200);
        jScrollPane1.setLocation(xPosition + 70, yPosition);
        this.add(jScrollPane1);
                
        jScrollPane2 = new JScrollPane();
        jScrollPane2.setSize(50, 200);
        jScrollPane2.setLocation(jLabel[3].getX() + 70, yPosition);
        this.add(jScrollPane2);
        
        jList1 = new JList();
        String layerNumberList[];
        if(jFrameDocrosData.layerPatternsList != null && jFrameDocrosData.layerPatternsList.first != null) {
            layerNumberList = new String[jFrameDocrosData.layerPatternsList.getNumberOfLayers()];
            for(i=0; i<layerNumberList.length; i++)
                layerNumberList[i] = String.valueOf(i + 1);
            jList1.setModel(new JListLayersNumberModel(layerNumberList));
        }
        /*
        //**********************************************************************       
        layerNumberList = new String[20];
        for(i=0; i<20; i++)
            layerNumberList[i] = String.valueOf(i + 1);
        //**********************************************************************
        */
        jScrollPane1.setViewportView(jList1);
                        
        jList2 = new JList();
        if(jFrameDocrosData.layerStressStrainGraphics != null) {
            String layerAnalysedList[];
            layerAnalysedList = new String[jFrameDocrosData.layerStressStrainGraphics.length];
            for(i=0; i<jFrameDocrosData.layerStressStrainGraphics.length; i++)
                layerAnalysedList[i] = String.valueOf(jFrameDocrosData.layerStressStrainGraphics[i]);
            jList2.setModel(new JListLayersNumberModel(layerAnalysedList));
            
        }
        jScrollPane2.setViewportView(jList2);
        
        yPosition += 210;
  
        jLabelMessage = new JLabel[jLabelMessageText.length];
        for(i=0; i<jLabelMessageText.length; i++) {
            jLabelMessage[i] = new JLabel(jLabelMessageText[i]);
            jLabelMessage[i].setSize(jLabelTitle.getSize());
            jLabelMessage[i].setLocation(xPosition, yPosition);
            jLabelMessage[i].setForeground(Color.red);
            jLabelMessage[i].setVisible(false);
            this.add(jLabelMessage[i]);
        }
        
        yPosition += jFrameDocrosData.charHeight;
        
        jButton = new JButton[jButtonText.length];
        for(i=0; i<3; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize(98, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * 98, yPosition);
            jButton[i].addActionListener(new JButtonInputLayersToAnalyzeActionListener(this));
            this.add(jButton[i]);
        }      
        
        yPosition += 10 + jFrameDocrosData.charHeight;
        
        for(i=3; i<6; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize(98, 18);
            jButton[i].setLocation(xPosition + (i - 3) * 98, yPosition);
            jButton[i].addActionListener(new JButtonInputLayersToAnalyzeActionListener(this));
            this.add(jButton[i]);   
        }
        if(jList2.getModel().getSize() == 0) {
            jButton[1].setEnabled(false);
            jButton[2].setEnabled(false);
            jButton[5].setEnabled(false);
        }
        
        jList1.addFocusListener(new JListFocusListener(jLabelMessage, jButton[0], jList1));
        jList2.addFocusListener(new JListFocusListener(jLabelMessage, jButton[1], jList2));
        
        yPosition += 2 * jFrameDocrosData.charHeight;       
      
        this.setSize(331, yPosition + 4);
        this.setVisible(true);
    }

    private static class JListFocusListener implements FocusListener {

        JLabel jLabelMessage[];
        JButton jButton;
        JList jList;
        
        public JListFocusListener(JLabel[] jlabel, JButton jbutton, JList jlist) {
            jLabelMessage = jlabel;
            jButton = jbutton;
            jList = jlist;
        }

        @Override
        public void focusGained(FocusEvent fe) {
            int i;
            for(i=0; i<jLabelMessage.length; i++)
                jLabelMessage[i].setVisible(false);
            if(jList.getModel().getSize() > 0)
                jButton.setEnabled(true);
        }

        @Override
        public void focusLost(FocusEvent fe) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    
}
