
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class JComboBoxCrossSectionGraphics implements ActionListener {

    JFrameCrossSectionGraphics jFrameCrossSectionGraphics;
    
    public JComboBoxCrossSectionGraphics(JFrameCrossSectionGraphics jf) {
        jFrameCrossSectionGraphics = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;
        if(((JComboBox) ae.getSource()).getName().startsWith("X")) {
            //if(jFrameCrossSectionGraphics.jComboBox[1].getSelectedIndex() == 0) {
            jFrameCrossSectionGraphics.jComboBox[1].removeAllItems();
            if(jFrameCrossSectionGraphics.jComboBox[0].getSelectedIndex() == 0) {
                jFrameCrossSectionGraphics.jComboBox[1].addItem(jFrameCrossSectionGraphics.jComboText[0]);
                jFrameCrossSectionGraphics.jButton[0].setEnabled(false);
            }
            else {
                for(i=0; i<jFrameCrossSectionGraphics.jComboText.length; i++) {
                    if(i != jFrameCrossSectionGraphics.jComboBox[0].getSelectedIndex())
                        jFrameCrossSectionGraphics.jComboBox[1].addItem(jFrameCrossSectionGraphics.jComboText[i]);
                }                
            }
        }
        else {
            if(jFrameCrossSectionGraphics.jComboBox[1].getSelectedIndex() == 0)
                jFrameCrossSectionGraphics.jButton[0].setEnabled(false);
            else{
                if(jFrameCrossSectionGraphics.jComboBox[0].getSelectedIndex() > 0)
                    jFrameCrossSectionGraphics.jButton[0].setEnabled(true);
            }
        }
    }
}
