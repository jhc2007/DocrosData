
package docrosdata;

public class DocrosData {

    public static void main(String[] args) {
        
        String material[] = {
            "NLMM101",
            "NLMM102",
            "NLMM103",
            "NLMM104",
            "NLMM105",
            "NLMM201",
            "NLMM202",
            "NLMM301"
        };
        
        String materialColor[] = {
            "gray", //NLMM101
            "gray", //NLMM102
            "gray", //NLMM103
            "gray", //NLMM104
            "gray", //NLMM105
            "blue", //NLMM201
            "blue", //NLMM202
            "green" //NLMM301
        };
        
        int materialNumberParameters[] = {7, 13, 7, 13, 16, 8, 8, 4};

        String materialParametersSymbols[][] = {
            {"Ec", "εcc", "fcc", "εccr-", "εct", "fct", "εccr+"},                                                           //NLMM101
            {"Ec", "εcc", "fcc", "εccr", "εct1", "fct1", "εct2", "fct2", "εct3", "fct3", "εct4", "fct4", "εct5"},           //NLMM102
            {"Ec", "εcc", "fcc", "εccr-", "εct", "fct", "εccr+"},                                                           //NLMM103
            {"Ec", "εcc", "fcc", "εccr", "εct1", "fct1", "εct2", "fct2", "εct3", "fct3", "εct4", "fct4", "εct5"},           //NLMM104
            {"Ec", "εcc", "fcc", "εccr", "εct1", "fct1", "εct2", "fct2", "α1", "ω1", "α2", "ω2", "α3", "ω3", "ω4", "lcs"},  //NLMM105
            {"Es", "Esh", "εsy", "fsy", "εsh", "fsh", "εsu", "fsu"},                                                        //NLMM201
            {"Es", "Esh", "εsy", "fsy", "εsh", "fsh", "εsu", "fsu"},                                                        //NLMM202
            {"ffmax", "Ef", "εfmax", "CE"},                                                                                 //NLMM301
        };
               
        JFrameDocrosData jFrameDocros;
        jFrameDocros = new JFrameDocrosData(material, materialColor, materialNumberParameters, materialParametersSymbols);
        jFrameDocros.setVisible(true);
    }
}
