
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class JComboBoxInputCyclicLoadTypeData implements ActionListener {

    JFrameInputCyclicLoadTypeData jFrameInputCyclicLoadTypeData;
    
    public JComboBoxInputCyclicLoadTypeData(JFrameInputCyclicLoadTypeData jf) {
        
        jFrameInputCyclicLoadTypeData = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        int i;
        for(i=0; i<jFrameInputCyclicLoadTypeData.jLabelMessageMain.length; i++)
            jFrameInputCyclicLoadTypeData.jLabelMessageMain[i].setVisible(false);
        jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(true);
        
        if(((JComboBox) ae.getSource()).getName() == null) {
            if(((JComboBox) ae.getSource()).getSelectedIndex() == 0) {
                jFrameInputCyclicLoadTypeData.hideJPanelProp();
                for(i=0; i<jFrameInputCyclicLoadTypeData.jTextFieldMain.length; i++) {
                    jFrameInputCyclicLoadTypeData.jTextFieldMain[i].setText("");
                    jFrameInputCyclicLoadTypeData.jTextFieldMain[i].setBackground(Color.white);
                    jFrameInputCyclicLoadTypeData.jTextFieldMain[i].setEnabled(false);
                }
                jFrameInputCyclicLoadTypeData.jComboBoxMain[1].setSelectedIndex(0);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[1].setEnabled(false);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setSelectedIndex(0);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setEnabled(false);
                jFrameInputCyclicLoadTypeData.jButtonAddSteps.setEnabled(false);
                jFrameInputCyclicLoadTypeData.jButtonMain[0].setVisible(false);
                jFrameInputCyclicLoadTypeData.jButtonMain[1].setVisible(false);
                jFrameInputCyclicLoadTypeData.jButtonMain[2].setText("New");
            }
            else {
                String name = String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[0].getSelectedItem());
                CyclicLoadTypeData node = jFrameInputCyclicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList.getCyclicLoadTypeData(name);
                for(i=0; i<jFrameInputCyclicLoadTypeData.jTextFieldMain.length; i++) {
                    jFrameInputCyclicLoadTypeData.jTextFieldMain[i].setBackground(Color.white);
                    jFrameInputCyclicLoadTypeData.jTextFieldMain[i].setEnabled(true); 
                }    
                jFrameInputCyclicLoadTypeData.jComboBoxMain[1].setSelectedItem(jFrameInputCyclicLoadTypeData.jFrameDocrosData.layerPatternsList.getLayerPatternsName(node.layerNumber));
                jFrameInputCyclicLoadTypeData.jComboBoxMain[1].setEnabled(true);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setSelectedItem(node.layerNumber);
                jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setEnabled(true);
                jFrameInputCyclicLoadTypeData.jTextFieldMain[3].setText(String.valueOf(node.strainIncrement));
                jFrameInputCyclicLoadTypeData.jTextFieldMain[4].setText(String.valueOf(node.steps));           
                jFrameInputCyclicLoadTypeData.cycles = new int[node.steps];
                jFrameInputCyclicLoadTypeData.cycles = node.cycles;
                jFrameInputCyclicLoadTypeData.amplitude = new double[node.steps];
                jFrameInputCyclicLoadTypeData.amplitude = node.amplitude;
                jFrameInputCyclicLoadTypeData.jButtonAddSteps.setEnabled(true);
                jFrameInputCyclicLoadTypeData.jButtonMain[0].setText("Delete");
                jFrameInputCyclicLoadTypeData.jButtonMain[0].setEnabled(true);
                jFrameInputCyclicLoadTypeData.jButtonMain[0].setVisible(true);
                jFrameInputCyclicLoadTypeData.jButtonMain[1].setText("Reset");
                jFrameInputCyclicLoadTypeData.jButtonMain[1].setEnabled(true);
                jFrameInputCyclicLoadTypeData.jButtonMain[1].setVisible(true);
                jFrameInputCyclicLoadTypeData.jButtonMain[2].setText("OK");
                //jFrameInputCyclicLoadTypeData.jButtonMain[2].setEnabled(true);
            }
        }
        else {
            jFrameInputCyclicLoadTypeData.jComboBoxMain[2].removeAllItems();
            jFrameInputCyclicLoadTypeData.jComboBoxMain[2].addItem("Select");
            jFrameInputCyclicLoadTypeData.jComboBoxMain[2].setSelectedIndex(0);
            if(jFrameInputCyclicLoadTypeData.jComboBoxMain[1].getSelectedIndex() > 0) {
                String layerPatternsName = String.valueOf(jFrameInputCyclicLoadTypeData.jComboBoxMain[1].getSelectedItem());
                int firstLayer, lastLayer;
                firstLayer = jFrameInputCyclicLoadTypeData.jFrameDocrosData.layerPatternsList.getFirstLayer(layerPatternsName);
                lastLayer = jFrameInputCyclicLoadTypeData.jFrameDocrosData.layerPatternsList.getLastLayer(layerPatternsName);
                for(i=firstLayer; i<=lastLayer; i++)
                    jFrameInputCyclicLoadTypeData.jComboBoxMain[2].addItem(i);
            }
        }
    }
    
}
