
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class JComboBoxInputPreStressedLayers implements ActionListener {

    JFrameInputPreStressedLayers jFrameInputPreStressedLayers;
    
    public JComboBoxInputPreStressedLayers(JFrameInputPreStressedLayers jf) {
        
        jFrameInputPreStressedLayers = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;    
        for(i=0; i<jFrameInputPreStressedLayers.jLabelMessage.length; i++)
            jFrameInputPreStressedLayers.jLabelMessage[i].setVisible(false);
        jFrameInputPreStressedLayers.jButton[2].setEnabled(true);
        
        if(((JComboBox) ae.getSource()).getName() == null) {
            //Pre-Stressed Range
            jFrameInputPreStressedLayers.jTextField.setText(null);
            jFrameInputPreStressedLayers.jTextField.setBackground(Color.white);
            if(jFrameInputPreStressedLayers.jComboBox[0].getSelectedIndex() == 0) {
                jFrameInputPreStressedLayers.jComboBox[1].setSelectedIndex(0);
                for(i=1; i<4; i++) {
                    jFrameInputPreStressedLayers.jComboBox[i].setEnabled(false);
                    jFrameInputPreStressedLayers.jComboBox[i].setSelectedIndex(0);
                }
                jFrameInputPreStressedLayers.jTextField.setText(null);
                jFrameInputPreStressedLayers.jTextField.setBackground(Color.white);
                jFrameInputPreStressedLayers.jTextField.setEnabled(false);
                jFrameInputPreStressedLayers.jButton[0].setVisible(false);
                jFrameInputPreStressedLayers.jButton[1].setVisible(false);
                jFrameInputPreStressedLayers.jButton[2].setText("New");               
            }
            else {
                if(jFrameInputPreStressedLayers.jComboBox[0].getSelectedIndex() > 0) {
                    for(i=1; i<4; i++)
                        jFrameInputPreStressedLayers.jComboBox[i].setEnabled(true);
                    jFrameInputPreStressedLayers.jTextField.setEnabled(true);
                    PreStressedLayers node = jFrameInputPreStressedLayers.jFrameDocrosData.preStressedLayersList.getPreStressedLayers(jFrameInputPreStressedLayers.jComboBox[0].getSelectedIndex());
                    jFrameInputPreStressedLayers.jComboBox[1].setSelectedItem(node.layerPatternsName);
                    jFrameInputPreStressedLayers.jComboBox[2].setSelectedItem(node.firstLayer);
                    jFrameInputPreStressedLayers.jComboBox[3].setSelectedItem(node.lastLayer);
                    jFrameInputPreStressedLayers.jTextField.setText(String.valueOf(node.strain));
                    jFrameInputPreStressedLayers.jButton[0].setText("Delete");
                    jFrameInputPreStressedLayers.jButton[0].setVisible(true);
                    jFrameInputPreStressedLayers.jButton[1].setText("Reset");
                    jFrameInputPreStressedLayers.jButton[1].setVisible(true);
                    jFrameInputPreStressedLayers.jButton[2].setText("OK");                
                }
            }
        }
        else {
            int firstLayer, lastLayer;
            String patternName;
            if(((JComboBox) ae.getSource()).getName().equals("PatternName")) {
                //Pattern Name
                for(i=2; i<4; i++) {
                    jFrameInputPreStressedLayers.jComboBox[i].removeAllItems();
                    jFrameInputPreStressedLayers.jComboBox[i].addItem("Select");
                    jFrameInputPreStressedLayers.jComboBox[i].setSelectedIndex(0);
                }
                if(jFrameInputPreStressedLayers.jComboBox[1].getSelectedIndex() > 0) {                   
                    patternName = String.valueOf(jFrameInputPreStressedLayers.jComboBox[1].getSelectedItem());
                    firstLayer = jFrameInputPreStressedLayers.jFrameDocrosData.layerPatternsList.getFirstLayer(patternName);     
                    lastLayer = jFrameInputPreStressedLayers.jFrameDocrosData.layerPatternsList.getLastLayer(patternName);
                    for(i=firstLayer; i<=lastLayer; i++) {
                        jFrameInputPreStressedLayers.jComboBox[2].addItem(i);
                        jFrameInputPreStressedLayers.jComboBox[3].addItem(i);
                    }
                }
            }
            else { //First Layer
                jFrameInputPreStressedLayers.jComboBox[3].removeAllItems();
                jFrameInputPreStressedLayers.jComboBox[3].addItem("Select");
                if(jFrameInputPreStressedLayers.jComboBox[2].getSelectedIndex() > 0) {
                    firstLayer = Integer.valueOf(String.valueOf(jFrameInputPreStressedLayers.jComboBox[2].getSelectedItem()));
                    lastLayer = Integer.valueOf(String.valueOf(jFrameInputPreStressedLayers.jComboBox[2].getItemAt(jFrameInputPreStressedLayers.jComboBox[2].getItemCount() - 1)));
                    for(i=firstLayer; i<=lastLayer; i++) 
                        jFrameInputPreStressedLayers.jComboBox[3].addItem(i);
                }
                else {
                    for(i=1; i<jFrameInputPreStressedLayers.jComboBox[2].getItemCount(); i++)
                        jFrameInputPreStressedLayers.jComboBox[3].addItem(jFrameInputPreStressedLayers.jComboBox[2].getItemAt(i));
                }
            }
        }
    }
    
}
