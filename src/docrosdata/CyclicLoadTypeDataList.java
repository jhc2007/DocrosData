
package docrosdata;

public class CyclicLoadTypeDataList {
    
    CyclicLoadTypeData first;
    CyclicLoadTypeData last;
    int count;
    
    public CyclicLoadTypeDataList() {
        
        first = null;
        last = null;
        count = 0;
    }
    
    void addCyclicLoadTypeData(CyclicLoadTypeData node) {
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }

    CyclicLoadTypeData getCyclicLoadTypeData(String nam) {
        
        CyclicLoadTypeData node = first;
        while(node != null && !node.name.equals(nam))
            node = node.next;
        return node;
    }
    
    void delCyclicLoadTypeData(String n) {
        
        CyclicLoadTypeData node1, node2;
        node1 = first;
        node2 = node1.next;
        if(node1.name.equals(n)) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && !node2.name.equals(n)) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
    }
    
    void printCyclicLoadTypeDataList() {
        
        int j, i = 1;
        CyclicLoadTypeData node = first;
        while(node != null) {
            System.out.println(i + " " + node.name + " " + node.layerNumber + " " + node.steps + " " + node.strainIncrement);
            for(j=0; j<node.steps; j++)
                System.out.println(node.cycles[j] + " " + node.amplitude[j]);
            node = node.next;
            i++;
        }
    }
    
}
