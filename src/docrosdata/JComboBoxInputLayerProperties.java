
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class JComboBoxInputLayerProperties implements ActionListener {

    JFrameInputLayerProperties jFrameInputLayerProperties;
    
    public JComboBoxInputLayerProperties(JFrameInputLayerProperties jf) {
        
        jFrameInputLayerProperties = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(((JComboBox) ae.getSource()).getName().equals("MaterialType")) {

            int i;
            String type = String.valueOf(jFrameInputLayerProperties.jComboBox[2].getSelectedItem());

            //System.out.println("TYPE: " + type);
            //System.out.println("count: " + jFrameInputMaterialModel.jComboBoxName.getItemCount());
            //System.out.println("item 0: " + jFrameInputMaterialModel.jComboBoxName.getItemAt(0));

            jFrameInputLayerProperties.jComboBox[3].removeAllItems();
            jFrameInputLayerProperties.jComboBox[3].addItem("Select");
            jFrameInputLayerProperties.jComboBox[3].setSelectedIndex(0);

            //for(i=1; i<jFrameInputMaterialModel.jComboBoxName.getItemCount(); i++)
            //    jFrameInputMaterialModel.jComboBoxName.removeItemAt(i);

            if(!type.equals("Select")) {

                NonLinearMaterialModelList list = jFrameInputLayerProperties.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + type);

                if(list != null) {
                    NonLinearMaterialModel node = list.first;
                    while(node != null) {
                        jFrameInputLayerProperties.jComboBox[3].addItem(node.name);
                        node = node.next;
                    }
                }
                //jFrameInputLayerProperties.jButton[1].setVisible(true);
            }
            //else
            //   jFrameInputMaterialModel.jButton[1].setVisible(false);
        }
        else {
            if(((JComboBox) ae.getSource()).getSelectedIndex() > 0) {
                
                jFrameInputLayerProperties.jButton[0].setVisible(true);
                jFrameInputLayerProperties.jButton[1].setText("Reset");
                jFrameInputLayerProperties.jButton[1].setVisible(true);
                jFrameInputLayerProperties.jButton[1].setEnabled(true);
                jFrameInputLayerProperties.jButton[2].setText("OK");
                jFrameInputLayerProperties.jButton[2].setEnabled(true);
                String name = String.valueOf(((JComboBox) ae.getSource()).getSelectedItem());
                //System.out.println("NAME: " + name);
                LayerProperties node = jFrameInputLayerProperties.jFrameDocrosData.layerPropertiesList.getLayerProperties(name);
                if(node.countLayerPatterns > 0)
                    jFrameInputLayerProperties.jButton[0].setEnabled(false);
                jFrameInputLayerProperties.jComboBox[1].setSelectedItem(node.geoProp.name);
                jFrameInputLayerProperties.jComboBox[2].setSelectedItem(node.materialModel.type);
                jFrameInputLayerProperties.jComboBox[3].setSelectedItem(node.materialModel.name);
            }
            else {
                int i;
                jFrameInputLayerProperties.jButton[0].setVisible(false);
                jFrameInputLayerProperties.jButton[1].setVisible(false);
                jFrameInputLayerProperties.jButton[2].setText("New");
                for(i=1; i<4; i++)
                    jFrameInputLayerProperties.jComboBox[i].setSelectedIndex(0);
            }
        }
    }
}
