
package docrosdata;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
      
class JButtonLayerGraphicsActionListener implements ActionListener {
    
    JFrameLayerGraphics jFrameStressStrainGraphics;

    public JButtonLayerGraphicsActionListener(JFrameLayerGraphics jf) {
        
        jFrameStressStrainGraphics = jf;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        switch (ae.getActionCommand()) {
            case "Open":
                createStressStrainLayerFile(Integer.valueOf(String.valueOf(jFrameStressStrainGraphics.jComboBox[1].getSelectedItem())));
                break;
            case "Close":
                jFrameStressStrainGraphics.dispose();
                break;
        
        }
    }
    
    void createStressStrainLayerFile(int layerNumber) {
        
        int i, j, numLayerProperties, layerInc, phase = 1;
        String line, strain, stress;
        BufferedWriter bufferedWriter;
        File docrosOutFile, tempDataFile;
        Scanner scan;
        LayerPatterns node = jFrameStressStrainGraphics.jFrameDocrosData.layerPatternsList.getLayerPatterns(layerNumber);
        Boolean isFirstPhase = true;

        docrosOutFile = new File(jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getPath().substring(0, jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getPath().length() - 6) + "OUT.dat");
        tempDataFile = new File(jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getPath().substring(0, jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getPath().length() - jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getName().length()) + "tempLayerData.dat");
        
        if (tempDataFile.exists())
            tempDataFile.delete();
        try {
            tempDataFile.createNewFile();
        } catch (IOException ex) {
                Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        tempDataFile.deleteOnExit();       
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(tempDataFile.getAbsoluteFile()));
            scan = new Scanner(docrosOutFile);
            bufferedWriter.write(jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getName().substring(0, jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getName().length() - 6));
            bufferedWriter.newLine();
            bufferedWriter.write(String.valueOf(layerNumber));
            bufferedWriter.newLine();
            while(scan.hasNextLine()) {
                numLayerProperties = jFrameStressStrainGraphics.jFrameDocrosData.layerPatternsList.getNumberOfLayerPropertiesInPhase(layerNumber, phase);
                line = scan.nextLine();
                while(scan.hasNextLine() && !line.startsWith("<MONOTONIC_LOAD>") && !line.startsWith("<CYCLIC_LOAD>"))
                    line = scan.nextLine();
                if(numLayerProperties > 0 && scan.hasNextLine()) {
                    if(isFirstPhase) {
                        bufferedWriter.write(String.valueOf(phase));
                        bufferedWriter.newLine();
                        //isFirstPhase = false;
                    }
                    for(i=0; i<numLayerProperties; i++) 
                        bufferedWriter.write("\t" + "Layer " + layerNumber + " (" + jFrameStressStrainGraphics.jFrameDocrosData.layerPatternsList.getLayerPropertiesMaterialName(layerNumber, phase, i) + ") - Phase " + phase + "\t");
                    bufferedWriter.newLine();
                    for(i=0; i<numLayerProperties; i++) 
                        bufferedWriter.write("Strain (-)" + "\t" + "Stress (" + jFrameStressStrainGraphics.jFrameDocrosData.unitsInputOutputData[0] + "/" + jFrameStressStrainGraphics.jFrameDocrosData.unitsInputOutputData[1] + "2)" + "\t");
                    bufferedWriter.newLine();
                    if(isFirstPhase) {
                        if(phase == 1) {
                            for(i=0; i<numLayerProperties; i++) 
                                bufferedWriter.write("0.0" + "\t" + "0.0" + "\t");
                            bufferedWriter.newLine();
                        }
                        isFirstPhase = false;
                    }
                }
                while(scan.hasNextLine() && !line.startsWith("</")) {
                    line = scan.nextLine();
                    while(scan.hasNextLine() && !line.startsWith("</") && !line.startsWith("## Layer"))
                        line = scan.nextLine();
                    if(line.startsWith("## Layer")) {
                        layerInc = layerNumber;
                        for(i=0; i<numLayerProperties; i++) {    
                            for(j=1; j<layerInc; j++)
                                line = scan.nextLine();
                            scan.next();
                            scan.next();
                            strain = scan.next();
                            scan.next();
                            scan.next();
                            stress = scan.next();//.replace(".", ",");
                            scan.nextLine();
                            bufferedWriter.write(strain + "\t" + stress + "\t");
                            //if(numLayerProperties > 1)
                            layerInc = jFrameStressStrainGraphics.jFrameDocrosData.layerPatternsList.getLayerNumberIncrement(node, layerInc, phase, i + 1);
                            //System.out.println("LayerAux");
                        }
                        if(numLayerProperties > 0)
                            bufferedWriter.newLine();
                    }
                }
                phase++;
            }
            bufferedWriter.close();           
        } catch (IOException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        BufferedInputStream input;
        FileOutputStream output;
        File createStressStrainGraphicFile;
        byte[] buffer = new byte[256];
        int bytesRead;            
        try {   
            createStressStrainGraphicFile = new File(jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getPath().substring(0, jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getPath().length() - jFrameStressStrainGraphics.jFrameDocrosData.dataFile.getName().length()) + "tempCreateLayerGraphic.xlsm");
            if(!createStressStrainGraphicFile.exists()) {
                createStressStrainGraphicFile.createNewFile();
                createStressStrainGraphicFile.deleteOnExit(); 
                input = new BufferedInputStream(getClass().getResourceAsStream("/graphics/createLayerGraphic.xlsm"));            
                try {
                    output = new FileOutputStream(createStressStrainGraphicFile);
                    while((bytesRead = input.read(buffer)) != -1)
                        output.write(buffer, 0, bytesRead);
                    output.close();
                }
                catch (IOException e) {
                    Logger.getLogger(JButtonHelpActionListener.class.getName()).log(Level.SEVERE, null, e);
                }
                input.close();
                }
            Desktop.getDesktop().open(createStressStrainGraphicFile);

        } catch (IOException ex) {
            Logger.getLogger(JButtonHelpActionListener.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
}
