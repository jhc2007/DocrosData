
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;

class JRadioButtonIntupPhaseDataActionListener implements ActionListener {

    JFrameInputPhaseData jFrameInputPhaseData;
    
    public JRadioButtonIntupPhaseDataActionListener(JFrameInputPhaseData jf) {
        
        jFrameInputPhaseData = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        int i;
        for(i=0; i<jFrameInputPhaseData.jLabelMessage.length; i++)
            jFrameInputPhaseData.jLabelMessage[i].setVisible(false);
        jFrameInputPhaseData.jButton[2].setEnabled(true);
        if(((JRadioButton) ae.getSource()).getName().equals(jFrameInputPhaseData.jRadioButtonText[0])) {
            if(jFrameInputPhaseData.jRadioButton[0].isSelected()) {
                jFrameInputPhaseData.jRadioButton[1].setSelected(false);
                addMonotonicLoadTypeDataList();
                stopConditionNameSetVisible(true);
            }
            else {
                jFrameInputPhaseData.jRadioButton[1].setSelected(true);
                addCyclicLoadTypeDataList();
                stopConditionNameSetVisible(false);
            }
        }
        else {
            if(jFrameInputPhaseData.jRadioButton[1].isSelected()) {
                jFrameInputPhaseData.jRadioButton[0].setSelected(false); 
                addCyclicLoadTypeDataList();
                stopConditionNameSetVisible(false);
            }
            else {
                jFrameInputPhaseData.jRadioButton[0].setSelected(true);
                addMonotonicLoadTypeDataList();
                stopConditionNameSetVisible(true);
            }
        } 
    }
    
    void stopConditionNameSetVisible(Boolean visible) {
        jFrameInputPhaseData.jLabel[3].setVisible(visible);
        jFrameInputPhaseData.jComboBox[2].setSelectedIndex(0);
        jFrameInputPhaseData.jComboBox[2].setVisible(visible);
    }
    
    void addMonotonicLoadTypeDataList() {
        
        jFrameInputPhaseData.jComboBox[1].removeAllItems();
        jFrameInputPhaseData.jComboBox[1].addItem("Select");
        if(jFrameInputPhaseData.jFrameDocrosData.monotonicLoadTypeDataList != null) {
            MonotonicLoadTypeData node = jFrameInputPhaseData.jFrameDocrosData.monotonicLoadTypeDataList.first;
            while(node != null) {
                jFrameInputPhaseData.jComboBox[1].addItem(node.name);
                node = node.next;
            }
            jFrameInputPhaseData.jComboBox[1].setSelectedIndex(0);
        }
    }
    
    void addCyclicLoadTypeDataList() {
                        
        jFrameInputPhaseData.jComboBox[1].removeAllItems();
        jFrameInputPhaseData.jComboBox[1].addItem("Select");
        if(jFrameInputPhaseData.jFrameDocrosData.cyclicLoadTypeDataList != null) {
            CyclicLoadTypeData node = jFrameInputPhaseData.jFrameDocrosData.cyclicLoadTypeDataList.first;
            while(node != null) {
                jFrameInputPhaseData.jComboBox[1].addItem(node.name);
                node = node.next;
            }
            jFrameInputPhaseData.jComboBox[1].setSelectedIndex(0);
        }
    }
}
