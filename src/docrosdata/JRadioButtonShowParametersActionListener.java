
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

class JRadioButtonShowParametersActionListener implements ActionListener {

    JLabel jLabelParameterSymbol[];
    JLabel jLabelParameterUnit[];
    JTextField jTextFieldParameter[];
    JRadioButton jRadioButton[];
    
    public JRadioButtonShowParametersActionListener(JLabel[] jlps, JLabel[] jlpu, JTextField[] jtp, JRadioButton[] jrb) {
        
        jLabelParameterSymbol = jlps;
        jLabelParameterUnit = jlpu;
        jTextFieldParameter = jtp;
        jRadioButton = jrb;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;
        if(((JRadioButton) ae.getSource()).getName().equals("Trilinear")) {
            if(jRadioButton[0].isSelected()) {
                jRadioButton[1].setSelected(false);
                for(i=12; i<14; i++) {
                    jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                    jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                    jTextFieldParameter[i].setText("0.0");
                    jTextFieldParameter[i].setBackground(Color.white);
                    jTextFieldParameter[i].setEnabled(false);
                }
            }
            else {
                jRadioButton[1].setSelected(true);
                for(i=12; i<14; i++) {
                    jLabelParameterSymbol[i].setForeground(Color.black);
                    jLabelParameterUnit[i].setForeground(Color.black);
                    jTextFieldParameter[i].setText(null);
                    jTextFieldParameter[i].setEnabled(true);
                }
             }
        }
        else {
            if(jRadioButton[1].isSelected()) {
                jRadioButton[0].setSelected(false); 
                for(i=12; i<14; i++) {
                    jLabelParameterSymbol[i].setForeground(Color.black);
                    jLabelParameterUnit[i].setForeground(Color.black);
                    jTextFieldParameter[i].setText(null);
                    jTextFieldParameter[i].setEnabled(true);
                }
            }
            else {
                jRadioButton[0].setSelected(true);
                for(i=12; i<14; i++) {
                    jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                    jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                    jTextFieldParameter[i].setText("0.0");
                    jTextFieldParameter[i].setBackground(Color.white);
                    jTextFieldParameter[i].setEnabled(false);
                }
            }
        } 
    }
    
}
