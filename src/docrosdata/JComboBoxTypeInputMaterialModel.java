
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JComboBoxTypeInputMaterialModel implements ActionListener {

    JFrameInputMaterialModel jFrameInputMaterialModel;
    
    public JComboBoxTypeInputMaterialModel(JFrameInputMaterialModel jf) {
        
        jFrameInputMaterialModel = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        jFrameInputMaterialModel.jComboBoxName.removeAllItems();
        jFrameInputMaterialModel.jComboBoxName.addItem("Select");
        jFrameInputMaterialModel.jComboBoxName.setSelectedIndex(0);

        if(jFrameInputMaterialModel.jComboBoxType.getSelectedIndex() > 0) {
            
            String type = String.valueOf(jFrameInputMaterialModel.jComboBoxType.getSelectedItem()).substring(0, 7);
            //System.out.println("type:" + type);
            if(getClass().getResourceAsStream("/help/help" + type + ".pdf") != null) {
                jFrameInputMaterialModel.jButtonHelp.removeActionListener(jFrameInputMaterialModel.jButtonHelp.getActionListeners()[0]);
                jFrameInputMaterialModel.jButtonHelp.addActionListener(new JButtonHelpActionListener(type));//, type));
                jFrameInputMaterialModel.jButtonHelp.setVisible(true);
            }
            else
                jFrameInputMaterialModel.jButtonHelp.setVisible(false);
            NonLinearMaterialModelList list = jFrameInputMaterialModel.jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + type);
            if(list != null) {
                NonLinearMaterialModel node = list.first;
                while(node != null) {
                    jFrameInputMaterialModel.jComboBoxName.addItem(node.name);
                    node = node.next;
                }
            }
            //jFrameInputMaterialModel.jButton[1].setVisible(true);
            jFrameInputMaterialModel.jButton[2].setEnabled(true);
        }
        else {
            if(getClass().getResourceAsStream("/help/helpMaterialMode.pdf") != null) {
                jFrameInputMaterialModel.jButtonHelp.removeActionListener(jFrameInputMaterialModel.jButtonHelp.getActionListeners()[0]);
                jFrameInputMaterialModel.jButtonHelp.addActionListener(new JButtonHelpActionListener("MaterialModel"));//, "Material Model"));
                jFrameInputMaterialModel.jButtonHelp.setVisible(true);
            }
            else
                jFrameInputMaterialModel.jButtonHelp.setVisible(false);
            jFrameInputMaterialModel.jButton[2].setEnabled(false);
        }
    }
}
