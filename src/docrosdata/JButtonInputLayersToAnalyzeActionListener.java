
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputLayersToAnalyzeActionListener implements ActionListener {

    JFrameInputLayersToAnalyze jFrameInputLayersToAnalyse;
    
    public JButtonInputLayersToAnalyzeActionListener(JFrameInputLayersToAnalyze jf) {
        
        jFrameInputLayersToAnalyse = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        switch (ae.getActionCommand()) {
            case "Previous":
                jFrameInputLayersToAnalyse.dispose();
                JFrameInputLayerPatterns jFrameInputLayerPatterns = new JFrameInputLayerPatterns(jFrameInputLayersToAnalyse.jFrameDocrosData, jFrameInputLayersToAnalyse.getLocation());
                break;
            case "Next":
                jFrameInputLayersToAnalyse.dispose();
                JFrameInputPreStressedLayers jFrameInputPreStressedLayers = new JFrameInputPreStressedLayers(jFrameInputLayersToAnalyse.jFrameDocrosData, jFrameInputLayersToAnalyse.getLocation());
                break;
            case "Close":
                jFrameInputLayersToAnalyse.dispose();
                break;
            case "OK":
                if(jFrameInputLayersToAnalyse.jList2.getModel().getSize() > 0) {
                    int i;
                    jFrameInputLayersToAnalyse.jFrameDocrosData.layerStressStrainGraphics = new int[jFrameInputLayersToAnalyse.jList2.getModel().getSize()];
                    for(i=0; i<jFrameInputLayersToAnalyse.jList2.getModel().getSize(); i++)
                        jFrameInputLayersToAnalyse.jFrameDocrosData.layerStressStrainGraphics[i] = Integer.valueOf(String.valueOf(jFrameInputLayersToAnalyse.jList2.getModel().getElementAt(i)));
                    //if(jFrameInputLayersToAnalyse.jFrameDocrosData.mainParameters != null)
                    //    jFrameInputLayersToAnalyse.jFrameDocrosData.mainParameters.numLayers = jFrameInputLayersToAnalyse.jList1.getModel().getSize();
                    //jFrameInputLayersToAnalyse.jFrameDocrosData.jMenuItemEdit[6].setEnabled(true);
                    //jFrameInputLayersToAnalyse.jFrameDocrosData.jMenuItemEdit[7].setEnabled(true);
                    //jFrameInputLayersToAnalyse.dispose();
                    jFrameInputLayersToAnalyse.jFrameDocrosData.jMenuItemSave.setEnabled(true);
                    if(jFrameInputLayersToAnalyse.jFrameDocrosData.dataFile != null)
                        jFrameInputLayersToAnalyse.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
                    jFrameInputLayersToAnalyse.jButton[5].setEnabled(true);
                }
                break;
            case "Remove":
                if(!jFrameInputLayersToAnalyse.jList2.isSelectionEmpty()) {
                    String list[];
                    int i, j = 0, selectedIndices[];//, array[];
                    selectedIndices = jFrameInputLayersToAnalyse.jList2.getSelectedIndices();
                    list = new String[jFrameInputLayersToAnalyse.jList2.getModel().getSize() - selectedIndices.length];
                    for(i=0; i<jFrameInputLayersToAnalyse.jList2.getModel().getSize(); i++)
                        if(!isInArray(i, selectedIndices)) {
                            list[j] = String.valueOf(jFrameInputLayersToAnalyse.jList2.getModel().getElementAt(i));
                            j++;
                        }
                    jFrameInputLayersToAnalyse.jList2.setModel(new JListLayersNumberModel(list));                                                              
                    if(jFrameInputLayersToAnalyse.jList2.getModel().getSize() == 0) {
                        jFrameInputLayersToAnalyse.jButton[1].setEnabled(false);
                        jFrameInputLayersToAnalyse.jButton[2].setEnabled(false);
                        jFrameInputLayersToAnalyse.jButton[5].setEnabled(false);
                    }
                }
                else {
                    jFrameInputLayersToAnalyse.jLabelMessage[1].setVisible(true);
                    jFrameInputLayersToAnalyse.jButton[1].setEnabled(false);
                }
                break;
            case "Add":
                if(!jFrameInputLayersToAnalyse.jList1.isSelectionEmpty()) {
                    int i, repetition = 0, array1[], array2[], array3[];
                    String list[];
                    array1 = jFrameInputLayersToAnalyse.jList1.getSelectedIndices();
                    if(jFrameInputLayersToAnalyse.jList2.getModel().getSize() > 0) {
                        array2 = new int[jFrameInputLayersToAnalyse.jList2.getModel().getSize()];
                        for(i=0; i<array2.length; i++)
                            array2[i] = Integer.valueOf(String.valueOf(jFrameInputLayersToAnalyse.jList2.getModel().getElementAt(i)));
                        for(i=0; i<array1.length; i++) { 
                            array1[i]++;
                            if(isInArray(array1[i], array2)) {
                                array1[i] = -1;
                                repetition++;
                            }
                        }
                        array3 = new int[array1.length + array2.length];
                        for(i=0; i<array1.length; i++)
                            array3[i] = array1[i];
                        for(i=0; i<array2.length; i++)
                            array3[array1.length + i] = array2[i];
                    }
                    else {
                        array3 = new int[array1.length];
                        for(i=0; i<array1.length; i++)
                            array3[i] = array1[i] + 1;
                    }
                    array3 = getSortedArray(array3);
                    list = new String[array3.length - repetition];
                    repetition = 0;
                    for(i=0; i<array3.length; i++) {
                        if(array3[i] != -1) {
                            list[repetition] = String.valueOf(array3[i]);
                            repetition++;
                        }
                    }
                    jFrameInputLayersToAnalyse.jButton[1].setEnabled(true);
                    jFrameInputLayersToAnalyse.jButton[2].setEnabled(true);
                    //jFrameInputLayersToAnalyse.jButton[4].setEnabled(true);
                    jFrameInputLayersToAnalyse.jList2.setModel(new JListLayersNumberModel(list));
                }
                else {
                    jFrameInputLayersToAnalyse.jLabelMessage[0].setVisible(true);
                    jFrameInputLayersToAnalyse.jButton[0].setEnabled(false);
                }
                break;
        }
        

    }
    
    Boolean isInArray(int value, int[] array) {
        
        int i = 0;
        while(i < array.length) {
            if(array[i] == value)
                return true;
            i++;
        }
        return false;
    }
    
    int[] getSortedArray(int[] array) {

        int i, j, minPosition, minValue;
        for(i=0; i<array.length; i++) {
            minValue = array[i];
            minPosition = i;
            for(j=i+1; j<array.length; j++) {
                if(array[j] < minValue) {
                    minValue = array[j];
                    minPosition = j;
                }
            }
            array[minPosition] = array[i];
            array[i] = minValue;
        }
        return array;
    }    

}
