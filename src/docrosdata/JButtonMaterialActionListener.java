
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class JButtonMaterialActionListener implements ActionListener {

    JFrameDocrosData jFrameDocrosData;
    JPanelLayerPatterns jPanelLayerPatterns;
    JFrame jFrameMaterial;
    JComboBox jComboBoxLawType;
    JComboBox jComboBoxLawName;
    JButton jButtonMaterial[];
    NonLinearMaterialModel materialModel;
    NonLinearMaterialModel modelSelected;
    LayerProperties layerProperties;
    JTextField jTextFieldLayerPropertiesName;
    Boolean /*newLayerPropertiesName,*/ viewChanged;
    JLabel jLabel1, jLabel2, jLabelLayerPropertiesName, jLabelLayerName;


    public JButtonMaterialActionListener(JFrameDocrosData jfdd, JPanelLayerPatterns jplp, JFrame jfm, JComboBox jcblt, JComboBox jcbln, JButton jbm[], JLabel jlln) {

        jFrameDocrosData = jfdd;
        jPanelLayerPatterns = jplp;
        jFrameMaterial = jfm;
        jComboBoxLawType = jcblt;
        jComboBoxLawName = jcbln;
        jButtonMaterial = jbm;
        jLabelLayerName = jlln;

        materialModel = jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].materialModel;
        //modelSelected = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jComboBoxLawType.getSelectedItem()), String.valueOf(jComboBoxLawName.getSelectedItem()));
        layerProperties = jplp.layerPatterns.layerProperties[jplp.indexLayerProperties];

        jTextFieldLayerPropertiesName = new JTextField("");
        jTextFieldLayerPropertiesName.setVisible(false);
        //newLayerPropertiesName = false;
        viewChanged = false;
        jLabel1 = new JLabel("Insert a New Layer Properties Name");
        jLabel2 = new JLabel("This name is already used");
        jLabelLayerPropertiesName = new JLabel("Name");

    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        modelSelected = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jComboBoxLawType.getSelectedItem()), String.valueOf(jComboBoxLawName.getSelectedItem()));
        switch (ae.getActionCommand()) {
            case "View":
                viewJPanelLayerPatterns();
                break;
            case "OK":
                if(!materialModelChanged()) {
                    jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                    jFrameMaterial.dispose();
                }
                else {
                    if(newLayerPropertiesNameInserted()) {
                        changeLists();
                        jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                        jFrameMaterial.dispose();
                    }
                }
                break;
            case "Close":
                jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                jFrameMaterial.dispose();
                break;
            }
    }

    Boolean materialModelChanged() {

        if(materialModel.equals(modelSelected))
            return false;
        return true;

    }

    void viewJPanelLayerPatterns() {

        if(!materialModelChanged() && viewChanged) {
            jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
            viewChanged = false;
        }
        if(materialModelChanged()) {
            if(materialModel.countLayerPatterns == 1) {
                layerProperties.materialModel = modelSelected;
                jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                layerProperties.materialModel = materialModel;
            }
            else {
                if(layerProperties.countLayerPatterns > 1) {
                    LayerProperties copy = new LayerProperties(jTextFieldLayerPropertiesName.getText());//, layerProperties);
                    copy.geoProp = layerProperties.geoProp;
                    copy.materialModel = modelSelected;
                    jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties] = copy;
                    jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                    jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties] = layerProperties;                    
                }
                else {
                    layerProperties.materialModel = modelSelected;
                    jFrameDocrosData.drawFigure(jFrameDocrosData.phase);
                    layerProperties.materialModel = materialModel;
                }

            }

        }

    }


    void changeLists() {

        //NonLinearMaterialModel newModel =
        //NonLinearMaterialModel oldModel = jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].materialModel;

        if(materialModel.countLayerPatterns == 1) {

            materialModel.countLayerPatterns--;
            modelSelected.countLayerPatterns++;
            layerProperties.materialModel = modelSelected;
            if(materialModel.countLayerPatterns == 0)
                jFrameDocrosData.nonLinearMaterialModelTypeList.delNonLinearMaterialModel(materialModel);

        }
        else {

            if(layerProperties.countLayerPatterns > 1) {

                LayerProperties copy = new LayerProperties(jTextFieldLayerPropertiesName.getText());//, layerProperties);
                jFrameDocrosData.layerPropertiesList.addLayerProperties(copy);
                layerProperties.countLayerPatterns--;
                //materialModel.countLayerPatterns--;
                copy.countLayerPatterns++;
                //modelSelected.countLayerPatterns++;
                copy.geoProp = layerProperties.geoProp;
                copy.materialModel = modelSelected;
                jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties] = copy;
                //jFrameDocrosData.changedLists[jFrameDocrosData.layerPatternsListIndex] = true;
            }
            else
                layerProperties.materialModel = modelSelected;

            materialModel.countLayerPatterns--;
            modelSelected.countLayerPatterns++;
            //jFrameDocrosData.changedLists[jFrameDocrosData.nonLinearMaterialModelTypeListIndex] = true;
        }
        //jFrameDocrosData.changedLists[jFrameDocrosData.layerPropertiesListIndex] = true;
        jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameDocrosData.dataFile != null)
            jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }

    Boolean newLayerPropertiesNameInserted() {

        if(/*newLayerPropertiesName ||*/ layerProperties.countLayerPatterns <= 1)
            return true;

        int i;

        jComboBoxLawType.setEnabled(false);
        jComboBoxLawName.setEnabled(false);

        if(jTextFieldLayerPropertiesName.isVisible()) {

            if(jTextFieldLayerPropertiesName.getText().equals("") || jTextFieldLayerPropertiesName.getText().equals("Insert a name"))
                return false;

            if(jFrameDocrosData.layerPropertiesList.existsLayerPropertiesName(jTextFieldLayerPropertiesName.getText())) {
                jTextFieldLayerPropertiesName.setBackground(Color.yellow);
                jLabel2.setVisible(true);
            }
            else
                return true;
        }
        else {

            //jLabel3.setVisible(false);
            jButtonMaterial[0].setVisible(false);
            //jLabelLine2 = new JLabel("Insert a new layer properties name");
            jLabel1.setSize(jFrameDocrosData.charWidth * jLabel1.getText().length(), jFrameDocrosData.charHeight);
            jLabel1.setLocation(12, jButtonMaterial[0].getY());
            jFrameMaterial.add(jLabel1);

            jLabelLayerName.setVisible(false);

            //jLabelLayerPropertiesName.setSize(jFrameDocrosData.charWidth * jLabelLayerPropertiesName.getText().length(), jFrameDocrosData.charHeight);
            //jLabelLayerPropertiesName.setLocation(jLabel1.getX(), jLabel1.getY() + jFrameDocrosData.charHeight);
            //jFrameMaterial.add(jLabelLayerPropertiesName);

            jTextFieldLayerPropertiesName.setSize(jComboBoxLawName.getWidth(), jFrameDocrosData.charHeight);
            //jTextFieldLayerPropertiesName.setLocation(jComboBoxLawName.getX(), jLabelLayerPropertiesName.getY());
            jTextFieldLayerPropertiesName.setLocation(jLabelLayerName.getLocation());
            jTextFieldLayerPropertiesName.setVisible(true);
            //jTextFieldLayerPropertiesName.setBackground(Color.yellow);
            jTextFieldLayerPropertiesName.addFocusListener(new JTextFieldNameFocusListener(null, 0, jLabel2, jButtonMaterial[2]));
            jFrameMaterial.add(jTextFieldLayerPropertiesName);

            jLabel2.setSize(jFrameDocrosData.charWidth * jLabel2.getText().length(), jFrameDocrosData.charHeight);
            jLabel2.setLocation(jComboBoxLawName.getX(), jComboBoxLawName.getY() + jFrameDocrosData.charHeight);
            jLabel2.setVisible(false);
            jFrameMaterial.add(jLabel2);

            jButtonMaterial[0].setEnabled(false);
        }

        return false;
    }

}
