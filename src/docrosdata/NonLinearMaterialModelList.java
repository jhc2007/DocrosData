
package docrosdata;

public class NonLinearMaterialModelList {

    NonLinearMaterialModel first;
    NonLinearMaterialModel last;
    int count;
    
    public NonLinearMaterialModelList() {
        
        first = null;
        last = null;
        count = 0;
    }
    
    void addNonLinearMaterialModel(NonLinearMaterialModel node) {
        
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
    
    NonLinearMaterialModel getNonLinearMaterialModel(String n) {
        
        NonLinearMaterialModel temp;
        temp = first;
        while(temp != null && !(temp.name).equals(n))
            temp = temp.next;
        return temp;
    }
    
}
