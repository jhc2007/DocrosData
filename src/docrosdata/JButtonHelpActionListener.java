package docrosdata;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

class JButtonHelpActionListener implements ActionListener {

    String fileName;
    
    public JButtonHelpActionListener(String name) {//, String label) {
        fileName = name;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(getClass().getResourceAsStream("/help/help" + fileName + ".pdf") != null) {
            
            BufferedInputStream input;
            FileOutputStream output;
            File helpFile;
            byte[] buffer = new byte[256];
            int bytesRead;            
            try {   
                input = new BufferedInputStream(getClass().getResourceAsStream("/help/help" + fileName + ".pdf"));            
                //helpFile = new File("C:" + File.separator + "Windows" + File.separator + "Temp" + File.separator + fileName + ".pdf");
                helpFile = new File(System.getProperty("java.io.tmpdir") + fileName + ".pdf");
                if(!helpFile.exists())
                    helpFile.createNewFile();
                helpFile.deleteOnExit(); 
                try {
                    output = new FileOutputStream(helpFile);
                    while((bytesRead = input.read(buffer)) != -1)
                        output.write(buffer, 0, bytesRead);
                    output.close();
                }
                catch (IOException e) {
                    Logger.getLogger(JButtonHelpActionListener.class.getName()).log(Level.SEVERE, null, e);
                }
                Desktop.getDesktop().open(helpFile);
                input.close();
            } catch (IOException ex) {
                Logger.getLogger(JButtonHelpActionListener.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
        /*
        JFrame jFrame;
        ImageIcon icon;
        JLabel jLabel;
        JScrollPane jScrollPane;

        jFrame = new JFrame(jFrameLabel);
        jFrame.setLayout(null);

        jScrollPane = new JScrollPane();
        jFrame.setContentPane(jScrollPane);

        icon = new ImageIcon(this.getClass().getResource("/help/" + fileName));
        jLabel = new JLabel(icon);

        jScrollPane.setViewportView(jLabel);

        jFrame.setSize(icon.getIconWidth() + 34, icon.getIconHeight());
        jFrame.setVisible(true);
        */
    }
}
