
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JMenuItemNewActionListener implements ActionListener {

    JFrameDocrosData jFrameDocrosData;
    
    public JMenuItemNewActionListener(JFrameDocrosData jfdd) {
        
        jFrameDocrosData = jfdd;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        jFrameDocrosData.resetAll();
        jFrameDocrosData.createNonLinearMaterialModelTypeList();
        jFrameDocrosData.dataFile = null;
        jFrameDocrosData.drawFigure(0);
        
        jFrameDocrosData.jMenuItemEdit[0].setEnabled(true);
        //jFrameDocrosData.jMenuItemEdit[1].setEnabled(true);
        //jFrameDocrosData.jMenuItemEdit[2].setEnabled(true);
        
        JFrameInputMainParameters jFrameInputMainParameters;
        //JFrameInputMaterialModel jFrameInputMaterialModel;
        //JFrameInputLaminateGeoProp jFrameInputLaminateGeoProp;
        //JFrameInputLayerProperties jFrameInputLayerProperties;
        //JFrameInputLayerPatterns jFrameInputLayerPatterns;
        //JFrameInputLayersToAnalyze jFrameInputLayersToAnalyze;
        //JFrameInputPreStressedLayers jFrameInputPreStressedLayers;
        //JFrameInputMonotonicLoadTypeData jFrameInputMonotonicLoadTypeData;
        //JFrameInputCyclicLoadTypeData jFrameInputCyclicLoadTypeData;
        //JFrameInputPhaseData jFrameInputPhaseData;
        
        jFrameInputMainParameters = new JFrameInputMainParameters(jFrameDocrosData, null);
        //jFrameInputMaterialModel = new JFrameInputMaterialModel(jFrameDocrosData, null);
        //jFrameInputLaminateGeoProp = new JFrameInputLaminateGeoProp(jFrameDocrosData, null);
        //jFrameInputLayerProperties = new JFrameInputLayerProperties(jFrameDocrosData, null);
        //jFrameInputLayerPatterns = new JFrameInputLayerPatterns(jFrameDocrosData, null);
        //jFrameInputLayersToAnalyze = new JFrameInputLayersToAnalyze(jFrameDocrosData, null);
        //jFrameInputPreStressedLayers = new JFrameInputPreStressedLayers(jFrameDocrosData, null);
        //jFrameInputMonotonicLoadTypeData = new JFrameInputMonotonicLoadTypeData(jFrameDocrosData, null);
        //jFrameInputCyclicLoadTypeData = new JFrameInputCyclicLoadTypeData(jFrameDocrosData, null);
        //jFrameInputPhaseData = new JFrameInputPhaseData(jFrameDocrosData);
    }
    
}
