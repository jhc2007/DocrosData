
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JComboBoxInputLayerPatterns implements ActionListener {

    JFrameInputLayerPatterns jFrameInputLayerPatterns;
    
    public JComboBoxInputLayerPatterns(JFrameInputLayerPatterns jf) {
        
        jFrameInputLayerPatterns = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {        
        
        if(jFrameInputLayerPatterns.jComboBoxMain[0].getSelectedIndex() > 0) {
            int i;
            String name = String.valueOf(jFrameInputLayerPatterns.jComboBoxMain[0].getSelectedItem());
            LayerPatterns node = jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.getLayerPatterns(name);
            jFrameInputLayerPatterns.jTextFieldNumber.setText(String.valueOf(node.numLayers));
            setAllEnabled(true);
            for(i=0; i<node.layerProperties.length; i++)
                jFrameInputLayerPatterns.jComboBoxMain[1].addItem(node.layerProperties[i].name + " / " + node.layerPropertiesPhase[i]);
            jFrameInputLayerPatterns.jComboBoxMain[1].setSelectedIndex(0);
            jFrameInputLayerPatterns.jButton[1].setText("Reset");      
            jFrameInputLayerPatterns.jButton[2].setText("OK");
        }
        else {
            setAllEnabled(false);
            jFrameInputLayerPatterns.jTextFieldNumber.setText("");
            jFrameInputLayerPatterns.jButton[2].setText("New");
         }
    }
    
    void setAllEnabled(Boolean enabled) {
        
        int i;
        for(i=0; i<jFrameInputLayerPatterns.jLabelMessage.length; i++)
            jFrameInputLayerPatterns.jLabelMessage[i].setVisible(false);
        jFrameInputLayerPatterns.jTextFieldNumber.setEnabled(enabled);
        if(enabled)
            jFrameInputLayerPatterns.jTextFieldNumber.setBackground(Color.white);
        else
            jFrameInputLayerPatterns.jTextFieldNumber.setBackground(jFrameInputLayerPatterns.getBackground());
        jFrameInputLayerPatterns.jComboBoxMain[1].removeAllItems();
        jFrameInputLayerPatterns.jComboBoxMain[1].setEnabled(enabled);
        jFrameInputLayerPatterns.jButtonDelProp.setEnabled(enabled);
        jFrameInputLayerPatterns.jComboBoxProp[0].setEnabled(enabled);
        jFrameInputLayerPatterns.jComboBoxProp[0].setSelectedIndex(0);
        jFrameInputLayerPatterns.jComboBoxProp[1].setEnabled(enabled);
        jFrameInputLayerPatterns.jComboBoxProp[1].setSelectedIndex(0);
        jFrameInputLayerPatterns.jButtonAddProp.setEnabled(enabled);
        jFrameInputLayerPatterns.jButton[0].setVisible(enabled);
        jFrameInputLayerPatterns.jButton[1].setVisible(enabled);
        jFrameInputLayerPatterns.jButton[2].setEnabled(true);
    }
}
