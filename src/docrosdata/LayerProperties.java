/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package docrosdata;

/**
 *
 * @author Moi
 */
public class LayerProperties {
    
    String name;
    NonLinearMaterialModel materialModel;
    LaminateGeoProp geoProp;
    int countLayerPatterns;
    LayerProperties next;
    
    public LayerProperties(String n) {
        name = n;
        materialModel = null;
        geoProp = null;
        countLayerPatterns = 0;
        next = null;
    }

    /*
    public LayerProperties(LayerProperties lp) {
        
        name = lp.name;
        materialModel = lp.materialModel;
        geoProp = null;
        countLayerPatterns = 0;
        next = null;
    }
    
    public LayerProperties(String n, LayerProperties lp) {
        
        name = n;
        materialModel = lp.materialModel;
        geoProp = null;
        countLayerPatterns = 0;
        next = null;
    }
    */
}
