
package docrosdata;

public class CyclicLoadTypeData {
    
    String name;
    int layerNumber;
    //String layerPatternsName;
    int steps;
    int cycles[];
    double amplitude[];
    double strainIncrement;
    CyclicLoadTypeData next;
    
    public CyclicLoadTypeData(String nam, int layer, int sts) {
        
        name = nam;
        layerNumber = layer;
        steps = sts;
        cycles = new int[sts];
        amplitude = new double[sts];
        next = null;
    }
    
    public CyclicLoadTypeData(String nam, int layer, int sts, double inc) {
        
        name = nam;
        layerNumber = layer;
        steps = sts;
        cycles = new int[sts];
        amplitude = new double[sts];
        strainIncrement = inc;
        next = null;
    }
    
}
