
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class JFrameInputStopConditionLayerStrain extends JFrame {
  
    JFrameDocrosData jFrameDocrosData;
    Point location;
    
    JLabel jLabel[];
    JLabel jLabelMessage[];
    JTextField jTextField[];
    JComboBox jComboBox[];
    JButton jButtonHelp, jButtonUsedName;
    JButton jButton[];
    
    String jLabelText[] = {"Stop Condition Name", "Pattern Name", "Layer Number", "Strain"};
    String jButtonText[] = {"Delete", "Back", "OK", "Previous", "Close", "Next"};
    
    String jLabelMessageText[] = {
        "Insert stop condition name",   //0
        "This name is already used",    //1
        "Select pattern name",          //2
        "Select layer Number",          //3
        "Insert strain"                 //4
    };
    
    public JFrameInputStopConditionLayerStrain(JFrameDocrosData jf, Point loc) {

        super("<STOP_CONDITION_LAYER_STRAIN>");
        jFrameDocrosData = jf;
        location = loc;
        init();
    
    }
    
    private void init() {
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);                
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, jLabelWidth, xPosition = 10, yPosition = 10;
        
        jLabelWidth = jFrameDocrosData.charWidth * (JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText) - 4);        
        
        jLabel = new JLabel[jLabelText.length];
        jTextField = new JTextField[2];
        jComboBox = new JComboBox[3];       
        
        JLabel jLabelTitle = new JLabel("Stop Condition Layer Strain");
        jLabelTitle.setSize(jFrameDocrosData.charWidth * (jLabelTitle.getText().length() + 4), jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(xPosition + jLabelWidth, yPosition);
        this.add(jLabelTitle);
        
        if(getClass().getResourceAsStream("/help/helpStopConditionLayerStrain.pdf") != null) {
        jButtonHelp = new JButton("Help");
        jButtonHelp.setSize(63, 18);
        jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
        jButtonHelp.addActionListener(new JButtonHelpActionListener("StopConditionLayerStrain"));//, "Monotonic Load Type Data"));
        this.add(jButtonHelp);
        }
        yPosition += jFrameDocrosData.charHeight;
        
        for(i=0; i<jLabelText.length; i++) {
            jLabel[i] = new JLabel(jLabelText[i]);
            jLabel[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
            jLabel[i].setLocation(xPosition, yPosition);
            this.add(jLabel[i]);
            if(i < 3) {
                jComboBox[i] = new JComboBox();
                if(i == 2)
                    jComboBox[i].setSize(8 * jFrameDocrosData.charWidth, jFrameDocrosData.charHeight);
                else
                    jComboBox[i].setSize(jLabelTitle.getWidth() - 1, jFrameDocrosData.charHeight);
                jComboBox[i].setLocation(xPosition + jLabelWidth, yPosition);
                jComboBox[i].addItem("Select");
                if(i == 0) {
                    if(jFrameDocrosData.stopConditionLayerStrainList != null && jFrameDocrosData.stopConditionLayerStrainList.first != null) {
                        StopConditionLayerStrain node = jFrameDocrosData.stopConditionLayerStrainList.first;
                        while(node != null) {
                            jComboBox[i].addItem(node.name);
                            node = node.next;
                        }
                    }
                }
                else {
                    if(i == 1 && jFrameDocrosData.layerPatternsList != null && jFrameDocrosData.layerPatternsList.first != null) {
                        LayerPatterns node = jFrameDocrosData.layerPatternsList.first;
                        while(node != null) {
                            jComboBox[i].addItem(node.name);
                            node = node.next;
                        }
                    }
                }
                jComboBox[i].setSelectedIndex(0);
                this.add(jComboBox[i]);
            }
            yPosition += jFrameDocrosData.charHeight;
        }
        jComboBox[1].setName("LayerPatternsName");

        for(i=0; i<2; i++) {
            jTextField[i] = new JTextField();            
            if(i == 0) {
                jTextField[i].setSize(jLabelTitle.getWidth() - 64, jFrameDocrosData.charHeight);
                jTextField[i].setLocation(jComboBox[0].getLocation());
                jTextField[i].setName("NameStopCondition");
            }
            else {
                jTextField[i].setSize(jLabelTitle.getWidth(), jFrameDocrosData.charHeight);
                jTextField[i].setLocation(jLabelTitle.getX(), jLabel[3].getY());
            }
            jTextField[i].addKeyListener(new JTextFieldKeyListener());
            this.add(jTextField[i]);
        }
        
        jButtonUsedName = new JButton("Used");
        jButtonUsedName.setSize(63, 26);
        jButtonUsedName.setLocation(xPosition + jLabelWidth + jLabelTitle.getWidth() - 64, jTextField[0].getY());
        jButtonUsedName.addActionListener(new JButtonInputStopConditionLayerStrainActionListener(this));
        this.add(jButtonUsedName);
        
        jLabelMessage = new JLabel[jLabelMessageText.length];
        for(i=0; i<jLabelMessageText.length; i++) {
            jLabelMessage[i] = new JLabel(jLabelMessageText[i]);
            jLabelMessage[i].setSize(jLabelTitle.getWidth(), jFrameDocrosData.charHeight);
            jLabelMessage[i].setLocation(xPosition + jLabelWidth, yPosition);
            jLabelMessage[i].setForeground(Color.red);
            jLabelMessage[i].setVisible(false);
            this.add(jLabelMessage[i]);
        }
        
        yPosition += jFrameDocrosData.charHeight;
        
        jButton = new JButton[jButtonText.length];
        
        for(i=0; i<3; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * jButton[i].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputStopConditionLayerStrainActionListener(this));
            this.add(jButton[i]);
        }
        jButton[0].setVisible(false);
        
        yPosition += jFrameDocrosData.charHeight + 10;
               
        for(i=3; i<jButtonText.length; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
            jButton[i].setLocation(xPosition + (i - 3) * jButton[3].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputStopConditionLayerStrainActionListener(this));
            this.add(jButton[i]);
        }
        
        jTextField[0].addFocusListener(new JTextFieldNameFocusListener(jLabelMessage, 0, jLabelMessage[1], jButton[2]));
        jTextField[1].addFocusListener(new JTextFieldDoubleFocusListener(jLabelMessage, 4, null, jButton[2]));

        jComboBox[0].addActionListener(new JComboBoxInputStopConditionLayerStrain(this));
        jComboBox[1].addActionListener(new JComboBoxInputStopConditionLayerStrain(this));
        jComboBox[2].addFocusListener(new JComboBoxFocusListener(jLabelMessage, 3, jButton[2]));
        
        if(jComboBox[0].getItemCount() == 1) {
            jButton[1].setVisible(false);
            //jButton[5].setEnabled(false);
            jComboBox[0].setVisible(false);
            jButtonUsedName.setEnabled(false);
        }
        else {
            jComboBox[0].setSelectedIndex(0);
            jTextField[0].setVisible(false);
            jButtonUsedName.setVisible(false);
        }
        
        yPosition += jFrameDocrosData.charHeight + 30;
        
        this.setSize(jLabelWidth + jLabelTitle.getWidth() + 26, yPosition);
        this.setVisible(true);
    }    
    
}
