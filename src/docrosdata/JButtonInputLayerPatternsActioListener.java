
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputLayerPatternsActioListener implements ActionListener {
    
    JFrameInputLayerPatterns jFrameInputLayerPatterns;

    public JButtonInputLayerPatternsActioListener(JFrameInputLayerPatterns jf) {
        
        jFrameInputLayerPatterns = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;
        switch (ae.getActionCommand()) {
            case "Used":
                String layerPatternsNames[];
                layerPatternsNames = new String[jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.count];
                LayerPatterns node = jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.first;
                for(i=0; i<layerPatternsNames.length; i++) {
                    layerPatternsNames[i] = node.name;
                    node = node.next;
                }
                JFrameUsedNames jFrameUsedNames = new JFrameUsedNames("Used Layer Patterns Names:", layerPatternsNames, jFrameInputLayerPatterns.getLocation());
                break;
            case "Remove":
                jFrameInputLayerPatterns.jComboBoxMain[1].removeItem(jFrameInputLayerPatterns.jComboBoxMain[1].getSelectedItem());
                if(jFrameInputLayerPatterns.jComboBoxMain[1].getItemCount() == 0) {
                    jFrameInputLayerPatterns.jComboBoxMain[1].setEnabled(false);
                    jFrameInputLayerPatterns.jButtonDelProp.setEnabled(false);
                }
                break;
            case "New":
                setAllEnabled(true);
                break;
            case "OK":
                if(jFrameInputLayerPatterns.jTextFieldName.isVisible()) {
                    if(jFrameInputLayerPatterns.jTextFieldName.getText().equals("") || jFrameInputLayerPatterns.jTextFieldName.getText().equals("Insert name")) {
                        jFrameInputLayerPatterns.jTextFieldName.setText("Insert name");
                        jFrameInputLayerPatterns.jTextFieldName.setBackground(Color.yellow);
                        jFrameInputLayerPatterns.jButton[2].setEnabled(false);
                    }
                    else {
                        if(existsLayerPatternsName()) {
                            jFrameInputLayerPatterns.jLabelMessage[0].setVisible(true);
                            jFrameInputLayerPatterns.jTextFieldName.setBackground(Color.yellow);
                            jFrameInputLayerPatterns.jButton[2].setEnabled(false);
                        }
                        else {
                            if(numberOfLayersInserted() && layerPropertiesInserted()) {
                                //System.out.println("AQUI_3");
                                addLayerPatterns();
                                setAllEnabled(false);
                            }
                        }
                    }
                }
                else {
                    if(numberOfLayersInserted() && layerPropertiesInserted()) {
                        updateLayerPatterns();
                        jFrameInputLayerPatterns.jComboBoxMain[0].setSelectedIndex(0);
                    }
                }
                break;
            case "Previous":
                jFrameInputLayerPatterns.dispose();
                JFrameInputLayerProperties jFrameInputLayerProperties = new JFrameInputLayerProperties(jFrameInputLayerPatterns.jFrameDocrosData, jFrameInputLayerPatterns.getLocation());
                break;
            case "Next":
                jFrameInputLayerPatterns.dispose();
                if(drawIsPossible()) {
                    jFrameInputLayerPatterns.jFrameDocrosData.drawFigure(1);
                    jFrameInputLayerPatterns.jFrameDocrosData.createJMenuItemView(); 
                }
                JFrameInputLayersToAnalyze jFrameInputLayersToAnalyse = new JFrameInputLayersToAnalyze(jFrameInputLayerPatterns.jFrameDocrosData, jFrameInputLayerPatterns.getLocation());
                break;
            case "Close":
                jFrameInputLayerPatterns.dispose();                
                break;
            case "Add":
                addLayerProperties();
                break;
            case "Delete":
                deleteLayerPatterns();
                break;
            case "Reset":
                jFrameInputLayerPatterns.jComboBoxMain[0].setSelectedIndex(jFrameInputLayerPatterns.jComboBoxMain[0].getSelectedIndex());
                break;
            case "Back":
                setAllEnabled(false);
                break;
        }
        
    }
    
    Boolean drawIsPossible() {
        
        if(!jFrameInputLayerPatterns.jFrameDocrosData.nonLinearMaterialModelTypeList.existsMaterialModel())
            return false;
        
        if(jFrameInputLayerPatterns.jFrameDocrosData.laminateGeoPropList == null || jFrameInputLayerPatterns.jFrameDocrosData.laminateGeoPropList.first == null)
            return false;
        
        if(jFrameInputLayerPatterns.jFrameDocrosData.layerPropertiesList == null || jFrameInputLayerPatterns.jFrameDocrosData.layerPropertiesList.first == null)
            return false;
        
        if(jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList == null || jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.first == null)
            return false;

        return true;
    }
    
    void setAllEnabled(Boolean enabled) {

        jFrameInputLayerPatterns.jTextFieldName.setText("");
        jFrameInputLayerPatterns.jTextFieldName.setBackground(Color.white);
        jFrameInputLayerPatterns.jTextFieldName.setVisible(enabled);
        jFrameInputLayerPatterns.jButtonUsedName.setVisible(enabled);
        jFrameInputLayerPatterns.jComboBoxMain[0].setVisible(!enabled);
        jFrameInputLayerPatterns.jComboBoxMain[0].setSelectedIndex(0);
        jFrameInputLayerPatterns.jTextFieldNumber.setText("");
        if(enabled) {
            jFrameInputLayerPatterns.jTextFieldNumber.setBackground(Color.white);
            if(jFrameInputLayerPatterns.jComboBoxMain[0].getItemCount() > 1)
                jFrameInputLayerPatterns.jButtonUsedName.setEnabled(enabled);
            else
                jFrameInputLayerPatterns.jButtonUsedName.setEnabled(!enabled);
        }
        else
            jFrameInputLayerPatterns.jTextFieldNumber.setBackground(jFrameInputLayerPatterns.getBackground());
        jFrameInputLayerPatterns.jTextFieldNumber.setEnabled(enabled);
        jFrameInputLayerPatterns.jComboBoxMain[1].removeAllItems();
        jFrameInputLayerPatterns.jComboBoxMain[1].setEnabled(false);
        jFrameInputLayerPatterns.jButtonDelProp.setEnabled(false);
        /***********************************************************************
        jFrameInputLayerPatterns.jComboBoxProp[0].removeAllItems();
        if(jFrameInputLayerPatterns.jFrameDocrosData.layerPropertiesList != null && jFrameInputLayerPatterns.jFrameDocrosData.layerPropertiesList.first != null) {
            LayerProperties node = jFrameInputLayerPatterns.jFrameDocrosData.layerPropertiesList.first;
            while(node != null) {
                jFrameInputLayerPatterns.jComboBoxProp[0].addItem(node.name);
                node = node.next;
            }
        }
        ***********************************************************************/
        jFrameInputLayerPatterns.jComboBoxProp[0].setSelectedIndex(0);
        jFrameInputLayerPatterns.jComboBoxProp[0].setEnabled(enabled);
        jFrameInputLayerPatterns.jComboBoxProp[1].setSelectedIndex(0);
        jFrameInputLayerPatterns.jComboBoxProp[1].setEnabled(enabled);
        jFrameInputLayerPatterns.jButtonAddProp.setEnabled(enabled);
        jFrameInputLayerPatterns.jButton[0].setVisible(false);
        jFrameInputLayerPatterns.jButton[1].setVisible(enabled);
        if(enabled) {
            jFrameInputLayerPatterns.jButton[1].setText("Back");
            jFrameInputLayerPatterns.jButton[1].setEnabled(enabled);
            jFrameInputLayerPatterns.jButton[2].setText("OK");
        }
        else {
            jFrameInputLayerPatterns.jButton[2].setText("New");
        }
        jFrameInputLayerPatterns.jButton[2].setEnabled(true);
    }
    
    void setAllJLabelMessageInvisible() {
        int i;
        for(i=0; i<jFrameInputLayerPatterns.jLabelMessage.length; i++)
            jFrameInputLayerPatterns.jLabelMessage[i].setVisible(false);
    }
    
    void updateLayerPatterns() {
    
        int i, numLayerProp;
        int phases[];
        String layerPatternsName, layerPropNameAndPhase;
        LayerProperties layerProperties[];
        
        layerPatternsName = String.valueOf(jFrameInputLayerPatterns.jComboBoxMain[0].getSelectedItem());
        numLayerProp = jFrameInputLayerPatterns.jComboBoxMain[1].getItemCount();
        layerProperties = new LayerProperties[numLayerProp];
        phases = new int[numLayerProp];        
        for(i=0; i<numLayerProp; i++) {
            layerPropNameAndPhase = String.valueOf(jFrameInputLayerPatterns.jComboBoxMain[1].getItemAt(i));
            layerProperties[i] = jFrameInputLayerPatterns.jFrameDocrosData.layerPropertiesList.getLayerProperties(getLayerPropertiesName(layerPropNameAndPhase));
            phases[i] = getLayerPropertiesPhase(layerPropNameAndPhase);
        }
        LayerPatterns node = jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.getLayerPatterns(layerPatternsName);
        node.numLayers = Integer.valueOf(jFrameInputLayerPatterns.jTextFieldNumber.getText());
        node.layerProperties = layerProperties;
        node.layerPropertiesPhase = phases;
        
        jFrameInputLayerPatterns.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputLayerPatterns.jFrameDocrosData.dataFile != null)
            jFrameInputLayerPatterns.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void deleteLayerPatterns() {
        
        int i;
        String name = String.valueOf(jFrameInputLayerPatterns.jComboBoxMain[0].getSelectedItem());
        LayerPatterns node = jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.getLayerPatterns(name);
        for(i=0; i<node.layerProperties.length; i++) {
            node.layerProperties[i].countLayerPatterns--;
            node.layerProperties[i].geoProp.countLayerPatterns--;
            node.layerProperties[i].materialModel.countLayerPatterns--;
            /*******************************************************************
            if(node.layerProperties[i].geoProp.countLayerPatterns == 0)
                jFrameInputLayerPatterns.jFrameDocrosData.laminateGeoPropList.delLaminateGeoProp(node.layerProperties[i].geoProp.name);    
            if(node.layerProperties[i].materialModel.countLayerPatterns == 0)
                jFrameInputLayerPatterns.jFrameDocrosData.nonLinearMaterialModelTypeList.delNonLinearMaterialModel(node.layerProperties[i].materialModel);
            if(node.layerProperties[i].countLayerPatterns == 0)
                jFrameInputLayerPatterns.jFrameDocrosData.layerPropertiesList.delLayerProperties(node.layerProperties[i].name);
            *******************************************************************/
        }    
        if(jFrameInputLayerPatterns.jFrameDocrosData.monotonicLoadTypeDataList != null && jFrameInputLayerPatterns.jFrameDocrosData.monotonicLoadTypeDataList != null) {
            MonotonicLoadTypeData monotonic = jFrameInputLayerPatterns.jFrameDocrosData.monotonicLoadTypeDataList.first;
            while(monotonic != null) {
                if(monotonic.layerPatternsName.equals(name)) {
                    jFrameInputLayerPatterns.jFrameDocrosData.monotonicLoadTypeDataList.delMonotonicLoadTypeData(monotonic.name);
                    if(jFrameInputLayerPatterns.jFrameDocrosData.phaseDataList != null && jFrameInputLayerPatterns.jFrameDocrosData.phaseDataList.first != null) {
                        PhaseData phase = jFrameInputLayerPatterns.jFrameDocrosData.phaseDataList.first;
                        while(phase != null) {
                            if(phase.loadType.equals("_MONOTONIC") && phase.loadName.equals(monotonic.name))
                                jFrameInputLayerPatterns.jFrameDocrosData.phaseDataList.delPhaseData(phase);
                            phase = phase.next;
                        }
                    }
                }
                monotonic = monotonic.next;
            }
        }
        
        if(jFrameInputLayerPatterns.jFrameDocrosData.cyclicLoadTypeDataList != null && jFrameInputLayerPatterns.jFrameDocrosData.cyclicLoadTypeDataList != null) {
            CyclicLoadTypeData cyclic = jFrameInputLayerPatterns.jFrameDocrosData.cyclicLoadTypeDataList.first;
            while(cyclic != null) {
                if(jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.getLayerPatternsName(cyclic.layerNumber).equals(name)) {
                    jFrameInputLayerPatterns.jFrameDocrosData.cyclicLoadTypeDataList.delCyclicLoadTypeData(cyclic.name);
                    if(jFrameInputLayerPatterns.jFrameDocrosData.phaseDataList != null && jFrameInputLayerPatterns.jFrameDocrosData.phaseDataList.first != null) {
                        PhaseData phase = jFrameInputLayerPatterns.jFrameDocrosData.phaseDataList.first;
                        while(phase != null) {
                            if(phase.loadType.equals("_CYCLIC") && phase.loadName.equals(cyclic.name))
                                jFrameInputLayerPatterns.jFrameDocrosData.phaseDataList.delPhaseData(phase);
                            phase = phase.next;
                        }
                    }
                }
                cyclic = cyclic.next;
            }
        }
        jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.delLayerPatterns(name);
        jFrameInputLayerPatterns.jComboBoxMain[0].removeItem(name);
        jFrameInputLayerPatterns.jComboBoxMain[0].setSelectedIndex(0);
        //jFrameInputLayerPatterns.hideLayerProperties();
        if(jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList == null || jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.first == null) {
            setAllEnabled(true);
            jFrameInputLayerPatterns.jButton[1].setVisible(false);
            jFrameInputLayerPatterns.jButtonNext.setEnabled(false);
            for(i=5; i<10; i++)
                jFrameInputLayerPatterns.jFrameDocrosData.jMenuItemEdit[i].setEnabled(false);
        }
        //jFrameInputLayerPatterns.jFrameDocrosData.changedLists[jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsListIndex] = true;
        jFrameInputLayerPatterns.jFrameDocrosData.jMenuItemSave.setEnabled(true);  
        if(jFrameInputLayerPatterns.jFrameDocrosData.dataFile != null)
            jFrameInputLayerPatterns.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
 
    void addLayerPatterns() {
        
        int i;
        if(jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList == null)
            jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList = new LayerPatternsList();
        LayerPatterns node = new LayerPatterns(jFrameInputLayerPatterns.jTextFieldName.getText(), Integer.valueOf(jFrameInputLayerPatterns.jTextFieldNumber.getText()), jFrameInputLayerPatterns.jComboBoxMain[1].getItemCount());
        for(i=0; i<jFrameInputLayerPatterns.jComboBoxMain[1].getItemCount(); i++) {
            LayerProperties layerProp = jFrameInputLayerPatterns.jFrameDocrosData.layerPropertiesList.getLayerProperties(getLayerPropertiesName(String.valueOf(jFrameInputLayerPatterns.jComboBoxMain[1].getItemAt(i))));
            layerProp.countLayerPatterns++;
            layerProp.geoProp.countLayerPatterns++;
            layerProp.materialModel.countLayerPatterns++;
            node.layerProperties[i] = layerProp;
            node.layerPropertiesPhase[i] = getLayerPropertiesPhase(String.valueOf(jFrameInputLayerPatterns.jComboBoxMain[1].getItemAt(i)));
        }  
        jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList.addLayerPatterns(node);
        jFrameInputLayerPatterns.jComboBoxMain[0].addItem(node.name);
        jFrameInputLayerPatterns.jButtonNext.setEnabled(true);
        for(i=5; i<10; i++)
            jFrameInputLayerPatterns.jFrameDocrosData.jMenuItemEdit[i].setEnabled(true);
        jFrameInputLayerPatterns.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputLayerPatterns.jFrameDocrosData.dataFile != null)
            jFrameInputLayerPatterns.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }

    String getLayerPropertiesName(String nameAndPhase) {
        
        int i = 0;
        char c;
        String name = "";
        while((c = nameAndPhase.charAt(i)) != ' ') {
            name += c;
            i++;
        }
        return name;
    }
    
    int getLayerPropertiesPhase(String nameAndPhase) {
        
        int i = 0;
        while(!nameAndPhase.substring(i).startsWith(" / "))
            i++;
        return Integer.valueOf(nameAndPhase.substring(i + 3));//, nameAndPhase.length() - 1));
    }
    
    Boolean layerPropertiesInserted() {
        
        if(jFrameInputLayerPatterns.jComboBoxMain[1].getItemCount() == 0) {
            //jFrameInputLayerPatterns.jComboBoxMain[1].setBackground(Color.yellow);
            jFrameInputLayerPatterns.jLabelMessage[2].setVisible(true);
            jFrameInputLayerPatterns.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean numberOfLayersInserted() {
        
        if(jFrameInputLayerPatterns.jTextFieldNumber.getText().equals("")) {
            jFrameInputLayerPatterns.jTextFieldNumber.setBackground(Color.yellow);
            jFrameInputLayerPatterns.jLabelMessage[1].setVisible(true);
            jFrameInputLayerPatterns.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean existsLayerPatternsName() {
        
        LayerPatternsList list = jFrameInputLayerPatterns.jFrameDocrosData.layerPatternsList;
        if(list == null)
            return false;
        LayerPatterns node = list.first;
        while(node != null) {
            if(node.name.equals(jFrameInputLayerPatterns.jTextFieldName.getText()))
                return true;
            node = node.next;
        }   
        return false;
    }
    
    void addLayerProperties() {
        
        int i;
        Boolean hasNamePhase = false;
        String name = String.valueOf(jFrameInputLayerPatterns.jComboBoxProp[0].getSelectedItem());
        String phase = String.valueOf(jFrameInputLayerPatterns.jComboBoxProp[1].getSelectedItem());               
        for(i=0; i<jFrameInputLayerPatterns.jComboBoxMain[1].getItemCount(); i++)
            if(String.valueOf(jFrameInputLayerPatterns.jComboBoxMain[1].getItemAt(i)).equals(name + " / " + phase))
                hasNamePhase = true;
        if(!hasNamePhase) {       
            jFrameInputLayerPatterns.jComboBoxMain[1].addItem(name + " / " + phase);
            jFrameInputLayerPatterns.jComboBoxMain[1].setEnabled(true);
            jFrameInputLayerPatterns.jButtonDelProp.setEnabled(true);
            jFrameInputLayerPatterns.jLabelMessage[2].setVisible(false);
            jFrameInputLayerPatterns.jButton[2].setEnabled(true);
        }
        //jFrameInputLayerPatterns.jComboBoxMain[2].addItem(phase);
    }

}
