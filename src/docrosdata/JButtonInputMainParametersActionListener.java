
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputMainParametersActionListener implements ActionListener {

    JFrameInputMainParameters jFrameInputMainParameters;
       
    public JButtonInputMainParametersActionListener(JFrameInputMainParameters jf) {
        
        jFrameInputMainParameters = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        switch (ae.getActionCommand()) {
            case "OK":
                if(titleInserted() && axialLoadInserted() && toleranceInserted()) {// && numberLayersInserted() && numberLayersAnalyseInserted()) {
                    insertMainParameters();
                    jFrameInputMainParameters.jFrameDocrosData.jMenuItemEdit[1].setEnabled(true);
                    jFrameInputMainParameters.jButton[2].setEnabled(true);
                    //jFrameInputMainParameters.jFrameDocrosData.
                }
                break;
            case "Next":
                jFrameInputMainParameters.dispose();
                JFrameInputMaterialModel jFrameInputMaterialModel = new JFrameInputMaterialModel(jFrameInputMainParameters.jFrameDocrosData, jFrameInputMainParameters.getLocation());
                break;
            case "Close":
                jFrameInputMainParameters.dispose();
                break;
        }
    }
   
    void insertMainParameters() {
        
        //jFrameInputMainParameters.jFrameDocrosData.layerStressStrainGraphics[jFrameInputMainParameters.layerNumberIndex] = Integer.valueOf(jFrameInputMainParameters.jTextField[5].getText());
        String title = jFrameInputMainParameters.jTextField[0].getText();
        String sxs = '_' + String.valueOf(jFrameInputMainParameters.jComboBox[0].getSelectedItem()).toUpperCase();
        double axial = Double.valueOf(jFrameInputMainParameters.jTextField[1].getText());
        String units[] = {String.valueOf(jFrameInputMainParameters.jComboBox[1].getSelectedItem()), String.valueOf(jFrameInputMainParameters.jComboBox[2].getSelectedItem())};
        double tolerance = Double.valueOf(jFrameInputMainParameters.jTextField[2].getText());
        //int numLayers = Integer.valueOf(jFrameInputMainParameters.jTextField[3].getText());        
        jFrameInputMainParameters.jFrameDocrosData.mainParameters = new MainParameters(title, sxs, axial);
        jFrameInputMainParameters.jFrameDocrosData.unitsInputOutputData = new String[2];
        jFrameInputMainParameters.jFrameDocrosData.unitsInputOutputData = units;
        jFrameInputMainParameters.jFrameDocrosData.tolerances = tolerance;
        
        //jFrameInputMainParameters.jFrameDocrosData.changedMainParameters = true;
        jFrameInputMainParameters.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputMainParameters.jFrameDocrosData.dataFile != null)
            jFrameInputMainParameters.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
 
    }

    Boolean toleranceInserted() {
        
        if(jFrameInputMainParameters.jTextField[2].getText().isEmpty()) {
            jFrameInputMainParameters.jLabelMessage[2].setVisible(true);
            jFrameInputMainParameters.jTextField[2].setBackground(Color.yellow);
            jFrameInputMainParameters.jButton[0].setEnabled(false);
            //jFrameInputMainParameters.jButton[1].setEnabled(false);
            return false;
        }
        try{
            Double.valueOf(jFrameInputMainParameters.jTextField[2].getText());
        }
        catch(Exception e) {
            jFrameInputMainParameters.jLabelMessage[2].setVisible(true);
            jFrameInputMainParameters.jTextField[2].setBackground(Color.yellow);
            jFrameInputMainParameters.jButton[0].setEnabled(false);
            //jFrameInputMainParameters.jButton[1].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean axialLoadInserted() {
        
        if(jFrameInputMainParameters.jTextField[1].getText().isEmpty()) {
            jFrameInputMainParameters.jLabelMessage[1].setVisible(true);
            jFrameInputMainParameters.jTextField[1].setBackground(Color.yellow);
            jFrameInputMainParameters.jButton[0].setEnabled(false);
            //jFrameInputMainParameters.jButton[1].setEnabled(false);
            return false;
        }
                try{
            Double.valueOf(jFrameInputMainParameters.jTextField[1].getText());
        }
        catch(Exception e) {
            jFrameInputMainParameters.jLabelMessage[1].setVisible(true);
            jFrameInputMainParameters.jTextField[1].setBackground(Color.yellow);
            jFrameInputMainParameters.jButton[0].setEnabled(false);
            //jFrameInputMainParameters.jButton[1].setEnabled(false);
            return false;
        }
        return true;
    }
      
    Boolean titleInserted() {
        
        if(jFrameInputMainParameters.jTextField[0].getText().isEmpty()) {
            jFrameInputMainParameters.jLabelMessage[0].setVisible(true);
            jFrameInputMainParameters.jTextField[0].setBackground(Color.yellow);
            jFrameInputMainParameters.jButton[0].setEnabled(false);
            //jFrameInputMainParameters.jButton[1].setEnabled(false);
            return false;
        }
        return true;
        
    }
    
}
