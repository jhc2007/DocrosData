
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class JComboBoxInputMonotonicLoadTypeData implements ActionListener {

    JFrameInputMonotonicLoadTypeData jFrameInputMonotonicLoadTypeData;
    
    public JComboBoxInputMonotonicLoadTypeData(JFrameInputMonotonicLoadTypeData jf) {
        
        jFrameInputMonotonicLoadTypeData = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;
        for(i=0; i<jFrameInputMonotonicLoadTypeData.jLabelMessage.length; i++)
            jFrameInputMonotonicLoadTypeData.jLabelMessage[i].setVisible(false);
        jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(true);
        
        if(((JComboBox) ae.getSource()).getName() == null) {
           
            if(jFrameInputMonotonicLoadTypeData.jComboBox[0].getSelectedIndex() == 0) {
                //for(i=0; i<jFrameInputMonotonicLoadTypeData.jTextField.length; i++) {
                    jFrameInputMonotonicLoadTypeData.jTextField[1].setText("");
                    jFrameInputMonotonicLoadTypeData.jTextField[1].setBackground(Color.white);
                    jFrameInputMonotonicLoadTypeData.jTextField[1].setEnabled(false);
                //}
                jFrameInputMonotonicLoadTypeData.jComboBox[1].setSelectedIndex(0);
                jFrameInputMonotonicLoadTypeData.jComboBox[1].setEnabled(false);
                jFrameInputMonotonicLoadTypeData.jComboBox[2].setSelectedIndex(0);
                jFrameInputMonotonicLoadTypeData.jComboBox[2].setEnabled(false);
                jFrameInputMonotonicLoadTypeData.jButton[0].setVisible(false);
                //jFrameInputMonotonicLoadTypeData.jButton[1].setText("Back");
                jFrameInputMonotonicLoadTypeData.jButton[1].setVisible(false);
                jFrameInputMonotonicLoadTypeData.jButton[2].setText("New");
                
            }
            else {
                //System.out.println("AQUI 2");
                //for(i=0; i<jFrameInputMonotonicLoadTypeData.jTextField.length; i++) {
                    jFrameInputMonotonicLoadTypeData.jTextField[1].setBackground(Color.white);
                    jFrameInputMonotonicLoadTypeData.jTextField[1].setEnabled(true);
                //}
                jFrameInputMonotonicLoadTypeData.jComboBox[1].setEnabled(true);
                jFrameInputMonotonicLoadTypeData.jComboBox[2].setEnabled(true);
                String name = String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[0].getSelectedItem());
                MonotonicLoadTypeData node = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.getMonotonicLoadTypeData(name);
                jFrameInputMonotonicLoadTypeData.jComboBox[1].setSelectedItem(node.layerPatternsName);
                jFrameInputMonotonicLoadTypeData.jComboBox[2].setSelectedItem(node.layerNumber);
                //System.out.println("AQUI: " + String.valueOf(node.compressionStrainIncrement));
                jFrameInputMonotonicLoadTypeData.jTextField[1].setText(String.valueOf(node.compressionStrainIncrement));
                //jFrameInputMonotonicLoadTypeData.jTextField[4].setText(node.stopConditionName);
                //jFrameInputMonotonicLoadTypeData.jTextField[5].setText(String.valueOf(node.strain));                 
                jFrameInputMonotonicLoadTypeData.jButton[0].setText("Delete");
                jFrameInputMonotonicLoadTypeData.jButton[0].setVisible(true);
                jFrameInputMonotonicLoadTypeData.jButton[1].setText("Reset");
                jFrameInputMonotonicLoadTypeData.jButton[1].setVisible(true);
                jFrameInputMonotonicLoadTypeData.jButton[2].setText("OK");
                //jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(true);
            }
        }
        else {
            jFrameInputMonotonicLoadTypeData.jComboBox[2].removeAllItems();
            jFrameInputMonotonicLoadTypeData.jComboBox[2].addItem("Select");
            jFrameInputMonotonicLoadTypeData.jComboBox[2].setSelectedIndex(0);
            if(jFrameInputMonotonicLoadTypeData.jComboBox[1].getSelectedIndex() > 0) {
                String layerPatternsName = String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[1].getSelectedItem());
                int firstLayer, lastLayer;
                firstLayer = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.layerPatternsList.getFirstLayer(layerPatternsName);
                lastLayer = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.layerPatternsList.getLastLayer(layerPatternsName);
                for(i=firstLayer; i<=lastLayer; i++)
                    jFrameInputMonotonicLoadTypeData.jComboBox[2].addItem(i);
            }
        }
    }
    
}
