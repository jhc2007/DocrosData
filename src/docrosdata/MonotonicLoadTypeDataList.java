
package docrosdata;

public class MonotonicLoadTypeDataList {
    
    MonotonicLoadTypeData first;
    MonotonicLoadTypeData last;
    int count;
    
    public MonotonicLoadTypeDataList() {
        
        first = null;
        last = null;
        count = 0;
    }   
    
    void addMonotonicLoadTypeData(MonotonicLoadTypeData node) {
        
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
    
    void delMonotonicLoadTypeData(String nam) {
        
        MonotonicLoadTypeData node1, node2;
        node1 = first;
        node2 = node1.next;
        if(node1.name.equals(nam)) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && !node2.name.equals(nam)) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
    }
    
    MonotonicLoadTypeData getMonotonicLoadTypeData(String loadName) {
        
        MonotonicLoadTypeData node = first;
        while(node != null) {
            if(node.name.equals(loadName))
                return node;
            node = node.next;
        }
        return node;
    }

}
