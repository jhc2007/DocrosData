
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class JFrameInputLayerProperties extends JFrame {

    JFrameDocrosData jFrameDocrosData;
    Point location;
    JLabel jLabel[];
    JLabel jLabelMessage[];
    JTextField jTextFieldName;
    JComboBox jComboBox[];
    JButton jButton[];
    JButton jButtonNext, jButtonPrev, jButtonClose, jButtonUsedName;
    JButton jButtonHelp;
    
    String jLabelText[] = {"Properties Name", "Geometry Name", "Material Type", "Material Name"};
    String jButtonText[] = {"Delete", "Reset", "OK"};
    String jLabelMessageText[] = {
        "This name is already used",//0
        "Select geometry name",   //1
        "Select material type" ,  //2
        "Select material name"    //3
    };
    
    public JFrameInputLayerProperties(JFrameDocrosData jf, Point loc) {
        
        super("<LAYER_PROPERTIES>");
        jFrameDocrosData = jf;
        location = loc;
        init();
    }
    
    private void init() {
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        //this.setAlwaysOnTop(true);             
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, jLabelWidth, xPosition = 10, yPosition = 10;
        
        jLabelWidth = jFrameDocrosData.charWidth * JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText);
        
        JLabel jLabelTitle[];
        jLabelTitle = new JLabel[2];
        jLabelTitle[0] = new JLabel("Geometry and Material");
        jLabelTitle[1] = new JLabel("Properties of the Layers");
        jLabelTitle[0].setSize(jFrameDocrosData.charWidth * jLabelTitle[0].getText().length(), jFrameDocrosData.charHeight);
        jLabelTitle[1].setSize(jFrameDocrosData.charWidth * (jLabelTitle[1].getText().length() + 4), jFrameDocrosData.charHeight);
        jLabelTitle[0].setLocation(jLabelWidth + xPosition, yPosition);
        //yPosition += jFrameDocrosData.charHeight;
        jLabelTitle[1].setLocation(jLabelWidth + xPosition, yPosition + 18);
        this.add(jLabelTitle[0]);
        this.add(jLabelTitle[1]);
        
        if(getClass().getResourceAsStream("/help/helpLayerProperties.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle[1].getX() + jLabelTitle[1].getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("LayerProperties"));//, "Layer Properties"));
            this.add(jButtonHelp);
        }
        yPosition += 56;
        
        jLabel = new JLabel[4];
        jLabelMessage = new JLabel[4];
        jComboBox = new JComboBox[4];
        jButton = new JButton[jButtonText.length];
        
        jTextFieldName = new JTextField();
        jTextFieldName.setName("NameLayerProperties");
        jTextFieldName.setSize(jLabelTitle[1].getWidth() - 64, jFrameDocrosData.charHeight);
        jTextFieldName.setLocation(xPosition + jLabelWidth, yPosition);
        jTextFieldName.addKeyListener(new JTextFieldKeyListener());
        this.add(jTextFieldName);
        
        jButtonUsedName = new JButton("Used");
        jButtonUsedName.setSize(63, 26);
        jButtonUsedName.setLocation(xPosition + jLabelWidth + jLabelTitle[1].getWidth() - 64, yPosition);
        jButtonUsedName.addActionListener(new JButtonInputLayerPropertiesActioListener(this));
        this.add(jButtonUsedName);
        
        for(i=0; i<4; i++) {
            jLabel[i] = new JLabel(jLabelText[i]);
            jLabel[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
            jLabel[i].setLocation(xPosition, yPosition);
            this.add(jLabel[i]);
            
            jComboBox[i] = new JComboBox();
            jComboBox[i].setSize(jLabelTitle[1].getSize());
            jComboBox[i].setLocation(xPosition + jLabelWidth, yPosition);
            jComboBox[i].addItem("Select");
            switch (i) {
                case 0:
                    if(jFrameDocrosData.layerPropertiesList != null && jFrameDocrosData.layerPropertiesList.first != null) {
                        LayerProperties node = jFrameDocrosData.layerPropertiesList.first;
                        while(node != null) {
                            jComboBox[i].addItem(node.name);
                            node = node.next;
                        }
                        jTextFieldName.setVisible(false);
                        jButtonUsedName.setVisible(false);
                    }
                    else {
                        jButtonUsedName.setEnabled(false);
                        jComboBox[i].setVisible(false);
                    }
                    break;
                case 1:
                    if(jFrameDocrosData.laminateGeoPropList != null && jFrameDocrosData.laminateGeoPropList.first != null) {
                        LaminateGeoProp node = jFrameDocrosData.laminateGeoPropList.first;
                        while(node != null) {
                            jComboBox[i].addItem(node.name);
                            node = node.next;
                        }
                    }
                    break;
                case 2:
                    if(jFrameDocrosData.nonLinearMaterialModelTypeList != null && jFrameDocrosData.nonLinearMaterialModelTypeList.first != null) {
                        NonLinearMaterialModelType node = jFrameDocrosData.nonLinearMaterialModelTypeList.first;
                        while(node != null) {
                            if(node.list.count > 0)
                                jComboBox[i].addItem(node.name.substring(1));
                            node = node.next;
                        }
                    }
                    break;
            }
            jComboBox[i].setSelectedIndex(0);
            this.add(jComboBox[i]);
            
            yPosition += 30;
        }
        
        jComboBox[0].setName("LayerPropertiesName");
        jComboBox[0].addActionListener(new JComboBoxInputLayerProperties(this));
        jComboBox[2].setName("MaterialType");
        jComboBox[2].addActionListener(new JComboBoxInputLayerProperties(this));
        
        for(i=0; i<4; i++) {
            jLabelMessage[i] = new JLabel(jLabelMessageText[i]);
            jLabelMessage[i].setSize(jLabelTitle[1].getSize());
            jLabelMessage[i].setLocation(xPosition + jLabelWidth, yPosition);
            jLabelMessage[i].setForeground(Color.red);
            jLabelMessage[i].setVisible(false);
            this.add(jLabelMessage[i]);
        }
        
        yPosition += jFrameDocrosData.charHeight;
        
        for(i=0; i<jButtonText.length; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jLabelTitle[1].getWidth()) / 3, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * jButton[i].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputLayerPropertiesActioListener(this));
            this.add(jButton[i]);
        }
        jButton[0].setVisible(false);
        jButton[1].setVisible(false);
        
        yPosition += jFrameDocrosData.charHeight + 10;
        
        jButtonPrev = new JButton("Previous");
        jButtonPrev.setSize((jLabelWidth + jLabelTitle[1].getWidth()) / 3, 18);
        jButtonPrev.setLocation(xPosition, yPosition);
        jButtonPrev.addActionListener(new JButtonInputLayerPropertiesActioListener(this));
        this.add(jButtonPrev);
        
        jButtonClose = new JButton("Close");
        jButtonClose.setSize((jLabelWidth + jLabelTitle[1].getWidth()) / 3, 18);
        jButtonClose.setLocation(xPosition + jButtonPrev.getWidth(), yPosition);
        jButtonClose.addActionListener(new JButtonInputLayerPropertiesActioListener(this));
        this.add(jButtonClose);
        
        jButtonNext = new JButton("Next");
        jButtonNext.setSize((jLabelWidth + jLabelTitle[1].getWidth()) / 3, 18);
        jButtonNext.setLocation(xPosition + 2 * jButtonPrev.getWidth(), yPosition);
        jButtonNext.addActionListener(new JButtonInputLayerPropertiesActioListener(this));
        this.add(jButtonNext);
        
        jTextFieldName.addFocusListener(new JTextFieldNameFocusListener(jLabelMessage, -1, null, jButton[2]));
     
        for(i=0; i<4; i++)
            jComboBox[i].addFocusListener(new JComboBoxFocusListener(jLabelMessage, -1, jButton[2]));
        
        if(jFrameDocrosData.layerPropertiesList != null && jFrameDocrosData.layerPropertiesList.first != null)
            jButton[2].setText("New");
        else
            jButtonNext.setEnabled(false);
        
        yPosition += 2 * jFrameDocrosData.charHeight;
        
        this.setSize(jLabelWidth + jLabelTitle[1].getWidth() + 26, yPosition);
        this.setVisible(true);
    }
    
}
