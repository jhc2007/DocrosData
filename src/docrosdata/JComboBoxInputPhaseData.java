
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JComboBoxInputPhaseData implements ActionListener {

    JFrameInputPhaseData jFrameInputPhaseData;
    
    public JComboBoxInputPhaseData(JFrameInputPhaseData jf) {
        
        jFrameInputPhaseData = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        int i;
        for(i=0; i<jFrameInputPhaseData.jLabelMessage.length; i++)
            jFrameInputPhaseData.jLabelMessage[i].setVisible(false);
        jFrameInputPhaseData.jButton[2].setEnabled(true);
        jFrameInputPhaseData.jLabel[3].setVisible(false);
        jFrameInputPhaseData.jComboBox[2].setVisible(false);
        
        if(jFrameInputPhaseData.jComboBox[0].getSelectedIndex() > 0) {          

            int phase;
            phase = jFrameInputPhaseData.jComboBox[0].getSelectedIndex();
            //System.out.println("PAHSE: " + phase);
            PhaseData node = jFrameInputPhaseData.jFrameDocrosData.phaseDataList.getPhaseData(phase);
            //System.out.println("Node Type: " + node.loadType);
            jFrameInputPhaseData.jComboBox[1].removeAllItems();
            jFrameInputPhaseData.jComboBox[1].addItem("Select");

            if(node.loadType.equals("_MONOTONIC")) {
                jFrameInputPhaseData.jRadioButton[0].setSelected(true);
                jFrameInputPhaseData.jRadioButton[1].setSelected(false);
                MonotonicLoadTypeData monotonic = jFrameInputPhaseData.jFrameDocrosData.monotonicLoadTypeDataList.first;
                while(monotonic != null) {
                    jFrameInputPhaseData.jComboBox[1].addItem(monotonic.name);
                    monotonic = monotonic.next;
                }
                jFrameInputPhaseData.jLabel[3].setVisible(true);
                jFrameInputPhaseData.jComboBox[2].setVisible(true);
                jFrameInputPhaseData.jComboBox[2].setSelectedItem(node.stopConditionName);
            }
            else {
                jFrameInputPhaseData.jRadioButton[1].setSelected(true);
                jFrameInputPhaseData.jRadioButton[0].setSelected(false);
                CyclicLoadTypeData cyclic = jFrameInputPhaseData.jFrameDocrosData.cyclicLoadTypeDataList.first;
                while(cyclic != null) {
                    jFrameInputPhaseData.jComboBox[1].addItem(cyclic.name);
                    cyclic = cyclic.next;
                }
            }
            jFrameInputPhaseData.jComboBox[1].setSelectedItem(node.loadName);
            jFrameInputPhaseData.jComboBox[1].setEnabled(true);
            jFrameInputPhaseData.jRadioButton[0].setEnabled(true);
            jFrameInputPhaseData.jRadioButton[1].setEnabled(true);
            jFrameInputPhaseData.jButton[0].setText("Delete");
            jFrameInputPhaseData.jButton[0].setVisible(true);
            jFrameInputPhaseData.jButton[1].setText("Reset");
            jFrameInputPhaseData.jButton[1].setVisible(true);
            jFrameInputPhaseData.jButton[2].setText("OK");
        }
        else {
            jFrameInputPhaseData.jRadioButton[0].setSelected(false);
            jFrameInputPhaseData.jRadioButton[1].setSelected(false);
            jFrameInputPhaseData.jRadioButton[0].setEnabled(false);
            jFrameInputPhaseData.jRadioButton[1].setEnabled(false);
            jFrameInputPhaseData.jComboBox[1].setSelectedIndex(0);
            jFrameInputPhaseData.jComboBox[1].setEnabled(false);
            jFrameInputPhaseData.jButton[0].setVisible(false);
            jFrameInputPhaseData.jButton[1].setVisible(false);
            jFrameInputPhaseData.jButton[2].setText("New");
        }
    }
    
}
