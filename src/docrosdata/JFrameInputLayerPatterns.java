
package docrosdata;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

class JFrameInputLayerPatterns extends JFrame {

    JFrameDocrosData jFrameDocrosData;
    Point location;
    JLabel jLabelMain[];
    JLabel jLabelProp[];
    JLabel jLabelMessage[];
    JTextField jTextFieldName;
    JTextField jTextFieldNumber;
    JPanel jPanelProp;
    JComboBox jComboBoxMain[];
    JComboBox jComboBoxProp[];
    JButton jButton[];
    JButton jButtonPrev, jButtonNext, jButtonClose, jButtonUsedName;
    JButton jButtonAddProp;
    JButton jButtonDelProp;
    JButton jButtonHelp;
       
    String jLabelMainText[] = {"Pattern Name", "Number of Layers", "Layer Properties / Phase"};
    String jLabelPropText[] = {"Layer Properties", "Phase"};
    String jButtonText[] = {"Delete", "Back", "OK"};
    String jLabelMessageText[] = {"Name already used", "Insert number of layers", "Add layer properties"};
    
    public JFrameInputLayerPatterns(JFrameDocrosData jf, Point loc) {
        
        super("<LAYER_PATTERNS>");
        jFrameDocrosData = jf;
        location = loc;
        init();
    }
    
    private void init() {
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);
        //this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, jLabelWidth, jPanelImageWidth, xPosition = 10, yPosition = 10;

        JPanel jPanelImage;
        BufferedImage bufferedImage;
        JLabel jLabelImage;
        BufferedInputStream input;
        FileOutputStream output;
        File imageFile;
        byte[] buffer = new byte[256];
        int bytesRead; 
        Dimension screenDim;
        Image imageScaled;
        ImageIcon imageIcon;
        
        jLabelWidth = jFrameDocrosData.charWidth * (JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelMainText) - 4);
        
        JLabel jLabelTitle = new JLabel("Pattern Properties");
        jLabelTitle.setSize(jFrameDocrosData.charWidth * JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelMessageText), jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(jLabelWidth + xPosition, yPosition);
        this.add(jLabelTitle);
        
        if(getClass().getResourceAsStream("/help/helpLayerPatterns.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("LayerPatterns"));//, "Layer Patterns"));
            this.add(jButtonHelp);
        }
        yPosition += jFrameDocrosData.charHeight;
        
        jLabelMain = new JLabel[jLabelMainText.length];
        for(i=0; i<jLabelMainText.length; i++) {
            jLabelMain[i] = new JLabel(jLabelMainText[i]);
            jLabelMain[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
            jLabelMain[i].setLocation(xPosition, yPosition);
            this.add(jLabelMain[i]);          
            yPosition += jFrameDocrosData.charHeight;
        }
        
        yPosition = 10 + jFrameDocrosData.charHeight;
        
        jTextFieldName = new JTextField();
        jTextFieldName.setName("NameLayerPatterns");
        jTextFieldName.setSize(jLabelTitle.getWidth() - 64, jFrameDocrosData.charHeight);
        jTextFieldName.setLocation(xPosition + jLabelWidth, yPosition);
        jTextFieldName.addKeyListener(new JTextFieldKeyListener());
        this.add(jTextFieldName);
        
        jButtonUsedName = new JButton("Used");
        jButtonUsedName.setSize(63, 26);
        jButtonUsedName.setLocation(xPosition + jLabelWidth + jLabelTitle.getWidth() - 64, yPosition);
        jButtonUsedName.addActionListener(new JButtonInputLayerPatternsActioListener(this));
        this.add(jButtonUsedName);
        
        yPosition += jFrameDocrosData.charHeight;
        
        jTextFieldNumber = new JTextField();
        jTextFieldNumber.setName("Int");
        jTextFieldNumber.setSize(4 * jFrameDocrosData.charWidth, jFrameDocrosData.charHeight);
        jTextFieldNumber.setLocation(xPosition + jLabelWidth, yPosition);
        jTextFieldNumber.addKeyListener(new JTextFieldKeyListener());
        
        this.add(jTextFieldNumber);
   
        jComboBoxMain = new JComboBox[2];
        
        for(i=0; i<2; i++) {
            jComboBoxMain[i] = new JComboBox();
            jComboBoxMain[i].setSize(jLabelTitle.getSize());
            jComboBoxMain[i].setLocation(xPosition + jLabelWidth, jTextFieldName.getY() + i * 2 * jFrameDocrosData.charHeight);
            this.add(jComboBoxMain[i]);
        }
        jComboBoxMain[0].setName("Name");
        jComboBoxMain[0].addItem("Select");
        jComboBoxMain[1].setEnabled(false);

        yPosition += 2 * jFrameDocrosData.charHeight;
        
        jButtonDelProp = new JButton("Remove");
        jButtonDelProp.setSize(jLabelTitle.getWidth(), 18);
        jButtonDelProp.setLocation(jTextFieldName.getX(), yPosition);
        jButtonDelProp.setEnabled(false);
        jButtonDelProp.addActionListener(new JButtonInputLayerPatternsActioListener(this));
        this.add(jButtonDelProp);
        
        yPosition += 16 + 4 * jFrameDocrosData.charHeight;
        
        jLabelMessage = new JLabel[jLabelMessageText.length];
        for(i=0; i<jLabelMessageText.length; i++) {
            jLabelMessage[i] = new JLabel(jLabelMessageText[i]);
            jLabelMessage[i].setSize(jLabelTitle.getSize());
            jLabelMessage[i].setLocation(jLabelTitle.getX(), yPosition);
            jLabelMessage[i].setForeground(Color.red);
            jLabelMessage[i].setVisible(false);
            this.add(jLabelMessage[i]);
        }
        
        yPosition += jFrameDocrosData.charHeight;
        
        jButton = new JButton[jButtonText.length];
        for(i=0; i<jButtonText.length; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * jButton[i].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputLayerPatternsActioListener(this));
            this.add(jButton[i]);
        }
        jButton[0].setVisible(false);
        jButton[1].setVisible(false);
        
        yPosition += jFrameDocrosData.charHeight + 10;
        
        jButtonPrev = new JButton("Previous");
        jButtonPrev.setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
        jButtonPrev.setLocation(xPosition, yPosition);
        jButtonPrev.addActionListener(new JButtonInputLayerPatternsActioListener(this));
        this.add(jButtonPrev);

        jButtonClose = new JButton("Close");
        jButtonClose.setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
        jButtonClose.setLocation(xPosition + jButtonPrev.getWidth(), yPosition);
        jButtonClose.addActionListener(new JButtonInputLayerPatternsActioListener(this));
        this.add(jButtonClose);
        
        jButtonNext = new JButton("Next");
        jButtonNext.setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
        jButtonNext.setLocation(xPosition + 2 * jButtonPrev.getWidth(), yPosition);
        jButtonNext.addActionListener(new JButtonInputLayerPatternsActioListener(this));
        this.add(jButtonNext);

        jTextFieldName.addFocusListener(new JTextFieldNameFocusListener(jLabelMessage, -1, null, jButton[2]));
        jComboBoxMain[0].addActionListener(new JComboBoxInputLayerPatterns(this));
        jTextFieldNumber.addFocusListener(new JTextFieldIntNumberFocusListener(jLabelMessage, 1, jButton[2], null));

        jPanelImage = new JPanel();
        jPanelImage.setLocation(16 + jTextFieldName.getX() + jLabelTitle.getWidth(), jButtonHelp.getY());
        jPanelImage.setBackground(Color.white);
        jPanelImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        this.add(jPanelImage);
        
        jLabelImage = new JLabel();
        if(getClass().getResourceAsStream("/images/imageLayers.jpg") != null) {
            try {
                input = new BufferedInputStream(getClass().getResourceAsStream("/images/imageLayers.jpg"));
                imageFile = new File(System.getProperty("java.io.tmpdir") + "imageLayers.jpg");
                if(!imageFile.exists())
                    imageFile.createNewFile();
                imageFile.deleteOnExit();
                output = new FileOutputStream(imageFile);
                while((bytesRead = input.read(buffer)) != -1)
                    output.write(buffer, 0, bytesRead);
                output.close();
                bufferedImage = ImageIO.read(imageFile);
                jPanelImageWidth = bufferedImage.getWidth() * (yPosition + 10) / bufferedImage.getHeight() + 4;
                screenDim = Toolkit.getDefaultToolkit().getScreenSize();
                if(jLabelWidth + jLabelTitle.getWidth() + jPanelImageWidth + 56 > screenDim.width)
                    jPanelImageWidth = screenDim.width - jLabelWidth + jLabelTitle.getWidth() + jPanelImageWidth - 56;
                jPanelImage.setSize(jPanelImageWidth, yPosition + 5);
                jLabelImage.setSize(jPanelImageWidth - 8, jPanelImage.getHeight() - 8);                
                imageScaled = bufferedImage.getScaledInstance(jLabelImage.getWidth(), jLabelImage.getHeight(), Image.SCALE_SMOOTH);
                imageIcon = new ImageIcon(imageScaled);
                jLabelImage.setIcon(imageIcon);
            } catch (IOException ex) {
                Logger.getLogger(JFrameInputMaterialModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
            jPanelImage.setSize(0, 0);

        jPanelImage.add(jLabelImage);
                    
        yPosition += 30 + jFrameDocrosData.charHeight;        
                        
        this.setSize(jLabelWidth + jLabelTitle.getWidth() + jPanelImage.getWidth() + 46, yPosition);
        this.setVisible(true);
        
        jPanelProp = new JPanel();
        jPanelProp.setLayout(null);
        jPanelProp.setSize(jLabelWidth + jLabelTitle.getWidth(), 16 + 3 * jFrameDocrosData.charHeight);
        jPanelProp.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelProp.setLocation(xPosition, jLabelMain[2].getY() + 2 * jFrameDocrosData.charHeight);
        jPanelProp.setBackground(Color.LIGHT_GRAY);
        this.add(jPanelProp);
        
        jLabelProp = new JLabel[jLabelPropText.length];
        jComboBoxProp = new JComboBox[jLabelPropText.length];
        
        for(i=0; i<jLabelPropText.length; i++) {

            jLabelProp[i] = new JLabel(jLabelPropText[i]);
            jLabelProp[i].setSize(jFrameDocrosData.charWidth * jLabelProp[i].getText().length(), jFrameDocrosData.charHeight);
            jLabelProp[i].setLocation(10, xPosition + i * jFrameDocrosData.charHeight);
            jPanelProp.add(jLabelProp[i]);
            
            jComboBoxProp[i] = new JComboBox();
            jComboBoxProp[i].setSize(jLabelTitle.getWidth() - 10, jFrameDocrosData.charHeight);
            jComboBoxProp[i].setLocation(jLabelWidth, xPosition + i * jFrameDocrosData.charHeight);
            jPanelProp.add(jComboBoxProp[i]);
        }
        
        if(jFrameDocrosData.layerPropertiesList != null && jFrameDocrosData.layerPropertiesList.first != null) {
            LayerProperties node = jFrameDocrosData.layerPropertiesList.first;
            while(node != null) {
                jComboBoxProp[0].addItem(node.name);
                node = node.next;
            }
        }
        if(jFrameDocrosData.phaseDataList != null && jFrameDocrosData.phaseDataList.count > 0) {
            for(i=1; i<=jFrameDocrosData.phaseDataList.count; i++)
                jComboBoxProp[1].addItem(i);
        }
        else {
            for(i=1; i<4; i++)
                jComboBoxProp[1].addItem(i);
        }
   
        jButtonAddProp = new JButton("Add");
        jButtonAddProp.setSize(jComboBoxProp[0].getWidth(), 18);
        jButtonAddProp.setLocation(jComboBoxProp[1].getX(), jComboBoxProp[1].getY() + jFrameDocrosData.charHeight + 8);
        jButtonAddProp.addActionListener(new JButtonInputLayerPatternsActioListener(this));
        jPanelProp.add(jButtonAddProp);
        if(jFrameDocrosData.layerPatternsList != null && jFrameDocrosData.layerPatternsList.first != null) {
            LayerPatterns node = jFrameDocrosData.layerPatternsList.first;
            while(node != null) {
                jComboBoxMain[0].addItem(node.name);
                node = node.next;
            }
            jComboBoxMain[0].setSelectedIndex(0);
            jTextFieldName.setVisible(false);
            jButtonUsedName.setVisible(false);
        }
        else {
            jComboBoxMain[0].setVisible(false);
            jButtonNext.setEnabled(false);
            jButtonUsedName.setEnabled(false);
        }
    }
}
