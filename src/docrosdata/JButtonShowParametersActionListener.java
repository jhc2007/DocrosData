
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class JButtonShowParametersActionListener implements ActionListener {

    JFrameDocrosData jFrameDocrosData;
    JPanelLayerPatterns jPanelLayerPatterns;
    LayerProperties layerProperties;
    JFrame jFrameMaterial;
    JComboBox jComboBoxType;
    JComboBox jComboBoxName;
    JButton jButtonJFrameMaterial[];
    JLabel jLabelLayerPropertiesName;
    int yFirstPosition;
    int jFrameMaterialWidth;
    int jFrameMaterialHeight;
    
    JLabel jLabelParameterSymbol[];
    JLabel jLabelParameterUnit[];
    JTextField jTextFieldParameter[];
    JLabel jLabelCrackModel;
    JButton jButtonParameters[];
    JRadioButton jRadioButton[];
    
    String jButtonParametersLabel[] = {"Reset", "OK", "Close"};
    String jRadioButtonText[] = {"Trilinear", "Quadrilinear"};
    
    public JButtonShowParametersActionListener(JFrameDocrosData jfdd, JPanelLayerPatterns jplp, JFrame jfm, JComboBox jcbt, JComboBox jcbn, JButton jbjfm[], JLabel jllpn) {///, int ypos, int jfmw, int jfmh) {
        
        jFrameDocrosData = jfdd;
        jPanelLayerPatterns = jplp;
        layerProperties = jplp.layerPatterns.layerProperties[jplp.indexLayerProperties];
        jFrameMaterial = jfm;
        jComboBoxType = jcbt;
        jComboBoxName = jcbn;
        jButtonJFrameMaterial = jbjfm;
        jLabelLayerPropertiesName = jllpn;
        init();
    }
    
    private void init() {
        
        int i;
        yFirstPosition = jButtonJFrameMaterial[1].getY();
        jFrameMaterialWidth = jFrameMaterial.getWidth();
        jFrameMaterialHeight = jFrameMaterial.getHeight();
        //String jButtonParametersLabel[] = {"Reset", "OK", "Cancel"};
        jButtonParameters = new JButton[jButtonParametersLabel.length];
        for(i=0; i<jButtonParametersLabel.length; i++) {
            jButtonParameters[i] = new JButton(jButtonParametersLabel[i]);
            jButtonParameters[i].setSize(jButtonJFrameMaterial[1].getWidth(), 20);
        }
        jLabelCrackModel = new JLabel("Crack Model:");
        jLabelCrackModel.setSize(jFrameDocrosData.charWidth * jLabelCrackModel.getText().length(), 22);
        jLabelCrackModel.setVisible(false);
        jFrameMaterial.add(jLabelCrackModel);
        jRadioButton = new JRadioButton[jRadioButtonText.length];
        for(i=0; i<jRadioButtonText.length; i++) {
            jRadioButton[i] = new JRadioButton(jRadioButtonText[i]);
            jRadioButton[i].setName(jRadioButtonText[i]);
            jRadioButton[i].setSize(jFrameDocrosData.charWidth * (jRadioButtonText[i].length() - 1), 22);
            //jRadioButton[i].addActionListener(new JRadioButtonShowParametersActionListener(jLabelParameterSymbol, jLabelParameterUnit, jTextFieldParameter, jRadioButton));
            //jRadioButton[i].setLocation(xPosition + jLabelLeftWidth + j * jRadioButton[0].getWidth(), yPosition);
            jRadioButton[i].setVisible(false);
            jFrameMaterial.add(jRadioButton[i]);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) { 
        
        if(jComboBoxType.getSelectedItem() != null/* && jComboBoxLawName.getSelectedItem() != null*/) {
        
            int i, yPosition, yIncrement;
            int xPosition1 = 12;
            int xPosition2 = jComboBoxName.getX();
            int numParameters = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelType('_' + String.valueOf(jComboBoxType.getSelectedItem())).numberParameters;
            
            yPosition = yFirstPosition;
           
            if(jButtonJFrameMaterial[0].getText().equals("Hide Parameters")) {              

                jComboBoxType.setEnabled(true);
                jComboBoxName.setEnabled(true);

                for(i=0; i<jLabelParameterSymbol.length; i++) {
                    jLabelParameterSymbol[i].setVisible(false);
                    jLabelParameterUnit[i].setVisible(false);
                    jTextFieldParameter[i].setVisible(false);
                }
                
                jLabelCrackModel.setVisible(false);
                
                for(i=0; i<jRadioButtonText.length; i++)
                    jRadioButton[i].setVisible(false);
                
                for(i=1; i<jButtonJFrameMaterial.length; i++)
                     jButtonJFrameMaterial[i].setVisible(true);//.setLocation(jButtonJFrameMaterial[i].getX(), yFirstPosition);

                for(i=0; i<jButtonParametersLabel.length; i++)
                    jButtonParameters[i].setVisible(false);

                jButtonJFrameMaterial[0].setText("Show Parameters");

                jFrameMaterial.setSize(jFrameMaterialWidth, jFrameMaterialHeight);

            }
            else {
                String type, symbol;
                jLabelParameterSymbol = new JLabel[numParameters];
                jLabelParameterUnit = new JLabel[numParameters];
                jTextFieldParameter = new JTextField[numParameters];
                jComboBoxType.setEnabled(false);
                jComboBoxName.setEnabled(false);
                type = String.valueOf(jComboBoxType.getSelectedItem());
                
                if(type.equals("NLMM105")) { 
                    yIncrement = 22;
                    jLabelCrackModel.setVisible(true);                
                    jRadioButton[0].setVisible(true);
                    jRadioButton[1].setVisible(true);
                }
                else {
                    yIncrement = 24;
                    jLabelCrackModel.setVisible(false);
                    jRadioButton[0].setVisible(false);
                    jRadioButton[1].setVisible(false);
                }
                
                for(i=0; i<numParameters; i++) {   
                    
                    symbol = jFrameDocrosData.materialParametersSymbols[jFrameDocrosData.getMaterialIndex(type)][i];
                                                            
                    if(i == 12 && type.equals("NLMM105")) { 
                        jLabelCrackModel.setLocation(xPosition1, yPosition);
                        jRadioButton[0].setLocation(xPosition2 - 2, yPosition);
                        jRadioButton[1].setLocation(xPosition2 + 78, yPosition);
                        yPosition += yIncrement;
                    }
                    
                    jLabelParameterSymbol[i] = new JLabel(symbol);
                    jLabelParameterSymbol[i].setSize(jFrameDocrosData.charWidth * jLabelParameterSymbol[i].getText().length(), yIncrement);
                    jLabelParameterSymbol[i].setLocation(12, yPosition);
                    jLabelParameterSymbol[i].setVisible(true);
                    jFrameMaterial.add(jLabelParameterSymbol[i]);
                    
                    switch (symbol.charAt(0)) {
                        case 'E':
                            jLabelParameterUnit[i] = new JLabel("[" + jFrameDocrosData.unitsInputOutputData[0] + "/" + jFrameDocrosData.unitsInputOutputData[1] + "2]", SwingConstants.RIGHT);
                            break;
                        case 'f':
                            jLabelParameterUnit[i] = new JLabel("[" + jFrameDocrosData.unitsInputOutputData[0] + "/" + jFrameDocrosData.unitsInputOutputData[1] + "2]", SwingConstants.RIGHT);
                            break;
                        case 'ε':
                            jLabelParameterUnit[i] = new JLabel("[-]", SwingConstants.RIGHT); 
                            break;
                        case 'ω':
                            jLabelParameterUnit[i] = new JLabel("[" + jFrameDocrosData.unitsInputOutputData[1] + "]", SwingConstants.RIGHT);
                            break;
                        case 'l':
                            jLabelParameterUnit[i] = new JLabel("[" + jFrameDocrosData.unitsInputOutputData[1] + "]", SwingConstants.RIGHT);
                            break;
                        default:
                            jLabelParameterUnit[i] = new JLabel("[0-1]", SwingConstants.RIGHT);
                    }
                    jLabelParameterUnit[i].setSize(xPosition2 - xPosition1 - jLabelParameterSymbol[i].getWidth() - 10, yIncrement);
                    jLabelParameterUnit[i].setLocation(xPosition1 + jLabelParameterSymbol[i].getWidth(), yPosition);
                    jLabelParameterUnit[i].setVisible(true);
                    jFrameMaterial.add(jLabelParameterUnit[i]);
      
                    if(jComboBoxName.getSelectedItem() != null) {
                        double parameterValue = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jComboBoxType.getSelectedItem()), String.valueOf(jComboBoxName.getSelectedItem())).parameters[i];
                        jTextFieldParameter[i] = new JTextField(String.format("%7.4E", parameterValue).replace(',', '.'));
                        jTextFieldParameter[i].addFocusListener(new JTextFieldFocusListener(jButtonParameters, jButtonJFrameMaterial[0], parameterValue));
                    }
                    else {
                        jTextFieldParameter[i] = new JTextField("");
                        jTextFieldParameter[i].addFocusListener(new JTextFieldFocusListener(jButtonParameters, jButtonJFrameMaterial[0], 0));
                    }
                    //jTextFieldParameter[i] = new JTextField(formatter.format(jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModel('_' + String.valueOf(jComboBoxLawType.getSelectedItem()), String.valueOf(jComboBoxLawName.getSelectedItem())).parameters[i]));
                    if(jFrameDocrosData.charWidth * jTextFieldParameter[i].getText().length() > jComboBoxName.getWidth())
                        jTextFieldParameter[i].setSize(jFrameDocrosData.charWidth * jTextFieldParameter[i].getText().length(), yIncrement);
                    else
                        jTextFieldParameter[i].setSize(jComboBoxName.getWidth(), yIncrement);
                    jTextFieldParameter[i].setLocation(jComboBoxName.getX(), yPosition);
                    
                    jTextFieldParameter[i].addKeyListener(new JTextFieldKeyListener());
                    jFrameMaterial.add(jTextFieldParameter[i]);
                    
                    yPosition += yIncrement;
                }
                if(type.equals("NLMM105")) {
                    jRadioButton[0].addActionListener(new JRadioButtonShowParametersActionListener(jLabelParameterSymbol, jLabelParameterUnit, jTextFieldParameter, jRadioButton));
                    jRadioButton[1].addActionListener(new JRadioButtonShowParametersActionListener(jLabelParameterSymbol, jLabelParameterUnit, jTextFieldParameter, jRadioButton));
                    if(jTextFieldParameter[12].getText().equals("") || jTextFieldParameter[13].getText().equals("") || (Double.valueOf(jTextFieldParameter[12].getText()) == 0.0 && Double.valueOf(jTextFieldParameter[13].getText()) == 0.0)) {
                        jRadioButton[0].setSelected(true);
                        jRadioButton[1].setSelected(false);
                        for(i=12; i<14; i++) {
                            jLabelParameterSymbol[i].setForeground(Color.LIGHT_GRAY);
                            jLabelParameterUnit[i].setForeground(Color.LIGHT_GRAY);
                            jTextFieldParameter[i].setText("0.0");
                            jTextFieldParameter[i].setEnabled(false);
                        }
                    }
                    else {
                        jRadioButton[1].setSelected(true);
                        jRadioButton[0].setSelected(false);
                    }
                }
                
                yPosition += 8;
                
                for(i=1; i<jButtonJFrameMaterial.length; i++) {
                    //jButtonJFrameMaterial[i].setEnabled(false);
                    jButtonJFrameMaterial[i].setVisible(false);
                }
                
                for(i=0; i<jButtonParametersLabel.length; i++) {
                    jButtonParameters[i].setLocation(jButtonJFrameMaterial[i + 1].getX(), yPosition);
                    jButtonParameters[i].setVisible(true);
                    jButtonParameters[i].addActionListener(new JButtonParametersActionListener(jFrameDocrosData, jPanelLayerPatterns, jFrameMaterial, jComboBoxType, jComboBoxName, jLabelParameterSymbol, jLabelParameterUnit, jTextFieldParameter, jButtonJFrameMaterial[0], jButtonParameters, jLabelLayerPropertiesName, jFrameMaterialWidth, jFrameMaterialHeight, jRadioButton));
                    jFrameMaterial.add(jButtonParameters[i]);
                }
                
                if(jComboBoxName.getSelectedItem() == null) {                
                    ((JButton) ae.getSource()).setEnabled(false);//setVisible(false);
                    jButtonParameters[0].setEnabled(false);
                }
                else
                    jButtonJFrameMaterial[0].setText("Hide Parameters");
                                        
                jFrameMaterial.setSize(jFrameMaterialWidth, jFrameMaterialHeight + numParameters * 24);
            }

        }
    }
}

