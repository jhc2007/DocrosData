
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;

public class JMenuItemSaveActionListener implements ActionListener {

    JFrameDocrosData jFrameDocrosData;
    BufferedWriter bufferedWriterDataFile;

    public JMenuItemSaveActionListener(JFrameDocrosData jfdd) {

        jFrameDocrosData = jfdd;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        if(((JMenuItem) ae.getSource()).getText().equals("Save As") || jFrameDocrosData.dataFile == null) {

            JFileChooser jFileChooserSave;
            
            if(jFrameDocrosData.path == null)
                jFileChooserSave = new JFileChooser();
            else
                jFileChooserSave = new JFileChooser(jFrameDocrosData.path);
            
            if(((JMenuItem) ae.getSource()).getText().equals("Save As"))
                jFileChooserSave.setDialogTitle("Save As");
            
            if (jFileChooserSave.showSaveDialog(jFrameDocrosData) == javax.swing.JFileChooser.APPROVE_OPTION) {
                
                jFrameDocrosData.dataFile = new File(jFileChooserSave.getSelectedFile().getAbsolutePath() + "_sd.dat");
                jFrameDocrosData.path = jFrameDocrosData.dataFile.getAbsolutePath().substring(0, jFrameDocrosData.dataFile.getAbsolutePath().length() - jFrameDocrosData.dataFile.getName().length());                
                try {
                    if(jFrameDocrosData.pathFile.exists()) {
                        BufferedWriter pathBufferedWriter;
                        pathBufferedWriter = new BufferedWriter(new FileWriter(jFrameDocrosData.pathFile.getAbsoluteFile()));
                        pathBufferedWriter.write(jFrameDocrosData.path);
                        pathBufferedWriter.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    jFrameDocrosData.dataFile.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }        
        try {
            bufferedWriterDataFile = new BufferedWriter(new FileWriter(jFrameDocrosData.dataFile.getAbsoluteFile()));
        } catch (IOException ex) {
            Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        writeTagAndCount("<DOCROS_DATA_FILE>", null, -1, null);
        writeMainParameters();
        writeUnitsInputOutputData();
        writeTolerances();
        writeLayerStressStrainGraphics();
        writeTagAndCount("<MESH>", null, -1, null);
        writeNonLinearMaterialModelLists();
        writeLaminateGeoPropList();
        writeLayerPropertiesList();
        writeLayerPatternsList();
        writeTagAndCount("</MESH>", null, -1, null);
        writeTagAndCount("<LOAD_PARAMETERS>", null, -1, null);
        writePreStressedLayersList();
        writeMonotonicLoadTypeDataList();
        writeStopConditionLayerStrainList();
        writeCyclicLoadTypeDataList();
        writePhaseDataList();
        writeTagAndCount("</LOAD_PARAMETERS>", null, -1, null);
        writeTagAndCount("</DOCROS_DATA_FILE>", null, -1, null);
        try {
            bufferedWriterDataFile.close();
        } catch (IOException ex) {
            Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        jFrameDocrosData.resetAll();
        jFrameDocrosData.openFile();
        jFrameDocrosData.jMenuItemSave.setEnabled(false);
        jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void writeComments(String comments[]) {
        
        int i;
        try {
            for(i=0; i<comments.length; i++) {
                bufferedWriterDataFile.write(comments[i]);
                bufferedWriterDataFile.newLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void writeTagAndCount(String tag, String comment1, int count, String comment2) {
        
        try {
            bufferedWriterDataFile.write(tag);
            bufferedWriterDataFile.newLine();
            bufferedWriterDataFile.newLine();
            if(count >= 0) {
                if(comment1 != null)
                    bufferedWriterDataFile.write(comment1);
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.write("COUNT = " + count + " ; ");
                if(comment2 != null)
                    bufferedWriterDataFile.write(comment2);
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.newLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void writeLayerStressStrainGraphics() {
        
        if(jFrameDocrosData.layerStressStrainGraphics != null) {
            
            int i;
            String comments[] = {
                "# Content of each column:",
                "# A -> Number of layers to be analyzed",
                "# B -> Numbers of the layers to be analyzed",
                "# A\tB"
            };
            try {
                writeTagAndCount("<LAYER-STRESS-STRAIN-GRAPHICS>", null, -1, null);
                writeComments(comments);
                bufferedWriterDataFile.write("  " + jFrameDocrosData.layerStressStrainGraphics.length + "\t");
                for(i=0; i<jFrameDocrosData.layerStressStrainGraphics.length; i++)
                    bufferedWriterDataFile.write(jFrameDocrosData.layerStressStrainGraphics[i] + " ");
                bufferedWriterDataFile.write(";");
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</LAYER-STRESS-STRAIN-GRAPHICS>", null, -1, null);
            } catch (IOException ex) {
                Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    void writeTolerances() {
        
        String comments[] = {
          "# Content of each column:",
          "# A -> Tolerance for the force equation",
          "# A"
        };
        if(jFrameDocrosData.tolerances >= 0) {
            try {
                writeTagAndCount("<TOLERANCES>", null, -1, null);
                writeComments(comments);
                bufferedWriterDataFile.write("  " + jFrameDocrosData.tolerances + " ;");
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</TOLERANCES>", null, -1, null);
            } catch (IOException ex) {
                Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    void writeUnitsInputOutputData() {
        
        String comments[] = {
            "# Input",
            "# Content of each column:",
            "# A -> Input force unit",
            "# B -> Input length unit",
            "# A\tB"
        };
        if(jFrameDocrosData.unitsInputOutputData != null) {
            try {
                writeTagAndCount("<UNITS_INPUT_OUTPUT_DATA>", null, -1, null);
                writeComments(comments);
                bufferedWriterDataFile.write("  " + jFrameDocrosData.unitsInputOutputData[0] + "\t" + jFrameDocrosData.unitsInputOutputData[1] + " ;");
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</UNITS_INPUT_OUTPUT_DATA>", null, -1, null);
            } catch (IOException ex) {
                Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    void writeMainParameters() {

        if(jFrameDocrosData.mainParameters != null) {
            try {
                writeTagAndCount("<MAIN_PARAMETERS>", null, -1, null);
                bufferedWriterDataFile.write("MAIN_TITLE = " + jFrameDocrosData.mainParameters.title + " ;");
                bufferedWriterDataFile.newLine();
                if(jFrameDocrosData.layerPatternsList != null)
                    bufferedWriterDataFile.write("NUM_LAYERS = " + jFrameDocrosData.layerPatternsList.getNumberOfLayers() + " ;");
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.write("CONTROL_FILE = _YES ;");
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.write("SxS-GRAPHICS = " + jFrameDocrosData.mainParameters.sxsGraphics + " ;");
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.write("AXIAL_LOAD = " + jFrameDocrosData.mainParameters.axialLoad + " ;");
                bufferedWriterDataFile.newLine();
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</MAIN_PARAMETERS>", null, -1, null);
            } catch (IOException ex) {
                Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void writeStopConditionLayerStrainList() {

        if(jFrameDocrosData.stopConditionLayerStrainList != null && jFrameDocrosData.stopConditionLayerStrainList.count > 0) {
            
            String comments[] = {
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name",
                "# C -> Layer Number",
                "# D -> Layer Pattern",
                "# E -> Strain",
                "# A\tB\tC\tD\tE"
            };
            int i, count;
            count = jFrameDocrosData.stopConditionLayerStrainList.count;
            writeTagAndCount("<STOP_CONDITION_LAYER_STRAIN>", "# Keyword: _SCT_LAYSTRAIN" + System.lineSeparator() + "# Data for strain stop conditions", count, null);
            writeComments(comments);
            StopConditionLayerStrain node = jFrameDocrosData.stopConditionLayerStrainList.first;
            try {
                for(i=1; i<=count; i++) {              
                    bufferedWriterDataFile.write("  " + i + "\t" + node.name + "\t" + node.layerNumber + "\t" + node.layerPatternsName + "\t" + node.strain + "\t;");
                    bufferedWriterDataFile.newLine();
                    node = node.next;
                }
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</STOP_CONDITION_LAYER_STRAIN>", null, -1, null);
            } catch (IOException ex) {
                    Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void writePreStressedLayersList() {
        
        if(jFrameDocrosData.preStressedLayersList != null && jFrameDocrosData.preStressedLayersList.count > 0) {
            String comments[] = {
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Layer Pattern Name",
                "# C -> Layer or Layer Range",
                "# D -> Strain",
                "# A\tB\tC\tD"
            };
            int i, count;
            count = jFrameDocrosData.preStressedLayersList.count;
            writeTagAndCount("<PRESTRESSED_LAYERS>", "# Pre-Stressed Layers", count, "# Number of Pre-Stressed Layers sets");
            writeComments(comments);
            PreStressedLayers node = jFrameDocrosData.preStressedLayersList.first;
            try {
                for(i=1; i<=count; i++) {
                    if(node.firstLayer == node.lastLayer)
                        bufferedWriterDataFile.write("  " + i + "\t" + node.layerPatternsName + "\t" + node.firstLayer + "\t" + node.strain + "\t;");
                    else
                        bufferedWriterDataFile.write("  " + i + "\t" + node.layerPatternsName + "\t[" + node.firstLayer + "-" + node.lastLayer + "]\t" + node.strain + "\t;");
                    bufferedWriterDataFile.newLine();
                    node = node.next;
                }
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</PRESTRESSED_LAYERS>", null, -1, null);
            } catch (IOException ex) {
                    Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    void writeMonotonicLoadTypeDataList() {

        if(jFrameDocrosData.monotonicLoadTypeDataList != null && jFrameDocrosData.monotonicLoadTypeDataList.count > 0) {
            String comments[] = {
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name",
                "# C -> Layer Number",
                "# D -> Layer Pattern",
                "# E -> Compression strain increment",
                "# A\tB\tC\tD\tE"
            };
            int i, count;
            count = jFrameDocrosData.monotonicLoadTypeDataList.count;
            writeTagAndCount("<MONOTONIC_LOAD_TYPE_DATA>", "# Keyword: _MONOTONIC" + System.lineSeparator() + "# Data for Monotonic Loads", count, "# Number of Monotonic Loads");
            writeComments(comments);
            MonotonicLoadTypeData node = jFrameDocrosData.monotonicLoadTypeDataList.first;
            try {
                for(i=1; i<=count; i++) {
                    bufferedWriterDataFile.write("  " + i + "\t" + node.name + "\t" + node.layerNumber + "\t" + node.layerPatternsName + "\t" + node.compressionStrainIncrement + "\t;");
                    bufferedWriterDataFile.newLine();
                    node = node.next;
                }
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</MONOTONIC_LOAD_TYPE_DATA>", null, -1, null);
            } catch (IOException ex) {
                    Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    void writePhaseDataList() {
        
        if(jFrameDocrosData.phaseDataList != null && jFrameDocrosData.phaseDataList.first != null) {
            String comments[] = {
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Load type (_MONOTONIC, _CYCLIC )",
                "# C -> Load type name",
                "# D -> Stop condition type",
                "# E -> Stop condition name",
                "# A\tB\tC\tD\tE"
            };
            int i, count;
            count = jFrameDocrosData.phaseDataList.count;
            writeTagAndCount("<PHASE_DATA>", "# Data for characterizing the phases", count, "# Number of Phases");
            writeComments(comments);
            PhaseData node = jFrameDocrosData.phaseDataList.first;
            try {
                for(i=1; i<=count; i++) {
                    bufferedWriterDataFile.write("  " + i + "\t" + node.loadType + "\t" + node.loadName + "\t");
                    if(node.loadType.equals("_MONOTONIC"))
                        bufferedWriterDataFile.write("_SCT_LAYSTRAIN" + "\t" + node.stopConditionName + "\t;");
                    else
                        bufferedWriterDataFile.write("_NONE\tNONE\t;");
                    bufferedWriterDataFile.newLine();
                    node = node.next;
                }
            bufferedWriterDataFile.newLine();
            writeTagAndCount("</PHASE_DATA>", null, -1, null);
            } catch (IOException ex) {
                    Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void writeCyclicLoadTypeDataList() {

        if(jFrameDocrosData.cyclicLoadTypeDataList != null && jFrameDocrosData.cyclicLoadTypeDataList.count > 0) {
            String comments[] = {
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name",
                "# C -> Layer Number",
                "# D -> Number Of Steps",
                "# E -> Description of the steps, [ NCycle_i @ Amplitude_i; NCycle_(i+1) @ Amplitude_(i+1) ]",
                "# F -> Strain increment",
                "# A\tB\tC\tD\tE\tF"
            };
            int i, j, count;
            count = jFrameDocrosData.cyclicLoadTypeDataList.count;
            writeTagAndCount("<CYCLIC_LOAD_TYPE_DATA>", "# Keyword: _CYCLIC" + System.lineSeparator() + "# Data for Cyclic Loads", count, "# Number of Cyclic Loads");
            writeComments(comments);
            CyclicLoadTypeData node = jFrameDocrosData.cyclicLoadTypeDataList.first;
            try {
                for(i=1; i<=count; i++) {
                    bufferedWriterDataFile.write("  " + i + "\t" + node.name + "\t" + node.layerNumber + "\t" + node.steps + "\t[ ");
                    for(j=0; j<node.steps; j++) {
                        bufferedWriterDataFile.write(node.cycles[j] + " @ " + node.amplitude[j]);
                        if(j < node.steps - 1)
                            bufferedWriterDataFile.write(" ; ");
                    }
                    bufferedWriterDataFile.write(" ]\t");
                    bufferedWriterDataFile.write(String.valueOf(node.strainIncrement) + "\t;");
                    bufferedWriterDataFile.newLine();
                    node = node.next;
                }
            bufferedWriterDataFile.newLine();
            writeTagAndCount("</CYCLIC_LOAD_TYPE_DATA>", null, -1, null);
            } catch (IOException ex) {
                    Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void writeNonLinearMaterialModelLists() {
        
        String materialComments[][] = {
            {   //NLMM101
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the material model",
                "# C -> Mass per unit volume",
                "# D -> Temperature coefficient",
                "# E -> Concrete initial Young modulus",
                "# F -> Concrete strain at peak confined stress",
                "# G -> Confined concrete stress",
                "# H -> Non-dimensional critical strain on the compression envelope",
                "# I -> Strain at peak tension stress",
                "# J -> Concrete tension strength",
                "# K -> Critical strain on the tension envelope curve",
                "# A\tB\tC\tD\tE\tF\tG\tH\tI\tJ\tK"
           },
           {    //NLMM102
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the material model",
                "# C -> Mass per unit volume",
                "# D -> Temperature coefficient",
                "# E -> Concrete initial Young modulus",
                "# F -> Concrete strain at peak confined stress",
                "# G -> Confined concrete stress",
                "# H -> Non-dimensional critical strain on the compression envelope",
                "# I -> Strain at the end of 1st linear portion of multilinear tension envelope",
                "# J -> Stress at the end of 1st linear portion of multilinear tension envelope",
                "# K -> Strain at the end of 2nd linear portion of multilinear tension envelope",
                "# L -> Stress at the end of 2nd linear portion of multilinear tension envelope",
                "# M -> Strain at the end of 3nd linear portion of multilinear tension envelope",
                "# N -> Stress at the end of 3nd linear portion of multilinear tension envelope",
                "# O -> Strain at the end of 4nd linear portion of multilinear tension envelope",
                "# P -> Stress at the end of 4nd linear portion of multilinear tension envelope",
                "# Q -> Strain at the end of 5nd linear portion of multilinear tension envelope",
                "# A\tB\tC\tD\tE\tF\tG\tH\tI\tJ\tK\tL\tM\tN\tO\tP\tQ"
           },
           {   //NLMM103
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the material model",
                "# C -> Mass per unit volume",
                "# D -> Temperature coefficient",
                "# E -> Concrete initial Young modulus",
                "# F -> Concrete strain at peak confined stress",
                "# G -> Confined concrete stress",
                "# H -> Non-dimensional critical strain on the compression envelope",
                "# I -> Strain at peak tension stress",
                "# J -> Concrete tension strength",
                "# K -> Critical strain on the tension envelope curve",
                "# A\tB\tC\tD\tE\tF\tG\tH\tI\tJ\tK"
           },
           {    //NLMM104
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the material model",
                "# C -> Mass per unit volume",
                "# D -> Temperature coefficient",
                "# E -> Concrete initial Young modulus",
                "# F -> Concrete strain at peak confined stress",
                "# G -> Confined concrete stress",
                "# H -> Non-dimensional critical strain on the compression envelope",
                "# I -> Strain at the end of 1st linear portion of multilinear tension envelope",
                "# J -> Stress at the end of 1st linear portion of multilinear tension envelope",
                "# K -> Strain at the end of 2nd linear portion of multilinear tension envelope",
                "# L -> Stress at the end of 2nd linear portion of multilinear tension envelope",
                "# M -> Strain at the end of 3nd linear portion of multilinear tension envelope",
                "# N -> Stress at the end of 3nd linear portion of multilinear tension envelope",
                "# O -> Strain at the end of 4nd linear portion of multilinear tension envelope",
                "# P -> Stress at the end of 4nd linear portion of multilinear tension envelope",
                "# Q -> Strain at the end of 5nd linear portion of multilinear tension envelope",
                "# A\tB\tC\tD\tE\tF\tG\tH\tI\tJ\tK\tL\tM\tN\tO\tP\tQ"
           },
           {    //NLMM105
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the material model",
                "# C -> Mass per unit volume",
                "# D -> Temperature coefficient",
                "# E -> Concrete initial Young modulus",
                "# F -> Concrete strain at peak confined stress",
                "# G -> Confined concrete stress",
                "# H -> Non-dimensional critical strain on the compression envelope",
                "# I -> Strain at the end of 1st linear portion of multilinear tension envelope",
                "# J -> Stress at the end of 1st linear portion of multilinear tension envelope",
                "# K -> Strain at the end of 2nd linear portion of multilinear tension envelope",
                "# L -> Stress at the end of 2nd linear portion of multilinear tension envelope",
                "# M -> Name of Crack Model (TRILINEAR ; QUADRILINEAR)",
                "# N -> Percentage of the tensile strength, for defining the first post-peak point",
                "# O -> First post-peak point of the crack opening",
                "# P -> Percentage of the tensile strength, for defining the second post-peak point",
                "# Q -> Second post-peak point of the crack opening",
                "# R -> Percentage of the tensile strength, for defining the third post-peak point (zero for trilinear)",
                "# S -> Third post-peak point of the crack opening (zero for trilinear)",
                "# T -> Ultimate post-peak point of the crack opening",
                "# U -> Non linear hinge length",
                "# A\tB\tC\tD\tE\tF\tG\tH\tI\tJ\tK\tL\tM\tN\tO\tP\tQ\tR\tS\tT\tU"
           },
           {    //NLMM201
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the material model",
                "# C -> Mass per unit volume",
                "# D -> Temperature coefficient",
                "# E -> Elastic modulus",
                "# F -> Tangent modulus at strain hardening",
                "# G -> Yielding strain",
                "# H -> Yielding stress",
                "# I -> Hardening strain",
                "# J -> Hardening stress",
                "# K -> Strain at the ultimate stress",
                "# L -> Ultimate stress",
                "# A\tB\tC\tD\tE\tF\tG\tH\tI\tJ\tK\tL"
           },
           {    //NLMM202
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the material model",
                "# C -> Mass per unit volume",
                "# D -> Temperature coefficient",
                "# E -> Elastic modulus",
                "# F -> Tangent modulus at strain hardening",
                "# G -> Yielding strain",
                "# H -> Yielding stress",
                "# I -> Hardening strain",
                "# J -> Hardening stress",
                "# K -> Strain at the ultimate stress",
                "# L -> Ultimate stress",
                "# A\tB\tC\tD\tE\tF\tG\tH\tI\tJ\tK\tL"
           },
           {    //NLMM301
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the material model",
                "# C -> Ultimate tensile strength (design)",
                "# D -> Tensile modulus of elasticity",
                "# E -> Rupture strain (design)",
                "# F -> Environmental-reduction factor",
                "# A\tB\tC\tD\tE\tF"
           }
        };
        int i, j, k, count;
        for(i=0; i<jFrameDocrosData.materials.length; i++) {
            if(jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + jFrameDocrosData.materials[i]) != null && jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + jFrameDocrosData.materials[i]).count > 0) {
                try {
                    count = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + jFrameDocrosData.materials[i]).count;
                    writeTagAndCount("<" + jFrameDocrosData.materials[i] + ">", "# Properties of the " + jFrameDocrosData.materials[i] + " material model" + System.lineSeparator() + "# Keyword: _" + jFrameDocrosData.materials[i], count, "# Number of " + jFrameDocrosData.materials[i] + " materials");
                    writeComments(materialComments[i]);
                    NonLinearMaterialModel temp;
                    temp = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + jFrameDocrosData.materials[i]).first;
                    for(j=1; j<=count; j++) {
                            bufferedWriterDataFile.write("  " + j + "\t" + temp.name + "\t");
                        if(jFrameDocrosData.materials[i].substring(4).startsWith("1") || jFrameDocrosData.materials[i].substring(4).startsWith("2"))
                            bufferedWriterDataFile.write("1.0\t1.0\t");
                        if(jFrameDocrosData.materials[i].equals("NLMM105")) {
                            for(k=0; k<8; k++)
                                bufferedWriterDataFile.write(temp.parameters[k] + "\t");
                            if(temp.parameters[12] == 0.0 && temp.parameters[13] == 0.0)
                                bufferedWriterDataFile.write("TRILINEAR\t");
                            else
                                bufferedWriterDataFile.write("QUADRILINEAR\t");
                            for(k=8; k<12; k++)
                                bufferedWriterDataFile.write(temp.parameters[k] + "\t");
                            if(temp.parameters[12] == 0.0 && temp.parameters[13] == 0.0)
                                bufferedWriterDataFile.write("None\tNone\t");
                            else {
                                bufferedWriterDataFile.write(temp.parameters[12] + "\t");
                                bufferedWriterDataFile.write(temp.parameters[13] + "\t");
                            }
                            for(k=14; k<temp.parameters.length; k++)
                                bufferedWriterDataFile.write(temp.parameters[k] + "\t");
                        }
                        else {
                            for(k=0; k<temp.parameters.length; k++)
                                bufferedWriterDataFile.write(temp.parameters[k] + "\t");
                        }
                        bufferedWriterDataFile.write(";");
                        bufferedWriterDataFile.newLine();
                        temp = temp.next;
                    }
                    bufferedWriterDataFile.newLine();
                    writeTagAndCount("</" + jFrameDocrosData.materials[i] + ">", null, -1, null);

                } catch (IOException ex) {
                    Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }


    void writeLaminateGeoPropList() {

        if(jFrameDocrosData.laminateGeoPropList != null && jFrameDocrosData.laminateGeoPropList.count > 0) {
            String comments[] = {
                "# Content of each column:",
                "# A -> Counter",
                "# B -> Name of the geometry properties set",
                "# C -> Thickness",
                "# D -> Width",
                "# A\tB\tC\tD"              
            };
            int i, count;
            count = jFrameDocrosData.laminateGeoPropList.count;
            writeTagAndCount("<LAMINATEGEOPROP>", "# Geometry properties of the laminates", count, "# Number of sets of geometry properties");
            writeComments(comments);
            LaminateGeoProp node = jFrameDocrosData.laminateGeoPropList.first;
            try {
                for(i=1; i<=count; i++) {
                    bufferedWriterDataFile.write("  " + i + "\t" + node.name + "\t" + node.thickness + "\t" + node.width + "\t;");
                    bufferedWriterDataFile.newLine();
                    node = node.next;
                }
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</LAMINATEGEOPROP>", null, -1, null);
            } catch (IOException ex) {
                Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void writeLayerPropertiesList() {

        if(jFrameDocrosData.layerPropertiesList != null && jFrameDocrosData.layerPropertiesList.count > 0) {
            String comments[] = {
                "# Content of each column:",
                "# A -> Counter",              
                "# B -> Layer name",
                "# C -> Type of constitutive law",
                "# D -> Name of constitutive law",
                "# E -> Geometry name",
                "# A\tB\tC\tD\tE"              
            };
            int i, count;
            count = jFrameDocrosData.layerPropertiesList.count;
            writeTagAndCount("<LAYER_PROPERTIES>", "# Properties of the layers", count, "# Number of layer properties");
            writeComments(comments);
            LayerProperties node = jFrameDocrosData.layerPropertiesList.first;
            try {
                 for(i=1; i<=count; i++) {
                    bufferedWriterDataFile.write("  " + i + "\t" + node.name + "\t_" + node.materialModel.type + "\t" + node.materialModel.name + "\t" + node.geoProp.name + "\t;");
                    bufferedWriterDataFile.newLine();
                     node = node.next;
                }
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</LAYER_PROPERTIES>", null, -1, null);
            } catch (IOException ex) {
                Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void writeLayerPatternsList() {

        if( jFrameDocrosData.layerPatternsList != null  && jFrameDocrosData.layerPatternsList.count > 0) {
            String comments[] = {
                "# Content of each column:",
                "# A -> Counter",              
                "# B -> Pattern name",
                "# C -> Number of layers",
                "# D -> Layer (or layer range)",
                "# E -> Number of distinct properties (defined in the <LAYER_PROPERTIES> block) in the layer pattern",
                "# F -> Name of the layer properties in the <LAYER_PROPERTIES> block and its phase or phase range",
                "# A\tB\tC\tD\tE\tF"
            };
            int i, j, count, firstLayer = 1, lastLayer;
            count =  jFrameDocrosData.layerPatternsList.count;
            writeTagAndCount("<LAYER_PATTERNS>", "# Layer patterns", count, "# Number of layer patterns");
            writeComments(comments);
            LayerPatterns node = jFrameDocrosData.layerPatternsList.first;
            try {
                for(i=1; i<=count; i++) {
                    lastLayer = firstLayer + node.numLayers - 1;
                    if(firstLayer == lastLayer)
                        bufferedWriterDataFile.write("  " + i + "\t" + node.name + "\t1\t" + firstLayer + "\t" + node.layerPropertiesPhase.length + "\t");
                    else
                        bufferedWriterDataFile.write("  " + i + "\t" + node.name + "\t" + node.numLayers + "\t[" + firstLayer + "-" + lastLayer + "]\t" + node.layerPropertiesPhase.length + "\t");
                    for(j=0; j<node.layerPropertiesPhase.length; j++)
                        bufferedWriterDataFile.write(node.layerProperties[j].name + "\t" + node.layerPropertiesPhase[j] + "\t");
                    bufferedWriterDataFile.write(";");
                    bufferedWriterDataFile.newLine();
                    firstLayer = lastLayer + 1;
                    node = node.next;
                }
                bufferedWriterDataFile.newLine();
                writeTagAndCount("</LAYER_PATTERNS>", null, -1, null);
            } catch (IOException ex) {
                Logger.getLogger(JMenuItemSaveActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
