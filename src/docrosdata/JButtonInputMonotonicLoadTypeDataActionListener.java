
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputMonotonicLoadTypeDataActionListener implements ActionListener {

    JFrameInputMonotonicLoadTypeData jFrameInputMonotonicLoadTypeData;
    
    public JButtonInputMonotonicLoadTypeDataActionListener(JFrameInputMonotonicLoadTypeData jf) {
        
        jFrameInputMonotonicLoadTypeData = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        int i;
        switch (ae.getActionCommand()) {
            case "Used":
                String monotonicLoadNames[];
                monotonicLoadNames = new String[jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.count];
                MonotonicLoadTypeData node = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.first;
                for(i=0; i<monotonicLoadNames.length; i++) {
                    monotonicLoadNames[i] = node.name;
                    node = node.next;
                }
                JFrameUsedNames jFrameUsedNames = new JFrameUsedNames("Used Monotonic Load Names:", monotonicLoadNames, jFrameInputMonotonicLoadTypeData.getLocation());
                break;
            case "OK":
                if(jFrameInputMonotonicLoadTypeData.jTextField[0].isVisible()) {
                    if(monotonicLoadNameInserted() && layerPatternsNameSelected() && layerNumberSelected() && incrementInserted())
                        addMonotonicLoadTypeData();
                    //jFrameInputMonotonicLoadTypeData.jButton[5].setEnabled(true);
                }
                else{
                    if(layerPatternsNameSelected() && layerNumberSelected() && incrementInserted())
                        updateMonotonicLoadTypeData(); 
                }
                break;
            case "New":
                for(i=0; i<jFrameInputMonotonicLoadTypeData.jTextField.length; i++) {
                    jFrameInputMonotonicLoadTypeData.jTextField[i].setText(null);
                    jFrameInputMonotonicLoadTypeData.jTextField[i].setBackground(Color.white);
                    jFrameInputMonotonicLoadTypeData.jTextField[i].setEnabled(true);
                }
                jFrameInputMonotonicLoadTypeData.jTextField[0].setVisible(true);
                jFrameInputMonotonicLoadTypeData.jButtonUsedName.setVisible(true);
                if(jFrameInputMonotonicLoadTypeData.jComboBox[0].getItemCount() > 1)
                    jFrameInputMonotonicLoadTypeData.jButtonUsedName.setEnabled(true);
                else
                    jFrameInputMonotonicLoadTypeData.jButtonUsedName.setEnabled(false);
                jFrameInputMonotonicLoadTypeData.jComboBox[0].setVisible(false);
                jFrameInputMonotonicLoadTypeData.jComboBox[1].setEnabled(true);
                jFrameInputMonotonicLoadTypeData.jComboBox[2].setEnabled(true);
                jFrameInputMonotonicLoadTypeData.jButton[1].setText("Back");
                jFrameInputMonotonicLoadTypeData.jButton[1].setVisible(true);
                jFrameInputMonotonicLoadTypeData.jButton[2].setText("OK");
                break;
            case "Back":
                jFrameInputMonotonicLoadTypeData.jTextField[0].setVisible(false);
                jFrameInputMonotonicLoadTypeData.jButtonUsedName.setVisible(false);
                jFrameInputMonotonicLoadTypeData.jComboBox[0].setVisible(true);
                jFrameInputMonotonicLoadTypeData.jComboBox[0].setSelectedIndex(0);
                break;
            case "Reset":
                jFrameInputMonotonicLoadTypeData.jComboBox[0].setSelectedIndex(jFrameInputMonotonicLoadTypeData.jComboBox[0].getSelectedIndex());               
                break;
            case "Delete":
                deleteMonotonicLoadTypeData();
                break;
            case "Previous":
                jFrameInputMonotonicLoadTypeData.dispose();
                JFrameInputPreStressedLayers jFrameInputPreStressedLayers = new JFrameInputPreStressedLayers(jFrameInputMonotonicLoadTypeData.jFrameDocrosData, jFrameInputMonotonicLoadTypeData.getLocation());
                //JFrameInputLayerPatterns jFrameInputLayerPatterns = new JFrameInputLayerPatterns(jFrameInputMonotonicLoadTypeData.jFrameDocrosData, jFrameInputMonotonicLoadTypeData.getLocation());
                break;            
            case "Next":
                jFrameInputMonotonicLoadTypeData.dispose();
                JFrameInputStopConditionLayerStrain jFrameInputStopConditionLayerStrain = new JFrameInputStopConditionLayerStrain(jFrameInputMonotonicLoadTypeData.jFrameDocrosData, jFrameInputMonotonicLoadTypeData.getLocation());
                break;
            case "Close":
                jFrameInputMonotonicLoadTypeData.dispose();
                break;
        }
    }
    
    void deleteMonotonicLoadTypeData() {
        int i;
        String monotonicLoadName = String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[0].getSelectedItem());
        jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.delMonotonicLoadTypeData(monotonicLoadName);
        jFrameInputMonotonicLoadTypeData.jComboBox[0].removeItem(monotonicLoadName);
        jFrameInputMonotonicLoadTypeData.jComboBox[0].setSelectedIndex(0);
        if(jFrameInputMonotonicLoadTypeData.jComboBox[0].getItemCount() == 1) {
            jFrameInputMonotonicLoadTypeData.jComboBox[0].setVisible(false);
            for(i=0; i<jFrameInputMonotonicLoadTypeData.jTextField.length; i++) {
                jFrameInputMonotonicLoadTypeData.jTextField[i].setText(null);
                jFrameInputMonotonicLoadTypeData.jTextField[i].setBackground(Color.white);
                jFrameInputMonotonicLoadTypeData.jTextField[i].setEnabled(true);
            }
            jFrameInputMonotonicLoadTypeData.jTextField[0].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButtonUsedName.setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButtonUsedName.setEnabled(false);
            jFrameInputMonotonicLoadTypeData.jComboBox[1].setEnabled(true);
            jFrameInputMonotonicLoadTypeData.jComboBox[2].setEnabled(true);
            jFrameInputMonotonicLoadTypeData.jComboBox[1].setSelectedIndex(0);
            jFrameInputMonotonicLoadTypeData.jComboBox[2].setSelectedIndex(0);
            jFrameInputMonotonicLoadTypeData.jButton[1].setVisible(false);
            jFrameInputMonotonicLoadTypeData.jButton[2].setText("OK");
            //jFrameInputMonotonicLoadTypeData.jButton[5].setEnabled(false);
        }
        if(jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataList != null && jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataList.first != null) {
            PhaseData node = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataList.first;
            while(node != null) {
                if(node.loadType.equals("_MONOTONIC") && node.loadName.equals(monotonicLoadName)) {
                    jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataList.delPhaseData(node);
                    //jFrameInputMonotonicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataListIndex] = true;
                    node = null;
                }
                else
                    node = node.next;
            }
        }
        if((jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList == null || jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.first == null) 
                && (jFrameInputMonotonicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList == null || jFrameInputMonotonicLoadTypeData.jFrameDocrosData.cyclicLoadTypeDataList.first == null)) {
            jFrameInputMonotonicLoadTypeData.jFrameDocrosData.jMenuItemEdit[10].setEnabled(false);
        }
        
        //jFrameInputMonotonicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataListIndex] = true;
        jFrameInputMonotonicLoadTypeData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputMonotonicLoadTypeData.jFrameDocrosData.dataFile != null)
            jFrameInputMonotonicLoadTypeData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);

    }
    
    void updateMonotonicLoadTypeData() {
        
        String monotonicLoadName = String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[0].getSelectedItem());
        String layerPatternsName = String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[1].getSelectedItem());
        int layerNumber = Integer.valueOf(String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[2].getSelectedItem()));
        double increment = Double.valueOf(jFrameInputMonotonicLoadTypeData.jTextField[1].getText());
        //String stopConditionName = jFrameInputMonotonicLoadTypeData.jTextField[4].getText();
        //double strain = Double.valueOf(jFrameInputMonotonicLoadTypeData.jTextField[5].getText());
        MonotonicLoadTypeData node = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.getMonotonicLoadTypeData(monotonicLoadName);
        node.layerPatternsName = layerPatternsName;
        node.layerNumber = layerNumber;
        node.compressionStrainIncrement = increment;
        jFrameInputMonotonicLoadTypeData.jComboBox[0].setSelectedIndex(0);
        //if(jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataList != null && jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataList.getPhaseData("_MONOTONIC", monotonicLoadName) != null)
        //    jFrameInputMonotonicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataListIndex] = true;
        //jFrameInputMonotonicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataListIndex] = true;
        jFrameInputMonotonicLoadTypeData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputMonotonicLoadTypeData.jFrameDocrosData.dataFile != null)
            jFrameInputMonotonicLoadTypeData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void addMonotonicLoadTypeData() {
                
        String monotonicLoadName = jFrameInputMonotonicLoadTypeData.jTextField[0].getText();
        String layerPatternsName = String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[1].getSelectedItem());
        int layerNumber = Integer.valueOf(String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[2].getSelectedItem()));
        double increment = Double.valueOf(jFrameInputMonotonicLoadTypeData.jTextField[1].getText());
        //String stopConditionName = jFrameInputMonotonicLoadTypeData.jTextField[4].getText();        
        //double strain = Double.valueOf(jFrameInputMonotonicLoadTypeData.jTextField[5].getText());
        MonotonicLoadTypeData node = new MonotonicLoadTypeData(monotonicLoadName, layerNumber, layerPatternsName, increment);
        if(jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList == null)
            jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList = new MonotonicLoadTypeDataList();
        jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.addMonotonicLoadTypeData(node);

        jFrameInputMonotonicLoadTypeData.jComboBox[0].addItem(node.name);
        jFrameInputMonotonicLoadTypeData.jComboBox[0].setSelectedIndex(0);
        jFrameInputMonotonicLoadTypeData.jComboBox[0].setVisible(true);
        jFrameInputMonotonicLoadTypeData.jTextField[0].setVisible(false);
        jFrameInputMonotonicLoadTypeData.jButtonUsedName.setVisible(false);
        jFrameInputMonotonicLoadTypeData.jButton[2].setText("New");
        //if(jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataList != null && jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataList.getPhaseData("_MONOTONIC", monotonicLoadName) != null)
        //    jFrameInputMonotonicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputMonotonicLoadTypeData.jFrameDocrosData.phaseDataListIndex] = true;
        if(jFrameInputMonotonicLoadTypeData.jFrameDocrosData.stopConditionLayerStrainList != null && jFrameInputMonotonicLoadTypeData.jFrameDocrosData.stopConditionLayerStrainList.first != null)
            jFrameInputMonotonicLoadTypeData.jFrameDocrosData.jMenuItemEdit[10].setEnabled(true);
        //jFrameInputMonotonicLoadTypeData.jFrameDocrosData.changedLists[jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataListIndex] = true;
        jFrameInputMonotonicLoadTypeData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputMonotonicLoadTypeData.jFrameDocrosData.dataFile != null)
            jFrameInputMonotonicLoadTypeData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    /*
    Boolean existsStopConditionName() {
        
        MonotonicLoadTypeDataList list = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList;
        if(list == null)
            return false;
        MonotonicLoadTypeData nodeSelected, node;
        nodeSelected = null;
        node = list.first;
        if(jFrameInputMonotonicLoadTypeData.jComboBox[0].isVisible()) {
            String monotonicLoadName = String.valueOf(jFrameInputMonotonicLoadTypeData.jComboBox[0].getSelectedItem());
            nodeSelected = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList.getMonotonicLoadTypeData(monotonicLoadName);
        }
        while(node != null) {
            if(node != nodeSelected && node.stopConditionName.equals(jFrameInputMonotonicLoadTypeData.jTextField[4].getText()))
                return true;
            node = node.next;
        }
        return false;
    }
    */
    Boolean existsMonotonicLoadTypeName() {
        
        MonotonicLoadTypeDataList list = jFrameInputMonotonicLoadTypeData.jFrameDocrosData.monotonicLoadTypeDataList;
        if(list == null)
            return false;
        MonotonicLoadTypeData node = list.first;
        while(node != null) {
            if(node.name.equals(jFrameInputMonotonicLoadTypeData.jTextField[0].getText()))
                return true;
            node = node.next;
        }
        return false;
    }
    /*
    Boolean strainInserted() {
        
        if(jFrameInputMonotonicLoadTypeData.jTextField[5].getText().equals("")) {
            jFrameInputMonotonicLoadTypeData.jTextField[5].setBackground(Color.yellow);
            jFrameInputMonotonicLoadTypeData.jLabelMessage[4].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(false);    
            return false;    
        }
        return true;
    }
    
    Boolean stopConditionNameInserted() {
        
        if(jFrameInputMonotonicLoadTypeData.jTextField[4].getText().equals("")) {
            //System.out.println("AQUI");
            jFrameInputMonotonicLoadTypeData.jTextField[4].setBackground(Color.yellow);
            jFrameInputMonotonicLoadTypeData.jLabelMessage[5].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(false);
            return false;
        }
        if(existsStopConditionName()) {
            jFrameInputMonotonicLoadTypeData.jTextField[4].setBackground(Color.yellow);
            jFrameInputMonotonicLoadTypeData.jLabelMessage[1].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    */
    Boolean incrementInserted() {
        
        if(jFrameInputMonotonicLoadTypeData.jTextField[1].getText().equals("")) {
            jFrameInputMonotonicLoadTypeData.jTextField[1].setBackground(Color.yellow);
            jFrameInputMonotonicLoadTypeData.jLabelMessage[4].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(false);    
            return false;    
        }
        return true;
    }
    
    Boolean layerNumberSelected() {
        
        if(jFrameInputMonotonicLoadTypeData.jComboBox[2].getSelectedIndex() == 0) {
            jFrameInputMonotonicLoadTypeData.jLabelMessage[3].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(false);    
            return false;
        }
        return true;
    }
          
    Boolean layerPatternsNameSelected() {
        
        if(jFrameInputMonotonicLoadTypeData.jComboBox[1].getSelectedIndex() == 0) {
            jFrameInputMonotonicLoadTypeData.jLabelMessage[2].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(false);    
            return false;
        }
        return true;
    }
    
    Boolean monotonicLoadNameInserted() {
              
        if(jFrameInputMonotonicLoadTypeData.jTextField[0].getText().equals("")) {
            //System.out.println("AQUI");
            jFrameInputMonotonicLoadTypeData.jTextField[0].setBackground(Color.yellow);
            jFrameInputMonotonicLoadTypeData.jLabelMessage[0].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(false);
            return false;
        }
        if(existsMonotonicLoadTypeName()) {
            jFrameInputMonotonicLoadTypeData.jTextField[0].setBackground(Color.yellow);
            jFrameInputMonotonicLoadTypeData.jLabelMessage[1].setVisible(true);
            jFrameInputMonotonicLoadTypeData.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    
}
