
package docrosdata;

class MainParameters {
    
    String title;
    //int numLayers;
    String sxsGraphics;
    double axialLoad;
    
    public MainParameters(String tit, String sxs, double axi) {
        
        title = tit;
        //numLayers = num;
        sxsGraphics = sxs;
        axialLoad = axi;
    }
    
}
