
package docrosdata;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class JTextFieldIntNumberFocusListener implements FocusListener {

    JLabel jLabelMessage[];
    int messageIndex;
    JButton jButton0;
    JButton jButton1;
    
    public JTextFieldIntNumberFocusListener(JLabel jl[], int index, JButton jb0, JButton jb1) {
        
        jLabelMessage = jl;
        messageIndex = index;
        jButton0 = jb0;
        jButton1 = jb1;
        
    }
    
    @Override
    public void focusGained(FocusEvent fe) {

        ((JTextField) fe.getSource()).setBackground(Color.white);
        jLabelMessage[messageIndex].setVisible(false);
        if(jButton0 != null)
            jButton0.setEnabled(true);
        if(jButton1 != null)
            jButton1.setEnabled(true);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent fe) {
        
        int i;
        if(jLabelMessage != null)
            for(i=0; i<jLabelMessage.length; i++)
                jLabelMessage[i].setVisible(false);
        if(((JTextField) fe.getSource()).getText().equals("")) {//|| ((JTextField) fe.getSource()).getText().equals("0")) {
            ((JTextField) fe.getSource()).setBackground(Color.yellow);
            jLabelMessage[messageIndex].setVisible(true);
            if(jButton0 != null)
                jButton0.setEnabled(false);
            if(jButton1 != null)
                jButton1.setEnabled(false);
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
