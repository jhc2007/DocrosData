
package docrosdata;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputLaminateGeoPropActioListener implements ActionListener {

    JFrameInputLaminateGeoProp jFrameInputLaminateGeoProp;
    
    public JButtonInputLaminateGeoPropActioListener(JFrameInputLaminateGeoProp jf) {
        
        jFrameInputLaminateGeoProp = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        int i;
        switch (ae.getActionCommand()) {
            case "Used":
                String laminateGeoPropNames[];
                laminateGeoPropNames = new String[jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.count];
                LaminateGeoProp node = jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.first;
                for(i=0; i<jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.count; i++) {
                    laminateGeoPropNames[i] = node.name;
                    node = node.next;
                }
                JFrameUsedNames jFrameUsedNames = new JFrameUsedNames("Used Geometry Names:", laminateGeoPropNames, jFrameInputLaminateGeoProp.getLocation());
                break;
            case "Back":
                setJComboBoxVisible(true);
                jFrameInputLaminateGeoProp.jLabelMessage.setVisible(false);
                jFrameInputLaminateGeoProp.jButton[1].setVisible(false);
                jFrameInputLaminateGeoProp.jButton[2].setText("New");
                break;
            case "New":
                setJComboBoxVisible(false);
                jFrameInputLaminateGeoProp.jButton[1].setText("Back");
                jFrameInputLaminateGeoProp.jButton[1].setVisible(true);
                jFrameInputLaminateGeoProp.jButton[2].setText("OK");
                break;            
            case "OK":
                if(jFrameInputLaminateGeoProp.jTextField[0].isVisible()) {
                    if(jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList != null && jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.existsLaminateGeoPropName(jFrameInputLaminateGeoProp.jTextField[0].getText())) {
                        jFrameInputLaminateGeoProp.jLabelMessage.setVisible(true);
                        jFrameInputLaminateGeoProp.jTextField[0].setBackground(Color.yellow);
                        jFrameInputLaminateGeoProp.jButton[2].setEnabled(false);
                    }
                    else {
                        if(!hasError()) {
                            addLaminateGeoProp();
                            setJComboBoxVisible(true);
                        }
                    }
                }
                else {
                    updateLaminateGeoProp();
                    setJComboBoxVisible(true);
                    jFrameInputLaminateGeoProp.jButton[0].setVisible(false);
                    jFrameInputLaminateGeoProp.jButton[1].setVisible(false);
                    jFrameInputLaminateGeoProp.jButton[2].setText("New"); 
                }
                break;
            case "Reset":
                resetParameters();
                jFrameInputLaminateGeoProp.jButton[2].setEnabled(true);
                break;
            case "Delete":
                deleteLaminateGeoProp();
                jFrameInputLaminateGeoProp.jButton[0].setVisible(false);
                jFrameInputLaminateGeoProp.jButton[1].setVisible(false);
                break;
            case "Next":
                jFrameInputLaminateGeoProp.dispose();
                JFrameInputLayerProperties jFrameInputLayerProperties = new JFrameInputLayerProperties(jFrameInputLaminateGeoProp.jFrameDocrosData, jFrameInputLaminateGeoProp.getLocation());
                break;
            case "Previous":
                jFrameInputLaminateGeoProp.dispose();
                JFrameInputMaterialModel jFrameInputMaterialModel = new JFrameInputMaterialModel(jFrameInputLaminateGeoProp.jFrameDocrosData, jFrameInputLaminateGeoProp.getLocation());
                break;
            case "Close":
                jFrameInputLaminateGeoProp.dispose();
                break;
        }
                
    }
    
    void deleteLaminateGeoProp() {
        
        String name = String.valueOf(jFrameInputLaminateGeoProp.jComboBoxName.getSelectedItem());
        jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.delLaminateGeoProp(name);
        jFrameInputLaminateGeoProp.jComboBoxName.removeItem(name);
        jFrameInputLaminateGeoProp.jComboBoxName.setSelectedIndex(0);
    
        if(jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList == null || jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.first == null) {      
            setJComboBoxVisible(false);
            jFrameInputLaminateGeoProp.jButtonNext.setEnabled(false);
            jFrameInputLaminateGeoProp.jFrameDocrosData.jMenuItemEdit[3].setEnabled(false);
        }
        else
            jFrameInputLaminateGeoProp.jButton[2].setText("New");
        //jFrameInputLaminateGeoProp.jFrameDocrosData.changedLists[jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropListIndex] = true;
        jFrameInputLaminateGeoProp.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputLaminateGeoProp.jFrameDocrosData.dataFile != null)
            jFrameInputLaminateGeoProp.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);

    }
    


    void updateLaminateGeoProp() {
        
       LaminateGeoProp node = jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(String.valueOf(jFrameInputLaminateGeoProp.jComboBoxName.getSelectedItem()));
       node.setThicknessWidth(jFrameInputLaminateGeoProp.jTextField[1].getText(), jFrameInputLaminateGeoProp.jTextField[2].getText());
       //jFrameInputLaminateGeoProp.jFrameDocrosData.changedLists[jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropListIndex] = true;
       jFrameInputLaminateGeoProp.jFrameDocrosData.jMenuItemSave.setEnabled(true);
       if(jFrameInputLaminateGeoProp.jFrameDocrosData.dataFile != null)
            jFrameInputLaminateGeoProp.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);

    }
    
    void resetParameters() {
        
        int i;
        
        String name = String.valueOf(jFrameInputLaminateGeoProp.jComboBoxName.getSelectedItem());
        LaminateGeoProp node = jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.getLaminateGeoProp(name);
        
        for(i=1; i<3; i++) {
            jFrameInputLaminateGeoProp.jTextField[i].setText(node.getPropertyValue(i));
            jFrameInputLaminateGeoProp.jTextField[i].setBackground(Color.white);
        }
        
        //jFrameInputLaminateGeoProp.jButton[0].setVisible(false);
    }
    
    void addLaminateGeoProp() {
        
        LaminateGeoProp node;
        
        if(jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList == null)
            jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList = new LaminateGeoPropList();
        
        node = new LaminateGeoProp(jFrameInputLaminateGeoProp.jTextField);
        jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.addLaminateGeoProp(node);
        
        jFrameInputLaminateGeoProp.jComboBoxName.addItem(jFrameInputLaminateGeoProp.jTextField[0].getText());
        
        jFrameInputLaminateGeoProp.jButtonNext.setEnabled(true);
        if(jFrameInputLaminateGeoProp.jFrameDocrosData.nonLinearMaterialModelTypeList.existsMaterialModel())
            jFrameInputLaminateGeoProp.jFrameDocrosData.jMenuItemEdit[3].setEnabled(true);       
        jFrameInputLaminateGeoProp.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputLaminateGeoProp.jFrameDocrosData.dataFile != null)
            jFrameInputLaminateGeoProp.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);

    }
    
    void setJComboBoxVisible(Boolean visible) {
    
        int i;
        jFrameInputLaminateGeoProp.jTextField[0].setText("");
        jFrameInputLaminateGeoProp.jTextField[0].setBackground(Color.white);
        jFrameInputLaminateGeoProp.jTextField[0].setVisible(!visible);
        if(jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList == null || jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.first == null)
            jFrameInputLaminateGeoProp.jButtonUsedName.setEnabled(visible);
        else
            jFrameInputLaminateGeoProp.jButtonUsedName.setEnabled(!visible);
        jFrameInputLaminateGeoProp.jButtonUsedName.setVisible(!visible);
        if(!visible && (jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList == null || jFrameInputLaminateGeoProp.jFrameDocrosData.laminateGeoPropList.first == null))
            jFrameInputLaminateGeoProp.jButtonUsedName.setEnabled(visible);
        
        for(i=1; i<3; i++) {
           jFrameInputLaminateGeoProp.jLabel[i].setVisible(!visible);
           jFrameInputLaminateGeoProp.jTextField[i].setText("");
           jFrameInputLaminateGeoProp.jTextField[i].setBackground(Color.white);
           jFrameInputLaminateGeoProp.jTextField[i].setVisible(!visible); 
        }      
        jFrameInputLaminateGeoProp.jComboBoxName.setVisible(visible);
        jFrameInputLaminateGeoProp.jComboBoxName.setSelectedItem("Select");
        if(visible)
            jFrameInputLaminateGeoProp.jButton[2].setText("New");
        else
            jFrameInputLaminateGeoProp.jButton[2].setText("OK");
    }
    
    Boolean hasError() {
        
        int i;
    
        String text;
        if(jFrameInputLaminateGeoProp.jTextField[0].getText().equals("") || jFrameInputLaminateGeoProp.jTextField[0].getText().equals("Insert name")) {
           jFrameInputLaminateGeoProp.jTextField[0].setText("Insert name");
           jFrameInputLaminateGeoProp.jTextField[0].setBackground(Color.yellow);
           jFrameInputLaminateGeoProp.jButton[2].setEnabled(false);
           return true;
        }
        for(i=1; i<3; i++) {
            if(jFrameInputLaminateGeoProp.jTextField[i].getText().equals("")) {
                jFrameInputLaminateGeoProp.jTextField[i].setText("Insert number");
                jFrameInputLaminateGeoProp.jTextField[i].setBackground(Color.yellow);
                jFrameInputLaminateGeoProp.jButton[2].setEnabled(false);
                return true;
            }
            else {
                try {
                    Double.valueOf(jFrameInputLaminateGeoProp.jTextField[i].getText());
                }
                catch(Exception e) {
                    text = jFrameInputLaminateGeoProp.jTextField[i].getText();
                    jFrameInputLaminateGeoProp.jTextField[i].setText(text + " NOT A NUMBER");
                    jFrameInputLaminateGeoProp.jTextField[i].setBackground(Color.yellow);
                    jFrameInputLaminateGeoProp.jButton[2].setEnabled(false);
                    return true;
                 }
            }
        }
        return false;
    }
    
}