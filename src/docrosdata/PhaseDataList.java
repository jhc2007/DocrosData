
package docrosdata;

public class PhaseDataList {
    
    PhaseData first;
    PhaseData last;
    int count;
    
    public PhaseDataList() {
        
        first = null;
        last = null;
        count = 0;
    }
    
    void addPhaseData(PhaseData node) {
        
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
    
    PhaseData getPhaseData(int phase) {
        
        int i = 1;
        PhaseData node = first;
        while(node != null && i != phase) {
            i++;
            node = node.next;
        }
        return node;
    }
    
    PhaseData getPhaseData(String type, String name) {
        
        PhaseData node = first;
        while(node != null) {
            if(node.loadType.equals(type) && node.loadName.equals(name))
                return node;
            node = node.next;
        }
        return node;
    }
    
    void delPhaseData(PhaseData node) {
        
        PhaseData node1, node2;
        node1 = first;
        node2 = node1.next;
        if(node1 == node) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && node2 != node) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
    }
    
    void delPhaseData(String type, String name) {
        
        PhaseData node1, node2;
        node1 = first;
        node2 = node1.next;
        if(node1.loadType.equals(type) && node1.loadName.equals(name)) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && (!node2.loadType.equals(type) || !node2.loadName.equals(name))) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
    }
    
}
