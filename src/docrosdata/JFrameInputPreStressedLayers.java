
package docrosdata;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class JFrameInputPreStressedLayers extends JFrame {
    
    JFrameDocrosData jFrameDocrosData;
    Point location;
    
    JLabel jLabel[];
    JLabel jLabelMessage[];
    JTextField jTextField;
    JComboBox jComboBox[];
    JButton jButtonHelp;
    JButton jButton[];
    
    String jLabelText[] = {"Pre-Stressed Range", "Pattern Name", "First Layer", "Last Layer", "Strain (%)"};
    String jButtonText[] = {"Delete", "Back", "OK", "Previous", "Close", "Next"};
    
    String jLabelMessageText[] = {
        "Select pattern name",  //0
        "Select first layer",   //1
        "Select last layer",    //2
        "Insert strain"         //3
    };
    
    public JFrameInputPreStressedLayers(JFrameDocrosData jf, Point loc) {

        super("<PRESTRESSED_LAYERS>");
        jFrameDocrosData = jf;
        location = loc;
        init();
    
    }
    
    private void init() {
        
        if(location == null)
            this.setLocation(15, 45);
        else
            this.setLocation(location);                
        this.setResizable(false);
        this.getContentPane().setLayout(null);
                
        int i, j, jLabelWidth, xPosition = 10, yPosition = 10;
        
        jLabelWidth = jFrameDocrosData.charWidth * (JPopupMenuItemActionListener.maxNumberCharacterStringArray(jLabelText));        

        JLabel jLabelTitle = new JLabel("Pre-Stressed Layers");
        jLabelTitle.setSize(jFrameDocrosData.charWidth * (jLabelTitle.getText().length() + 4), jFrameDocrosData.charHeight);
        jLabelTitle.setLocation(xPosition + jLabelWidth, yPosition);
        this.add(jLabelTitle);
        
        if(getClass().getResourceAsStream("/help/helpPreStressedLayers.pdf") != null) {
            jButtonHelp = new JButton("Help");
            jButtonHelp.setSize(63, 18);
            jButtonHelp.setLocation(jLabelTitle.getX() + jLabelTitle.getWidth() - 63, yPosition + 4);
            jButtonHelp.addActionListener(new JButtonHelpActionListener("PreStressedLayers"));//, "Monotonic Load Type Data"));
            this.add(jButtonHelp);
        }
        yPosition += 33;//jFrameDocrosData.charHeight;
        
        jLabel = new JLabel[jLabelText.length];
        jComboBox = new JComboBox[jLabelText.length - 1];       
        for(i=0; i<jLabelText.length; i++) {
            jLabel[i] = new JLabel(jLabelText[i]);
            jLabel[i].setSize(jLabelWidth, jFrameDocrosData.charHeight);
            jLabel[i].setLocation(xPosition, yPosition);
            this.add(jLabel[i]);
            if(i < 4) {
                jComboBox[i] = new JComboBox();
                if(i == 1)
                    jComboBox[i].setSize(jLabelTitle.getWidth() - 1, jFrameDocrosData.charHeight);
                else
                    jComboBox[i].setSize(8 * jFrameDocrosData.charWidth, jFrameDocrosData.charHeight);
                jComboBox[i].setLocation(xPosition + jLabelWidth, yPosition);
                jComboBox[i].addItem("Select");
                if(i == 0) {
                    if(jFrameDocrosData.preStressedLayersList != null && jFrameDocrosData.preStressedLayersList.first != null) {
                        PreStressedLayers node = jFrameDocrosData.preStressedLayersList.first;
                        j = 1;
                        while(node != null) {
                            jComboBox[i].addItem(j);
                            node = node.next;
                            j++;
                        }
                    }
                }
                else {
                    if(i == 1 && jFrameDocrosData.layerPatternsList != null && jFrameDocrosData.layerPatternsList.first != null) {
                        LayerPatterns node = jFrameDocrosData.layerPatternsList.first;
                        while(node != null) {
                            jComboBox[i].addItem(node.name);
                            node = node.next;
                        }
                    }
                }
                jComboBox[i].setSelectedIndex(0);
                this.add(jComboBox[i]);
            }
            else {
                jTextField = new JTextField();
                jTextField.setSize(jLabelTitle.getWidth() - 1, jFrameDocrosData.charHeight);
                jTextField.setLocation(xPosition + jLabelWidth, yPosition);
                jTextField.addKeyListener(new JTextFieldKeyListener());
                this.add(jTextField);
            }
            if(i == 0)
                yPosition += 66;// 2 * jFrameDocrosData.charHeight;
            else
                yPosition += 33;
        }
        jComboBox[1].setName("PatternName");
        jComboBox[2].setName("FirstLayer");  
        
        jLabelMessage = new JLabel[jLabelMessageText.length];
        for(i=0; i<jLabelMessageText.length; i++) {
            jLabelMessage[i] = new JLabel(jLabelMessageText[i]);
            jLabelMessage[i].setSize(jLabelTitle.getWidth(), jFrameDocrosData.charHeight);
            jLabelMessage[i].setLocation(xPosition + jLabelWidth, yPosition);
            jLabelMessage[i].setForeground(Color.red);
            jLabelMessage[i].setVisible(false);
            this.add(jLabelMessage[i]);
        }
        
        yPosition += 33;//jFrameDocrosData.charHeight;
        
        jButton = new JButton[jButtonText.length];       
        for(i=0; i<3; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, jFrameDocrosData.charHeight);
            jButton[i].setLocation(xPosition + i * jButton[i].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputPreStressedLayersActionListener(this));
            this.add(jButton[i]);
        }
        jButton[0].setVisible(false);
        
        yPosition += 43;//jFrameDocrosData.charHeight + 10;
               
        for(i=3; i<jButtonText.length; i++) {
            jButton[i] = new JButton(jButtonText[i]);
            jButton[i].setSize((jLabelWidth + jLabelTitle.getWidth()) / 3, 18);
            jButton[i].setLocation(xPosition + (i - 3) * jButton[3].getWidth(), yPosition);
            jButton[i].addActionListener(new JButtonInputPreStressedLayersActionListener(this));
            this.add(jButton[i]);
        }
               
        for(i=0; i<3; i++) {
            jComboBox[i].addActionListener(new JComboBoxInputPreStressedLayers(this));
            jComboBox[i + 1].addFocusListener(new JComboBoxFocusListener(jLabelMessage, i, jButton[2]));
        }       
        
        if(jComboBox[0].getItemCount() == 1) {
            jButton[1].setVisible(false);
            //jButton[4].setEnabled(false);
            jComboBox[0].setEnabled(false);
        }
        else
            jComboBox[0].setSelectedIndex(0);
 
        jTextField.addFocusListener(new JTextFieldDoubleFocusListener(jLabelMessage, 3, null, jButton[2]));
        
        yPosition += 63;//jFrameDocrosData.charHeight + 30;
        
        this.setSize(jLabelWidth + jLabelTitle.getWidth() + 26, yPosition);
        this.setVisible(true);
    }    
    
}
