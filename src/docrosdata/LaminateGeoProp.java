/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package docrosdata;

import javax.swing.JTextField;

/**
 *
 * @author Moi
 */
public class LaminateGeoProp {
    
    String name;
    double thickness;
    double width;
    int countLayerPatterns;
    LaminateGeoProp next;
    
    public LaminateGeoProp(String n, double t, double w) {
        name = n;
        thickness = t;
        width = w;
        countLayerPatterns = 0;
        next = null;
    }
    
    public LaminateGeoProp(JTextField jtf[]) {

        name = jtf[0].getText();
        thickness = Double.valueOf(jtf[1].getText());
        width = Double.valueOf(jtf[2].getText());
        countLayerPatterns = 0;
        next = null;
        
    }

    String getPropertyValue(int index) {
        
        switch (index) {
            case 0:
                return name;
            case 1:
                return String.valueOf(thickness);
            case 2:
                return String.valueOf(width);
        }
        return null;
    }
    
    String getPropertyValue(String property) {
          
        switch (property.toLowerCase()) {
            case "name":
                return name;
            case "thickness":
                return String.valueOf(thickness);
            case "width":
                return String.valueOf(width);
        } 
        return null;
    }
    
    void setThicknessWidth(String t, String w) {
        
        thickness = Double.valueOf(t);
        width = Double.valueOf(w);
    }
    
    void setPropertyValue(String property, String value) {
        
        switch (property.toLowerCase()) {
            case "name":
                name = value;
                break;
            case "thickness":
                thickness = Double.valueOf(value);
                break;
            case "width":
                width = Double.valueOf(value);
                break;
        } 
        
    }
    
    void setPropertiesValues(JTextField jTextField[]) {
        
        name = jTextField[0].getText();
        thickness = Double.valueOf(jTextField[1].getText());
        width = Double.valueOf(jTextField[2].getText());
    }
    
    void setPropertiesValues(String values[]) {
        
        name = values[0];
        thickness = Double.valueOf(values[1]);
        width = Double.valueOf(values[2]);
        
    }
        
}

