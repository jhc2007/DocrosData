
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JButtonInputPhaseDataActionListener implements ActionListener {

    JFrameInputPhaseData jFrameInputPhaseData;
    
    public JButtonInputPhaseDataActionListener(JFrameInputPhaseData jf) {
        
        jFrameInputPhaseData = jf;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        int i;
        switch (ae.getActionCommand()) {
            case "Reset":
                jFrameInputPhaseData.jComboBox[0].setSelectedIndex(jFrameInputPhaseData.jComboBox[0].getSelectedIndex());
                break;
            case "OK":
                if(!jFrameInputPhaseData.jComboBox[0].isVisible() || !jFrameInputPhaseData.jComboBox[0].isEnabled()) {
                    if(loadTypeSelected() && loadNameSelected() && stopNameSelected()) {                        
                        addPhaseData();   
                        jFrameInputPhaseData.jComboBox[0].setEnabled(true);
                        jFrameInputPhaseData.jComboBox[0].setVisible(true);
                        jFrameInputPhaseData.jLabelPhaseNumber.setVisible(false);
                        jFrameInputPhaseData.jComboBox[0].setSelectedIndex(0);
                    }
                }
                else {
                    if(loadTypeSelected() && loadNameSelected() && stopNameSelected()) {
                        updatePhaseData();   
                        jFrameInputPhaseData.jComboBox[0].setSelectedIndex(0);
                    }
                }
                break;
            case "New":
                jFrameInputPhaseData.jComboBox[0].setSelectedIndex(jFrameInputPhaseData.jComboBox[0].getItemCount() - 1);
                //jFrameInputPhaseData.jComboBox[0].setEnabled(false);
                jFrameInputPhaseData.jLabelPhaseNumber.setText(String.valueOf(jFrameInputPhaseData.jComboBox[0].getItemCount()));
                jFrameInputPhaseData.jLabelPhaseNumber.setVisible(true);
                jFrameInputPhaseData.jComboBox[0].setVisible(false);
                jFrameInputPhaseData.jRadioButton[0].setSelected(false);
                jFrameInputPhaseData.jRadioButton[1].setSelected(false);
                jFrameInputPhaseData.jComboBox[1].setSelectedIndex(0);
                jFrameInputPhaseData.jLabel[3].setVisible(false);
                jFrameInputPhaseData.jComboBox[2].setVisible(false);
                jFrameInputPhaseData.jButton[0].setVisible(false);
                jFrameInputPhaseData.jButton[1].setText("Back");
                jFrameInputPhaseData.jButton[1].setVisible(true);
                jFrameInputPhaseData.jButton[2].setText("OK");
                break;
            case "Back":
                jFrameInputPhaseData.jComboBox[0].setEnabled(true);
                jFrameInputPhaseData.jComboBox[0].setVisible(true);
                jFrameInputPhaseData.jLabelPhaseNumber.setVisible(false);
                jFrameInputPhaseData.jComboBox[0].setSelectedIndex(0);
                break;
            case "Delete":
                deletePhaseData();
                if(jFrameInputPhaseData.jComboBox[0].getItemCount() == 1) {
                    jFrameInputPhaseData.jButton[5].setEnabled(false);
                    jFrameInputPhaseData.jComboBox[0].setEnabled(false);
                    jFrameInputPhaseData.jComboBox[0].setVisible(false);
                    jFrameInputPhaseData.jLabelPhaseNumber.setText(String.valueOf(jFrameInputPhaseData.jComboBox[0].getItemCount()));
                    jFrameInputPhaseData.jLabelPhaseNumber.setVisible(true);
                    jFrameInputPhaseData.jRadioButton[0].setEnabled(true);
                    jFrameInputPhaseData.jRadioButton[1].setEnabled(true);
                    jFrameInputPhaseData.jComboBox[1].setEnabled(true);
                    jFrameInputPhaseData.jButton[0].setVisible(false);
                    jFrameInputPhaseData.jButton[1].setVisible(false);
                    jFrameInputPhaseData.jButton[2].setText("OK");
                }
                else
                    jFrameInputPhaseData.jComboBox[0].setSelectedIndex(0);
                break;
            case "Previous":
                jFrameInputPhaseData.dispose();
                JFrameInputCyclicLoadTypeData jFrameInputCyclicLoadTypeData = new JFrameInputCyclicLoadTypeData(jFrameInputPhaseData.jFrameDocrosData, jFrameInputPhaseData.getLocation());
                break;
            case "Finish":
                jFrameInputPhaseData.dispose();
                break;
            case "Close":
                jFrameInputPhaseData.dispose();
                break;
        }
    }
    
    void deletePhaseData() {
        
        int i, phase;
        phase = Integer.valueOf(String.valueOf(jFrameInputPhaseData.jComboBox[0].getSelectedItem()));
        PhaseData node = jFrameInputPhaseData.jFrameDocrosData.phaseDataList.getPhaseData(phase);
        jFrameInputPhaseData.jFrameDocrosData.phaseDataList.delPhaseData(node);
        jFrameInputPhaseData.jComboBox[0].removeAllItems();
        jFrameInputPhaseData.jComboBox[0].addItem("Select");
        for(i=1; i<=jFrameInputPhaseData.jFrameDocrosData.phaseDataList.count; i++)
            jFrameInputPhaseData.jComboBox[0].addItem(i);
        jFrameInputPhaseData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputPhaseData.jFrameDocrosData.dataFile != null)
            jFrameInputPhaseData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
        
    }
    
    void updatePhaseData() {
        
        int phase = Integer.valueOf(String.valueOf(jFrameInputPhaseData.jComboBox[0].getSelectedItem()));
        String loadName = String.valueOf(jFrameInputPhaseData.jComboBox[1].getSelectedItem());
        PhaseData node = jFrameInputPhaseData.jFrameDocrosData.phaseDataList.getPhaseData(phase);
        if(jFrameInputPhaseData.jRadioButton[0].isSelected())
            node.loadType = "_MONOTONIC";
        else
            node.loadType = "_CYCLIC";
        node.loadName = loadName;
        //setAllEnabled(false);
        //jFrameInputPhaseData.jFrameDocrosData.changedLists[jFrameInputPhaseData.jFrameDocrosData.phaseDataListIndex] = true;
        jFrameInputPhaseData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputPhaseData.jFrameDocrosData.dataFile != null)
            jFrameInputPhaseData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    void addPhaseData() {
        
        String loadType, loadName, stopName;
        if(jFrameInputPhaseData.jRadioButton[0].isSelected()) {
            loadType = "_MONOTONIC";
            stopName = String.valueOf(jFrameInputPhaseData.jComboBox[2].getSelectedItem());
        }
        else {
            loadType = "_CYCLIC";
            stopName = "NONE";
        }
        loadName = String.valueOf(jFrameInputPhaseData.jComboBox[1].getSelectedItem());
        PhaseData node = new PhaseData(loadType, loadName, stopName);
        if(jFrameInputPhaseData.jFrameDocrosData.phaseDataList == null)
            jFrameInputPhaseData.jFrameDocrosData.phaseDataList = new PhaseDataList();
        jFrameInputPhaseData.jFrameDocrosData.phaseDataList.addPhaseData(node);
        jFrameInputPhaseData.jComboBox[0].addItem(jFrameInputPhaseData.jFrameDocrosData.phaseDataList.count);
        jFrameInputPhaseData.jButton[5].setEnabled(true);
        jFrameInputPhaseData.jFrameDocrosData.jMenuItemSave.setEnabled(true);
        if(jFrameInputPhaseData.jFrameDocrosData.dataFile != null)
            jFrameInputPhaseData.jFrameDocrosData.jMenuItemSaveAs.setEnabled(true);
    }
    
    Boolean loadTypeSelected() {
        
        if(!jFrameInputPhaseData.jRadioButton[0].isSelected() && !jFrameInputPhaseData.jRadioButton[1].isSelected()) {
            jFrameInputPhaseData.jLabelMessage[0].setVisible(true);
            jFrameInputPhaseData.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }   
    
    Boolean loadNameSelected() {
        
        if(jFrameInputPhaseData.jComboBox[1].getSelectedIndex() == 0) {
            jFrameInputPhaseData.jLabelMessage[1].setVisible(true);
            jFrameInputPhaseData.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    
    Boolean stopNameSelected() {
        
        if(jFrameInputPhaseData.jRadioButton[1].isSelected())
            return true;
        if(jFrameInputPhaseData.jComboBox[2].getSelectedIndex() == 0) {
            jFrameInputPhaseData.jLabelMessage[2].setVisible(true);
            jFrameInputPhaseData.jButton[2].setEnabled(false);
            return false;
        }
        return true;
    }
    /*
    void setAllEnabled(Boolean enabled) {
        
        jFrameInputPhaseData.jRadioButton[0].setEnabled(enabled);
        //jFrameInputPhaseData.jRadioButton[0].setSelected(enabled);
        jFrameInputPhaseData.jRadioButton[1].setEnabled(enabled);
        //jFrameInputPhaseData.jRadioButton[1].setSelected(enabled);
        if(jFrameInputPhaseData.jComboBox[0].getItemCount() > 0) {
            jFrameInputPhaseData.jComboBox[0].setSelectedIndex(jFrameInputPhaseData.jComboBox[0].getItemCount() - 1);
            jFrameInputPhaseData.jButton[1].setText("Select");
        }
        else
            jFrameInputPhaseData.jButton[0].setVisible(enabled);
        jFrameInputPhaseData.jComboBox[0].setEnabled(enabled);
        jFrameInputPhaseData.jComboBox[1].setSelectedIndex(0);
        jFrameInputPhaseData.jComboBox[1].setEnabled(enabled);
        jFrameInputPhaseData.jButton[2].setText("New");
    }
    */
}
