
package docrosdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;

class JComboBoxLawTypeActionListener implements ActionListener {

    JFrameDocrosData jFrameDocrosData;
    JPanelLayerPatterns jPanelLayerPatterns;
    JComboBox jComboBoxLawName;
    //JLabel jLabelParameters[];
    //JTextField jTextFieldParameters[];
    //JButton jButtonShow;
    JButton jButton[];
    
    public JComboBoxLawTypeActionListener(JFrameDocrosData jfdd, JPanelLayerPatterns jplp, JComboBox jcbln, JButton jb[]) {
        
        jFrameDocrosData = jfdd;
        jPanelLayerPatterns = jplp;
        jComboBoxLawName = jcbln;
        //jLabelParameters = jlp;
        //jTextFieldParameters = jtfp;
        //jButtonShow = jbs;
        jButton = jb;
        
    }

    @Override
    public void actionPerformed(ActionEvent ae) {    
        
        int i;
        JComboBox jComboBoxLawType = (JComboBox) ae.getSource();
        String selectedItem = String.valueOf(jComboBoxLawType.getSelectedItem());
        //System.out.println("SelectedItem: " + selectedItem);
        
        NonLinearMaterialModel model;
        model = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelList('_' + selectedItem).first;
  
        String modelName, modelType;
        modelName = jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].materialModel.name; 
        modelType = jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].materialModel.type; 
        
        jComboBoxLawName.removeAllItems();
        
        //int parametersLength = jFrameDocrosData.nonLinearMaterialModelTypeList.getNonLinearMaterialModelType('_' + String.valueOf(jComboBoxLawType.getSelectedItem())).numberParameters;
        //jLabelParameters = new JLabel[parametersLength];
        //jTextFieldParameters = new JTextField[parametersLength];

        if(model == null) {
            jComboBoxLawName.setEnabled(false);
            for(i=0; i<3; i++)
                jButton[i].setEnabled(false);
        }
        else {
            while(model != null) {
                jComboBoxLawName.addItem(model.name);
                model = model.next;
            }
            if(selectedItem.equals(modelType))
                jComboBoxLawName.setSelectedItem(modelName);
            jComboBoxLawName.setEnabled(true);
            for(i=0; i<3; i++)
                jButton[i].setEnabled(true);
            //jButtonShow.setText("Show Parameters");
        }
        

        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
