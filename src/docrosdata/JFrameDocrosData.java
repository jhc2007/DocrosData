
package docrosdata;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Field;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class JFrameDocrosData extends javax.swing.JFrame {

    String materials[];
    String materialColor[];
    int materialNumberParameters[];
    String materialParametersSymbols[][];
    File dataFile, tempFile, pathFile;
    Scanner scan;
    MainParameters mainParameters;// = null;
    String unitsInputOutputData[];// = null;
    double tolerances;// = -1;
    int layerStressStrainGraphics[];// = null;
    LaminateGeoPropList laminateGeoPropList;// = null;
    NonLinearMaterialModelTypeList nonLinearMaterialModelTypeList;// = null;
    LayerPropertiesList layerPropertiesList;// = null;
    LayerPatternsList layerPatternsList;// = null;
    PreStressedLayersList preStressedLayersList;
    MonotonicLoadTypeDataList monotonicLoadTypeDataList;// = null;
    StopConditionLayerStrainList stopConditionLayerStrainList;
    CyclicLoadTypeDataList cyclicLoadTypeDataList;// = null;
    PhaseDataList phaseDataList;// = null;
    Boolean drawIsPossible;
    Boolean figureIsDrawn;
    String errorMessage;
    String path = null;
    JLabel jLabelMainTitle;
    JPanel jPanelMain;
    JPanel jPanelDocrosImage;
    JPanelLayerPatterns[] jPanelLayerPatterns;
    
    JMenuItem jMenuItemSave, jMenuItemSaveAs, jMenuItemExit;
    JMenuItem jMenuItemEdit[];
    JMenuItem jMenuItemView[];
    
    int charWidth = 9, charHeight = 3 * charWidth;
    int phase;

    public JFrameDocrosData(String mat[], String color[], int parameters[], String symbols[][]) {
        
        materials = mat;
        materialColor = color;
        materialNumberParameters = parameters;
        materialParametersSymbols = symbols;
        figureIsDrawn = false;
        initComponents();
        init();
        this.setPreferredSize(this.getMaximumSize());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.addComponentListener(new JFrameDocrosComponentListener(this));
    }
    
    private void init() {

        BufferedImage bufferedImage;
        JLabel jLabelImage, jLabelDocrosText[];
        BufferedInputStream input;
        FileOutputStream output;
        File imageFile;
        byte[] buffer = new byte[256];
        int bytesRead; 
        Dimension screenDim;
        Image imageScaled;
        ImageIcon imageIcon;
        int i;
        String docrosText[] = {
            "Version 2.0 (Beta)",
            "Data: December 2013",
            "Languages: C and Java",
            "Professional use not allowed",
            "Main developer: Joaquim Barros",
            "(Head of Structural Composites research group, barros@civil.uminho.pt, www.sc.civil.uminho.pt)",
            "Collaborators: Rajendra Varma, Vitor Cunha, Camilo Basto, Jorge Costa, Christoph Sousa"
        };
        
        pathFile = new File(System.getProperty("java.io.tmpdir") + "docrosDataPath.txt");
        if(!pathFile.exists()) {
            try {
                pathFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            scan = new Scanner(pathFile);
            if(scan.hasNext())
                path = scan.nextLine();
            scan.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        jPanelDocrosImage = new JPanel();
        
        //jPanelDocrosImage.setBackground(Color.white);
        //jPanelDocrosImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        this.add(jPanelDocrosImage);
        
        //bufferedImage = new BufferedImage();
        jLabelImage = new JLabel();
        //for(i=0; i<jFrameDocrosData.materials.length; i++) {
        if(getClass().getResourceAsStream("/images/imageDocros.jpg") != null) {
            try {
                input = new BufferedInputStream(getClass().getResourceAsStream("/images/imageDocros.jpg"));
                //imageFile = new File("C:" + File.separator + "Windows" + File.separator + "Temp" + File.separator + "image" + jFrameDocrosData.materials[i] + ".jpg");
                imageFile = new File(System.getProperty("java.io.tmpdir") + "imageDocros.jpg");
                if(!imageFile.exists())
                    imageFile.createNewFile();
                imageFile.deleteOnExit();
                output = new FileOutputStream(imageFile);
                while((bytesRead = input.read(buffer)) != -1)
                    output.write(buffer, 0, bytesRead);
                output.close();
                bufferedImage = ImageIO.read(imageFile);
                screenDim = Toolkit.getDefaultToolkit().getScreenSize();
                jPanelDocrosImage.setSize(screenDim.width - 300, screenDim.height - 200);
                //jPanelDocrosImage.setSize(bufferedImage.getWidth(), bufferedImage.getHeight());
                //jPanelDocrosImage.setLocation((this.getContentPane().getSize().width * 2 / 3 - jPanelDocrosImage.getWidth()) / 2, (this.getContentPane().getSize().width - jPanelDocrosImage.getHeight()) / 2);                              
                //screenDim = Toolkit.getDefaultToolkit().getScreenSize();
                jPanelDocrosImage.setLocation((screenDim.width - jPanelDocrosImage.getWidth()) / 2, 50);//(screenDim.height - jPanelDocrosImage.getHeight()) / 2);
                //jPanelDocrosImage.setLocation(50, 50);
                jPanelDocrosImage.setLayout(null);
                jPanelDocrosImage.setBackground(Color.white);
                jPanelDocrosImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
                jLabelImage.setSize(jPanelDocrosImage.getWidth() - 4, (jPanelDocrosImage.getWidth() - 4) * bufferedImage.getHeight() / bufferedImage.getWidth());                
                imageScaled = bufferedImage.getScaledInstance(jLabelImage.getWidth(), jLabelImage.getHeight(), Image.SCALE_SMOOTH);
                imageIcon = new ImageIcon(imageScaled);
                jLabelImage.setIcon(imageIcon);
                jLabelImage.setLocation(2, 2);
                jPanelDocrosImage.add(jLabelImage);
                jLabelDocrosText = new JLabel[7];
                for(i=0; i<7; i++) {
                    jLabelDocrosText[i] = new JLabel(docrosText[i], SwingConstants.CENTER);
                    jLabelDocrosText[i].setSize(jLabelImage.getWidth(), 28);
                    jLabelDocrosText[i].setLocation(2, jLabelImage.getHeight() - 8 + i * 28);
                    jPanelDocrosImage.add(jLabelDocrosText[i]);
                }
            } catch (IOException ex) {
                Logger.getLogger(JFrameInputMaterialModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        /*
        BufferedImage img = null;
        
        JLabel jLabelImage;
        jLabelImage = new JLabel();
        jLabelImage.setSize(300, 300);
        jLabelImage.setLocation(10, 10);
        //System.out.println(getClass().getResource("/images/imageNLMM104.jpg").toString().substring(6));
        
        try {
            img = ImageIO.read(new File(getClass().getResource("/images/imageNLMM201.jpg").toString().substring(6)));
        } catch (IOException e) {
            //System.out.println("AQUI");
        }
        
        //BufferedImage dimg;
        Image scaledInstance = img.getScaledInstance(jLabelImage.getWidth(), jLabelImage.getHeight(), Image.SCALE_SMOOTH);
        
        ImageIcon imageIcon = new ImageIcon(scaledInstance);
        jLabelImage.setIcon(imageIcon);

        this.add(jLabelImage);
        */
        /********************************************

        JFrame jFrameImage;
        ImageIcon icon;
        JLabel jLabelImage;//, jLabelHelp;
        jFrameImage = new JFrame("Image");
        //JPanel jPanelImage;
        JScrollPane jScrollPane;
      
        jFrameImage.setAlwaysOnTop(true);
        jFrameImage.setLayout(null);
        //jFrameImage.setResizable(false);
        
        icon = new ImageIcon(this.getClass().getResource("/images/miro1.jpg"));

        jLabelImage = new JLabel(icon);
        
        
        
        jScrollPane = new JScrollPane();
        //jScrollPane.setPreferredSize(new Dimension(icon.getIconWidth(), icon.getIconHeight() / 2));
        //jScrollPane.setSize(new Dimension(icon.getIconWidth(), icon.getIconHeight() / 2));
        //jScrollPane.setHorizontalScrollBar(null);
       
        jFrameImage.setContentPane(jScrollPane);

        jScrollPane.setViewportView(jLabelImage);
        
        jFrameImage.setSize(icon.getIconWidth() + 34, icon.getIconHeight() / 2);
        jFrameImage.setVisible(true);
        
        File myFile = new File("/path/to/file.pdf");
        Desktop.getDesktop().open(myFile);
        
        
        //*******************************************/
        /*
        
        File helpFile = new File(String.valueOf(this.getClass().getResource("/help/Getting_Started.pdf")).substring(6));
        
        try {
            Desktop.getDesktop().open(helpFile);
        } catch (IOException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        String jMenuItemEditText[] = {
            "Main Parameters",              //0
            "Material Model",               //1
            "Geometry Properties",          //2
            "Layer Properties",             //3
            "Layer Patterns",               //4
            "Layers to Analyze",            //5
            "Pre-Stressed Layers",          //6
            "Monotonic Load",               //7
            "Stop Condition Layer Strain",  //8
            "Cyclic Load",                  //9
            "Phase Data"};                  //10

        jMenuEdit.setEnabled(false);
        jMenuView.setEnabled(false);
        jMenuGraphics.setEnabled(false);
        
        jMenuItemNew.addActionListener(new JMenuItemNewActionListener(this));
        
        jMenuItemSave = new JMenuItem("Save");
        jMenuItemSave.addActionListener(new JMenuItemSaveActionListener(this));
        jMenuItemSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemSave.setEnabled(false);
        jMenuFile.add(jMenuItemSave);
        
        jMenuItemSaveAs = new JMenuItem("Save As");
        jMenuItemSaveAs.addActionListener(new JMenuItemSaveActionListener(this));
        //jMenuItemSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemSaveAs.setEnabled(false);
        jMenuFile.add(jMenuItemSaveAs);
        
        jMenuItemExit = new JMenuItem("Exit");
        jMenuItemExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                jMenuItemExitActionPerformed();
            }
        });
        jMenuFile.add(jMenuItemExit);
        
        jMenuItemEdit = new JMenuItem[jMenuItemEditText.length];
        for(i=0; i<jMenuItemEditText.length; i++) {
            jMenuItemEdit[i] = new JMenuItem(jMenuItemEditText[i]);
            jMenuItemEdit[i].setName(String.valueOf(i));
            jMenuItemEdit[i].setEnabled(false);
            jMenuItemEdit[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    switch (((JMenuItem) ae.getSource()).getName()) {
                        case "0":
                            jMenuItemEditMainParametersActionPerfomed();
                            break;
                        case "1":
                            jMenuItemEditMaterialModelActionPerfomed();
                            break;
                        case "2":
                            jMenuItemEditGeometryPropertiesActionPerfomed();
                            break;
                        case "3":
                            jMenuItemEditLayerPropertiesActionPerfomed();
                            break;
                        case "4":
                            jMenuItemEditLayerPatternsActionPerfomed();
                            break;
                        case "5":
                            jMenuItemEditLayersToAnalyseActionPerfomed();
                            break;
                        case "6":
                            jMenuItemEditPreStressedLayersActionPerfomed();
                            break;
                        case "7":
                            jMenuItemEditMonotonicLoadActionPerfomed();
                            break;
                        case "8":
                            jMenuItemEditStopConditionLayerStrainActionPerfomed();
                            break;
                        case "9":
                            jMenuItemEditCyclicLoadActionPerfomed();
                            break;
                        case "10":
                            jMenuItemEditPhaseDataActionPerfomed();
                            break;
                    }
                }
            });
            jMenuEdit.add(jMenuItemEdit[i]);                        
        }
        jMenuItemHelpDocros.addActionListener(new JButtonHelpActionListener("Docros"));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooserOpen = new javax.swing.JFileChooser();
        jMenuBarMain = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
        jMenuItemNew = new javax.swing.JMenuItem();
        jMenuItemOpen = new javax.swing.JMenuItem();
        jMenuEdit = new javax.swing.JMenu();
        jMenuView = new javax.swing.JMenu();
        jMenuRun = new javax.swing.JMenu();
        jMenuItemRunDocros = new javax.swing.JMenuItem();
        jMenuGraphics = new javax.swing.JMenu();
        jMenuItemGraphicsCrossSection = new javax.swing.JMenuItem();
        jMenuItemGraphicsLayer = new javax.swing.JMenuItem();
        jMenuHelp = new javax.swing.JMenu();
        jMenuItemHelpDocros = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("DocrosData");
        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(new java.awt.Color(255, 255, 255));
        setName("JFrameMain"); // NOI18N

        jMenuFile.setMnemonic('F');
        jMenuFile.setText("File");

        jMenuItemNew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemNew.setText("New");
        jMenuFile.add(jMenuItemNew);

        jMenuItemOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemOpen.setText("Open");
        jMenuItemOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemOpenActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuItemOpen);

        jMenuBarMain.add(jMenuFile);

        jMenuEdit.setMnemonic('E');
        jMenuEdit.setText("Edit");
        jMenuBarMain.add(jMenuEdit);

        jMenuView.setMnemonic('V');
        jMenuView.setText("View");
        jMenuBarMain.add(jMenuView);

        jMenuRun.setMnemonic('R');
        jMenuRun.setText("Run");

        jMenuItemRunDocros.setText("DOCROS");
        jMenuItemRunDocros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRunDocrosActionPerformed(evt);
            }
        });
        jMenuRun.add(jMenuItemRunDocros);

        jMenuBarMain.add(jMenuRun);

        jMenuGraphics.setMnemonic('G');
        jMenuGraphics.setText("Graphics");

        jMenuItemGraphicsCrossSection.setText("Cross Section");
        jMenuItemGraphicsCrossSection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemGraphicsCrossSectionActionPerformed(evt);
            }
        });
        jMenuGraphics.add(jMenuItemGraphicsCrossSection);

        jMenuItemGraphicsLayer.setText("Layer");
        jMenuItemGraphicsLayer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemGraphicsLayerActionPerformed(evt);
            }
        });
        jMenuGraphics.add(jMenuItemGraphicsLayer);

        jMenuBarMain.add(jMenuGraphics);

        jMenuHelp.setMnemonic('H');
        jMenuHelp.setText("Help");

        jMenuItemHelpDocros.setText("About DOCROS");
        jMenuHelp.add(jMenuItemHelpDocros);

        jMenuBarMain.add(jMenuHelp);

        setJMenuBar(jMenuBarMain);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 322, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemOpenActionPerformed
        
        if(path != null) {
            jFileChooserOpen = null;
            jFileChooserOpen = new JFileChooser(path);   
        }
        if(jFileChooserOpen.showOpenDialog(this) == javax.swing.JFileChooser.APPROVE_OPTION) {
            resetAll();
            dataFile = jFileChooserOpen.getSelectedFile();
            openFile();
        }
    }//GEN-LAST:event_jMenuItemOpenActionPerformed

    void openFile() {          
            
        int i;
        jMenuItemSaveAs.setEnabled(true);
        path = dataFile.getAbsolutePath().substring(0, dataFile.getAbsolutePath().length() - dataFile.getName().length());
        try {
            if(pathFile.exists()) {
                BufferedWriter pathBufferedWriter;
                pathBufferedWriter = new BufferedWriter(new FileWriter(pathFile.getAbsoluteFile()));
                pathBufferedWriter.write(path);
                pathBufferedWriter.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(i=0; i<3; i++)
            jMenuItemEdit[i].setEnabled(true);
        try {
            createTempFile();
            readMainParameters();
            readUnitsInputOutputData();
            readTolerances();
            readLayerStressStrainGraphics();
            createLaminateGeoPropList();
            createNonLinearMaterialModelTypeList();
            createAllNonLinearMaterialModelList();
            createLayerPropertiesList();
            createLayerPatternsList();
            createPreStressedLayersList();
            createMonotonicLoadTypeDataList(); 
            createStopConditionLayerStrain();
            createCyclicLoadTypeDataList();
            createPhaseDataList();
            //readLayerStrainStress();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        scan.close();
        /*
        if(tempFile != null && tempFile.exists())
            tempFile.delete();
        */
        this.setTitle("DocrosData > " + dataFile.getName());
        if(errorMessage.endsWith(">")) 
            createJFrameErrorMessage();
        if(drawIsPossible) {
            drawFigure(phase);
            createJMenuItemView();
            figureIsDrawn = true;
        }
        else
            drawFigure(0);
        File outFile = new File(dataFile.getPath().substring(0, dataFile.getPath().length() - 6) + "OUT.dat");
        if(outFile.exists()) {
            jMenuGraphics.setEnabled(true);
            //jMenuItemStressStrain.addActionListener(new JMenuItemStressStrainActionListener());
        }
    }

    
    private void jMenuItemRunDocrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRunDocrosActionPerformed
        
        if(path != null) {
            jFileChooserOpen = null;
            jFileChooserOpen = new JFileChooser(path);   
        }
        jFileChooserOpen.setDialogTitle("Run");
        if (jFileChooserOpen.showOpenDialog(this) == javax.swing.JFileChooser.APPROVE_OPTION) {
            resetAll();
            //dataFile = null;
            Process process;
            BufferedInputStream input;
            FileOutputStream output;
            File docrosExeFile;
            byte[] buffer = new byte[256];
            int bytesRead;  
            dataFile = jFileChooserOpen.getSelectedFile();
            path = dataFile.getAbsolutePath().substring(0, dataFile.getAbsolutePath().length() - dataFile.getName().length());
            try {
                if(pathFile.exists()) {
                    BufferedWriter pathBufferedWriter;
                    pathBufferedWriter = new BufferedWriter(new FileWriter(pathFile.getAbsoluteFile()));
                    pathBufferedWriter.write(path);
                    pathBufferedWriter.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                input = new BufferedInputStream(getClass().getResourceAsStream("/docros/DOCROS.exe"));
                //docrosExeFile = new File("C:" + File.separator + "Windows" + File.separator + "Temp" + File.separator + "DOCROS.exe");
                docrosExeFile = new File(System.getProperty("java.io.tmpdir") + "DOCROS.exe");
                if(!docrosExeFile.exists())
                    docrosExeFile.createNewFile();
                docrosExeFile.deleteOnExit(); 
                try {
                    output = new FileOutputStream(docrosExeFile);
                    while((bytesRead = input.read(buffer)) != -1)
                        output.write(buffer, 0, bytesRead);
                    output.close();
                }
                catch (IOException e) {
                    //
                }                
                input.close();
                process = Runtime.getRuntime().exec("cmd /c start cmd /k " + docrosExeFile.getAbsolutePath() + " \"" + dataFile.getAbsolutePath().substring(0, dataFile.getAbsolutePath().length() - 7) + "\"");
                try (BufferedReader processInput = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                    String line;
                    line = processInput.readLine();
                    while(line != null)
                        line = processInput.readLine();
                    openFile();
                }
            } catch (IOException ex) {
                Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jMenuItemRunDocrosActionPerformed

    private void jMenuItemGraphicsLayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemGraphicsLayerActionPerformed
   
        JFrameLayerGraphics jFrameLayerGraphics = new JFrameLayerGraphics(this);
    }//GEN-LAST:event_jMenuItemGraphicsLayerActionPerformed

    private void jMenuItemGraphicsCrossSectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemGraphicsCrossSectionActionPerformed
        
        JFrameCrossSectionGraphics jFrameCrossSectionGraphics = new JFrameCrossSectionGraphics(this);
    }//GEN-LAST:event_jMenuItemGraphicsCrossSectionActionPerformed
 
    private void jMenuItemExitActionPerformed() {

        if(tempFile != null && tempFile.exists())
            tempFile.delete();
        System.exit(0);
    }

    private void jMenuItemEditMainParametersActionPerfomed() {
        JFrameInputMainParameters jFrameInputMainParameters = new JFrameInputMainParameters(this, null);
    }
    
    private void jMenuItemEditMaterialModelActionPerfomed() {
        JFrameInputMaterialModel jFrameInputMaterialModel = new JFrameInputMaterialModel(this, null);
    }
    
    private void jMenuItemEditGeometryPropertiesActionPerfomed() {
        JFrameInputLaminateGeoProp jFrameInputLaminateGeoProp = new JFrameInputLaminateGeoProp(this, null);
    }
    
    private void jMenuItemEditLayerPropertiesActionPerfomed() {
        JFrameInputLayerProperties jFrameInputLayerProperties = new JFrameInputLayerProperties(this, null);
    }
    
    private void jMenuItemEditLayerPatternsActionPerfomed() {
        JFrameInputLayerPatterns jFrameInputLayerPatterns = new JFrameInputLayerPatterns(this, null);
    }
    
    private void jMenuItemEditLayersToAnalyseActionPerfomed() {
        JFrameInputLayersToAnalyze jFrameInputLayersToAnalyse = new JFrameInputLayersToAnalyze(this, null);
    }
    
    private void jMenuItemEditPreStressedLayersActionPerfomed() {
        JFrameInputPreStressedLayers jFrameInputPreStressedLayers = new JFrameInputPreStressedLayers(this, null);
    }
    
    private void jMenuItemEditMonotonicLoadActionPerfomed() {
        JFrameInputMonotonicLoadTypeData jFrameInputMonotonicLoadTypeData = new JFrameInputMonotonicLoadTypeData(this, null);
    }
    
    private void jMenuItemEditStopConditionLayerStrainActionPerfomed() {
        JFrameInputStopConditionLayerStrain jFrameInputStopConditionLayerStrain = new JFrameInputStopConditionLayerStrain(this, null);
    }
    
    private void jMenuItemEditCyclicLoadActionPerfomed() {
        JFrameInputCyclicLoadTypeData jFrameInputCyclicLoadTypeData = new JFrameInputCyclicLoadTypeData(this, null);
    }
    
    private void jMenuItemEditPhaseDataActionPerfomed() {
        JFrameInputPhaseData jFrameInputPhaseData = new JFrameInputPhaseData(this, null);
    }
    
    void updateErrorMessage(String tag) {

        errorMessage += ";" + "<" + tag + ">";
        
    }
      
    void createTempFile() throws FileNotFoundException {

        int c = 0, c2 = 0;

        //tempFile = new File(dataFile.getAbsolutePath().replaceFirst(dataFile.getName(), "temp_file.txt"));
        tempFile = new File(System.getProperty("java.io.tmpdir") + "docrosTempFile.txt");
        /*
        if (tempFile.exists())
            tempFile.delete();
        */
        try {
            tempFile.createNewFile();
        } catch (IOException ex) {
                Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        tempFile.deleteOnExit();
        BufferedWriter dataBufferedWriter = null;
        try {
            dataBufferedWriter = new BufferedWriter(new FileWriter(tempFile.getAbsoluteFile()));
        } catch (IOException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedReader dataFileBufferedReader = null;
        //BufferedWriter dataBufferedWriter = null;
        try {
            dataFileBufferedReader = new BufferedReader(new FileReader(dataFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            c = dataFileBufferedReader.read();
        } catch (IOException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (c != -1) {
            if (c == '#') {
                try {
                    dataFileBufferedReader.readLine();
                } catch (IOException ex) {
                    Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    c2 = dataFileBufferedReader.read();
                } catch (IOException ex) {
                    Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (c2 != '#') {
                    if (c2 != '\n' && c2 != ' ') {
                        //System.out.print('\n');
                        try {
                            dataBufferedWriter.newLine();
                        } catch (IOException ex) {
                            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                    //System.out.print((char) c2);
                    try {
                        dataBufferedWriter.write((char) c2);
                    } catch (IOException ex) {
                        Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        c = dataFileBufferedReader.read();
                    } catch (IOException ex) {
                        Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else
                    c = c2;
             }
             else {
                //System.out.print((char) c);
                //if(c != ';') {
                try {
                    dataBufferedWriter.write((char) c);
                } catch (IOException ex) {
                    Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
                }
                //}
                try {
                    c = dataFileBufferedReader.read();
                } catch (IOException ex) {
                    Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
                }
             }

         }

         try {
            dataFileBufferedReader.close();
            dataBufferedWriter.close();
         } catch (IOException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    int findCountTagTempFile(String tag) throws FileNotFoundException {
        
        int count;
        scan = new Scanner(tempFile);
        String str;
        while(scan.hasNext() && !(str = scan.next()).equals("<"+tag+">")) {}
        
        if(!scan.hasNext()) {
            return -1;
        }
        while(scan.hasNext() && !(str = scan.next()).equals("COUNT")) {}
        
        scan.next();
        count = scan.nextInt();
        scan.next();
        return count;
    }
    
    String scanNexts(Scanner s, int n) {
        int i;
        for(i=0; i<n; i++)
            s.next();
        return s.next();
    }
    
    void readMainParameters() {
        
        String str;
        try {
            scan = new Scanner(tempFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        while(scan.hasNext() && !(str = scan.next()).equals("<MAIN_PARAMETERS>")) {}
        if(!scan.hasNext())
            updateErrorMessage("MAIN_PARAMETERS");
        else {
            String tit;
            String title = scanNexts(scan, 2);
            while(!(tit = scan.next()).equals(";"))
                title += " " + tit;
            //int layers = Integer.valueOf(scanNexts(scan, 2));
            String sxs = scanNexts(scan, 10);
            double axial = Double.valueOf(scanNexts(scan, 3));
            mainParameters = new MainParameters(title, sxs, axial);      
        }
    }
    
    void readUnitsInputOutputData() {
        
        String str;
        try {
            scan = new Scanner(tempFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        while(scan.hasNext() && !(str = scan.next()).equals("<UNITS_INPUT_OUTPUT_DATA>")) {}
        if(!scan.hasNext()) {
            updateErrorMessage("UNITS_INPUT_OUTPUT_DATA");
            //System.out.println("<MAIN_PARAMETERS> do not exist");
        }
        else {
            unitsInputOutputData = new String[2];
            unitsInputOutputData[0] = scan.next();
            unitsInputOutputData[1] = scan.next();
        }
    }
    
    void readTolerances() {
        
        String str;
        try {
            scan = new Scanner(tempFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        while(scan.hasNext() && !(str = scan.next()).equals("<TOLERANCES>")) {}
        if(!scan.hasNext()) {
            updateErrorMessage("TOLERANCES");
            //System.out.println("<MAIN_PARAMETERS> do not exist");
        }
        else {
            if(mainParameters != null)
                jMenuItemEdit[1].setEnabled(true);
            //definedTags[tolerancesIndex] = true;
            tolerances = Double.valueOf(scan.next());
        }
    }
    
    void readLayerStressStrainGraphics() {
        
        String str;
        try {
            scan = new Scanner(tempFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        while(scan.hasNext() && !(str = scan.next()).equals("<LAYER-STRESS-STRAIN-GRAPHICS>")) {}
        if(!scan.hasNext()) {
            updateErrorMessage("LAYER-STRESS-STRAIN-GRAPHICS");
            //System.out.println("<MAIN_PARAMETERS> do not exist");
        }
        else {
            int i, numLayers;
            //definedTags[layerStressStrainGraphicsIndex] = true;
            //while(!(str = scan.next()).equals(";"))
            //    count++;
            numLayers = scan.nextInt();
            layerStressStrainGraphics = new int[numLayers];
            for(i=0; i<numLayers; i++)
                layerStressStrainGraphics[i] = scan.nextInt();
        }
    }

    void createLaminateGeoPropList() throws FileNotFoundException {
        //System.out.println("n="+n);
        int i, count;
        count = findCountTagTempFile("LAMINATEGEOPROP");
        if(count > 0) {
            //definedTags[laminateGeoPropIndex] = true;
            LaminateGeoProp node = null;
            laminateGeoPropList = new LaminateGeoPropList();
            for(i=0; i<count; i++) {
                //System.out.println("i="+i+"->"+scan.next());
                //scan.nextInt();
                while(!scan.next().equals(";"))
                    node = new LaminateGeoProp(scan.next(), Double.parseDouble(scan.next()), Double.parseDouble(scan.next()));
                //System.out.println(scan.next());
                //System.out.println(scan.nextFloat());
                //System.out.println(scan.next());
                //s = scan.next();
                laminateGeoPropList.addLaminateGeoProp(node);
                //scan.next();
            }
        }
        else {
            drawIsPossible = false;
            updateErrorMessage("LAMINATEGEOPROP");
            //if(count == 0)
            //    definedTags[laminateGeoPropIndex] = true;
        }
    }

    void createAllNonLinearMaterialModelList() throws FileNotFoundException {
        
        int i, j, k, count;
        Boolean materialModelDefined = false;
        NonLinearMaterialModelType type = nonLinearMaterialModelTypeList.first;
        for(i=0; i<materials.length; i++) {
            count = findCountTagTempFile(materials[i]);
            if(count > 0) {
                materialModelDefined = true;
                //NonLinearMaterialModelList list = new NonLinearMaterialModelList();
                //type.list = list;
                
                //gotoParameters(materials[i]);
                for(j=0; j<count; j++) {
                    double[] parameters = new double[type.numberParameters];
                    //scan.next();
                    String name = scanNexts(scan, 1);
                    if(materials[i].substring(4).startsWith("1") || materials[i].substring(4).startsWith("2")) {
                        scan.next();
                        scan.next();
                    }
                    if(materials[i].equals("NLMM105")) {
                        String crackModel;
                        for(k=0; k<8; k++)
                            parameters[k] = Double.valueOf(scan.next());                  
                        crackModel = scan.next();
                        for(k=8; k<12; k++)
                            parameters[k] = Double.valueOf(scan.next());
                        if(crackModel.equals("TRILINEAR")) {
                            parameters[12] = 0.0;
                            parameters[13] = 0.0;
                            scan.next();
                            scan.next();        
                        }
                        else {
                            parameters[13] = Double.valueOf(scan.next());
                            parameters[14] = Double.valueOf(scan.next());
                        }
                        for(k=14; k<type.numberParameters; k++)
                            parameters[k] = Double.valueOf(scan.next());
                    }
                    else {
                        for(k=0; k<type.numberParameters; k++)
                            parameters[k] = Double.valueOf(scan.next());
                    }
                    NonLinearMaterialModel model = new NonLinearMaterialModel(materials[i], name, parameters);
                    type.list.addNonLinearMaterialModel(model);
                    scan.nextLine();
                }
            }
            //else
            //updateErrorMessage(materials[i]);
            type = type.next;
        }
        if(!materialModelDefined)
            updateErrorMessage("NLMM");
        if(materialModelDefined && laminateGeoPropList != null && laminateGeoPropList.count > 0)
            jMenuItemEdit[3].setEnabled(true);
        else
            drawIsPossible = false;
    }

    void createLayerPropertiesList() throws FileNotFoundException {
        int i, count;
        //String type, name;
        count = findCountTagTempFile("LAYER_PROPERTIES");
        if(count > 0) {
            //definedTags[layerPropertiesIndex] = true;
            jMenuItemEdit[4].setEnabled(true);
            LayerProperties node = null;
            layerPropertiesList = new LayerPropertiesList();
            for(i=0; i<count; i++) {
                while(!scan.next().equals(";")) {
                    node = new LayerProperties(scan.next());
                    node.materialModel = nonLinearMaterialModelTypeList.getNonLinearMaterialModel(scan.next(), scan.next());
                    //node.materialModel.countLayerProperties++;
                    node.geoProp = laminateGeoPropList.getLaminateGeoProp(scan.next());
                    layerPropertiesList.addLayerProperties(node);
                }
            }
        }
        else {
            updateErrorMessage("LAYER_PROPERTIES");
            drawIsPossible = false;
            //if(count == 0)
            //    definedTags[layerPropertiesIndex] = true;
        }
    }

    void createPreStressedLayersList() throws FileNotFoundException {
        
        int i, count, firstLayer, lastLayer;
        String patternName, range, rangeSplit[];
        double strain;
        count = findCountTagTempFile("PRESTRESSED_LAYERS");
        if(count > 0) {
            preStressedLayersList = new PreStressedLayersList();
            PreStressedLayers node;
            for(i=0; i<count; i++) {
                patternName = scanNexts(scan, 1);
                range = scan.next();
                if(range.startsWith("[")) {
                    rangeSplit = new String[2];
                    range = range.substring(1, range.length() - 1);
                    rangeSplit = range.split("-");
                    firstLayer = Integer.valueOf(rangeSplit[0]);
                    lastLayer = Integer.valueOf(rangeSplit[1]);
                }
                else {
                    firstLayer = Integer.valueOf(range);
                    lastLayer = Integer.valueOf(range);
                }
                strain = Double.valueOf(scan.next());
                scan.nextLine();
                node = new PreStressedLayers(patternName, firstLayer, lastLayer, strain);
                preStressedLayersList.addPreStressedLayers(node);            }
        }
        else
            updateErrorMessage("PRESTRESSED_LAYERS");
    }
    
    void createLayerPatternsList() throws FileNotFoundException {
        
        int i, j, count, countLayers, countLayerProperties;
        String name;
        count = findCountTagTempFile("LAYER_PATTERNS");
        if(count > 0) {
            for(i=5; i<10; i++)
                jMenuItemEdit[i].setEnabled(true);
            LayerPatterns node = null;
            layerPatternsList = new LayerPatternsList();
            for(i=0; i<count; i++) {
                while(!scan.next().equals(";")) {
                    name = scan.next();
                    countLayers = scan.nextInt();   
                    scan.next();
                    countLayerProperties = scan.nextInt();
                    node = new LayerPatterns(name, countLayers, countLayerProperties);
                    //node.layerProperties = new LayerProperties[numberLayerProperties];
                    for(j=0; j<countLayerProperties; j++) {
                        node.layerProperties[j] = layerPropertiesList.getLayerProperties(scan.next());
                        node.layerProperties[j].countLayerPatterns++;
                        node.layerProperties[j].materialModel.countLayerPatterns++;
                        node.layerProperties[j].geoProp.countLayerPatterns++;
                        node.layerPropertiesPhase[j] = scan.nextInt();
                    }
                    //node.numProp = scan.nextInt();
                    layerPatternsList.addLayerPatterns(node);
                }
            }
        }
        else {
            updateErrorMessage("LAYER_PATTERNS");
            drawIsPossible = false;
            //if(count == 0)
            //    definedTags[layerPatternsIndex] = true;
        }
    }
    
    void createStopConditionLayerStrain() throws FileNotFoundException {
        
        int i, count;
        count = findCountTagTempFile("STOP_CONDITION_LAYER_STRAIN");
        if(count > 0) {
            stopConditionLayerStrainList = new StopConditionLayerStrainList();
            StopConditionLayerStrain node;
            for(i=0; i<count; i++) {
                node = new StopConditionLayerStrain(scanNexts(scan, 1), scan.nextInt(), scan.next(), Double.valueOf(scan.next()));
                scan.nextLine();
                stopConditionLayerStrainList.addStopConditionLayerStrain(node);               
            }
            if(monotonicLoadTypeDataList != null && monotonicLoadTypeDataList.first != null)
                jMenuItemEdit[10].setEnabled(true);
        }
        else
            updateErrorMessage("STOP_CONDITION_LAYER_STRAIN");
    }
    
    void createMonotonicLoadTypeDataList() throws FileNotFoundException {
        
        int i, layer, count;
        String monotonicLoadName, layerPatternsName;
        double increment;
        count = findCountTagTempFile("MONOTONIC_LOAD_TYPE_DATA");
        if(count > 0) {
            //definedTags[monotonicLoadTypeDataIndex] = true;
            monotonicLoadTypeDataList = new MonotonicLoadTypeDataList();
            MonotonicLoadTypeData node;
            for(i=0; i<count; i++) {
                //scan.next();
                monotonicLoadName = scanNexts(scan, 1);
                layer = scan.nextInt();
                layerPatternsName = scan.next();
                //System.out.println("READ Layer Pat Name: " + layerPatternsName);
                increment = Double.valueOf(scan.next());
                //System.out.println(monotonicLoadName + " " + layerPatternsName + " " + increment);
                node = new MonotonicLoadTypeData(monotonicLoadName, layer, layerPatternsName, increment);
                monotonicLoadTypeDataList.addMonotonicLoadTypeData(node);
                scan.nextLine();
            }
            if(stopConditionLayerStrainList != null && stopConditionLayerStrainList.first != null)
                jMenuItemEdit[10].setEnabled(true);
        }
        else {                    
            updateErrorMessage("MONOTONIC_LOAD_TYPE_DATA");    
            //if(count == 0)
            //    definedTags[monotonicLoadTypeDataIndex] = true;
        }
        

    }
    
    void createCyclicLoadTypeDataList() throws FileNotFoundException {
        
        int i, j, count, layer, steps;
        String cyclicLoadName;
        count = findCountTagTempFile("CYCLIC_LOAD_TYPE_DATA");
        if(count <= 0) {
            updateErrorMessage("CYCLIC_LOAD_TYPE_DATA");
            //if(count == 0)
            //    definedTags[cyclicLoadTypeDataIndex] = true;
        }
        if(count > 0 && layerPatternsList != null && layerPatternsList.count > 0) {
            //definedTags[cyclicLoadTypeDataIndex] = true;
            jMenuItemEdit[10].setEnabled(true);
            cyclicLoadTypeDataList = new CyclicLoadTypeDataList();
            CyclicLoadTypeData node;
            int cycles[];
            double increment, amplitude[];
            for(i=0; i<count; i++) {
                cyclicLoadName = scanNexts(scan, 1);
                layer = scan.nextInt();
                steps = scan.nextInt();
                cycles = new int[steps];
                amplitude = new double[steps];
                for(j=0; j<steps; j++) {
                    cycles[j] = Integer.valueOf(scanNexts(scan, 1));
                    amplitude[j] = Double.valueOf(scanNexts(scan, 1));
                }
                increment = Double.valueOf(scanNexts(scan, 1));
                node = new CyclicLoadTypeData(cyclicLoadName, layer, steps, increment);
                node.cycles = cycles;
                node.amplitude = amplitude;
                cyclicLoadTypeDataList.addCyclicLoadTypeData(node);
                scan.nextLine();
            }
        }
        //cyclicLoadTypeDataList.printCyclicLoadTypeDataList();
    }
    
    void createPhaseDataList() throws FileNotFoundException {
        
        int i, count;
        String loadType, loadName, stopName;
        count = findCountTagTempFile("PHASE_DATA");
        if(count <= 0)
            updateErrorMessage("PHASE_DATA");
        else {
            if((monotonicLoadTypeDataList != null && monotonicLoadTypeDataList.count > 0 &&
                stopConditionLayerStrainList != null && stopConditionLayerStrainList.count > 0) || 
               (cyclicLoadTypeDataList != null && cyclicLoadTypeDataList.count > 0)) {
                
                phaseDataList = new PhaseDataList();
                PhaseData node;
                for(i=0; i<count; i++) {
                    loadType = scanNexts(scan, 1);
                    loadName = scan.next();
                    stopName = scanNexts(scan, 1);
                    node = new PhaseData(loadType, loadName, stopName);
                    phaseDataList.addPhaseData(node);
                    scan.nextLine();
                }
                //createJMenuItemView();
            }
        }
    }
    /*
    void readLayerStrainStress() {
        
        File outFile;
        int layers, i, lineNumber = 1;
        //String line;
        outFile = new File(dataFile.getPath().substring(0, dataFile.getPath().length() - 6) + "OUT.dat");
        if(outFile.exists()) { 
            try {
                scan = new Scanner(outFile);
                layers = layerPatternsList.getNumberOfLayers();
                layerStrainStress = new double[layers][2];
                //line = scan.nextLine();
                while(!scan.nextLine().startsWith("</MONOTONIC_LOAD>")) {
                    //scan.nextLine();
                    lineNumber++;
                }
                scan = new Scanner(outFile);
                for(i=0; i < lineNumber - layers - 1; i++)
                    scan.nextLine();
                for(i=0; i<layers; i++) {
                    layerStrainStress[i][0] = Double.valueOf(scanNexts(scan, 1));
                    layerStrainStress[i][1] = Double.valueOf(scanNexts(scan, 3));
                    scan.nextLine();
                }
                //scan.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    */
    public void createJMenuItemView() {
        
        int i, numPhases = 1;
        if(phaseDataList != null)
            numPhases = phaseDataList.count;
        else {
            if(layerPatternsList != null)
                numPhases = layerPatternsList.getNumberOfPhases();
        }
        jMenuItemView = new JMenuItem[numPhases];
        jMenuView.removeAll();
        for(i=1; i<=numPhases; i++) {
            jMenuItemView[i - 1] = new JMenuItem("Phase " + i);
            jMenuItemView[i - 1].addActionListener(new JMenuItemViewActionListener(this, i));
            jMenuView.add(jMenuItemView[i - 1]);
            jMenuView.setEnabled(true);
        }
    }

    int countParameters() {
     
        String str;
        int count = -2;
        str = scan.next();
        while(!str.equals(";")) {
            count++;
            str = scan.next();
        }
        return count;
    }
       
    void drawFigure(int phase) {

        jPanelMain = new JPanel();
        jPanelMain.setLayout(null);
        jPanelMain.setSize(this.getContentPane().getSize());
     
        if(phase > 0 && drawIsPossible) {

            if(mainParameters != null) {
                jLabelMainTitle = new JLabel("Main Title > " + mainParameters.title, SwingConstants.RIGHT);
                jLabelMainTitle.setSize(jLabelMainTitle.getText().length() * charWidth, charHeight);
                jLabelMainTitle.setLocation(jPanelMain.getWidth() - jLabelMainTitle.getWidth() - 12, jPanelMain.getHeight() - jLabelMainTitle.getHeight());
                jPanelMain.add(jLabelMainTitle);
            }

            JPanel jPanelMainCenter = new JPanel();
            jPanelMainCenter.setLayout(null);

            jPanelLayerPatterns = new JPanelLayerPatterns[layerPatternsList.countJPanelLayerPatterns(phase)];
            LayerPatterns temp = layerPatternsList.first;
            jPanelMainCenter.setSize((jPanelMain.getWidth() * 2) / 3, jPanelMain.getHeight());
            jPanelMainCenter.setLocation(jPanelMain.getWidth() / 3, 0);

            this.getContentPane().setVisible(false);

            double zoomFactor;
            zoomFactor = getMin((jPanelMainCenter.getWidth() - 100) / layerPatternsList.widthLayerPatternsList(phase), (jPanelMainCenter.getHeight() - 100) / layerPatternsList.heightLayerPatternsList(phase));
           
            int i = 0, j, y = 50;
            while(temp != null) {
                int jPanelLayerPatternsWidth = 0;
                for(j=0; j<temp.layerProperties.length; j++) {
                    if(temp.layerPropertiesPhase[j] <= phase) {
                        if(j == 0) {
                            jPanelLayerPatterns[i] = new JPanelLayerPatterns(temp, j, zoomFactor);
                            jPanelLayerPatterns[i].setLocation((jPanelMainCenter.getWidth() - jPanelLayerPatterns[i].getWidth())/2, y);
                            jPanelLayerPatterns[i].setBackground(getMaterialColor(temp.layerProperties[j].materialModel.type));
                            jPanelLayerPatterns[i].addMouseListener(new JPanelLayerPatternsMouseListener(this, (JPanelLayerPatterns) jPanelLayerPatterns[i], null));
                            jPanelMainCenter.add(jPanelLayerPatterns[i]);
                            jPanelLayerPatternsWidth += jPanelLayerPatterns[i].getWidth();
                        }
                        else {
                            jPanelLayerPatterns[i] = new JPanelLayerPatterns(temp, j, zoomFactor);
                            jPanelLayerPatterns[i].setLocation((jPanelMainCenter.getWidth() - jPanelLayerPatternsWidth) / 2 - jPanelLayerPatterns[i].getWidth(), y);
                            jPanelLayerPatterns[i].setBackground(getMaterialColor(temp.layerProperties[j].materialModel.type));

                            jPanelLayerPatterns[i + 1] = new JPanelLayerPatterns(temp, j, zoomFactor);
                            jPanelLayerPatterns[i + 1].setLocation((jPanelMainCenter.getWidth() - jPanelLayerPatternsWidth) / 2 + jPanelLayerPatternsWidth, y);
                            jPanelLayerPatterns[i + 1].setBackground(getMaterialColor(temp.layerProperties[j].materialModel.type));

                            jPanelLayerPatterns[i].addMouseListener(new JPanelLayerPatternsMouseListener(this, (JPanelLayerPatterns) jPanelLayerPatterns[i], (JPanelLayerPatterns) jPanelLayerPatterns[i + 1]));
                            jPanelLayerPatterns[i + 1].addMouseListener(new JPanelLayerPatternsMouseListener(this, (JPanelLayerPatterns) jPanelLayerPatterns[i + 1], (JPanelLayerPatterns) jPanelLayerPatterns[i]));

                            jPanelMainCenter.add(jPanelLayerPatterns[i]);
                            jPanelMainCenter.add(jPanelLayerPatterns[i + 1]);

                            jPanelLayerPatternsWidth += (jPanelLayerPatterns[i].getWidth() * 2);

                            i++;
                        }
                        i++;
                    }
                }
                y += doubleToInt(zoomFactor * temp.heightLayerPatterns(phase));

                temp = temp.next;
            }
            jPanelMain.add(jPanelMainCenter);
        }
        jPanelMain.setVisible(true);
        this.setContentPane(jPanelMain);
        this.getContentPane().setVisible(true);    

    }

    double getMin(double x, double y) {
        
        if(x < y)
            return x;
        return y;
    }
    
    public static int doubleToInt(double x) {

        int i = (int) x;
        double d = x - i;
        if(d >= 0.5)
            i++;
        return i;
    }

    Color getMaterialColor(String x) {
        //System.out.println("X: "+x);
        int i = 0;
        while(!materials[i].equals(x))
            i++;
        Field f = null;
        try {
            f = Color.class.getField(materialColor[i]);
        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
          return (Color) f.get(null);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(JFrameDocrosData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    void createJFrameErrorMessage() {
        
        int i;
        JFrame jFrameErrorMessage;
        JLabel jLabelErrorMessage[];
        JButton jButtonOK;
        JPanel jPanelErrorMessage;
        
        String errorMessageArray[] = errorMessage.split(";");
        
        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        
        jFrameErrorMessage = new JFrame("<TAGS NOT DEFINED>");
        jFrameErrorMessage.setLayout(null);
        jFrameErrorMessage.setSize(278, (errorMessageArray.length + 5) * 18 + 4);
        jFrameErrorMessage.setResizable(false);
        jFrameErrorMessage.setLocation((screenDim.width - jFrameErrorMessage.getWidth()) / 2, (screenDim.height - jFrameErrorMessage.getHeight()) / 2);
        
        jPanelErrorMessage = new JPanel();
        jPanelErrorMessage.setLayout(new GridLayout(errorMessageArray.length, 1));
        jPanelErrorMessage.setSize(270, (errorMessageArray.length) * 18);
        jPanelErrorMessage.setLocation(0, 10);
        
        jLabelErrorMessage = new JLabel[errorMessageArray.length];
        for(i=0; i<errorMessageArray.length; i++) {
            jLabelErrorMessage[i] = new JLabel(errorMessageArray[i], SwingConstants.CENTER);
            jPanelErrorMessage.add(jLabelErrorMessage[i]);//, BorderLayout.CENTER);
        }
        jFrameErrorMessage.add(jPanelErrorMessage);
        jButtonOK = new JButton("OK");
        jButtonOK.setSize(80, 25);
        jButtonOK.setLocation((jPanelErrorMessage.getWidth() - jButtonOK.getWidth()) / 2, 36 + jPanelErrorMessage.getHeight());
        jButtonOK.addActionListener(new JButtonOKActionListener(jFrameErrorMessage));
        jFrameErrorMessage.add(jButtonOK);
        jFrameErrorMessage.setVisible(true);
    }

    private static class JButtonOKActionListener implements ActionListener {

        JFrame jFrameErrorMessage;
        
        public JButtonOKActionListener(JFrame jf) {
            
            jFrameErrorMessage = jf;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            jFrameErrorMessage.dispose();
        }
    }
    
    void resetAll() {
        
        int i;
        this.setTitle("DocrosData");
        if(jLabelMainTitle != null) {
            jLabelMainTitle.setVisible(false);
            jLabelMainTitle = null;
        }
        tempFile = null;
        //dataFile = null;
        scan = null;
        drawIsPossible = true;
        figureIsDrawn = false;
        phase = 1;
        errorMessage = "The following tags are not defined:; ";
        mainParameters = null;
        unitsInputOutputData = null;
        //unitsInputOutputData = new String[2];
        tolerances = -1;
        layerStressStrainGraphics = null;
        laminateGeoPropList = null;
        nonLinearMaterialModelTypeList = null;
        layerPropertiesList = null;
        layerPatternsList = null;
        preStressedLayersList = null;
        monotonicLoadTypeDataList = null;
        stopConditionLayerStrainList = null;
        cyclicLoadTypeDataList = null;
        phaseDataList = null;
        //layerStrainStress = null;
        jMenuItemSave.setEnabled(false);
        jMenuItemSaveAs.setEnabled(false);
        for(i=0; i<jMenuItemEdit.length; i++)
            jMenuItemEdit[i].setEnabled(false);
        jMenuEdit.setEnabled(true);
        jMenuView.setEnabled(false);
        jMenuGraphics.setEnabled(false);
        jMenuItemView = null;
    }
    
    void createNonLinearMaterialModelTypeList() {
        
        int i;       
        NonLinearMaterialModelType node;
        NonLinearMaterialModelList list;
        nonLinearMaterialModelTypeList = new NonLinearMaterialModelTypeList();
        for(i=0; i<materials.length; i++) {
            list = new NonLinearMaterialModelList();
            node = new NonLinearMaterialModelType('_' + materials[i]);
            node.numberParameters = materialNumberParameters[i];
            node.list = list;
            nonLinearMaterialModelTypeList.addNonLinearMaterialModelType(node);
           
        }
    }
    
    public int getMaterialIndex(String materialType) {
        
        int index = 0;
        while(!materials[index].equals(materialType))
            index++;
        return index;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser jFileChooserOpen;
    private javax.swing.JMenuBar jMenuBarMain;
    private javax.swing.JMenu jMenuEdit;
    private javax.swing.JMenu jMenuFile;
    private javax.swing.JMenu jMenuGraphics;
    private javax.swing.JMenu jMenuHelp;
    private javax.swing.JMenuItem jMenuItemGraphicsCrossSection;
    private javax.swing.JMenuItem jMenuItemGraphicsLayer;
    private javax.swing.JMenuItem jMenuItemHelpDocros;
    private javax.swing.JMenuItem jMenuItemNew;
    private javax.swing.JMenuItem jMenuItemOpen;
    private javax.swing.JMenuItem jMenuItemRunDocros;
    private javax.swing.JMenu jMenuRun;
    private javax.swing.JMenu jMenuView;
    // End of variables declaration//GEN-END:variables

}