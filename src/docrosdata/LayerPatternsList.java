
package docrosdata;

public class LayerPatternsList {
 
    LayerPatterns first;
    LayerPatterns last;
    int count;
    
    public LayerPatternsList() {
        
        first = null;
        last = null;
        count = 0;
    }
    
    int getNumberOfPhases() {
        
        LayerPatterns node = first;
        int phase = 1;
        while(node != null) {
            if(node.layerPropertiesPhase.length > phase)
                phase = node.layerPropertiesPhase.length;
            node = node.next;
        }
        return phase;
    }
    
    int getFirstLayer(String layerPatternsName) {
        
        int firstLayer = 1;
        LayerPatterns node = first;
        while (node != null) {
            if(node.name.equals(layerPatternsName))
                return firstLayer;
            firstLayer += node.numLayers;
            node = node.next;
        }
        return -1;
    }
    
    int getLastLayer(String layerPatternsName) {        
       
        LayerPatterns node = first;
        int lastLayer = node.numLayers;
        while (node != null) {
            if(node.name.equals(layerPatternsName))
                return lastLayer;
            node = node.next;
            lastLayer += node.numLayers;
        }
        return -1;
    }
    
    int getNumberOfLayers() {
        
        int number = 0;
        LayerPatterns node = first;
        while(node != null) {
            number += node.numLayers;
            node = node.next;
        }
        return number;
    }
    /*
    int getLayerPatternsNumber(String nam) {
        
        int i = 1;
        LayerPatterns node = first;
        while(node != null) {
            if(node.name.equals(nam))
                return i;
            i++;
            node = node.next;
        }
        return -1;
    }
    */
    
    
    
    int getNumberOfLayerPropertiesInPhase(int layer, int phase) {
        
        int i, num = 0;
        LayerPatterns node = getLayerPatterns(layer);
        for(i=0; i<node.layerPropertiesPhase.length; i++) {
            if(node.layerPropertiesPhase[i] <= phase)
                num++;
        }
        return num;
    }
    
    int getLayerNumberIncrement(LayerPatterns node, int layer, int phase, int position) {
        
        int i, j, num, pos;
        num = getNumberOfLayers();
        //LayerPatterns node = getLayerPatterns(layer);
        for(i=1; i<position; i++) {
            LayerPatterns aux = first;
            while(aux != null) {
                pos = 0;
                j = 0;
                while(j < aux.layerPropertiesPhase.length && pos < position) {
                    if(aux.layerPropertiesPhase[j] <= phase)
                        pos++;
                    j++;
                }
                if(j < aux.layerPropertiesPhase.length)
                    num += aux.numLayers;
                aux = aux.next;
            }
        }
        LayerPatterns aux = first;
        while(aux != node) {
                pos = 0;
                j = 0;
                while(j < aux.layerPropertiesPhase.length && pos < position) {
                    if(aux.layerPropertiesPhase[j] <= phase)
                        pos++;
                    j++;
                }
                if(j < aux.layerPropertiesPhase.length)
                    num += aux.numLayers;
                aux = aux.next;
        }
        num = num - getFirstLayer(node.name) + 1;
        return num;
    }
    
    String getLayerPropertiesMaterialName(int layer, int phase, int position) {
        
        int i, num = 0;
        LayerPatterns node = getLayerPatterns(layer);
        for(i=0; i<node.layerPropertiesPhase.length; i++) {
            if(node.layerPropertiesPhase[i] <= phase) {
                if(num == position)
                    return node.layerProperties[i].materialModel.name;
                num++;
            }
        }
        return null;
    }
    
    
    /*
    Boolean layerInPhase(int layer, int phase) {
        
        int i;
        LayerPatterns node = getLayerPatterns(layer);
        for(i=0; i<node.layerPropertiesPhase.length; i++) {
            if(node.layerPropertiesPhase[i] <= phase)
                return true;
        }
        return false;
    }
    */
    LayerPatterns getLayerPatterns(int layerNumber) {
                
        int firstLayer = 1;
        LayerPatterns node = first;
        while(node != null) {
            if(layerNumber <= firstLayer + node.numLayers - 1)
                return node;
            firstLayer += node.numLayers;
            node = node.next;
        }
        return node;
    }
    
    String getLayerPatternsName(int layerNumber) {
                
        int firstLayer = 1;
        LayerPatterns node = first;
        while(node != null) {
            if(layerNumber <= firstLayer + node.numLayers - 1)
                return node.name;
            firstLayer += node.numLayers;
            node = node.next;
        }
        return null;
    }
    
    void addLayerPatterns(LayerPatterns node) {
        
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
    
    void delLayerPatterns(String n) {
        
        LayerPatterns node1, node2;
        node1 = first;
        node2 = node1.next;
        if(node1.name.equals(n)) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && !node2.name.equals(n)) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
    }
    
    void delLayerPatterns() {
        
        LayerPatterns node1, node2;
        node1 = first;
        node2 = first.next;
        if(node2 == null) {
            first = null;
            last = null;
        }
        else {
            while(node2.next != null) {
                node1 = node2;
                node2 = node2.next;
            }           
            node1.next = null;
            last = node1;
        }
        count--;
    }
    
    double heightLayerPatternsList(int phase) {
        
        double height = 0;
        LayerPatterns temp;
        temp = first;
        while(temp != null) {
            height += temp.heightLayerPatterns(phase);
            temp = temp.next;
        }
        return height;
   }
    
    double widthLayerPatternsList(int phase) {
        
        double width, max = 0;
        int i;
        LayerPatterns temp;
        temp = first;
        while(temp != null) {
            width = temp.widthLayerPatterns(phase);
//            for(i=0; i<temp.layerProperties.length; i++)
//                width += temp.layerProperties[i].geoProp.width;
            if(width > max)
                max = width;
            temp = temp.next;
        }
        return max;
    }
    
    int countJPanelLayerPatterns(int phase) {
        
        int i, n = 0;
        LayerPatterns temp;
        temp = first;
        while(temp != null) {
            for(i=0; i<temp.layerProperties.length; i++) {
                if(temp.layerPropertiesPhase[i] <= phase) {
                    if(i == 0)
                        n++;
                    else
                        n += 2;
                }
            }
            //n += temp.layerProperties.length;
            temp = temp.next;
        }
        return n;
    }
    
    LayerPatterns getLayerPatterns(String n) {
        
        LayerPatterns node = first;
        while(node != null && !node.name.equals(n))
            node = node.next;
        return node;
        
    }
    
    void printLayerPatternsList() {
        System.out.println("LayerPatternsList:"+'\n');
        LayerPatterns temp;
        temp = first;
        int i;
        while(temp != null) {
            System.out.println("Pattern Name: "+temp.name);
            System.out.println("Num Layers:   "+temp.numLayers);
            //System.out.println("Num Phases:   "+temp.numPhases);
            for(i=0; i<temp.layerProperties.length; i++)
                System.out.println("Prop Name:    "+temp.layerProperties[i].name);
            //System.out.println("Num Prop:     "+temp.numProp);
            temp = temp.next;
        }
    }
 
}
