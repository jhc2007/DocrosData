
package docrosdata;

import javax.swing.JTextField;

public class LaminateGeoPropList {
    
    LaminateGeoProp first;
    LaminateGeoProp last;
    int count;
    
    public LaminateGeoPropList() {
        first = null;
        last = null;
        count = 0;
    }
    
    public LaminateGeoPropList(LaminateGeoProp node) {
        first = node;
        last = node;
        count = 1;
    }
    
    void addLaminateGeoProp(LaminateGeoProp node) {
        
        if(count == 0) {
            first = node;
            last = node;
        }
        else {
            if(count == 1) {
                first.next = node;
                last = node;
            }
            else {
                last.next = node;
                last = node;                    
            }
        }
        count++;
    }
    
    int maxNumberCharacterProperty() {
        
        int max = 0;
        LaminateGeoProp temp;
        temp = first;
        while(temp != null) {
            if(temp.name.length() > max)
                max = temp.name.length();
            if(String.valueOf(temp.thickness).length() > max)
                max = String.valueOf(temp.thickness).length();
            if(String.valueOf(temp.width).length() > max)
                max = String.valueOf(temp.width).length();
            temp = temp.next;
        }
        return max;
        
    }
    
     void delLaminateGeoProp() {
        
        LaminateGeoProp node1, node2;
        node1 = first;
        node2 = first.next;
        if(node2 == null) {
            first = null;
            last = null;
        }
        else {
            while(node2.next != null) {
                node1 = node2;
                node2 = node2.next;
            }           
            node1.next = null;
            last = node1;
        }
        count--;
    }
     
    void delLaminateGeoProp(String n) {
        
        LaminateGeoProp node1, node2;
        node1 = first;
        node2 = first.next;
        if(node1.name.equals(n)) {
            first = node2;
            if(first == null)
                last = null;
        }
        else {
            while(node2 != null && !node2.name.equals(n)) {
                node1 = node2;
                node2 = node2.next;
            }
            node1.next = node2.next;
            if(node2 == last)
                last = node1;
        }
        count--;
        
    }
    
    LaminateGeoProp getLaminateGeoProp(String n) {

        LaminateGeoProp temp;
        temp = first;
        while(temp != null && !(temp.name).equals(n))
            temp = temp.next;
        return temp;
    }
    
    LaminateGeoProp getLaminateGeoProp(JTextField jTextField[]) {
        
        LaminateGeoProp node = getLaminateGeoProp(jTextField[0].getText());
        if(node != null && node.thickness == Double.valueOf(jTextField[1].getText()) && node.width == Double.valueOf(jTextField[2].getText()))
            return node;
        return null;
    }
    
    Boolean existsLaminateGeoPropName(String n) {
        
        LaminateGeoProp temp;
        temp = first;
        while(temp != null) {
            if(temp.name.equals(n))
                return true;
            temp = temp.next;
        }
        return false;
    }
    
    void printLaminateGeoPropList() {
        System.out.println("LaminateGeoPropList:"+'\n');
        LaminateGeoProp temp = first;
        while(temp != null) {
            System.out.println("Name: "+temp.name);
            System.out.println("Thickness: "+temp.thickness);
            System.out.println("Width: "+temp.width);
            System.out.println("CountLayerPatterns: "+temp.countLayerPatterns+'\n');
            temp = temp.next;
        }
            
        
    }
    
 }        
