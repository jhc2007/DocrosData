
package docrosdata;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolTip;
import javax.swing.Popup;
import javax.swing.PopupFactory;

public class JPanelLayerPatternsMouseListener implements MouseListener {

    JPanelLayerPatterns jPanelLayerPatterns;
    JPanelLayerPatterns jPanelLayerPatternsTwin;
    JFrameDocrosData jFrameDocrosData;
    JPanel jPanelMainProperties;
    Popup popupLowProperties;
    JPopupMenu jPopupMenu;

    String mainProperties[];
    
    public JPanelLayerPatternsMouseListener(JFrameDocrosData jf, JPanelLayerPatterns jp, JPanelLayerPatterns jpt) {

        jPanelLayerPatterns = jp;
        jPanelLayerPatternsTwin = jpt;
        jFrameDocrosData = jf;
        
        jPanelMainProperties = new JPanel();
        jPanelMainProperties.setLayout(null);
        jPanelMainProperties.setLocation(0, 0);
        jPopupMenu = new JPopupMenu();

        String menuItemList[] = {"Material", "Geometry"};
        JMenuItem menuItem;
        int i, jLabelMaxWidth = 0;
        
        for(i=0; i<menuItemList.length; i++) {
            menuItem = new JMenuItem(menuItemList[i]);
            menuItem.addActionListener(new JPopupMenuItemActionListener(jPanelLayerPatterns, jPopupMenu, jFrameDocrosData, jPanelMainProperties));
            menuItem.setVisible(true);
            jPopupMenu.add(menuItem);
        }
        
        mainProperties = new String[] {
            "Pattern Name",
            jPanelLayerPatterns.layerPatterns.name,
            "Number of Layers",
            String.valueOf(jPanelLayerPatterns.layerPatterns.numLayers),
            "Properties Name",
            jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].name,
            "Material Type",
            jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].materialModel.type,
            "Material Name",
            jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].materialModel.name,
            "Geometry Name",
            jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].geoProp.name,
            "Layer Thickess",
            String.valueOf(jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].geoProp.thickness) + " " + jFrameDocrosData.unitsInputOutputData[1],
            "Layer Width",
            String.valueOf(jPanelLayerPatterns.layerPatterns.layerProperties[jPanelLayerPatterns.indexLayerProperties].geoProp.width) + " " + jFrameDocrosData.unitsInputOutputData[1]
        };
        
        JLabel jLabel[];
        jLabel = new JLabel[mainProperties.length];
        Font font;
        for(i=0; i<mainProperties.length; i++) {
            if(mainProperties[i].length() > jLabelMaxWidth)
                jLabelMaxWidth = mainProperties[i].length();
        }
        jLabelMaxWidth *= jFrameDocrosData.charWidth;
        for(i=0; i<mainProperties.length; i++) {  
            if(i == 3) {
                if(jPanelLayerPatterns.layerPatterns.numLayers > 1)
                    jLabel[i] = new JLabel(mainProperties[i] + " [Layer " + jFrameDocrosData.layerPatternsList.getFirstLayer(String.valueOf(jPanelLayerPatterns.layerPatterns.name)) + " to " + jFrameDocrosData.layerPatternsList.getLastLayer(String.valueOf(jPanelLayerPatterns.layerPatterns.name)) + "]");
                else
                    jLabel[i] = new JLabel(mainProperties[i] + " [Layer " + jFrameDocrosData.layerPatternsList.getFirstLayer(String.valueOf(jPanelLayerPatterns.layerPatterns.name)) + "]");
            }
            else
                jLabel[i] = new JLabel(mainProperties[i]);
            font = jLabel[i].getFont();
            if(i % 2 == 0)                
                jLabel[i].setFont(font.deriveFont(font.getStyle() | Font.BOLD));
            else
                jLabel[i].setFont(font.deriveFont(font.getStyle() & ~Font.BOLD));
            jLabel[i].setLocation(40, 50 + jFrameDocrosData.charHeight * i);
            jLabel[i].setSize(jLabelMaxWidth, jFrameDocrosData.charHeight);
            jPanelMainProperties.add(jLabel[i]);
        }
        jPanelMainProperties.setSize(jLabelMaxWidth + 100, jFrameDocrosData.jPanelMain.getHeight());//mainProperties.length * jFrameDocrosData.charHeight + 100);
        jPanelMainProperties.setBorder(javax.swing.BorderFactory.createLineBorder(Color.BLACK));
        jPanelMainProperties.setVisible(false);
        jFrameDocrosData.jPanelMain.add(jPanelMainProperties);
    }

    @Override
    public void mousePressed(MouseEvent me) {
        if(me.getButton() == 3)
            return;
        jPopupMenu.setVisible(false);
        jPanelMainProperties.setVisible(true);
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if(me.getButton() == 1)
            return;
        jPanelMainProperties.setVisible(false);
        popupLowProperties.hide();
        jPopupMenu.setLocation(me.getLocationOnScreen());        
        jPopupMenu.setVisible(true);
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {

        jPanelLayerPatterns.setBorder(javax.swing.BorderFactory.createLineBorder(Color.RED));//new java.awt.Color(0, 0, 0)));
        if(jPanelLayerPatternsTwin != null)
            jPanelLayerPatternsTwin.setBorder(javax.swing.BorderFactory.createLineBorder(Color.RED));//new java.awt.Color(0, 0, 0)));       
        JToolTip tip;
        jPopupMenu.setVisible(false);
        tip = jPanelLayerPatterns.createToolTip();
        tip.setLayout(null);
        String tipText = "";
        int lowProperties[] = {1, 3, 7};
        JLabel jLabel[];
        jLabel = new JLabel[lowProperties.length];
        int i;
        for(i=0; i<lowProperties.length; i++) {
            jLabel[i] = new JLabel();
            if(i == 1) {
                if(Integer.valueOf(mainProperties[lowProperties[i]]) > 1)
                    tipText += mainProperties[lowProperties[i]] + " Layers [" + jFrameDocrosData.layerPatternsList.getFirstLayer(String.valueOf(jPanelLayerPatterns.layerPatterns.name)) + "-" + jFrameDocrosData.layerPatternsList.getLastLayer(String.valueOf(jPanelLayerPatterns.layerPatterns.name)) + "], ";
                else
                    tipText += mainProperties[lowProperties[i]] + " Layer [" + jFrameDocrosData.layerPatternsList.getFirstLayer(String.valueOf(jPanelLayerPatterns.layerPatterns.name)) + "], ";
            }
            else {
                if(i == lowProperties.length - 1)
                    tipText += mainProperties[lowProperties[i]];
                else
                    tipText += mainProperties[lowProperties[i]] + ", ";
            }
        }
        tip.setTipText(tipText);
        PopupFactory factory = PopupFactory.getSharedInstance();
        popupLowProperties = factory.getPopup(jPanelLayerPatterns, tip, me.getLocationOnScreen().x, me.getLocationOnScreen().y);
        popupLowProperties.show();
    }

    @Override
    public void mouseExited(MouseEvent me) {

        if(!jPopupMenu.isVisible()) {
            jPanelLayerPatterns.setBorder(javax.swing.BorderFactory.createLineBorder(Color.BLACK));
            if(jPanelLayerPatternsTwin != null)
                jPanelLayerPatternsTwin.setBorder(javax.swing.BorderFactory.createLineBorder(Color.BLACK));
            jPanelMainProperties.setVisible(false);
        }
        popupLowProperties.hide();
    }
}
