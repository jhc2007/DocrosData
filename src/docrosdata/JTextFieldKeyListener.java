
package docrosdata;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;

class JTextFieldKeyListener implements KeyListener {
    
    public JTextFieldKeyListener() {
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        
        String name = ((JTextField) ke.getSource()).getName();
        String text = ((JTextField) ke.getSource()).getText();
        char key = ke.getKeyChar();
        if(name != null) {
            if(name.startsWith("Name")) {
                if(!name.equals("NameMainParameters")) {
                    if(key == ' ')
                        ke.consume();
                }
            }
            else {
                if(name.startsWith("Int")) { //int positive
                    if(key < '0' || key > '9')
                        ke.consume();
                    else {
                        if(key == '0' && text.length() == 0)
                            ke.consume();
                    }
                }
                else { //double positive
                    if((key < '0' || key > '9') && key != '+' && key != '-' && key != 'e' && key != 'E' && key != '.') 
                        ke.consume();
                    else {
                        if((key == '.' && text.contains(".")) ||
                           (key == '.' && (text.contains("e") || text.contains("E"))) ||    
                           ((key == 'e' || key == 'E') && (text.contains("e") || text.contains("E"))) ||
                           ((key == '+' || key == '-') && !(text.contains("e") || text.contains("E"))) ||    
                           ((key == '+' || key == '-') && (text.contains("+") || text.contains("-")))
                           ) //&& (!(text.startsWith("+") && !(text.startsWith("-"))))
                           ke.consume();
                    }
                }
            }
        }
        else { //double positive/negative
            if((key < '0' || key > '9') && key != '+' && key != '-' && key != 'e' && key != 'E' && key != '.') 
                ke.consume();
            else {
                 if((key == '.' && text.contains(".")) ||
                   (key == '.' && (text.contains("e") || text.contains("E"))) ||    
                   ((key == 'e' || key == 'E') && (text.contains("e") || text.contains("E"))) ||
                   ((key == '+' || key == '-') && (text.contains("e") || text.contains("E")) && containsPlusOrMinusPostE(text)) ||    
                   ((key == '+' || key == '-') && (text.contains("+") || text.contains("-")) && !(text.contains("e") || text.contains("E")))
                    ) 
                   ke.consume();
            }
        }
        /*

        */
    }

    @Override
    public void keyPressed(KeyEvent ke) {

        //System.out.println("Name: " + );
        //("Text: " + ();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    Boolean containsPlusOrMinusPostE(String text) {
        
        int index;
        if(text.contains("e")) {
            index = text.indexOf("e");
            if(text.substring(index).contains("+") || text.substring(index).contains("-"))
                return true;
        }
        else {
            index = text.indexOf("E");
            if(text.substring(index).contains("+") || text.substring(index).contains("-"))
                return true;
        }
        return false;
    }
    
}
